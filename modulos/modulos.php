<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

# session_set_cookie_params( 0, '/', '.0day.com.mx' );
session_start();
#if( !$_COOKIE['SMFCookie956'] ) # si no existe cookie del foro
#	setcookie("SMFCookie956", htmlentities("hola", ENT_QUOTES), -1, '/', '.0day.com.mx');

# include( "admin/geoip.php" );
include( "admin/config.php" );
include( "base.php" );
# include( "r_base.php" );
include( "admin/desktop_functions.php" );
include( "admin/handled_images.php" );

# navegador
$movil=0;
if( !strcmp( get_navegador("tipo"), "movil") ) # si es navegador movil
	$movil=1;
else if( !strcmp($_GET["ver"], "movil") )		$movil=1;

include( "admin/tema.php" );
include( "admin/paginacion.php" );
include( "admin/votaciones.php" );
include( "admin/short_urls.php" );
include( "admin/recaptcha.php" );
include( "admin/sockets.php" );
include( "admin/twitter.php" );
include( "admin/Facebook/autoload.php" );
include( "admin/facebook.php" );
include( "admin/socialnetworks.php" );
include( "admin/clima.php" );
include( "admin/googlemaps.php" );
include( "admin/googletraductor.php" );
include( "admin/googledivisas.php" );
# include( "admin/chat_support.php" );
include( "admin/sdk_moneybox.php" );
include( "admin/carrito.php" );
include( "admin/redpack.php" );
include( "admin/phpmailer/PHPMailerAutoload.php" );
include( "admin/paypal.php" );
?>
