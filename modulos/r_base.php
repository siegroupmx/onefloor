<?php
# conectar a la BDD remota 
function r_conectar()
	{
	if( !($link= mysql_connect( "". R_SERVER. "", "". R_USR. "", "". R_PASS. "" )) )
		{
		echo "<span id=\"letras_error\">Error 01: Error para Conectarse a MySQL.<br>";
		echo "Error en Usuario y/o Contrase&ntilde;a.<br>";
		echo mysql_error($link). "</span>";
		$link= "INSTALL";
		}
	else if( !mysql_select_db( "". R_BASE. "", $link ) )
		{
		echo "<span id=\"letras_error\">Error 02: Problemas para Tomar la Base de Datos.<br>";
		echo "No se Encuentra la Base de Datos.<br>";
		echo mysql_error($link). "</span>";
		$link= "INSTALL";
		}
	return $link;
	}

# insertar datos en bdd remota
function r_insertar_bdd( $base_t, $valores )
	{
	$link= r_conectar();
	
	if( count($valores)>0 ) //entonces existen valores en el array
		{
		$vars="";
		$datos="";
		$i=0;

		foreach( $valores as $a=>$b ) 
			{
			$i++;

			$vars .= $a;
			$datos .= $b;
		
			if( $i!=count($valores) )
				{
				$datos .= ", ";
				$vars .= ", ";
				}
			}

		if( !($resp= mysql_query( "insert into ". $base_t. " ( ". $vars. " ) values( ". $datos. " );", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			unset( $valores, $a, $b, $i, $var, $datos );
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

//funcion para consultar datos especificos en la BDD remota
function r_consultar_datos_general( $bdt, $bdt_where, $var )
	{
	$cons= r_consultar_con( $bdt, $bdt_where );
	$data= mysql_num_rows($cons);
	if( $data )
		{
		$tmp= mysql_fetch_array($cons);
		unset($data);
		limpiar($cons);		
		return $tmp[strtoupper($var)];
		}
	unset($data);
	limpiar($cons);
	return 0;
	}

# consultar comunmente 
function r_consultar( $base_t, $valores )
	{
	$link= r_conectar();
	
	if( strchr( $valores, ":" ) ) //si existe el  :  tons ahi mas de 1 valor
		{
		$valores= str_replace( ":", ",", $valores ); //cambiamos el :  por  ,
		
		if( !($resp= mysql_query( "select ". $valores. " from ". $base_t. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else if( !strcmp( $valores, "*" ) )
		{
		if( !($resp= mysql_query( "select * from ". $base_t. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else if( $valores ) //entonces solo se desea consulta 1 valor
		{
		if( !($resp= mysql_query( "select ". $valores. " from ". $base_t. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

# consulta datos donde los valores coincidan 
function r_consultar_con( $base_t, $valores )
	{
	$link= r_conectar();
	
	if( strchr( $valores, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		{
		$valores= str_replace( ":", " && ", $valores );
		
		if( !($resp= mysql_query( "select * from ". $base_t. " where ". $valores. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else if( $valores ) //solo se desea comparar con 1 valor
		{
		if( !($resp= mysql_query( "select * from ". $base_t. " where ". $valores. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

function r_consultar_indexados( $base_t, $valor, $muestra )
	{
	$link= r_conectar();
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		{
		echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
		echo mysql_error(). "</span>";
		}
	else if( $valor ) //solo se desea comparar con 1 valor
		{
		if( !($resp= mysql_query( "select * from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%';", $link )) )		
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

function r_consultar_indexados_enorden( $base_t, $valor, $muestra, $orden )
	{
	$link= r_conectar();
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		{
		echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
		echo mysql_error(). "</span>";
		}
	else if( $valor ) //solo se desea comparar con 1 valor
		{
		if( !($resp= mysql_query( "select * from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%' ORDER BY ". $orden. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

function r_consultar_indexados_enorden_limite( $base_t, $valor, $muestra, $limite, $orden )
	{
	$link= r_conectar();
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		{
		echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
		echo mysql_error(). "</span>";
		}
	else if( $valor ) //solo se desea comparar con 1 valor
		{
		if( !($resp= mysql_query( "select * from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%' ORDER BY ". $orden. " LIMIT ". $limite. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

function r_consultar_indexados_enorden_con( $base_t, $valor, $likevar, $muestra, $orden )
	{
	$link= r_conectar();
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		{
		echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
		echo mysql_error(). "</span>";
		}
	else if( $valor ) //solo se desea comparar con 1 valor
		{
		if( !($resp= mysql_query( "select * from ". $base_t. " where ". $valor. " && ". $likevar. " LIKE '%". $muestra. "%' ORDER BY ". $orden. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

function r_actualizar_bdd( $base_t, $valores )
	{
	$link= r_conectar();
	
	if( count($valores)>0 ) //existen valores en el array
		{
		$condicion= "";
		$datos="";
		$i=0;

		while( list($a, $b)=each($valores) )
			{
			$i++;
			if( $i==1 )
				$condicion .= $a. "=". $b;
			else
				{
				$datos .= $a. "=". $b;
			
				if( $i!=count($valores) )
					$datos .= ", ";
				}
			}

		if( !($resp= mysql_query( "update ". $base_t. " set ". $datos. " where ". $condicion. ";", $link )) )
			{
			echo "<span id=\"letras_error\">Error 03: Problema para Realizar Movimiento/Consulta.";
			echo mysql_error(). "</span>";
			}
		else
			{
			unset( $datos );
			unset($i);
			unset($a);
			unset($b);
			@mysql_close($link);
			return $resp;
			}
		}
	else
		echo "<span id=\"letras_error\">Error 04: Problema para Desifrar Datos para Realizar Consulta.</span>";
	
	@mysql_close($link);
	return "ERROR";
	}

?>