<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

function controles_menus( $id, $dato, $aux )
	{
	echo "<a href=\"index.php?id=". $id. "&mov=1&control=". $dato. ",". $aux. "\">";
	if( $aux==1 ) //visibilidad del MENU
		echo "<img src=\"../modulos/menus-img/conectar.png\" alt=\"Menu es Visible\" title=\"El Menu es Visible\" hspace=\"1px\" border=\"0\">";
	else
		echo "<img src=\"../modulos/menus-img/desconectar.png\" alt=\"Menu es Invisible\" title=\"El Menu es Invisible\" hspace=\"1px\" border=\"0\">";
	echo "</a>";

	if( is_admin() )
		{
		echo "<a href=\"index.php?id=". $id. "&mov=3&control=". $dato. "\">";
		echo "<img src=\"../modulos/menus-img/configurar_2.png\" alt=\"Configurar Menu\" title=\"Configurar Menu\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		echo "<a href=\"index.php?id=". $id. "&mov=4&control=". $dato. "\">";
		echo "<img src=\"../modulos/menus-img/eliminar.png\" alt=\"Eliminar Menu\" title=\"Eliminar Menu\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		if( $dato<contador_celdas( "MENUS" ) )
			{
			echo "<a href=\"index.php?id=". $id. "&mov=5&control=". $dato. "\">";
			echo "<img src=\"../modulos/menus-img/flecha_abajo.png\" alt=\"Bajar Menu\" title=\"Bajar Menu\" hspace=\"1px\" border=\"0\">";
			echo "</a>";
			}

		if( $dato>1 )
			{
			echo "<a href=\"index.php?id=". $id. "&mov=6&control=". $dato. "\">";
			echo "<img src=\"../modulos/menus-img/flecha_arriba.png\" alt=\"Subir Menu\" title=\"Subir Menu\" hspace=\"1px\" border=\"0\">";
			echo "</a>";
			}
		}

	unset($x);
	}

function contador_raizes( $arbol, $rama, $hoja)
	{
	$cons= consultar_con( $rama, "RELACION='". $hoja. "'" );

	if( mysql_num_rows($cons)>0 )
		{
		unset($cons);
		return 1;
		}
	unset($cons);
	return 0;
	}

function controles_secciones( $id, $dato, $aux )
	{
	$x= explode( ",", $aux ); //partimos variable '$aux'

	if( is_admin() )
		{
		echo "<a href=\"index.php?id=". $id. "&mov=1&control=". $dato. ",". $x[0]. "\">";
		if( $x[0]==1 )
			echo "<img src=\"../modulos/menus-img/conectar.png\" alt=\"Seccion es Visible\" title=\"La Seccion es Visible\" hspace=\"1px\" border=\"0\">";
		else
			echo "<img src=\"../modulos/menus-img/desconectar.png\" alt=\"Seccion es Invisible\" title=\"La Seccion es Invisible\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		echo "<a href=\"index.php?id=". $id. "&mov=2&control=". $dato. ",". $x[1]. "\">";
		if( $x[1]==1 )
			echo "<img src=\"../modulos/menus-img/carita_on.png\" alt=\"Seccion Abierta a Comentarios\" title=\"Seccion Abierta a Comentarios\" hspace=\"1px\" border=\"0\">";
		else
			echo "<img src=\"../modulos/menus-img/carita_off.png\" alt=\"Seccion Cerrada a Comentarios\" title=\"Seccion Cerrada a Comentarios\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		echo "<a href=\"index.php?id=". $id. "&mov=3&control=". $dato. "\">";
		echo "<img src=\"../modulos/menus-img/configurar_2.png\" alt=\"Configurar Seccion\" title=\"Configurar Seccion\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		echo "<a href=\"index.php?id=". $id. "&mov=4&control=". $dato. "\">";
		echo "<img src=\"../modulos/menus-img/eliminar.png\" alt=\"Eliminar Seccion\" title=\"Eliminar Seccion\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		echo "<a href=\"index.php?id=". $id. "&mov=5&control=". $dato. "\">";
		echo "<img src=\"../modulos/menus-img/flecha_abajo.png\" alt=\"Bajar Seccion\" title=\"Bajar Seccion\" hspace=\"1px\" border=\"0\">";
		echo "</a>";

		echo "<a href=\"index.php?id=". $id. "&mov=6&control=". $dato. "\">";
		echo "<img src=\"../modulos/menus-img/flecha_arriba.png\" alt=\"Subir Seccion\" title=\"Subir Seccion\" hspace=\"1px\" border=\"0\">";
		echo "</a>";
		}

	echo "<a href=\"index.php?id=noticias&mov=7&control=". $dato. "\">";
	echo "<img src=\"../modulos/menus-img/hoja_mas.png\" alt=\"Publicar\" title=\"Publicar\" hspace=\"1px\" border=\"0\">";
	echo "</a>";

	echo "<a href=\"index.php?id=noticias&mov=8&control=". $dato. "\">";
	echo "<img src=\"../modulos/menus-img/hoja_editar.png\" alt=\"Eliminar/Modificar Publicacion\" title=\"Eliminar/Modificar Publicacion\" hspace=\"1px\" border=\"0\">";
	echo "</a>";

	unset($x);
	}

function menus()
	{
	/*Informacion Importante:
	Los MENUS, solo pueden tener acceso a los valores: 1,3,4,5,6
	Las SECCIONES, tienes acceso a los valores: 1,2,3,4,5,6,7,8
	Examinar variable: $_GET["mov"] indica los valores
	Examinar variable: $_GET["control"] indica ID_MENU,ID_SECCION,VALOR
	*/
	if( $_GET["mov"] )
		{
		if( !strcmp($_GET["mov"], "1") && (is_admin() || is_admingrp()) ) //visibilidad (conector)
			{
			if( strchr( $_GET["control"], "," ) ) //si esta ',' entonces es valido el movimiento
				{
				$x= explode( ",", $_GET["control"] );

				if( sizeof($x)==3 ) //entonces estamos tratando una seccion
					{
					if( $x[2]==1 ) $v=0;
					else $v=1;

					$trama= array(
					"ID"=>"'". proteger_cadena($x[1]). "'",
					"VISIBILIDAD"=>"'". $v. "'" );

					actualizar_bdd( "SECCIONES", $trama );

					unset($trama);
					unset($v);
					unset($x);
					}
				else //es un MENU
					{
					if( $x[1]==1 ) $v=0;
					else $v=1;

					$trama= array(
					"ID"=>"'". proteger_cadena($x[0]). "'",
					"VISIBILIDAD"=>"'". $v. "'" );

					actualizar_bdd( "MENUS", $trama );

					unset($trama);
					unset($v);
					unset($x);
					}
				}
			}
		else if( !strcmp($_GET["mov"], "2") && is_admin() ) //comentarios (carita)
			{
			if( strchr( $_GET["control"], "," ) ) //si esta ',' entonces es valido el movimiento
				{
				$x= explode( ",", $_GET["control"] );
				if( $x[2]==1 ) $c=0;
				else $c=1;

				$trama=array(
				"ID"=>"'". proteger_cadena($x[1]). "'",
				"COMENTARIOS"=>"'". $c. "'" );

				actualizar_bdd( "SECCIONES", $trama );

				unset($trama);
				unset($c);
				unset($x);
				}
			}
		else if( !strcmp($_GET["mov"], "3") && is_admin() ) // configurar (llave)
			{
			if( strchr( $_GET["control"], "," ) ) //si esta ',' entonces es valido el movimiento y se refiere a una SECCION
				{
				$x= explode( ",", $_GET["control"] );
				$xcons= consultar_con( "SECCIONES", "ID='". proteger_cadena($x[1]). "'" );
				$xtmp= mysql_fetch_array( $xcons );
				$flags=1;
				unset($xcons);
				unset($x);
				}
			else //se refiere a un MENU
				{
				$xflags=1;
				$xcons= consultar_con( "MENUS", "ID='". proteger_cadena($_GET["control"]). "'" );
				$xtmp= mysql_fetch_array($xcons);
				unset( $xcons);
				}
			}
		else if( !strcmp($_GET["mov"], "4") && is_admin() ) # eliminar (tacha)
			{
			if( strchr( $_GET["control"], "," ) ) //entonces estamos eliminando una seccion y se refiere a una SECCION
				{
				$x= explode( ",", $_GET["control"] );
				$xcons= consultar_con( "SECCIONES", "ID='". proteger_cadena($x[1]). "'" );
				$xtmp= mysql_fetch_array( $xcons );
				$flags=2;
				unset($xcons);
				unset($x);
				}
			else //eliminatemos un MENU y secciones
				{
				$xflags=2;
				$xcons= consultar_con( "MENUS", "ID='". proteger_cadena($_GET["control"]). "'" );
				$xtmp= mysql_fetch_array($xcons);
				unset( $xcons);
				}
			}
		else if( !strcmp($_GET["mov"], "5") && (is_admin() || is_admingrp()) ) # flecha bajar
			{
			if( strstr($_GET["control"], ",") )
				{
				$x= explode(",", $_GET["control"]); # explotamos
				$cons= consultar_con("SECCIONES", "RELACION='". proteger_cadena($x[0]). "'", "ORDEN ASC");
				if( mysql_num_rows($cons) ) # si existen valores
					{
					# si es menor al numero total de secciones, se podra bajar 
					if( consultar_datos_general("SECCIONES", "RELACION='". proteger_cadena($x[0]). "' && ID='". proteger_cadena($x[1]). "'", "ORDEN") < mysql_num_rows($cons) )
						{
						$r= consultar_datos_general("SECCIONES", "RELACION='". proteger_cadena($x[0]). "' && ID='". proteger_cadena($x[1]). "'", "ORDEN"); # obtenemos orden actual
						while($buf= mysql_fetch_array($cons))
							{
							if( !strcmp($buf["ID"], $x[1]) ) # si  es la seccion
								{
								$trama= array("id"=>"'". $buf["ID"]. "'", "orden"=>"'". ($r+1). "'");
								actualizar_bdd( "SECCIONES", $trama);
								}
							else if( $buf["ORDEN"]==($r+1) ) # si es al que sustituiremos
								{
								$trama= array("id"=>"'". $buf["ID"]. "'", "orden"=>"'". $r. "'");
								actualizar_bdd( "SECCIONES", $trama);
								}
							}
						}
					}
				limpiar($cons);
				}
			}
		else if( !strcmp($_GET["mov"], "6") && (is_admin() || is_admingrp()) ) # flecha arriba
			{
			if( strstr($_GET["control"], ",") )
				{
				$x= explode(",", $_GET["control"]); # explotamos
				$cons= consultar_con("SECCIONES", "RELACION='". proteger_cadena($x[0]). "'", "ORDEN ASC");
				if( mysql_num_rows($cons) ) # si existen valores
					{
					# si es mayor a uno (la primer seccion) se podra subir 
					if( consultar_datos_general("SECCIONES", "RELACION='". proteger_cadena($x[0]). "' && ID='". proteger_cadena($x[1]). "'", "ORDEN") > 1)
						{
						$r= consultar_datos_general("SECCIONES", "RELACION='". proteger_cadena($x[0]). "' && ID='". proteger_cadena($x[1]). "'", "ORDEN"); # obtenemos orden actual
						while($buf= mysql_fetch_array($cons))
							{
							if( $buf["ORDEN"]==($r-1) ) # si es al que sustituiremos
								{
								$trama= array("id"=>"'". $buf["ID"]. "'", "orden"=>"'". $r. "'");
								actualizar_bdd( "SECCIONES", $trama);
								}
							else if( !strcmp($buf["ID"], $x[1]) ) # si  es la seccion
								{
								$trama= array("id"=>"'". $buf["ID"]. "'", "orden"=>"'". ($r-1). "'");
								actualizar_bdd( "SECCIONES", $trama);
								}
							}
						}
					}
				limpiar($cons);
				}
			}
		else if( !strcmp($_GET["mov"], "7") && is_admin() ) # agregar (hoaj +)
			{
			if( strchr( $_GET["control"], "," ) ) //si esta ',' entonces es valido el movimiento
				{
				}
			}
		else if( !strcmp($_GET["mov"], "8") && is_admin() ) # eliminar (hoja -)
			{
			if( strchr( $_GET["control"], "," ) ) //si esta ',' entonces es valido el movimiento
				{
				}
			}
		}

	echo '<div id="col_01">';
			echo "Desde este panel podras <b>mover</b>, <b>cambiar orden</b> o <b>modificar</b> los menus creados...";
			echo "<p>Existen ". contador_celdas( "MENUS" );
			if( contador_celdas( "MENUS" )==1 )
				echo " menu";
			else
				echo " menus";

			echo " y ". contador_celdas( "SECCIONES" );

			if( contador_celdas( "SECCIONES" )==1  )
				echo " seccion";
			else
				echo " secciones";

				#echo "<table cellspacing=\"0\"0 cellpadding=\"0\" align=\"center\" id=\"tabla_lista_03\">";
				echo '<div id="menusadmin_list">
				<h1>MENUS & SECCIONES</h1>';

				$cons_menus= consultar_enorden( "MENUS", "ORDEN ASC" ); //consultamos MENUS ordenados por ID_MENU
				$aux=""; //variable para uso posterior

				while( $aux= mysql_fetch_array($cons_menus) ) //recorremos los MENUS conforme su orden
					{
					$cons_sec= consultar_enorden_con( "SECCIONES", "RELACION='". $aux["ID"]. "'", "ORDEN ASC" ); //filtramos solo SECCIONES del MENU que deseamos

					echo '<div class="in">'; //iniciamos insercion de nombre de MENU
					echo '<a href="javascript:;" onclick="capa_verno(\'capa_sec_'. $aux["ID"]. '\');">';
					echo "<img src=\"../modulos/menus-img/lista_mas.png\" width=\"10px\" border=\"0\" alt=\"Aumenta el Arbol del Menu\" title=\"Aumenta el Arbol del Menu\">";
					echo "</a>";
					echo $aux["NOMBRE"]; //imprimimos nombre del MENU (por default o_O :P jojo)

					controles_menus( $_GET["id"], $aux["ID"], $aux["VISIBILIDAD"] ); //funcion para controles MENU
					echo '<div id="capa_sec_'. $aux["ID"]. '" style="visibility:hidden;display:none;">
					<ul>';
					while( $buf_sec=mysql_fetch_array($cons_sec) ) //recorremos las SECCIONES segun el orden
						{
						echo '<li><img src="../modulos/menus-img/etiqueta.png" width="15px"> '. $buf_sec["NOMBRE"];
						controles_secciones( $_GET["id"], $aux["ID"].",".$buf_sec["ID"], $buf_sec["VISIBILIDAD"].",".$buf_sec["COMENTARIOS"] );//funcion para controles de SECCIONES
						echo '</li>';
						}
					echo '</div></div>';
					}
				unset($aux);
				unset($cons_sec);
				unset($cons_menus);
				unset($buf_sec);

				echo '</div>';
	echo "</div>";
	
	echo '<div id="col_02">';
			switch( $_GET["mov"] )
				{
				case 'agregar': //agregar MENU o SECCION
					if( isset($_POST["nombre_seccion"]) ) //agregando SECCION
						{
						if( strcmp( $_POST["nombre_seccion"], "" ) && strcmp( $_POST["tipo_seccion"], "") && strcmp( $_POST["orden_seccion"], "error") )
							{
							do //generamos numero aleatorio de 4 a 10 digitos
								{
								$idtrack= generar_idtrack(); //obtenemos digito aleatorio
								}while( !strcmp( $idtrack, consultar_datos_general( "SECCIONES", "ID='". $idtrack. "'", "ID" ) ) );
							
							$orden_c= consultar_con("SECCIONES", "RELACION='". proteger_cadena($_POST["orden_seccion"]). "'");
							$orden= mysql_num_rows($orden_c)+1;
							//limpiar($orden_c);
							
							$trama= array(
							"id"=>"'". $idtrack. "'", 
							"nombre"=>"'". proteger_cadena($_POST["nombre_seccion"]). "'",
							"url_nombre"=>"'". url_cleaner($_POST["nombre_seccion"]). "'",
							"tipo"=> "'". proteger_cadena($_POST["tipo_seccion"]). "'",
							"relacion"=>"'". proteger_cadena($_POST["orden_seccion"]). "'",
							"visibilidad"=>"'1'",
							"fecha"=>"'". time(). "'", 
							"comentarios"=> "'". proteger_cadena($_POST["comentarios_seccion"]). "'", 
							"orden"=>"'". $orden. "'" );

							if( insertar_bdd( "SECCIONES", $trama )==0 )
								echo "<td align=\"center\">Error en la Insercion de los Datos...</td><tr>";
							else
								echo "<td align=\"center\">Datos Agregados con Exito...</td><tr>";

							echo '<td align="center">Datos proporcionados:
							<p><b>Nombre de Seccion:</b> <i>'. proteger_cadena($_POST["nombre_seccion"]). '<br>
							<b>Tipo de Seccion:</b> '. $_POST["tipo_seccion"]. '<br>
							<b>ID:</b> '. $idtrack. '<br>
							<b>Comentarios:</b> '. $_POST["comentarios_seccion"];

							unset($trama);
							unset($idtrack);
							}
						else
							echo "<td align=\"center\">Has dejado campos sin rellenar...";
						}
					else if( isset($_POST["nombre_menu"]) ) //agregando MENU
						{
						if( strcmp($_POST["nombre_menu"], "" ) )
							{
							do //generamos numero aleatorio de 4 a 10 digitos
								{
								$idtrack= generar_idtrack(); //obtenemos digito aleatorio
								}while( !strcmp( $idtrack, consultar_datos_general( "MENUS", "ID='". $idtrack. "'", "ID" ) ) );

							$orden_c= consultar( "MENUS", "*" );
							$orden= (mysql_num_rows($orden_c))+1;
							limpiar($orden_c); 
								
							$trama= array(
							"id"=>"'". $idtrack. "'", 
							"nombre"=>"'". proteger_cadena($_POST["nombre_menu"]). "'",
							"url_nombre"=>"'". url_cleaner($_POST["nombre_menu"]). "'", 
							"fecha"=>"'". time(). "'", 
							"visibilidad"=>"'1'",
							"orden"=>"'". $orden. "'" );

							if( insertar_bdd( "MENUS", $trama )==0 )
								echo "<td align=\"center\">Error en la Insercion de los Datos...</td><tr>";
							else
								echo "<td align=\"center\">Datos Agregados con Exito...</td><tr>";

							unset($trama);
							unset($idtrack);

							echo '<td align="Center">Datos proporcionados:
								<p><b>Nombre de Menu:</b> '. proteger_cadena($_POST["nombre_menu"]);
							}
						else
							echo "<td align=\"center\">Has dejado campos sin rellenar...";
						}
					echo "<p><b><a href=\"index.php?id=". $_GET["id"]. "\">[Continuar]</a></b></td>";
					break;
				case 'modificar': //modificar MENU o SECCION
					if( isset($_POST["nombre_seccion"]) ) //modificar SECCION
						{
						if( strcmp( $_POST["nombre_seccion"], "" ) && strcmp( $_POST["tipo_seccion"], "") && strcmp( $_POST["orden_seccion"], "") )
							{
							$trama= array(
							"id"=>"'". proteger_cadena($_GET["id_src"]). "'", //valor especial enviado del <form>
							"nombre"=>"'". proteger_cadena($_POST["nombre_seccion"]). "'",
							"url_nombre"=>"'". url_cleaner($_POST["nombre_seccion"]). "'",
							"tipo"=>"'". proteger_cadena($_POST["tipo_seccion"]). "'",
							"relacion"=>"'". proteger_cadena($_POST["orden_seccion"]). "'",
							"comentarios"=>"'". $_POST["comentarios_seccion"]. "'" );
							
							//si existen noticias en la seccion actual que deberan modificarse
							if( mysql_num_rows($cons_mod_not= consultar_con( "NOTICIAS", "MENU='". consultar_datos_general( "SECCIONES", "ID='". proteger_cadena($_GET["id_src"]). "'", "relacion"). "':SECCION='". proteger_cadena($_GET["id_src"]). "'" ))>0 )
								{
								while( $buf_mod_not=mysql_fetch_array($cons_mod_not) )
									{
									$trama_not_mod= array( "id"=>"'". $buf_mod_not["ID"]. "'", 
																"MENU"=>"'". proteger_cadena($_POST["orden_seccion"]). "'", 
																	);
									actualizar_bdd( "NOTICIAS", $trama_not_mod );
									unset($trama_not_mod);
									}
								}
							unset($cons_mod_not);

							if( actualizar_bdd( "SECCIONES", $trama )==0 )
								echo "<td align=\"center\">Error en la Insercion de los Datos...</td><tr>";
							else
								echo "<td align=\"center\">Datos Agregados con Exito...</td><tr>";

							unset($trama);

							echo "<td align=\"center\">Datos proporcionados:";
							echo "<p><b>Nombre de Seccion:</b> <i>". proteger_cadena($_POST["nombre_seccion"]). "<br>";
							echo "<b>Tipo de Seccion:</b> ". proteger_cadena($_POST["tipo_seccion"]). "<br>";
							echo "<b>Orden:</b> ". proteger_cadena($_POST["orden_seccion"]). "<br>";
							echo "<b>Comentarios:</b> ". proteger_cadena($_POST["comentarios_seccion"]);
							}
						else
							echo "<td align=\"center\">Has dejado campos sin rellenar...";
						}
					else if( isset($_POST["nombre_menu"]) ) //modificar MENU
						{
						if( strcmp($_POST["nombre_menu"], "" ) )
							{
							$trama= array(
							"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
							"nombre"=>"'". proteger_cadena($_POST["nombre_menu"]). "'",
							"url_nombre"=>"'". url_cleaner($_POST["nombre_menu"]). "'",
							"visibilidad"=>"'1'" );

							if( actualizar_bdd( "MENUS", $trama )==0  )
								echo "<td align=\"center\">Error en la Insercion de los Datos...</td><tr>";
							else
								echo "<td align=\"center\">Datos Agregados con Exito...</td><tr>";

							unset($trama);

							echo '<td align="Center">Datos proporcionados:
								<p><b>Nombre de Menu:</b> '. $_POST["nombre_menu"];
							}
						else
							echo "<td align=\"center\">Has dejado campos sin rellenar...";
						}
					echo "<p><b><a href=\"index.php?id=". $_GET["id"]. "\">[Continuar]</a></b></td>";
					break;
				case 'eliminar':
					if( isset($_POST["nombre_seccion"]) ) //eliminando SECCION
						{
						if( isset($_GET["id_src"]) )
							{
							if( eliminar_bdd( "SECCIONES", "ID='". proteger_cadena($_GET["id_src"]). "'" )==0 )
								echo "<td align=\"center\">Error en la Insercion de los Datos...</td><tr>";
							else
								echo "<td align=\"center\">Datos Eliminados con Exito...</td><tr>";
							}
						else
							echo "<td>No intentes Colapsar el sistema :P</td>";
						}
					else if( isset($_POST["nombre_menu"]) ) //eliminando MENU
						{
						if( isset($_GET["id_src"]) )
							{
							$cons= consultar_con( "MENUS", "ID='". proteger_cadena($_GET["id_src"]). "'" ); //sacamos nombre del menu
							$buf= mysql_fetch_array( $cons );
							
							if( mysql_num_rows($cons)>0 )
								{
								if( contador_raizes( "MENUS", "SECCIONES", proteger_cadena($_GET["id_src"]) ) ) //verificamos existencia de secciones con el ID del menu
									eliminar_bdd( "SECCIONES", "RELACION='". $buf["ID"]. "'" ); //eliminamos secciones relacionadas al menu
								if( eliminar_bdd( "MENUS", "ID='". proteger_cadena($_GET["id_src"]). "'" )==0 )
									echo "<td align=\"center\">Error en la Eliminacion de los Datos...</td><tr>";
								else
									echo "<td align=\"center\">Datos Eliminados con Exito...</td><tr>";
								}
							else
								echo "<td align=\"center\">Menu inexistente...</td><tr>";
							unset($buf);
							unset($cons);
							}
						}
					echo "<td align=\"center\"><p><b><a href=\"index.php?id=". proteger_cadena($_GET["id"]). "\">[Continuar]</a></b></td>";
					break;
				default:
			if( is_admin() )
				{
			echo "Este panel te permite crear <b>menus</b> y <b>secciones</b>, brindandote una mejor definicion. Solo rellena los espacios segun tu necesidad";

				# Formulario MENUS
				echo '<div id="tabla_lista_02">';
				echo "<form action=";
				if( $xflags==1 )
					echo "\"index.php?id=menus&mov=modificar&id_src=". $xtmp["ID"]. "\"";
				else if( $xflags==2 )
					echo "\"index.php?id=menus&mov=eliminar&id_src=". $xtmp["ID"]. "\"";
				else
					echo "\"index.php?id=menus&mov=agregar\"";
				echo ' method="POST">
				<ul>
					<li class="titulo">[MENU] '. (($xflags==1 || $xflags==2)? "{". $xtmp["NOMBRE"]. "}":"" ). '</li>';
				echo '<li class="txt">Nombre:</td>';
				echo '<li><input type="text" name="nombre_menu" ';
				if( $xflags==1 || $xflags==2)
					echo " value=\"". $xtmp["NOMBRE"]. "\"";
				echo "></li>";
				echo '<li class="sub">';
				echo "<input type=\"submit\" value=\"";
				if( $xflags==1 )
					echo "Modificar Menu";
				else if( $xflags==2 )
					echo "Si, Eliminar Menu";
				else
					echo "Crear Menu";
				echo "\" class=\"boton_01\">";
				echo "</li>";
				echo "</form>";
				echo "</ul>";

				if( $xflags==1 || $xflags==2 )
					{
					unset( $xflags);
					unset( $xtmp);
					}

				echo '<div id="tabla_lista_02">';
				echo "<form action=";
				if( $flags==1 )
					echo "\"index.php?id=menus&mov=modificar&id_src=". $xtmp["ID"]. "\"";
				else if( $flags==2 )
					echo "\"index.php?id=menus&mov=eliminar&id_src=". $xtmp["ID"]. "\"";
				else
					echo "\"index.php?id=menus&mov=agregar\"";
				echo ' method="POST">
					<ul>
					<li class="titulo">[SECCION]'. (($flags==1 || $flags==2) ? " {". $xtmp["NOMBRE"]. "}":"");
				echo '<li class="txt">Nombre:</li>';
				echo "<li><input type=\"text\" name=\"nombre_seccion\" ";
				if( $flags==1 || $flags==2 )
					echo "value=\"". $xtmp["NOMBRE"]. "\"";
				echo "></li>";
				echo '<li class="txt">Tipo:</li>';
				echo "<li>";
				echo "<select name=\"tipo_seccion\">";
				if( $flags==1 || $flags==2 )
					echo "<option value=\"". $xtmp["TIPO"]. "\">". $xtmp["TIPO"]. "</option>";
				echo "<option value=\"noticia\">Noticia/Post</option>";
				echo "<option value=\"noticia_limpia\">Noticia Limpia/Solo Hoja</option>";
				echo "<option value=\"galeria\">Galeria/Fotos/Album</option>";
				echo "<option value=\"descriptiva\">Descriptiva/Descargas</option>";
				echo "<option value=\"scriptin\">Script-In</option>";
				echo "</select>";
				echo "</li>";
				echo '<li class="txt">Orden:</li>';
				echo "<li><select name=\"orden_seccion\">";
				//echo "<option value=\"sin_orden\">Sin orden</option>";

				if( $flags==1 || $flags==2)
					echo "<option value=\"". $xtmp["RELACION"]. "\">". consultar_datos_general( "MENUS", "ID='". $xtmp["RELACION"]. "'", "nombre" ). "</option>";

				if( contador_celdas( "MENUS" )>0 )
					{
					$cons= consultar_enorden( "MENUS", "NOMBRE" );
					echo "<option value=\"error\"></option>";
					while( $buf= mysql_fetch_array($cons) )
						echo "<option value=\"". $buf["ID"]. "\">". $buf["NOMBRE"]. "</option>";

					unset($cons);
					unset($buf);
					}

				echo "</select>";
				echo "</li>";
				echo '<li class="sub">';
				echo "<input type=\"checkbox\" value=\"1\" name=\"comentarios_seccion\"";
				if( $flags==1 || $flags==2 )
					if( $xtmp["COMENTARIOS"]==1 )
						echo " checked";
				echo ">";
				echo "Permitir Comentarios.</li>";
				echo '<li class="sub">';
				echo "<input type=\"submit\" value=";
				if( $flags==1 )
					echo "\"Modificar Seccion\"";
				else if( $flags==2 )
					echo "\"Si, Eliminar Seccion\"";
				else
					echo "\"Crear Seccion\"";
				echo " class=\"boton_01\">";
				echo "</li>";
				echo "</form>";

				if( $flags==1 || $flags==2 )
					{
					$flags=0;
					unset( $xtmp );
					}

				echo "</ul>";
				}
					break;
				}
		echo "</div>";
	}

//llamadas para el panel de control
if( $_GET["id"]=="menus" )
	{
	if( is_admin() || is_coadmin() || is_autor() )
		menus();
	else		echo 'Area Restringida...';
	}
?>