<?php
function usuarios()
	{
	echo '<td>';
	echo '<div id="tabla_usuarios">';
		echo '<div id="tabla_usuarios_minipanel">';
		echo '<a href="index.php?id='. $_GET["id"]. '&mov=new">Nuevo</a> | ';
		echo '<a href="index.php?id='. $_GET["id"]. '&mov=listar">Listar</a>';
		echo ' | ';
		echo '<a href="index.php?id='. $_GET["id"]. '&mov=grupos">Grupos</a>';
		echo ' | ';
		echo '<a href="index.php?id='. $_GET["id"]. '&mov=log">Log</a>';
		echo '</div>';
		
	echo '<div id="tabla_usuarios_area">';
	switch($_GET["mov"])
		{
		case 'guardar':
			echo '<div id="tabla_usuarios_lista_full">';
			echo '<p align="center">';
			if( strcmp($_POST["usuarios_nombre"], "" ) && strcmp($_POST["usuarios_nick"], "") && strcmp($_POST["usuarios_password"], "") && 
			strcmp($_POST["usuarios_email"], "") && strcmp($_POST["usuarios_pais"], "error") && strcmp($_POST["usuarios_estado"], "") )
				{
				if( strcmp($_POST["usuarios_tipo_usr"], "usuarios") && strcmp($_POST["usuarios_tipo_usr"], "Usuarios") )
					{
					$grp_cons= consultar_con( "PRIVILEGIOS", "NOMBRE='". proteger_cadena($_POST["usuarios_tipo_usr"]). "'" );
					$buf_grp= mysql_fetch_array($grp_cons);
					
					if( grupos_comprueba_miembro($buf_grp["USUARIOS"],$_GET["usr_id"]) )
						{
						if( !strcmp($buf_grp["USUARIOS"], "" ) || !strcmp($buf_grp["USUARIOS"], "vacio" ) ) //si no existen usuarios en este grupo
							$xb= $_GET["usr_id"];
						else //existen mas de un usuario
							$xb= $buf_grp["USUARIOS"]. ':'. $_GET["usr_id"]; //concatenamos
				
						$trama= array(
								"ID"=>"'". proteger_cadena($buf_grp["ID"]). "'",
								"USUARIOS"=>"'". proteger_cadena($xb). "'"
								);
					
						if( actualizar_bdd( "PRIVILEGIOS", $trama )==0 )
							echo 'Error en la Actualizacion de los Datos.';
						
						unset($xb);
						unset($trama);
						}
					unset($buf_grp);
					unset($grp_cons);
					}

				$valores= array(
					"id"=>"'". proteger_cadena($_GET["usr_id"]). "'", 
					"nombre"=>"'". proteger_cadena($_POST["usuarios_nombre"]). "'", 
					"nick"=>"'". proteger_cadena($_POST["usuarios_nick"]). "'", 
					"password"=>"'". proteger_cadena($_POST["usuarios_password"]). "'",
					"tipo_usr"=>"'". proteger_cadena($_POST["usuarios_tipo_usr"]). "'",  
					"email"=>"'". proteger_cadena($_POST["usuarios_email"]). "'", 
					"estado"=>"'". proteger_cadena(ucfirst($_POST["usuarios_estado"])) . "'", 
					"pais"=>"'". proteger_cadena(ucfirst($_POST["usuarios_pais"])). "'" );

				if( actualizar_bdd( "USUARIOS", $valores )==0 )
					echo 'Error en la Actualizacion de los Datos.';
				else
					echo 'Registro actualizado con exito...';
				}
			else
				echo 'ERROR !! Haz dejado algun valor sin rellenar.';
				
			echo '<br>[<a href="index.php?id='. $_GET["id"]. '&mov=listar">Regresar</a>]';
			echo '</p>';
			echo '</div>';
			break;
		case 'guardar_grupo':
			if( isset($_POST["grupos_nombre"]) ) //si existe nombre continuamos
				{
				do //generamos numero aleatorio de 4 a 10 digitos
					{
					$idtrack= generar_idtrack(); //obtenemos digito aleatorio
					}while( !strcmp( $idtrack, consultar_datos_general( "PRIVILEGIOS", "ID='". $idtrack. "'", "ID" ) ) );

				if( isset($_GET["id_src"]) ) //si existe ID_SRC entonces es edicion de un grupo
					{
					$trama= array(
						"id"=>"'". proteger_cadena($_GET["id_src"]). "'", 
						"nombre"=>"'". proteger_cadena($_POST["grupos_nombre"]). "'" );
					}
				else
					{
					$trama= array(
						"id"=>"'". $idtrack. "'", 
						"nombre"=>"'". proteger_cadena($_POST["grupos_nombre"]). "'", 
						"usuarios"=>"'vacio'",
						"secciones"=>"'vacio'" );
					}
					
				echo '<p align="center">';
				if( isset($_GET["id_src"]) )
					{
					if( actualizar_bdd( "PRIVILEGIOS", $trama )==0 )
						echo 'Error en la actualizacion de los Datos.';
					else
						echo 'Grupo actualizado con exito.';
					}
				else
					{
					if( insertar_bdd( "PRIVILEGIOS", $trama )==0 )
						echo 'Error en la Insercion de los Datos.';
					else
						echo 'Grupo agregado con exito.';
					}
				
				unset($trama);
				unset($idtrack);
				echo '<br>[<a href="index.php?id='. $_GET["id"]. '&mov=grupos">Finalizar</a>]';
				echo '</p>';
				}
			else //no existo nombre, entonces dejo vacio el input
				echo '<p align="center">Debes dar un nombre para crear el grupo.<br>[<a href="index.php?id='. $_GET["id"]. '&mov=grupos">Regresar</a>]</p>';
			break;
		case 'guardar_priv':
			//guardar_priv&grp_sec='. $buf_sec["ID"]. '&grp_priv='. $_GET["grp_export"]
			if( isset($_GET["grp_sec"]) && isset($_GET["grp_priv"]) )
				{
				$priv_cons= consultar_con( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["grp_priv"]). "'" );
				$buf_priv= mysql_fetch_array($priv_cons);
				
				if( !strcmp($buf_priv["SECCIONES"], "" ) || !strcmp($buf_priv["SECCIONES"], "0" ) || !strcmp($buf_priv["SECCIONES"], "vacio" ) ) //si no existen secciones a este grupo
					$valores= $_GET["grp_sec"];
				else //existen mas de una seccion
					$valores= $buf_priv["SECCIONES"]. ':'. $_GET["grp_sec"]; //concatenamos
				
				$trama= array(
							"ID"=>"'". proteger_cadena($_GET["grp_priv"]). "'",
							"SECCIONES"=>"'". proteger_cadena($valores). "'"   
							);
				
				if( actualizar_bdd( "PRIVILEGIOS", $trama )==0 )
					echo 'Error en la Insercion de los Datos.';
				else
					echo '<p align="center">Registro actualizado con exito...';
					echo '<br>[<a href="index.php?id='. $_GET["id"]. '&mov=grupos"><span style="font-size:0.9em;color:black;">Regresar</span></a>]</p>';
				
				unset($buf_priv);
				unset($valores);
				unset($priv_cons);
				}
			break;
		case 'editar':
			echo '<div id="tabla_usuarios_lista_full">';
			
			if( isset($_GET["usr_id"]) && mysql_num_rows(consultar_con( "USUARIOS", "ID='". proteger_cadena($_GET["usr_id"]). "'" ))>0)
				{
				$usr_cons= consultar_con( "USUARIOS", "ID='". proteger_cadena($_GET["usr_id"]). "'" );
				$usr_buf= mysql_fetch_array($usr_cons);
			
				echo 'Recuerda que al intentar modificar datos de un usuario puede causarle problemas de acceso/login en caso de mofidicar datos vital como: ';
				echo 'nick &oacute; email. Por lo cual recomendamos solo modificar informacion siempre y cuando esto lo requiera y el dueno de la cuenta lo reclame.<p>';
				echo 'A continuacion se muestra el registro solicitado:<p>';
				echo '<div id="formulario_registro_nuevo">';
					echo '<form action="index.php?id='. $_GET["id"]. '&mov=guardar&usr_id='. $_GET["usr_id"]. '" method="POST">';
					echo 'Nombre: <input type="text" name="usuarios_nombre" value="'. $usr_buf["NOMBRE"]. '"><br>';
					echo 'Nick: <input type="text" name="usuarios_nick" value="'. $usr_buf["NICK"]. '"><br>';
					echo 'Password: <input type="password" name="usuarios_password" value="'. $usr_buf["PASSWORD"]. '"><br>';
					echo 'Correo Electronico: <input type="text" name="usuarios_email" value="'. $usr_buf["EMAIL"]. '"><br>';
					
					# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soporte, 10=>usuario
					if( is_admin() )
						$grupos= array( "1"=>"Administrador", "2"=>"Autor", "3"=>"Editor", "4"=>"CoAdmin", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
					else if( is_coadmin() )
						$grupos= array( "2"=>"Autor", "3"=>"Editor", "4"=>"CoAdmin", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
					else if( is_soporte() )
						$grupos= array( "2"=>"Autor", "3"=>"Editor", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
					else if( is_autor() )
						$grupos= array( "2"=>"Autor", "10"=>"Usuario" ); 
					else
						$grupos= array( "3"=>"Editor", "10"=>"Usuario" );
						
					$grupos_user= lista_grupos("select");
											
					echo 'Grupo Actual: <i>'. lista_grupos("getmyname"). '</i><br>';
					echo 'Grupo: <select name="usuarios_tipo_usr">
						<option value="error" />';
						foreach( $grupos as $key=>$val )
							echo '<option value="'. $key. '">* '. $val. '</option>';
						if( is_array($grupos_user) && count($grupos_user) )
							{
							foreach( $grupos_user as $key )
								echo '<option value="'. desproteger_cadena($key). '">'. desproteger_cadena($key). '</option>';
							}
					echo '</select><br>';
					echo 'Pais: ';
					echo '<select name="usuarios_pais">';
					if( !strcmp( $usr_buf["PAIS"], "" ) )
						echo '<option value"error">sin establecer</option>';
					else
						echo '<option value="'. $usr_buf["PAIS"]. '">'. $usr_buf["PAIS"]. '</option>';
						
						lista_paises( "select" );
					echo '</select><br>';
					echo 'Estado: <input type="text" name="usuarios_estado" value="'. $usr_buf["ESTADO"]. '"><br>';
				echo '</div>';
				echo '<center><input type="submit" value="Modificar" style="padding:2px 3px 2px 3px;margin-bottom:10px;"></span></center>';
				unset($grupos, $grupos_user);
				}
			else
				echo '<p align="center">El Registro que intentas solicitar <b>no existe</b>...</p>';
			echo '</div>';
			break;
		case 'eliminar':
			echo '<div id="tabla_usuarios_lista_full">';
			
			if( isset($_GET["resp_sec"]) && $_GET["resp_sec"]==1 )
				{
				echo '<p align="center">';
				if( isset($_GET["usr_id"]) && mysql_num_rows(consultar_con("USUARIOS", "ID='". proteger_cadena($_GET["usr_id"]). "'"))>0 )
					{
					$tmp= mysql_fetch_array(consultar_con("USUARIOS", "ID='". proteger_cadena($_GET["usr_id"]). "'"));
					if( strcmp($tmp["TIPO_USR"], "Administrador") )
						{
						if( !eliminar_bdd( "USUARIOS", "ID='". proteger_cadena($_GET["usr_id"]). "'" ) )
							echo 'Problemas para eliminar datos.';
						else if( !elimina_espacio_personal($_GET["usr_id"]) )
							echo 'Problemas para eliminar archivos de la cuenta.';
						else
							echo 'Registro eliminado con exito.';
						}
					else
						echo 'No es posible borrar un usuario con privilegio de Administrador';
					}
				else
					echo 'El registro que intentas eliminar <b>no existe</b>';
				echo '</p>';
				echo '<p align="center">';
				echo '[<a href="index.php?id='. $_GET["id"]. '&mov=listar"><span style="font-size:0.9em;color:black;">Regresar</span></a>]';
				echo '</p>';
				}
			else
				{
				echo '<p align="center">';
				echo 'Esta seguro que desea eliminarlo?.';
				echo '<p><center>[<a href="index.php?id='. $_GET["id"]. '&mov='. $_GET["mov"]. '&usr_id='. $_GET["usr_id"]. '&resp_sec=1">';
					echo '<span style="font-size:0.9em;color:black;">Si, Eliminar Registro</span></a>]';
				echo '[<a href="index.php?id='. $_GET["id"]. '&mov=listar">';
					echo '<span style="font-size:0.9em;color:black;">No !!</span></a>]';
				echo '</center>';
				}

			echo '</div>';
			break;
		case 'eliminar_grupo':
		echo '<p align="center">';
		if( isset($_GET["id_src"]) )
			{
			$grp_cons= consultar_con( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["id_src"]). "'" );
			
			if( mysql_num_rows($grp_cons)==0 )
				echo 'No es posible eliminar un grupo que no existe.';
			else
				{
				if( !eliminar_bdd( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["id_src"]). "'" ) )
					echo 'Problemas para eliminar datos.';
				else
					echo 'Datos Eliminados con exito.';
				}
			
			unset($grp_cons);
			}
		else
			echo 'No es posible eliminar un grupo que no existe.';
			
		echo '<br>[<a href="index.php?id='. $_GET["id"]. '&mov=grupos"><span style="font-size:0.9em;color:black;">Regresar</span></a>]';
		echo '</p>';
			break;
		case 'listar':
			echo '<div id="tabla_usuarios_busquedas">';
				echo '<form action="index.php?id='. $_GET["id"]. '&mov='. $_GET["mov"]. '&muestra=1" method="POST">';
				echo '<img src="../modulos/usuarios-img/buscar.png" border="0"> ';
				echo '<input type="text" name="usuarios_buscar">';
				echo '</form>';
			echo '</div><br><br>';
			
			//echo '<div id="tabla_usuarios_lista_full">';
			echo '<table id="tabla_usuarios_lista_full2">';
			
			//paginacion
			$minimo= 15; //selector minimo del numero de registros a mostrar por consulta
			$pag= paginacion( $_GET["page"], "USUARIOS", $minimo );
			$grupos= array( "1"=>"Administrador", "2"=>"Autor", "3"=>"Editor", "4"=>"CoAdmin", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
			
			if( isset($_GET["muestra"]) && $_GET["muestra"]==1 )
				$cons_users= consultar_indexados( "USUARIOS", "NICK", proteger_cadena($_POST["usuarios_buscar"]) );
			else
				$cons_users= consultar_limite_enorden( "USUARIOS", (($pag*$minimo)-$minimo). ','. $minimo, "FECHA_REGISTRO DESC");
				
			if( mysql_num_rows($cons_users)==0 )
				echo '<p align="center">No existen resultados en la busqueda...</p>';
			else
				{
				//echo '<table>';
				echo '<th class="celda" width="30px;"> :: </th>';
				echo '<th class="celda" width="200px;">USERNAME</th>';
				echo '<th class="celda" width="50px;">CORREO</th>';
				echo '<th class="celda" width="100px;">GRUPO</th>';
				echo '<th class="celda" width="90px">REGISTRO</th>';
				echo '<th class="celda" width="90px">U.LOG</th>';
				echo '<th class="celda" width="130px">UBICACION</th>';
				echo '<th class="celda" width="130px">MAS INFO.</th><tr>';
				while( $buf_usr= mysql_fetch_array($cons_users) )
					{
					echo '<td><a href="index.php?id='. $_GET["id"]. '&mov=editar&usr_id='. $buf_usr["ID"]. '">
					<img src="../modulos/usuarios-img/editar.png" alt="Editar Usuario" title="Editar Grupo" border="0" style="width:12px;"></a> ';
					echo '<a href="index.php?id='. $_GET["id"]. '&mov=eliminar&usr_id='. $buf_usr["ID"]. '">
					<img src="../modulos/usuarios-img/eliminar.png" alt="Eliminar Usuario" title="Eliminar Grupo" border="0" style="width:12px;"></a></td>';
					echo '<td style="font-size:12px;padding-left:8px;">'. $buf_usr["NICK"]. '</td>';
					echo '<td style="text-align:center;font-size:12px;"><a href="mailto:'. $buf_usr["EMAIL"]. '">
					<img src="../modulos/usuarios-img/email.gif" border="0" style="width:14px;" alt="'. $buf_usr["EMAIL"]. '" title="'. $buf_usr["EMAIL"]. '"></a></td>';
					echo '<td style="text-align:center;font-size:12px;">'. $grupos[$buf_usr["TIPO_USR"]]. '</td>';
					echo '<td style="text-align:center;font-size:12px;">'. date( "d/", $buf_usr["FECHA_REGISTRO"]).noticia_cortada(mes_esp(date( "m", $buf_usr["FECHA_REGISTRO"])), 3).'/'.date( "Y", $buf_usr["FECHA_REGISTRO"]). '</td>';
					echo '<td style="text-align:center;font-size:12px;">';
					if( !strcmp($buf_usr["ULTIMO_LOGIN"], "") )
						echo 'sin establecer';
					else
						{
						echo date( "d/", $buf_usr["ULTIMO_LOGIN"]);
						echo noticia_cortada(mes_esp(date( "m", $buf_usr["ULTIMO_LOGIN"])), 3). '/';
						echo date( "Y", $buf_usr["ULTIMO_LOGIN"]);
						}
					
					echo '</td>';
					echo '<td style="text-align:center;font-size:12px;">'. $buf_usr["PAIS"]. '</td>';
					echo '<td style="text-align:center;font-size:12px;">';
					if( strcmp($buf_usr["MSN"], "") )
						echo '<a href="mailto:'. $buf_usr["MSN"]. '" ><img src="../modulos/usuarios-img/msn.gif" alt="'. $buf_usr["MSN"]. '" title="'. $buf_usr["MSN"]. '" border="0" style="width:14px;"></a>';
					if( strcmp($buf_usr["URL_WEB"], "") )
						echo '<a href="'. $buf_usr["URL_WEB"]. '" ><img src="../modulos/usuarios-img/www.gif" alt="'. $buf_usr["TITULO_WEB"]. '" title="'. $buf_usr["TITULO_WEB"]. '" border="0" style="width:14px;"></a>';
					if( strcmp($buf_usr["YOUTUBE"], "") )
						echo '<a href="'. $buf_usr["YOUTUBE"]. '" ><img src="../modulos/usuarios-img/youtube.gif" alt="'. $buf_usr["YOUTUBE"]. '" title="'. $buf_usr["YOUTUBE"]. '" border="0" style="width:14px;"></a>';
					//if( strcmp($buf_usr[""], "") )
					//	echo '<a href="" ><img src="" alt="" title=""></a>';
					echo '</td>';
					echo '<tr>';
					}
				echo '</table>';
				}
			//selector de paginacion o roll out
			selector_paginacion( "USUARIOS", $minimo, $pag, '?id='. $_GET["id"]. '&mov='. $_GET["mov"], "0", "0" );
			
			unset($buf_usr, $grupos, $cons_users);
			
			//echo '</div>';
			break;
		case 'grupos':
			echo '<div id="tabla_grupos_01">';
				echo '<ul>';
				echo '<li>Grupos</li>';
				echo 'En esta parte podras crear grupos a los cuales brindarles una exclusividad para cada seccion, de tal manera que la seccion establecida ';
				echo 'solo podra ser vista por los miembros del grupo.';
				echo '</ul>';
				
				if( isset($_GET["grp_edit"]) ) //editar nombre del grupo
					$grp_cons= consultar_con( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["grp_edit"]). "'" ); //consultar
				else if( isset($_GET["grp_del"]) )
					$grp_cons= consultar_con( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["grp_del"]). "'" ); //consultar
				
				echo '<p>';
				if( isset($_GET["grp_edit"]) )
					echo '<form action="index.php?id='. $_GET["id"]. '&mov=guardar_grupo&id_src='. $_GET["grp_edit"]. '" method="POST">';
				else if(isset($_GET["grp_del"]) )
					echo '<form action="index.php?id='. $_GET["id"]. '&mov=eliminar_grupo&id_src='. $_GET["grp_del"]. '" method="POST">';
				else
					echo '<form action="index.php?id='. $_GET["id"]. '&mov=guardar_grupo" method="POST">';
				echo 'Nombre: ';
				echo '<input type="text" name="grupos_nombre"';
				
				if( isset($_GET["grp_edit"]) || isset($_GET["grp_del"]) ) //si existen valores
					{
					$buf_grp= mysql_fetch_array($grp_cons);
					
					echo ' value="'. $buf_grp["NOMBRE"]. '"'; //nombre del grupo actual a modificar
					}
				echo '>';
				echo '<input type="submit" style="width:100px;height:22px;margin-left:5px;font-size:0.9em;" value="';
				
				if( isset($_GET["grp_edit"]) ) //si existen valores
					echo 'Modificar'; //imprimimos boton de modificar
				else if(isset($_GET["grp_del"]) )
					echo 'Si, Eliminar';
				else	echo 'Crear Grupo';
				echo '">';
				echo '</form>';
				
				if( isset($_GET["grp_edit"]) )
					{
					unset($buf_grp);
					unset($grp_cons);
					}
				
				echo '<div id="tabla_grupos_lista">';
				
					$grp_cons= consultar( "PRIVILEGIOS", "*" );
					if( mysql_num_rows($grp_cons)==0 )
						echo '<center>No existen Grupos aun...</center>';
					else
						{
						echo '<table>';
						echo '<th>--</th>';
						echo '<th style="width:170px;">NOMBRE DE GRUPOS</th>';
						echo '<th style="width:80px;">USUARIOS</th>';
						echo '<th style="width:80px;">SECCIONES</th><tr>';
						while( $buf_grp=mysql_fetch_array($grp_cons) )
							{
							echo '<td>';
							echo '<a href="index.php?id='. $_GET["id"]. '&mov=grupos&grp_edit='. $buf_grp["ID"]. '"><img src="../modulos/usuarios-img/editar.png" border="0" alt="Editar Grupo" title="Editar Grupo"></a> ';
							echo '<a href="index.php?id='. $_GET["id"]. '&mov=grupos&grp_del='. $buf_grp["ID"]. '"><img src="../modulos/usuarios-img/eliminar.png" border="0" alt="Eliminar Grupo" title="Eliminar Grupo"></a> ';
							echo '<a href="index.php?id='. $_GET["id"]. '&mov=grupos&grp_export='. $buf_grp["ID"]. '"><img src="../modulos/usuarios-img/exportar.png" border="0" alt="Exportar Grupo" title="Exportar Grupo"></a> ';
							echo '</td>';
							echo '<td>';
							echo ' '. $buf_grp["NOMBRE"];
							echo '</td>';
							echo '<td align="center">';
							if( strcmp($buf_grp["USUARIOS"], "vacio") && strcmp($buf_grp["USUARIOS"], "0") )
								echo contador_concatenados($buf_grp["USUARIOS"], ":" );
							else	echo $buf_grp["USUARIOS"];
							echo '</td>';
							echo '<td align="center">';
							if( strcmp($buf_grp["SECCIONES"], "vacio") && strcmp($buf_grp["SECCIONES"], "0") )
								echo contador_concatenados( $buf_grp["SECCIONES"], ":" );
							else	echo $buf_grp["SECCIONES"];
							echo '</td><tr>';
							}
						unset($buf_grp);
						}
						echo '</table>';
					unset($grp_cons);
				echo '</div>';
			echo '</div>';

			echo '<div id="tabla_grupos_02">';
				echo '<ul>';
				echo '<li>Exportaciones</li>
				En esta parte asignaras un acceso exclusivo a secciones de Menus ya creados, de tal manera que primer debes realizar la exportacion del Grupo deseado ';
				echo 'y continuar los pasos que te indicara el sistema';
				echo '</ul>';
				
				echo '<p>';
				echo '<b>1-</b> Primer <b>Exporta el Grupo</b>, haciendo click en el icono Exportacion..';
				
				if( isset($_GET["grp_export"]) )
					{
					echo '<br>';
					echo '<div id="tabla_grupos_lista"><center>';
					echo 'Grupo seleccionado: ';
					
					$grp_cons= consultar_con( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["grp_export"]). "'" );
					
					if( mysql_num_rows($grp_cons)==0 )
						echo '<b>grupo no existen..</b></center></div>';
					else
						{
						$buf_grp= mysql_fetch_array($grp_cons);

						echo '<b>'. $buf_grp["NOMBRE"]. '</b></center></div>';
						echo '<p>';
						if( isset($_POST["grp_seccion"]) && $_GET["grp_export"] )
							{
							echo '<div id="tabla_grupos_lista">';
							echo '<center>';
							
							if( strcmp($_POST["grp_seccion"], "") && strcmp($_POST["grp_seccion"], "error") )
								{
								$sec_cons= consultar_con( "SECCIONES", "ID='". proteger_cadena($_POST["grp_seccion"]). "'" );
								$buf_sec= mysql_fetch_array($sec_cons);
								$grp_privcons= consultar_con( "PRIVILEGIOS", "ID='". proteger_cadena($_GET["grp_export"]). "'" ); //elejimos el grupo
								$buf_priv= mysql_fetch_array($grp_privcons);

								//si esta vacio o si no esta en la lista, procedemos a insertar el ID de la seccion en el grupo
								if( !strcmp($buf_priv["SECCIONES"], "") || grupos_comprueba_miembro($buf_priv["SECCIONES"],$_POST["grp_seccion"]) )
									{
									echo 'Seccion seleccionada: <b>'. $buf_sec["NOMBRE"]. '</b></center></div>';
									
									echo '<p><div id="tabla_grupos_lista">';
										echo 'Haz elejido la seccion <b>'. $buf_sec["NOMBRE"]. '</b> para que solo los miembros del grupo ';
										echo '<b>'. $buf_priv["NOMBRE"]. '</b> puedan visualizar y tener acceso a esta seccion.';
										echo '<p align="center">Si todo es correcto procede a <b>Guardar</b>.</p>';
									echo '</center></div></p>';
									
									echo '<center><form action="index.php?id='. $_GET["id"]. '&mov=guardar_priv&grp_sec='. $buf_sec["ID"]. '&grp_priv='. $_GET["grp_export"]. '" method="POST">';
									echo '<input type="submit" value="Guardar" style="width:150px;">';
									echo '</form></center>';
									}
								else
									echo 'Esta seccion ya pertenece a este grupo...</center></div>';
							
								unset($sec_cons);
								unset($buf_sec);
								unset($buf_priv);
								unset($grp_privcons);
								}
							else
								echo 'La seccion seleccionada no es valida, vuelve a exportar el grupo...</center></div>'; 
							}
						else
							{
							echo '<b>2-</b> Ahora elije la seccion a la que deseas <b>agregarle la exclusividad</b>.';						
							echo '<form action="index.php?id='. $_GET["id"]. '&mov='. $_GET["mov"]. '&grp_export='. $_GET["grp_export"]. '" method="POST">';
							echo '<div id="tabla_grupos_lista"><center>Elije Seccion: ';
						
							echo '<select name="grp_seccion">';
							echo '<option value="error"></option>';
							$menus_cons= consultar_enorden( "SECCIONES", "RELACION DESC");
					
							if( mysql_num_rows($menus_cons)==0 ) //sino existen secciones
								echo '<option value="error">cree primero algun menu...</option>';
							else //si existen secciones y se en listaran
								{
								$abc='';
								while( $menus_buf= mysql_fetch_array($menus_cons) ) //listaremos todas las secciones
									{
									# si 'abc' es distinto del valor "RELACION" accedemos
									if( strcmp( $abc, $menus_buf["RELACION"] ) )
										{
										if( strcmp($abc, "") ) //si 'abc' esta vacio accedemos
											echo '<option value="error"></option>'; //imprimimos espacio vacio
										$abc= consultar_datos_general( "MENUS", "ID='". $menus_buf["RELACION"]. "'", "nombre" ); //copiamos valor
										echo '<option value="error" style="color:gray;">:: '. $abc. ' ::</option>'; //imprimimos menus como metodo de separacion
										}
									
									echo '<option value="'. $menus_buf["ID"]. '">&nbsp;&nbsp;&nbsp;&nbsp;'. $menus_buf["NOMBRE"]; //imprimimos valor
								
									//if( !strcmp( $index_buf["INDEX_DEFAULT"], "index.php?ver='.$menus_buf["ID"] ) ) //si estamos en un valor distinto
									//	echo ' (cargada)';
								
									echo '</option>';
									}
					
								unset($abc);
								}
							unset( $menus_cons );
							unset($menus_buf);
							echo '</select>';
							echo '</center></div>';
							echo '</p>';
						
							echo '<center><input type="submit" value="Procesar" style="width:150px;"></center>';
							}
						
						unset($buf_grp);
						}
						
					unset($grp_cons);
					}
				echo '</p>';
			echo '</div>';
			break;
		case 'log':

			//paginacion
			$minimo= 12; //selector minimo del numero de registros a mostrar por consulta
			$pag= paginacion( $_GET["page"], "LOG", $minimo );
			
			if( isset($_GET["muestra"]) && $_GET["muestra"]==1 )
				$cons_log= consultar_indexados( "LOG", "NICK", proteger_cadena($_POST["usuarios_buscar"]) );
			else
				$cons_log= consultar_limite_enorden( "LOG", (($pag*$minimo)-$minimo). ','. $minimo, "FECHA_LOGIN DESC");

			echo '
			<div class="w3-container">
				<form class="w3-padding" action="index.php?id='. $_GET["id"]. '&mov='. $_GET["mov"]. '&muestra=1" method="POST">
					<div class="w3-threequarter">
					<input type="text" class="w3-input w3-border" placeholder="registro a buscar..." id="q" name="q"></div>
					<div class="w3-quarter"><button class="w3-margin-left w3-button w3-padding w3-purple">Buscar</button></div>
				</form>
				<p class="w3-padding-32">
					<div class="w3-rows">
						<ul class="w3-bar w3-small w3-red">
							<li class="w3-bar-item" style="width:5%;">::</li>
							<li class="w3-bar-item" style="width:8%;">Usuario</li>
							<li class="w3-bar-item" style="width:20%;">Geo</li>
							<li class="w3-bar-item" style="width:10%;">IP</li>
							<li class="w3-bar-item" style="width:15%;">S.O/NAV</li>
							<li class="w3-bar-item" style="width:10%;">Fecha</li>
							<li class="w3-bar-item" style="width:32%;">Referencia</li>
						</ul>
					</div>
					<div class="w3-rows w3-small">';
			if( mysql_num_rows($cons_log)==0 )
				echo '<div class="w3-bar w3-small" style="width:100%;">No hay datos aun...</div>';
			else
				{
				$a=0;
				while( $buf_log= mysql_fetch_array($cons_log) )
					{
					$parse= parse_url($buf_log["REFERENCIA"]); # parseamos
					$ref= ( (strstr($_SERVER['HTTP_HOST'], $parse["path"]) || strstr($_SERVER['HTTP_HOST'], $parse["host"])) ? 0:1);

					$robot= is_a_robot($buf_log["NAVEGADOR"]);
					echo '
					<div class="w3-bar w3-tiny'. ($robot ? ' w3-pale-yellow':($ref ? ' w3-pale-green':($parse["query"] ? ' w3-green':''))). '">
							<li class="w3-bar-item" style="width:5%;">';
					if( $robot )
						echo '<img src="../admin/imagenes/letra_i.png" alt="'. $robot. '" title="'. $robot. '" border="0">';
					else if( $ref ) //entonces fue una busqueda o referido
						echo '<img src="../admin/imagenes/letra_r.png" alt="'. $buf_log["REFERENCIA"]. '" title="'. $buf_log["REFERENCIA"]. '" border="0">';
					else if( !strcmp(urlparser($buf_log["REFERENCIA"], array("www", 1)), urlparser($_SERVER['HTTP_HOST'], array("www", 1))) )
						echo '<img src="../admin/imagenes/letra_u.png" alt="'. $buf_log["REFERENCIA"]. '" title="'. $buf_log["REFERENCIA"]. '" border="0">';
					echo '</li>
							<li class="w3-bar-item" style="width:8%;">'. desproteger_cadena($buf_log["NICK"]). '</li>
							<li class="w3-bar-item" style="width:20%;">'. desproteger_cadena($buf_log["UBICACION"]). '</li>
							<li class="w3-bar-item" style="width:10%;">'. desproteger_cadena($buf_log["IP"]). '</li>
							<li class="w3-bar-item" style="width:15%;">'. desproteger_cadena($buf_log["SO"].'/'.$buf_log["NAVEGADOR"]). '</li>
							<li class="w3-bar-item" style="width:10%;">'. date( "d/m/Y, g:i a", $buf_log["FECHA_LOGIN"]). '</li>
							<li class="w3-bar-item" style="width:32%;">'. ($robot ? 'Index / ':''). desproteger_cadena($buf_log["REFERENCIA"]). '</li>
						</div>
					';
					unset($robot);
					}
				}

				echo '</div>
				</p>
			</div>
			';
			
			unset($buf_log, $cons_log);
			
			//selector de paginacion o roll out
			echo '<div class="container w3-padding w3-small">';
				selector_paginacion( "LOG", $minimo, $pag, '?id='. $_GET["id"]. '&mov='. $_GET["mov"], "0", "0" );
			echo '</div>';
			break;
		case 'new';
			echo "<div id=\"tabla_usuarios_lista_full\">";
		
			if( !strcmp($_GET["set"], "guardar") )
				{
				echo "<p align=\"center\">";
				if( strcmp($_POST["usuarios_nombre"], "" ) && strcmp($_POST["usuarios_nick"], "") && strcmp($_POST["usuarios_password"], "") && 
				strcmp($_POST["usuarios_email"], "") )
					{
					if( strcmp($_POST["usuarios_tipo_usr"], "usuarios") && strcmp($_POST["usuarios_tipo_usr"], "Usuarios") )
						{
						$grp_cons= consultar_con( "PRIVILEGIOS", "NOMBRE='". proteger_cadena($_POST["usuarios_tipo_usr"]). "'" );
						$buf_grp= mysql_fetch_array($grp_cons);
					
						if( grupos_comprueba_miembro($buf_grp["USUARIOS"],$_GET["usr_id"]) )
							{
							if( !strcmp($buf_grp["USUARIOS"], "" ) || !strcmp($buf_grp["USUARIOS"], "vacio" ) ) //si no existen usuarios en este grupo
								$xb= proteger_cadena($_GET["usr_id"]);
							else //existen mas de un usuario
								$xb= $buf_grp["USUARIOS"]. ":". proteger_cadena($_GET["usr_id"]); //concatenamos
				
							$trama= array(
									"ID"=>"'". $buf_grp["ID"]. "'",
									"USUARIOS"=>"'". $xb. "'"
									);
						
							if( actualizar_bdd( "PRIVILEGIOS", $trama )==0 )
								echo "Error en la Actualizacion de los Datos.";
						
							unset($xb);
							unset($trama);
							}
						unset($buf_grp);
						unset($grp_cons);
						}

					do //generamos numero aleatorio de 4 a 10 digitos
						{
						$idtrack= generar_idtrack(); //obtenemos digito aleatorio
						}while( !strcmp( $idtrack, consultar_datos_general( "USUARIOS", "ID='". $idtrack. "'", "ID" ) ) );

					$valores= array(
						"id"=>"'". $idtrack. "'", 
						"nombre"=>"'". proteger_cadena($_POST["usuarios_nombre"]). "'", 
						"nick"=>"'". proteger_cadena($_POST["usuarios_nick"]). "'", 
						"password"=>"'". proteger_cadena($_POST["usuarios_password"]). "'",
						"tipo_usr"=>"'". proteger_cadena($_POST["usuarios_tipo_usr"]). "'",  
						"email"=>"'". proteger_cadena($_POST["usuarios_email"]). "'", 
						"estado"=>"'0'", 
						"pais"=>"'0'",
						"notificacion_mp"=>"'1'", 
						"notificacion_comentarios"=>"'1'", 
						"notificacion_noticias"=>"'1'",  
						"fecha_registro"=>"'". time(). "'" );
					
					$dir_actual= getcwd();
					chdir( "../" );
					if( inicializar_espacio_personal(proteger_cadena($idtrack)) ) # incializamos espacio
						{
						if( insertar_bdd( "USUARIOS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							{
							echo "Registro generado";
							# $to= $_POST["usuarios_email"];
							$to= TITULO_WEB. ' <'. $_POST["usuarios_email"]. '>';
							$asunto= 'Bienvenido al Sistema :: Datos de su Cuenta.';
							$enlace= 'Le confirmamos que su cuenta ha sido asignada por el <b>Administrador</b> del sistema. Por favor no comparta la siguiente informacion de acceso, ya que es para que 
							solo usted pueda identificarse.
							<p>
							<b>Usuario:</b> '. proteger_cadena($_POST["usuarios_nick"]). '
							<br><b>Clave:</b> '. proteger_cadena($_POST["usuarios_password"]). '
							</p>';
							if( enviar_correo( $to, $asunto, 7, $enlace, 0, 0, 0, 0 ) )
								echo ' y confirmaci'. acento("o"). 'n enviada por correo.';
							else echo ', pero la confirmaci'. acento("o"). 'n por correo fallo.';
							}
						}
					else
						echo 'Error: No se pudo inicializar el espacio para el usuario.';
					chdir($dir_actual);
					}
				else
					echo "ERROR !! Haz dejado algun valor sin rellenar.";
				
				echo "<br>[<a href=\"?id=". proteger_cadena($_GET["id"]). "&mov=listar\">Regresar</a>]";
				echo "</p>";
				}
			else
				{
				echo 'En el siguiente formulario podra <b>agregar nuevas cuentas</b> de usuario de forma personal, asegurese de rellenar todos los campos requeridos para evitar problemas en el 
				procesamiento del formulario. Tambien recuerde que si algun dato personal (direccion, ciudad, etc...) esta incorrecto, el usuario propietario de la cuenta podra <b>modificar</b> estos 
				datos desde su <b>Perfil</b> propio.';
				echo "<div id=\"formulario_registro_nuevo\">";
					echo "<form action=\"index.php?id=". $_GET["id"]. "&mov=". $_GET["mov"]. "&set=guardar\" method=\"POST\">";
					echo "Nombre: <input type=\"text\" name=\"usuarios_nombre\" value=\"". $usr_buf["NOMBRE"]. "\"><br>";
					echo "Nick: <input type=\"text\" name=\"usuarios_nick\" value=\"". $usr_buf["NICK"]. "\"><br>";
					echo "Password: <input type=\"password\" name=\"usuarios_password\" value=\"". $usr_buf["PASSWORD"]. "\"><br>";
					echo "Correo Electronico: <input type=\"text\" name=\"usuarios_email\" value=\"". $usr_buf["EMAIL"]. "\"><br>";
					if( strcmp($usr_buf["TIPO_USR"], "Administrador") )
						{
						# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
					# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soporte, 10=>usuario
						if( is_admin() )
							$grupos= array( "1"=>"Administrador", "2"=>"Autor", "3"=>"Editor", "4"=>"CoAdmin", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
						else if( is_coadmin() )
							$grupos= array( "2"=>"Autor", "3"=>"Editor", "4"=>"CoAdmin", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
						else if( is_soporte() )
							$grupos= array( "2"=>"Autor", "3"=>"Editor", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
						else if( is_autor() )
							$grupos= array( "2"=>"Autor", "10"=>"Usuario" ); 
						else
							$grupos= array( "3"=>"Editor", "10"=>"Usuario" );

						$grupos_user= lista_grupos("select");
												
						echo 'Grupo: <select name="usuarios_tipo_usr">
							<option value="error" />';
							foreach( $grupos as $key=>$val )
								echo '<option value="'. $key. '">* '. $val. '</option>';
							if( is_array($grupos_user) && count($grupos_user) )
								{
								foreach( $grupos_user as $key )
									echo '<option value="'. desproteger_cadena($key). '">'. desproteger_cadena($key). '</option>';
								}
						echo "</select><br>";
						unset($grupos, $grupos_user);
						}
					else
						echo "<input type=\"hidden\" name=\"usuarios_tipo_usr\" value=\"". $usr_buf["TIPO_USR"]. "\">";
				echo "</div>";
				echo "<center><input type=\"submit\" value=\"Registrar\" style=\"padding:2px 3px 2px 3px;margin-bottom:10px;\"></span></center>";
				}
			echo "</div>";
			break;
		default:
			if( isset($_GET["mov"]) )
				echo '<p>Seccion invalida...</p>';
			else
				{
				echo 'En este apartado tendras a tu disponibilidad los siguientes servicios:
				<ul>
				<li>Listar</li>
					Podras observar, modificar, eliminar los usuarios inscritos a tu sitios web
				<li>Grupos</li>
					Podras agregar, modificar, eliminar grupos en tu sitio web y apartir de estos grupos integrar un conjunto de usuarios. Tambien 
				realizar acciones en determinas secciones para otorgar un servicio de exclusividad.</ul>';
				}
			break;
		}
	echo '</div>';
	echo '</div>';
	echo '</td>';
	}
	
//llamadas para el PANEL DE CONTROL
if( $_GET["id"]=="usuarios" ) //variable reservada para modulos
	{
	echo '<table cellspacing="0" cellpadding="0" align="center" id="tabla_areatrabajo">';
	if( is_admin() || is_coadmin() )
		usuarios();
	else		echo '<td align="center">Area Restringida...</td>';
	echo '</table>';
	}
?>
