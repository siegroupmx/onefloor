<?php
//Funcion para administrar la configuracion del servidor y web
function configuracion()
	{
	switch( $_GET["mov"] )
		{
		case 'guardar':
				if( !strcmp($_POST["configuracion_titulo"], "error") || 
					!strcmp($_POST["configuracion_meta_descripcion"], "error") || !strcmp( $_POST["configuracion_meta_claves"], "error") || 
					!strcmp($_POST["configuracion_meta_clasificacion"], "error" ) || !strcmp($_POST["configuracion_tema"], "error") || 
					!strcmp($_POST["configuracion_caritas"], "error") )
					{
					echo "<td align=\"center\">Existen datos importantes que deben configurarse primero, porfavor revise su seccion de menus.";
					echo "<p><a href=\"index.php?id=configuracion\">[Continuar]</a></td>";
					}
				else
					{
					$fp= fopen( "../".$_POST["configuracion_caritas"]."/settings", "r" );
					$buf_tmp= fgets( $fp, 1024 );
					$x= explode( "=", $buf_tmp );
					$buf_caritas= $x[1];
					fclose($fp);
					unset($fp);
					unset($x);
					unset($buf_tmp);
					
					$fp= fopen( "../".$_POST["configuracion_tema"]."/settings", "r" );
					$buf_tmp= fgets( $fp, 1024 );
					$x= explode( "=", $buf_tmp );
					$buf_tema= $x[1]; 
					fclose($fp);
					unset($fp);
					unset($x);
					unset($buf_tmp);
					
					if( !strcmp($_POST["subir"], "Subir") )
						{
						$dir_upload= "../".TEMA_URL."/banners/";
						if( strcmp($_FILES["upload_banner"]["name"], "") )
							{
							if( comprobar_extencion_imagen( $_FILES ) ) //comprobando extencion
								{
								set_time_limit(600);
								move_uploaded_file( $_FILES["upload_banner"]["tmp_name"], $dir_upload.$_FILES["upload_banner"]["name"] );
								
								$banner_list=""; //variable banner lista
								if( strcmp(consultar_datos_general("SERVER_CONFIG", "ID='1'", "BANNER"), "") ) //verificamos si existen banners
									$banner_list .= consultar_datos_general("SERVER_CONFIG", "ID='1'", "BANNER"). ":"; //concatenamos banners existentes
								$banner_list .= proteger_cadena($_FILES["upload_banner"]["name"]); //concatenamos banner nuevo
								
								$tr_upload= array(
										"id"=>"1", 
										"banner"=>"'". $banner_list. "'"
										);
										
								actualizar_bdd( "SERVER_CONFIG", $tr_upload );
								set_time_limit(30);
								}
							}
						else	echo "upload vacio..";
						}
					
					$trama= array(
					"id"=>"1", 
					"titulo_web"=>"'". proteger_cadena($_POST["configuracion_titulo"]). "'", 
					"index_default"=>"'". proteger_cadena("index.php?ver=".$_POST["configuracion_index"]). "'",  
					"meta_descripcion"=>"'". proteger_cadena($_POST["configuracion_meta_descripcion"]). "'", 
					"meta_claves"=>"'". proteger_cadena($_POST["configuracion_meta_claves"]). "'", 
					"meta_clasificacion"=>"'". proteger_cadena($_POST["configuracion_meta_clasificacion"]). "'",
					"tema_name"=>"'". proteger_cadena($buf_tema). "'",  
					"tema_url"=>"'". proteger_cadena($_POST["configuracion_tema"]). "'",
					"tema_url_m"=>"'". proteger_cadena($_POST["configuracion_tema"]). "'",
					"caritas_name"=>"'". proteger_cadena($buf_caritas). "'",   
					"caritas_url"=>"'". proteger_cadena($_POST["configuracion_caritas"]). "'", 
					"relay_limit"=>"'". proteger_cadena($_POST["configuracion_relay_limit"]). "'", 
					"registro_usuario"=>"'". proteger_cadena($_POST["configuracion_nuevo_registro"]). "'" );
					
					if( actualizar_bdd( "SERVER_CONFIG", $trama )==0 )
						echo "<td align=\"center\">Error en la Insercion de los Datos.<p>";
					else
						{
						echo "<td align=\"center\">Tus datos se han actualizado con exito.<p>";
						unset($trama);

						//0 => nuevo comentario 
						//1 => error en el sistema 
						//2 => registre un usuario 
						//3 => nueva noticia
						//4 => sistema de recuperacion de datos del usuario
						$trama= array(
						"id"=>"'2'", 
						"mensaje"=>"'". proteger_cadena($_POST["notificacion_registro"]). "'" );
						actualizar_bdd( "MENSAJES_NOTIFICACION", $trama );
						unset($trama);

						$trama= array(
						"id"=>"'0'",  
						"mensaje"=>"'". proteger_cadena($_POST["notificacion_comentario"]). "'" );
						actualizar_bdd( "MENSAJES_NOTIFICACION", $trama );
						unset($trama);

						$trama= array(
						"id"=>"'3'", 
						"mensaje"=>"'". proteger_cadena($_POST["notificacion_noticia"]). "'" );
						actualizar_bdd( "MENSAJES_NOTIFICACION", $trama );
						unset($trama);

						$trama= array(
						"id"=>"'4'", 
						"mensaje"=>"'". proteger_cadena($_POST["notificacion_recuperaciondatos"]). "'" );
						actualizar_bdd( "MENSAJES_NOTIFICACION", $trama );
						unset($trama);

						$trama= array(
						"id"=>"'1'", 
						"mensaje"=>"'". proteger_cadena($_POST["notificacion_errorsistema"]). "'" );
						actualizar_bdd( "MENSAJES_NOTIFICACION", $trama );
						unset($trama);

						$trama= array(
						"id"=>"'5'", 
						"mensaje"=>"'". proteger_cadena($_POST["notificacion_mp"]). "'" );
						actualizar_bdd( "MENSAJES_NOTIFICACION", $trama );
						unset($trama);
						}

					echo "<a href=\"index.php?id=configuracion\">[Continuar]</a></td>";
					}
			break;
		default:
			echo "<td colspan=\"2\">";
			echo "En este apartado podras configurar el titulo de tu web y herramientas para obtener mejores resultados por parte de los distintos";
			echo " buscadores, asi que no olvides configurar bien tus META's correspondientes.";
			echo " Respecto a los distintos temas y caritas para tu sitio web, es necesario posiciones tus temas y caritas en su carpeta correspondiente.";
			echo 			" En seguida elige tu tema y carita en este apartado y asegurate de guardar tu configuracion.";
			echo "</td><tr>";
			echo "<td>";
			echo "<form action=\"index.php?id=configuracion&mov=guardar\" method=\"POST\" enctype=\"multipart/form-data\">";
			
			echo "<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" id=\"tabla_configuracion\">";
			echo "<th colspan=\"2\">TU SITIO WEB</th><tr>";
			echo "<td align=\"right\">Titulo del Web:</td><td align=\"left\"><input type=\"text\" name=\"configuracion_titulo\" value=\"". TITULO_WEB. "\"></td><tr>";
			echo "<td align=\"right\">Index del Sitio:</td><td align=\"left\">";
				echo "<select name=\"configuracion_index\">";
				
					$index_cons= consultar( "SERVER_CONFIG", "*" );
					$index_buf= mysql_fetch_array($index_cons);

					$menus_cons= consultar_enorden( "SECCIONES", "RELACION DESC");
					
					if( mysql_num_rows($menus_cons)==0 ) //sino existen secciones
						echo "<option value=\"error\">cree primero alguna seccion de menu...</option>";
					else //si existen secciones y se en listaran
						{
						//si INDEX_DEFAULT esta vacio o con un 0, entonces esta sin establecer
						if( !strcmp( $index_buf["INDEX_DEFAULT"], "" ) || !strcmp( $index_buf["INDEX_DEFAULT"], "0" ))
							echo "<option value=\"0\">sin establecer</option>";
						else
							{
							$w= explode( "=", $index_buf["INDEX_DEFAULT"] );
							
							echo "<option value=\"". $w[1]. "\">Ya establecido...</option>";
							unset($w);
							}
						
						$abc="";
						while( $menus_buf= mysql_fetch_array($menus_cons) ) //listaremos todas las secciones
							{
							//si 'abc' es distinto del valor "RELACION" accedemos
							if( strcmp( $abc, $menus_buf["RELACION"] ) )
								{
								if( strcmp($abc, "") ) //si 'abc' esta vacio accedemos
									echo "<option value=\"error\"></option>"; //imprimimos espacio vacio
								$abc= consultar_datos_general( "MENUS", "ID='". $menus_buf["RELACION"]. "'", "id"); //copiamos valor
								echo "<option value=\"error\">:: ". consultar_datos_general( "MENUS", "ID='". $menus_buf["RELACION"]. "'", "nombre"). " ::</option>"; //imprimimos menus como metodo de separacion
								}
								
							echo "<option value=\"". $menus_buf["ID"]. "\">". $menus_buf["NOMBRE"]; //imprimimos valor
							
							if( !strcmp( $index_buf["INDEX_DEFAULT"], "index.php?ver=".$menus_buf["ID"] ) ) //si estamos en un valor distinto
								echo " (cargada)";
							
							echo "</option>";
							}
					
						unset($abc);
						}
					unset( $menus_cons );
					unset( $menus_buf );
					unset( $index_cons );
					unset( $index_buf );
					
				echo "</select></td><tr>";
			echo "<td align=\"right\">Meta Descripcion:</td><td align=\"left\"><textarea name=\"configuracion_meta_descripcion\">". META_DESCRIPCION. "</textarea></td><tr>";
			echo "<td align=\"right\">Meta Claves:</td><td align=\"left\"><textarea name=\"configuracion_meta_claves\">". META_CLAVES. "</textarea></td><tr>";
			echo "<td align=\"right\">Meta Clasificacion:</td><td align=\"left\"><textarea name=\"configuracion_meta_clasificacion\">". META_CLASIFICACION. "</textarea></td><tr>";
			echo "<td align=\"right\">Mensaje Registro Usuario:</td><td align=\"left\"><textarea name=\"configuracion_nuevo_registro\">". consultar_datos_base( "SERVER_CONFIG", 1, "registro_usuario" ). "</textarea></td><tr>";
			echo "</table>";

			echo "<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" id=\"tabla_configuracion\">";
			echo "<th colspan=\"2\">RELAY SMTP - CORREO</th><tr>";
			echo "<td colspan=\"2\" style=\"text-align:left;\">Esta opcion le permite configurar el limite maximo de salida de<br>correos que le proporciona su SMTP Relay.</td><tr>";
			echo "<td style=\"width:130px;text-align:right;\">Relay SMTP Limit:</td>";
			echo "<td style=\"text-align:left;\"><input type=\"text\" name=\"configuracion_relay_limit\" style=\"width:100px;\" value=\"";
			echo consultar_datos_base( "SERVER_CONFIG", 1, "relay_limit" );
			echo "\"></td>";
			echo "</table>";

			echo "<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" id=\"tabla_configuracion\">";
			echo "<th colspan=\"2\">BANNERS</th><tr>";
			echo "<td>Los siguientes banners se visualizaran de forma aleatoria.</td><tr>";
			echo "<td style=\"border:solid 0px gray;border-bottom-width:1px;\">";
			echo "<input type=\"file\" name=\"upload_banner\"> ";
			echo "<input type=\"submit\" value=\"Subir\" style=\"width:50px;\" name=\"subir\"></td><tr>";
			
			$ban_cons= consultar( "SERVER_CONFIG", "*" );
			
			if( mysql_num_rows($ban_cons)>0 )
				{
				$buf_ban=mysql_fetch_array($ban_cons);
				
				if( strcmp($buf_ban["BANNER"], "") )
					{
					if( strchr( $buf_ban["BANNER"], ":") ) //si existe delimitados
						{
						$x= explode( ":", $buf_ban["BANNER"] );
					
						for( $i=0; $i<sizeof($x); $i++ )
							echo "<td style=\"font-family:verdana;font-size:11px;padding-left:3px;padding-top:1px;text-align:left;\">". $x[$i]. "</td><tr>";
						
						unset($x);
						}
					else
						echo "<td style=\"font-family:verdana;font-size:11px;padding-left:3px;padding-top:1px;text-align:left;\">". $buf_ban["BANNER"]. "</td>";
					unset($buf_ban);
					}
				else
					echo "<td style=\"font-family:verdana;font-size:10px;padding-left:3px;padding-top:1px;\">No banners....</td>";
				}
			else
				echo "<td style=\"font-family:verdana;font-size:10px;padding-left:3px;padding-top:1px;\">No banners....</td>";
			unset($ban_cons);

			echo "</table>";
			echo "<td>";

			//0 => nuevo comentario 
			//1 => error en el sistema 
			//2 => registre un usuario 
			//3 => nueva noticia
			//4 => sistema de recuperacion de datos del usuario
			echo "<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" id=\"tabla_configuracion\">";
				echo "<th colspan=\"2\">MENSAJES DE NOTIFICACION</th><tr>";
				echo "<td align=\"left\" colspan=\"2\">Los siguientes mensajes se incluiran como <b>asunto</b> en notificaciones por correo electronico (<b>BBCode</b> On).</td><tr>";
				echo "<td>Registro Nuevo Usuario:</td><td><textarea name=\"notificacion_registro\">". consultar_datos_base( "MENSAJES_NOTIFICACION", 2, "mensaje" ). "</textarea></td><tr>";
				echo "<td>Comentario Nuevo:</td><td><textarea name=\"notificacion_comentario\">". consultar_datos_base( "MENSAJES_NOTIFICACION", 0, "mensaje" ). "</textarea></td><tr>";
				echo "<td>Noticia Nueva en el sitio:</td><td><textarea name=\"notificacion_noticia\">". consultar_datos_base( "MENSAJES_NOTIFICACION", 3, "mensaje" ). "</textarea></td><tr>";
				echo "<td>Nuevo Mensaje Privado:</td><td><textarea name=\"notificacion_mp\">". consultar_datos_base( "MENSAJES_NOTIFICACION", 5, "mensaje" ). "</textarea></td><tr>";
				echo "<td>Recuperacion de Perfil:</td><td><textarea name=\"notificacion_recuperaciondatos\">". consultar_datos_base( "MENSAJES_NOTIFICACION", 4, "mensaje" ). "</textarea></td><tr>";
				echo "<td>Error del Sistema:</td><td><textarea name=\"notificacion_errorsistema\">". consultar_datos_base( "MENSAJES_NOTIFICACION", 1, "mensaje" ). "</textarea></td><tr>";
			echo "</table>";

			echo "<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" id=\"tabla_configuracion\">";
			echo "<th colspan=\"2\">TEMAS E ICONOS</th><tr>";
			echo "<td align=\"right\">Nombre del Tema Actual: </td><td align=\"left\"> <b>". TEMA_NAME. "</b></td><tr>";
			echo "<td align=\"right\">URL del Tema:</td><td align=\"left\">";
				echo"<select name=\"configuracion_tema\">";
					ver_contenido_directorio( "templates/", TEMA_URL, "select" );
				echo "</select>";
			echo "</td><tr>";
			echo "<td align=\"right\">Nombre de Caritas: </td><td align=\"left\">";
			if( !strcmp(CARITAS_NAME, "") )
				echo " <b>sin establecer</b>";
			else
				echo "<b>". CARITAS_NAME. "</b>";
			echo "</td><tr>";
			echo "<td align=\"right\">URL de Caritas: </td>";
			echo "<td align=\"left\">";
			echo "<select name=\"configuracion_caritas\">";
								ver_contenido_directorio( "caritas/", CARITAS_URL, "select" );
			echo "</select>";
			echo "</td>";
			echo "</table>";
			
			echo "</td>";
			echo "<tr>";
			echo "<td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"Guardar Configuracion\" class=\"boton_01\" style=\"margin-bottom:4px;margin-top:4px;\"></td>";
			echo "</form>";
			break;
		}
	}


if( $_GET["id"]=="configuracion" )
	{
	echo "<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" id=\"tabla_areatrabajo\">";
	if( is_admin() || is_coadmin() )
		configuracion();
	else	echo '<td align="center">Area Restringida...</td>';
	
	echo "</table>";
	}
	
?>