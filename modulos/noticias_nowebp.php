<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

function noticias_publicar( $ctl )
	{
	if( strchr( $ctl, "," ) ) //si existe delimitador, entonces se desea agregar noticia
		{
		$x= explode( ",", $ctl );
		$tipo= consultar_datos_general( "SECCIONES", "ID='". proteger_cadena($x[1]). "'", "tipo" );
		echo "<span>Publicando en: <b>". consultar_datos_general( "MENUS", "ID='". proteger_cadena($x[0]). "'", "nombre"). " / ". consultar_datos_general( "SECCIONES", "ID='". $x[1]. "'", "nombre"). "</b></span>";
		}
	else //entonces se desea modificar noticia
		{
		$cons_not= consultar_con( "NOTICIAS", "ID='". $ctl. "'" ); //filtramos la NOTICIA a modiciar
		$buf_not= mysql_fetch_array($cons_not);
		//filtramos la seccion donde se colgo la noticia
		$cons_sec= consultar_con( "SECCIONES", "ID='". $buf_not["SECCION"]. "':RELACION='". $buf_not["MENU"]. "'" );
		$buf_sec= mysql_fetch_array($cons_sec);
		echo "<span><b>Modificando en:</b> ". consultar_datos_general( "MENUS", "ID='". $buf_sec["RELACION"]. "'", "nombre" ). "/". $buf_sec["NOMBRE"]. "</span>";
		$tipo= $buf_sec["TIPO"];
		}

	switch( $tipo )
		{
		case 'noticia':
			noticia_post( $ctl );
			break;
		case 'noticia_limpia':
			noticia_limpia( $ctl );
			break;
		case 'galeria':
			noticia_galeria( $ctl );
			break;
		case 'descriptiva':
			noticia_descriptiva( $ctl );
			break;
		case 'scriptin':
			noticia_scriptin( $ctl );
			break;
		}

	unset($buf_menu);
	unset($buf_sec);
	unset($cons_menu);
	unset($cons_sec);
	}

//Formulario para la Publicacion en tipo: Noticia/Post
function noticia_post( $ctl )
	{
	if( !strchr( $ctl, "," ) )
		{
		$x= consultar_con( "NOTICIAS", "ID='". $ctl. "'" );
		$buf_x= mysql_fetch_array($x);
		//$msg= $buf_x["MENSAJE"]; //modo inseguro :D, pero es para testear ;)
		$msg= $buf_x["MENSAJE"];
		}

	if( !strchr( $ctl, "," ) )
		echo "<form action=\"index.php?id=noticias&mov=agregar_modificacion&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";
	else
		echo "<form action=\"index.php?id=noticias&mov=agregar_noticia&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";

	echo '<div id="formulario_noticia">
		<ul>
			<li class="mini">Titulo:</li>
			<li class="mini"><input type="text" name="titulo_noticia" class="estilo_01"'. (!strchr( $ctl, "," ) ? ' value="'. $buf_x["TITULO"]. '"':''). '></li>
			<li>Contenido:</li>
			<li>
				<textarea name="mensaje_noticia" class="estilo_02">'. (!strchr( $ctl, "," ) ? $msg:''). '</textarea>
			</li>
			<li>';

	if( !strchr( $ctl, "," ) ) //entonces se esta modificando noticia
		{
		if( $buf_x["IMAGENES_NOMBRE"] ) # si contiene datos, tiene imagen o imagenes
			{
			$tipo= substr($buf_x["IMAGENES_NOMBRE"], -3 ); # extramos extencion 
			if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
				echo '<b>Imagen:</b> '. $buf_x["IMAGENES_NOMBRE"];
			else
				echo '<b>Galeria:</b> '. $buf_x["IMAGENES_NOMBRE"]. '/';
			}
		else		echo '<i>... no hay imagen subida...</i>';

		echo "<input type=\"file\" name=\"imagen_noticia\" class=\"estilo_04\">";
		if( $i<5 )
			echo "<br>";
		}
	else //se esta publicando noticia nueva
		echo "<input type=\"file\" name=\"imagen_noticia\" class=\"estilo_04\">";

	echo '</li>
		<li>
			<input type="submit" class="boton_01" value="'. (!strchr( $ctl, "," ) ? 'Modificar':'Publicar'). ' Noticia">
		</li>

		</div>
	</form>';

	if( !strchr( $ctl, "," ) )
		unset($x, $buf_x, $msg);
	}

//Formulario para la Publicacion en tipo: Noticia Limpia/Hoja completa
function noticia_limpia( $ctl )
	{
	if( !strchr( $ctl, "," ) )
		{
		$x= consultar_con( "NOTICIAS", "ID='". $ctl. "'" );
		$buf_x= mysql_fetch_array($x);
		$msg= $buf_x["MENSAJE"];
		}


	if( !strchr( $ctl, "," ) )
		echo "<form action=\"index.php?id=noticias&mov=agregar_modificacion&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";
	else
		echo "<form action=\"index.php?id=noticias&mov=agregar_noticia&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";

	echo '<div id="formulario_noticia">
		<ul>
			<li class="mini">Titulo:</li>
			<li class="mini"><input type="text" name="titulo_noticia" class="estilo_01"'. (!strchr( $ctl, "," ) ? ' value="'. $buf_x["TITULO"]. '"':''). '></li>
			<li>Contenido:</li>
			<li>
				<textarea name="mensaje_noticia" class="estilo_02">'. (!strchr( $ctl, "," ) ? $msg:''). '</textarea>
			</li>
			<li>';

	if( !strchr( $ctl, "," ) ) //entonces se esta modificando noticia
		{
		if( $buf_x["IMAGENES_NOMBRE"] ) # si contiene datos, tiene imagen o imagenes
			{
			$tipo= substr($buf_x["IMAGENES_NOMBRE"], -3 ); # extramos extencion 
			if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
				echo '<b>Imagen:</b> '. $buf_x["IMAGENES_NOMBRE"];
			else
				echo '<b>Galeria:</b> '. $buf_x["IMAGENES_NOMBRE"]. '/';
			}
		else		echo '<i>... no hay imagen subida...</i>';

		echo "<input type=\"file\" name=\"imagen_noticia\" class=\"estilo_04\">";
		if( $i<5 )
			echo "<br>";
		}
	else //se esta publicando noticia nueva
		echo "<input type=\"file\" name=\"imagen_noticia\" class=\"estilo_04\">";

	echo '</li>
		<li>
			<input type="submit" class="boton_01" value="'. (!strchr( $ctl, "," ) ? 'Modificar':'Publicar'). ' Noticia">
		</li>

		</div>
	</form>';

	if( !strchr( $ctl, "," ) )
		unset($x, $buf_x, $msg);
	}

//Formulario para la Publicacion en tipo: Galeria
function noticia_galeria( $ctl )
	{
	if( !strchr( $ctl, "," ) )
		{
		$x= consultar_con( "NOTICIAS", "ID='". $ctl. "'" );
		$buf_x= mysql_fetch_array($x);
		$msg= $buf_x["MENSAJE"];
		}

	if( !strchr( $ctl, "," ) )
		echo "<form name=\"form\" action=\"index.php?id=noticias&mov=agregar_modificacion&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";
	else
		echo "<form name=\"form\" action=\"index.php?id=noticias&mov=agregar_noticia&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";

	echo '<div id="formulario_noticia">
		<ul>
			<li class="txt">Titulo:</li>
			<li class="mini"><input type="text" name="titulo_noticia" class="estilo_01"'. (!strchr( $ctl, "," ) ? ' value="'. $buf_x["TITULO"]. '"':''). '></li>
			<li>Contenido:</li>
			<li>
				<textarea name="mensaje_noticia" class="estilo_02">'. (!strchr( $ctl, "," ) ? $msg:''). '</textarea>
			</li>
			<li>
				<div class="marco">
					Solo formato: JPG';

	if( !strchr( $ctl, "," ) ) //entonces se esta modificando noticia
		{
		if( $buf_x["IMAGENES_NOMBRE"] && $buf_x["IMAGENES_URL"] ) # si contiene datos, tiene imagen o imagenes
			{
			$tipo= substr($buf_x["IMAGENES_NOMBRE"], -3 ); # extramos extencion 
			if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
				echo '<b>Imagen:</b> '. $buf_x["IMAGENES_NOMBRE"];
			else
				echo '<b>Galeria:</b> '. $buf_x["IMAGENES_NOMBRE"]. '/';
			}
		else if( $buf_x["IMAGENES_URL"] && !$buf_x["IMAGENES_NOMBRE"] )
			echo '<b>Galeria:</b> '. desproteger_cadena($buf_x["IMAGENES_URL"]).$buf_x["ID"].'.zip';
		else		echo '<i>... no hay imagen subidas...</i>';

		echo "<input type=\"file\" name=\"imagen_noticia\" class=\"estilo_04\">";
		}
	else //se esta publicando noticia nueva
		echo "<input type=\"file\" name=\"imagen_noticia\" class=\"estilo_04\">";

	echo '		</div>
		</li>
		<li>
			<div id="boton3" onclick="document.form.submit();">'. (!strchr( $ctl, "," ) ? 'Modificar':'Publicar'). ' Noticia</div>
		</li>

		</div>
	</form>';

	if( !strchr( $ctl, "," ) )
		unset($x, $i, $tmp, $buf_x, $msg);
	}

//Formulario para la Publicacion en tipo: Descriptiva
function noticia_descriptiva( $ctl )
	{
	if( !strchr( $ctl, "," ) )
		{
		$x= consultar_con( "NOTICIAS", "ID='". $ctl. "'" );
		$buf_x= mysql_fetch_array($x);
		$msg= $buf_x["MENSAJE"];
		}

	if( !strchr( $ctl, "," ) )
		echo "<form action=\"index.php?id=noticias&mov=agregar_modificacion&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";
	else
		echo "<form action=\"index.php?id=noticias&mov=agregar_noticia&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";

	echo '<div id="formulario_noticia">
		<ul>
			<li class="mini">Titulo:</li>
			<li class="mini"><input type="text" name="titulo_noticia" class="estilo_01"'. (!strchr( $ctl, "," ) ? ' value="'. $buf_x["TITULO"]. '"':''). '></li>
			<li>Contenido:</li>
			<li>
				<textarea name="mensaje_noticia" class="estilo_02">'. (!strchr( $ctl, "," ) ? $msg:''). '</textarea>
			</li>
			<li><u>Solo se permiten archivos con extenciones:</u> <i>mp3, zip, rar, tar.gz, tar.bz2, doc, odt, txt y html</i></li>
			<li>';

	if( !strchr( $ctl, "," ) ) //entonces se esta modificando noticia
		{
		//$tmp= explode( ":", $buf_x["ARCHIVOS_NOMBRE"] ); //uso FUTURO

		if( strcmp($buf_x["ARCHIVOS_NOMBRE"], "") && strcmp($buf_x["ARCHIVOS_NOMBRE"], "0") )
			echo "<b>Archivo Subido:</b> <i>". $buf_x["ARCHIVOS_NOMBRE"]. "</i>";
		else
			echo "<input type=\"file\" name=\"archivo_noticia_01\" class=\"estilo_04\">";
		}
		
	else
		{
		echo '<input type="checkbox" onclick="capa_verno(\'upload_file\'); capa_verno(\'upload_manual\')" value="1" name="manual_file">Subir manualmente el archivo.
		<br>';
		echo "<div id=\"upload_file\"><input type=\"file\" name=\"archivo_noticia_01\" class=\"estilo_04\"></div>";
		echo '<div id="upload_manual" style="visibility:hidden;display:none;">
				<input type="text" name="manual_name" value="nombre del archivo exacto" onclick="this.value=\'\'"> <select name="manual_extencion">
					<option name=".mp3">.mp3</option>
					<option name=".zip">.zip</option>
					<option name=".rar">.rar</option>
					<option name=".tar.gz">.tar,gz</option>
					<option name=".tar.bz2">.tar.bz2</option>
					<option name=".doc">.doc</option>
					<option name=".odt">.odt</option>
					<option name=".txt">.txt</option>
					<option name=".html">.html</option>
				</select>
				<br><small><u>NOTA: primero sube el archivo a la carpeta <b>uploads/noticias/</b> y escribe como nombre, el mismo nombre del archivo subdido</u></small>
		</div>';
		}

	echo '</li>
		<li>
			<input type="submit" class="boton_01" value="'. (!strchr( $ctl, "," ) ? 'Modificar':'Publicar'). ' Noticia">
		</li>

		</div>
	</form>';

	if( !strchr( $ctl, "," ) )
		unset($x, $buf_x, $msg);
	}

//Formulario para la Publicacion en tipo: Scriptin
function noticia_scriptin( $ctl )
	{
	if( !strchr( $ctl, "," ) )
		{
		$x= consultar_con( "NOTICIAS", "ID='". $ctl. "'" );
		$buf_x= mysql_fetch_array($x);
		$file_desc= fopen( "../".$buf_x["ARCHIVOS_URL"].$buf_x["ARCHIVOS_NOMBRE"], "r" );
		$msg= fread( $file_desc, filesize("../".$buf_x["ARCHIVOS_URL"].$buf_x["ARCHIVOS_NOMBRE"]) );
		fclose($file_desc);
		}

	if( !strchr( $ctl, "," ) )
		echo "<form action=\"index.php?id=noticias&mov=agregar_modificacion&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";
	else
		echo "<form action=\"index.php?id=noticias&mov=agregar_noticia&id_src=". $ctl. "\" method=\"POST\" enctype=\"multipart/form-data\">";

	echo '<div id="formulario_noticia">
		<ul>
			<li class="mini">Titulo:</li>
			<li class="mini"><input type="text" name="titulo_noticia" class="estilo_01"'. (!strchr( $ctl, "," ) ? ' value="'. $buf_x["TITULO"]. '"':''). '></li>';

	if( !strchr( $ctl, "," ) )
		{
		echo '<li>Contenido:</li>
			<li>
				<textarea name="mensaje_noticia" class="estilo_02">'. (!strchr( $ctl, "," ) ? $msg:''). '</textarea>
			</li>
			<li>';
		//$tmp= explode( ":", $buf_x["ARCHIVOS_NOMBRE"] ); //uso FUTURO

		if( strcmp($buf_x["ARCHIVOS_NOMBRE"], "") && strcmp($buf_x["ARCHIVOS_NOMBRE"], "0") )
			echo "<b>Archivo Subido:</b> <i>". $buf_x["ARCHIVOS_NOMBRE"]. "</i>";
		else
			echo "<input type=\"file\" name=\"archivo_noticia_01\" class=\"estilo_04\">";
		echo '</li>';
		}
	else
		echo "<li><input type=\"file\" name=\"archivo_noticia_01\" class=\"estilo_04\"></li>";


	echo '
		<li>
			<input type="submit" class="boton_01" value="'. (!strchr( $ctl, "," ) ? 'Modificar':'Publicar'). ' Script">
		</li>

		</div>
	</form>';


	if( !strchr( $ctl, "," ) )
		unset($x, $buf_x, $msg);
	}

//Funcion que confirma la coherencia de los datos a consultar
function comprobar_existencia( $valor )
	{
	$x= explode( ",", $valor );
	if( !consultar_datos_general( "SECCIONES", "ID='". proteger_cadena($x[1]). "':RELACION='". proteger_cadena($x[0]). "'", "ID" ) )
		return 0;

	return 1;
	}

function comprobar_existencia_mod( $valor )
	{
	if( !consultar_datos_general("NOTICIAS", "ID='". proteger_cadena($valor). "'", "ID") )
		return 0;

	return 1;
	}

function comprobar_variables_mod( $valor )
	{
	if( !comprobar_existencia_mod( $valor ) )
		return 0;

	$sec= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($valor). "'", "SECCION");
	$rel= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($valor). "'", "MENU");
	$b= consultar_con( "SECCIONES", "ID='". $sec. "':RELACION='". $rel. "'" );
	$buf= mysql_fetch_array($b);
	unset($sec, $rel);

	if( !mysql_num_rows($b) ) 	return 0;
	else if( !strcmp($buf["TIPO"], "noticia") ) 	return 1;
	else if( !strcmp($buf["TIPO"], "noticia_limpia") ) 	return 1;
	else if( !strcmp($buf["TIPO"], "galeria") ) 	return 1;
	else if( !strcmp($buf["TIPO"], "descriptiva") ) 	return 1;
	else if( !strcmp($buf["TIPO"], "scripting") ) 	return 1;

	return 0;
	}

function imprimir_tipo_extencion( $extencion, $nombre )
	{
	/*
	image/jpeg	Imagen JPEG, JPG
	image/png	Imagen PNG
	image/gif	Imagen GIF
	*/

	if( strpos( $extencion, "jpg" ) )
		return "jpg";
	else if( strpos( $extencion, "jpeg" ) )
		return "jpeg";
	else if( strpos( $extencion, "png" ) )
		return "png";
	else if( strpos( $extencion, "gif" ) )
		return "gif";
	else if( strpos( $extencion, "zip" ) )
		return "zip";
	else if( strpos( $extencion, "gzip" ) && strpos( $nombre, "tar.gz" ) )
		return "tar.gz";
	else if( strpos( $extencion, "octet-stream" ) && strpos( $nombre, "tar.bz2" ) )
		return "tar.bz2";
	else if( strpos( $nombre, "tar.bz2" ) )
		return "tar.bz2";
	else if( strpos( $nombre, "tar.gz" ) )
		return "tar.gz";
	else if( strpos( $nombre, "zip" ) )
		return "zip";
	else if( strpos( $nombre, "doc" ) )
		return "doc";
	else if( strpos( $extencion, "octet-stream" ) && strpos( $nombre, "rar" ) )
		return "rar";
	else if( strpos( $extencion, "octet-stream" ) && strpos( $nombre, "doc" ) )
		return "doc";
	else if( strpos( $extencion, "octet-stream" ) && strpos( $nombre, "odt" ) )
		return "odt";
	else if( strpos( $extencion, "opendocument" ) && strpos( $nombre, "odt" ) )
		return "odt";
	else if( strpos( $extencion, "pdf" ) )
		return "pdf";
	else if( strpos( $extencion, "plain" ) )
		return "txt";
	else if( strpos( $extencion, "html" ) )
		return "html";
	else if( strpos( $extencion, "flash" ) )
		return "swf";
	else if( strpos( $extencion, "mp3" ) )
		return "mp3";
	else
		return "desconocido";
	}

//elimina archivos adjuntos de la noticia antes de eliminarla de la B.D
function eliminar_archivos( $base_tabla, $id )
	{
	$cons= consultar_con( $base_tabla, "ID='". $id. "'" );
	$datos= mysql_fetch_array($cons);

	if( strcmp( $datos["ARCHIVOS_URL"], "" ) ) //entonces eliminamos archivos
		{
		$tmp= explode( ":", $datos["ARCHIVOS_NOMBRE"] );

		for( $i=0; $i<sizeof($tmp); $i++ )
			{
			if( strcmp( $tmp[$i], "" ) )
				if( !unlink( "../".$datos["ARCHIVOS_URL"].$tmp[$i] ) )
				//if( !unlink( "../".$tmp[$i] ) )
					return 0;
			}
		}

	unset($i);
	unset($tmp);
	unset($cons);
	unset($datos);
	return 1;
	}

//elimina imagenes adjuntas de la noticia antes de eliminarla de la B.D
function eliminar_imagenes( $base_tabla, $id )
	{
	$cons= consultar_con( $base_tabla, "ID='". $id. "'" );
	$datos= mysql_fetch_array($cons);

	if( strcmp( $datos["IMAGENES_URL"], "" ) ) //entonces eliminamos archivos
		{
		$tmp= explode( ":", $datos["IMAGENES_NOMBRE"] );

		for( $i=0; $i<sizeof($tmp); $i++ )
			{
			if( strcmp( $tmp[$i], "" ) )
				{
				unlink( "../".$datos["IMAGENES_URL"].$tmp[$i] );
				#unlink( "../".$datos["IMAGENES_URL"].substr($tmp[$i], 0, -4).".webp" );
				}
			}
		}

	unset($i, $tmp, $cons, $datos);
	return 1;
	}

function noticias()
	{
	echo "<div id=\"tabla_noticias_01\">";
	switch( $_GET["mov"] ) //$_GET["mov"]
		{
		case 'agregar_noticia':
			//enviando correo informatico a los usuarios registrados
			$cons_users= consultar( "USUARIOS", "*" ); //consultamos usuarios
			if( mysql_num_rows($cons_users)>0 ) //si existen usuarios
				{
				while( $buf_users= mysql_fetch_array($cons_users) )
					{
					if( strcmp($buf_users["EMAIL"], "") && !strcmp( $buf_users["EMAIL"], "0") )
						enviar_correo( $buf_users["EMAIL"], consultar_datos_base( "MENSAJES_NOTIFICACION", 3, "mensaje" ), 3, $link, 0, 0, 0, 0, 0 );
					}
				}
			unset($cons_users);
			
			echo "<form action=\"index.php?id=noticias\" method=\"POST\">";

			//comprueba que exista la variable, comprueba que exista la ',', comprueba que sea coherente la peticion
			if( !strstr( $_GET["id_src"], "," ) )
				echo "<td>No se indico los IDs para Agregar.</td><tr>";
			else if( !comprobar_existencia($_GET["id_src"]) )
				echo "<td>El menu donde intenta Agregar no existe.</td><tr>";
			else if( !comprobar_variables( "MENUS", "SECCIONES", $_GET["id_src"] ) )
				echo "<td>La seccion donde intenta Agregar no existe.</td><tr>";
			else 
				{
				$x= explode( ",", $_GET["id_src"] ); //explotamos la variable
				$cons_sec= consultar_con( "SECCIONES", "ID='". $x[1]. "'" );
				$buf_sec= mysql_fetch_array($cons_sec);

				echo "<td>";
				switch( $buf_sec["TIPO"] )
					{
					case 'noticia':
						$msg= proteger_cadena($_POST["mensaje_noticia"]);
						
						do //generamos numero aleatorio de 4 a 10 digitos
							{
							$idtrack= generar_idtrack(); //obtenemos digito aleatorio
							}while( !strcmp( $idtrack, consultar_datos_general( "NOTICIAS", "ID='". $idtrack. "'", "ID" ) ) );

						//comprueba que la extencion sea valida
						if( $_FILES["imagen_noticia"]["tmp_name"] )
							{
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]);
							$path .= ( file_exists($path.'/imagenes2/') ? '/imagenes2/':'/imagenes/' ); # direccion de la carpeta

							if( $_FILES["imagen_noticia"]["name"] ) # si hay datos 
								{
								if( strcmp($trama, "") ) $trama .= ":";
								$newname= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["imagen_noticia"]["name"], 0, -4)).substr($_FILES["imagen_noticia"]["name"], -4)); # nombre con identificador 

								if( !move_uploaded_file( $_FILES["imagen_noticia"]["tmp_name"], '../'.$path.$newname ) )
									echo "Problema para subir imagenes/archivos al servidor.";
								else
									{
									if( $trama )	$trama .= ',';
									
									$tipo= substr( url_cleaner($_FILES["imagen_noticia"]["name"]), -3 ); # extraemos extencion
									#system("/usr/bin/cwebp -q 80 -m 6 ../".$path.$newname. " -o ../".$path.substr($newname, 0, -4).".webp"); # jpeg to webp

									if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
										{
										$trama .= $newname; # guardamos nombre de imagen
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$newname, $val, $key ); # redimencionamos todas las imagenes
										}
									else if( !strcmp($tipo, "zip") ) # si es comprimido zip
										{
										$zip= new ZipArchive; # abrimos clase
										if( !$zip->open('../'.$path.$newname) ) # abrimos ZIP 
											echo '<br>Archivo '. $newname. ' no se pudo extraer.';
										else if( !$zip->extractTo('../'.$path.$idtrack.'/') ) # descomprimimos
											echo '<br>Error: al extraer archivo '. $newname. '.';
										$zip->close(); # cerramos 
										
										renombrar_archivos('../'.$path.$idtrack.'/'); # renombra un archivo o todos los que estan en el directorio 
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$idtrack.'/', $val, $key ); # redimencionamos todas las imagenes
										rename('../'.$path.$newname, '../'.$path.$idtrack.'.zip' ); # renombramos zip al ID de su carpeta
										}
									unset($tipo);
									}
								}
							}

						//generamos array de insercion mysql
						$valores= array(
						"id"=>"'". $idtrack. "'", 
						"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
						"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'", //$_GET["titulo_noticia"]
						"mensaje"=>"'". $msg. "'", //$_GET["mensaje_noticia"]
						"imagenes_url"=>"'". $path. "'",
						"imagenes_nombre"=>"'". $trama. "'",
						"fecha"=>"'". time(). "'",
						"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
						"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
						"notify_mail"=>"'0'", 
						"notify_sms"=>"'0'" );

						unset($msg, $idtrack);
				
						if( insertar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						break;
					case 'noticia_limpia':
						$msg= proteger_cadena($_POST["mensaje_noticia"]);

						do //generamos numero aleatorio de 4 a 10 digitos
							{
							$idtrack= generar_idtrack(); //obtenemos digito aleatorio
							}while( !strcmp( $idtrack, consultar_datos_general( "NOTICIAS", "ID='". $idtrack. "'", "ID" ) ) );

						//comprueba que la extencion sea valida
						if( $_FILES["imagen_noticia"]["tmp_name"] )
							{
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]);
							$path .= ( file_exists($path.'/imagenes2/') ? '/imagenes2/':'/imagenes/' ); # direccion de la carpeta

							if( $_FILES["imagen_noticia"]["name"] ) # si hay datos 
								{
								if( strcmp($trama, "") ) $trama .= ":";
								$newname= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["imagen_noticia"]["name"], 0, -4)).substr($_FILES["imagen_noticia"]["name"], -4)); # nombre con identificador 

								if( !move_uploaded_file( $_FILES["imagen_noticia"]["tmp_name"], '../'.$path.$newname ) )
									echo "Problema para subir imagenes/archivos al servidor.";
								else
									{
									if( $trama )	$trama .= ',';
									
									$tipo= substr( url_cleaner($_FILES["imagen_noticia"]["name"]), -3 ); # extraemos extencion
									#system("/usr/bin/cwebp -q 80 -m 6 ../".$path.$newname. " -o ../".$path.substr($newname, 0, -4).".webp"); # jpeg to webp

									if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
										{
										$trama .= $newname; # guardamos nombre de imagen
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$newname, $val, $key ); # redimencionamos todas las imagenes
										}
									else if( !strcmp($tipo, "zip") ) # si es comprimido zip
										{
										$zip= new ZipArchive; # abrimos clase
										if( !$zip->open('../'.$path.$newname) ) # abrimos ZIP 
											echo '<br>Archivo '. $newname. ' no se pudo extraer.';
										else if( !$zip->extractTo('../'.$path.$idtrack.'/') ) # descomprimimos
											echo '<br>Error: al extraer archivo '. $newname. '.';
										$zip->close(); # cerramos 
										
										renombrar_archivos('../'.$path.$idtrack.'/'); # renombra un archivo o todos los que estan en el directorio
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$idtrack.'/', $val, $key ); # redimencionamos todas las imagenes 
										rename('../'.$path.$newname, '../'.$path.$idtrack.'.zip' ); # renombramos zip al ID de su carpeta
										}
									unset($tipo);
									}
								}
							}

						$valores= array(
						"id"=>"'". $idtrack. "'", 
						"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
						"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
						"fecha"=>"'". time(). "'",
						"mensaje"=>"'". $msg. "'",
						"imagenes_url"=>"'". $path. "'",
						"imagenes_nombre"=>"'". $trama. "'",
						"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
						"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
						"notify_mail"=>"'0'", 
						"notify_sms"=>"'0'" );

						unset($msg, $idtrack);

						if( insertar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						break;
					case 'galeria':
						/*
						image/jpeg	Imagen JPEG, JPG
						image/png	Imagen PNG
						image/gif	Imagen GIF
						*/

						do //generamos numero aleatorio de 4 a 10 digitos
							{
							$idtrack= generar_idtrack(); //obtenemos digito aleatorio
							}while( !strcmp( $idtrack, consultar_datos_general( "NOTICIAS", "ID='". $idtrack. "'", "ID" ) ) );
						$trama= ''; //trama de imagenes delimitada por ":"
						$path= ''; # path donde estaran los uploads 

						//comprueba que la extencion sea valida
						if( $_FILES["imagen_noticia"]["tmp_name"] )
							{
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]);
							$path .= ( file_exists($path.'/imagenes2/') ? '/imagenes2/':'/imagenes/' ); # direccion de la carpeta

							if( $_FILES["imagen_noticia"]["name"] ) # si hay datos 
								{
								if( strcmp($trama, "") ) $trama .= ":";
								$newname= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["imagen_noticia"]["name"], 0, -4)).substr($_FILES["imagen_noticia"]["name"], -4)); # nombre con identificador 

								if( !move_uploaded_file( $_FILES["imagen_noticia"]["tmp_name"], '../'.$path.$newname ) )
									echo "Problema para subir imagenes/archivos al servidor.<br>[". $_FILES["imagen_noticia"]["tmp_name"]. "]<br>[". $_FILES["imagen_noticia"]["name"]. "]<br>[". $path.$newname. "]<br>";
								else
									{
									if( $trama )	$trama .= ',';
									
									$tipo= substr( url_cleaner($_FILES["imagen_noticia"]["name"]), -3 ); # extraemos extencion
									#system("/usr/bin/cwebp -q 80 -m 6 ../".$path.$newname. " -o ../".$path.substr($newname, 0, -4).".webp"); # jpeg to webp

									if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
										{
										$trama .= $newname; # guardamos nombre de imagen
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$newname, $val, $key ); # redimencionamos todas las imagenes
										}
									else if( !strcmp($tipo, "zip") ) # si es comprimido zip
										{
										# creamos directorio para guardar archivo(s) de la noticia 
										if( !mkdir('../'.$path.$idtrack, 0755) ) # creamos directorio 
											echo '<br>Error: no se pudo crear directorio para descomprimir archivo '. $_FILES["imagen_noticia"]["name"]. '.';

										$zip= new ZipArchive; # abrimos clase
										if( !$zip->open('../'.$path.$newname) ) # abrimos ZIP 
											echo '<br>Archivo '. $newname. ' no se pudo extraer.';
										else if( !$zip->extractTo('../'.$path.$idtrack.'/') ) # descomprimimos
											echo '<br>Error: al extraer archivo '. $newname. '.';
										$zip->close(); # cerramos 
										
										renombrar_archivos('../'.$path.$idtrack.'/'); # renombra un archivo o todos los que estan en el directorio
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$idtrack.'/', $val, $key ); # redimencionamos todas las imagenes
										rename('../'.$path.$newname, '../'.$path.$idtrack.'.zip' ); # renombramos zip al ID de su carpeta
										unset($tiposarr);
										}
									unset($tipo);
									}
								}
							}

						$valores= array(
								"id"=>"'". $idtrack. "'", 
								"autor"=>"'". proteger_cadena($_SESSION["log_id"]). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". proteger_cadena($_POST["mensaje_noticia"]). "'",
								"imagenes_url"=>"'". $path. "'",
								"imagenes_nombre"=>"'". $trama. "'",
								"fecha"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
								"notify_mail"=>"'0'", 
								"notify_sms"=>"'0'" );

						if( insertar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						
						unset($idtrack, $path, $msg);
						break;
					case 'descriptiva':
						//$_FILES["var"]["name"]	nombre original
						//$_FILES["var"]["tmp_name"]	nombre temporal que le asigna PHP
						//$_FILES["var"]["size"]	//tamano del archivo
						//$_FILES["var"]["type"]	//tipo de archivo

						/*
						application/zip			Archivo ZIP
						application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
						application/gzip		Archivo TAR.GZ
						application/pdf			Archivo PDF
						text/plain			Archivo TXT
						text/html			Archivo HTML
						application/x-shockwave-flash	Archivo SWF
						*/

						do //generamos numero aleatorio de 4 a 10 digitos
							{
							$idtrack= generar_idtrack(); //obtenemos digito aleatorio
							}while( !strcmp( $idtrack, consultar_datos_general( "NOTICIAS", "ID='". $idtrack. "'", "ID" ) ) );

						//comprueba que la extencion sea valida
						if( comprobar_extencion_archivo( $_FILES ) || $_POST["manual_file"]==1 )
							{
							$path= "uploads/noticias/"; //direccion de la carpeta
							$trama=""; //trama de archivos delimitada por ":"
							$j=0;
							
							if( !strcmp($_POST["manual_file"], "1") ) # si UPLOAD manual esta activado
								{
								$dt= "_". date( "dmy", time() ). "_". $idtrack;
								$trama .= proteger_cadena($_POST["manual_name"]).$dt.proteger_cadena($_POST["manual_extencion"]);
								
								//if( !move_uploaded_file( $path.proteger_cadena($_POST["manual_name"]).proteger_cadena($_POST["manual_extencion"]), $trama ) )
								if( !rename("../".$path.proteger_cadena($_POST["manual_name"]).proteger_cadena($_POST["manual_extencion"]), "../".$path.$trama ) )
									echo 'Problemas para modificar/encontrar archivo subido manualmente.<br>';
								else
									{
									$j++;
									symlink( $trama, "../".$path.$idtrack.".mp3" ); // creamos enlace simbolico
									}
								unset($dt);
								}
							else //desactivado UPLOAD manual, se usa el sistema web
								{
								for( $i=1; $i<=count($_FILES); $i++ )
									{
									if( strcmp($_FILES["archivo_noticia_0$i"]["name"], "") && !evitar_repeticion( "archivos", $_FILES["archivo_noticia_0$i"]["name"], "NOTICIAS" ) )
										{
										$dt= "_". date( "dmy", time() ). "_". $idtrack;

										if( strcmp($trama, "") ) $trama .= ":";

										if( !move_uploaded_file( $_FILES["archivo_noticia_0$i"]["tmp_name"], "../".$path.$dt.$_FILES["archivo_noticia_0$i"]["name"] ) )
											echo "Problema para subir archivos al servidor.<br>";
										else
											{
											$j++;
											$trama .= $dt.$_FILES["archivo_noticia_0$i"]["name"];
											}
										unset($dt);
										}
									}
								}
								
							if( $j>0 ) //noticia subida y archivos
								{
								$msg= proteger_cadena($_POST["mensaje_noticia"]); //proteger cadena

								$valores= array(
								"id"=>"'". $idtrack. "'", 
								"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". $msg. "'",
								"archivos_url"=>"'". $path. "'",
								"archivos_nombre"=>"'". $trama. "'",
								"fecha"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
								"notify_mail"=>"'0'", 
								"notify_sms"=>"'0'" );

								unset($path, $num, $ext, $j, $msg);
								}
							else //noticia subida pero archivo NO
								{
								$msg= proteger_cadena($_POST["mensaje_noticia"]); //proteger cadena
								
								$valores= array(
								"id"=>"'". $idtrack. "'", 
								"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". $msg. "'",
								"fecha"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
								"notify_mail"=>"'0'", 
								"notify_sms"=>"'0'" );

								unset($path, $num, $ext, $j, $msg);
								}
							}
						else
							{
							$msg= proteger_cadena($_POST["mensaje_noticia"]); //proteger cadena
								
							$valores= array(
							"id"=>"'". $idtrack. "'", 
							"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
							"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
							"mensaje"=>"'". $msg. "'",
							"fecha"=>"'". time(). "'",
							"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
							"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
							"notify_mail"=>"'0'", 
							"notify_sms"=>"'0'" );

							unset($path, $num, $ext, $j, $msg);
							}

						if( insertar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						
						unset($idtrack);
						break;
					case 'scriptin':
						//$_FILES["var"]["name"]	nombre original
						//$_FILES["var"]["tmp_name"]	nombre temporal que le asigna PHP
						//$_FILES["var"]["size"]	//tamano del archivo
						//$_FILES["var"]["type"]	//tipo de archivo

						/*
						application/zip			Archivo ZIP
						application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
						application/gzip		Archivo TAR.GZ
						application/pdf			Archivo PDF
						text/plain			Archivo TXT
						text/html			Archivo HTML
						application/x-shockwave-flash	Archivo SWF
						*/

						do //generamos numero aleatorio de 4 a 10 digitos
							{
							$idtrack= generar_idtrack(); //obtenemos digito aleatorio
							}while( !strcmp( $idtrack, consultar_datos_general( "NOTICIAS", "ID='". $idtrack. "'", "ID" ) ) );


						//comprueba que la extencion sea valida
						if( comprobar_extencion_archivo_scriptin( $_FILES ) )
							{
							$path= "uploads/noticias/"; //direccion de la carpeta
							$trama=""; //trama de archivos delimitada por ":"
							$j=0;

							for( $i=1; $i<=count($_FILES); $i++ )
								{
								if( strcmp($_FILES["archivo_noticia_0$i"]["name"], "") && !evitar_repeticion( "archivos", $_FILES["archivo_noticia_0$i"]["name"], "NOTICIAS" ) )
									{
									if( strcmp($trama, "") ) $trama .= ":";

									if( !move_uploaded_file( $_FILES["archivo_noticia_0$i"]["tmp_name"], "../".$path.$_FILES["archivo_noticia_0$i"]["name"] ) )
										echo 'Problema para subir archivos al servidor ['. $_FILES["archivo_noticia_0$i"]["tmp_name"]. '].<br>';
									else
										{
										$j++;
										$trama .= $idtrack.substr($_FILES["archivo_noticia_0$i"]["name"], -4);
										rename("../".$path.$_FILES["archivo_noticia_0$i"]["name"], "../".$path.$trama );
										}
									}
								}
							if( $j>0 ) //noticia subida y archivos
								{
								$msg= proteger_cadena($_POST["mensaje_noticia"]); //proteger cadena

								$valores= array(
								"id"=>"'". $idtrack. "'", 
								"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". $msg. "'",
								"archivos_url"=>"'". $path. "'",
								"archivos_nombre"=>"'". $trama. "'",
								"fecha"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

								unset($path, $num, $ext, $j, $msg);
								}
							else //noticia subida pero archivo NO
								{
								$msg= proteger_cadena($_POST["mensaje_noticia"]); //proteger cadena
								
								$valores= array(
								"id"=>"'". $idtrack. "'", 
								"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". $msg. "'",
								"fecha"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

								unset($path, $num, $ext, $j, $msg);
								}
							}
						else
							{
							$msg= proteger_cadena($_POST["mensaje_noticia"]); //proteger cadena
								
							$valores= array(
							"id"=>"'". $idtrack. "'", 
							"autor"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
							"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
							"mensaje"=>"'". $msg. "'",
							"fecha"=>"'". time(). "'",
							"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
							"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'", 
							"notify_mail"=>"'0'", 
							"notify_sms"=>"'0'" );

							unset($path, $num, $ext, $j, $msg);
							}

						if( insertar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						
						unset($idtrack);
						break;
					}
				echo "</td><tr>";

				unset($buf_sec, $cons_sec);
				}

			echo "<td><input type=\"submit\" value=\"Siguiente\" class=\"boton_01\"></td>";
			echo "</form>";
			break;
		case 'modificar_noticia':
			$vals= "id=";
			$vals .= "'". proteger_cadena($_GET["id_not"]). "'";
			
			$cons= consultar_con( "NOTICIAS", $vals );
			
			if( mysql_num_rows($cons) == 0 )
				{
				echo "<form action=\"index.php?id=noticias\" method=\"POST\">";
				echo "<td>La noticia que intentas modificar no existe</td><tr>";
				echo "<td><input type=\"submit\" value=\"Siguiente\" class=\"boton_01\"></td>";
				echo "</form>";
				}
			else if( $cons=="ERROR" )
				{
				echo "<form action=\"index.php?id=noticias\" method=\"POST\">";
				echo "<td>Error al Realizar Consulta.</td>";
				echo "<td><input type=\"submit\" value=\"Siguiente\" class=\"boton_01\"></td>";
				echo "</form>";
				}
			else
				{
				echo "<td style=\"padding-bottom:10px;\">";
				noticias_publicar( $_GET["id_not"] );
				}
			break;
		case 'agregar_modificacion':
			echo "<form action=\"index.php?id=noticias\" method=\"POST\">";

			//comprueba que exista la variable, comprueba que exista la ',', comprueba que sea coherente la peticion
			if( strstr( $_GET["id_src"], "," ) )
				echo '<td>La peticion esta mal formada al agregar modificacion...</td>';
			else if( !comprobar_existencia_mod($_GET["id_src"]) )
				echo '<td>No existen referencias para la seccion al agregar modificacion...</td>';
			else if( !comprobar_variables_mod( $_GET["id_src"] ) )
				echo '<td>Falta rellenar campos al agregar modificacion...</td><tr>';
			else
				{
				$xcons= consultar_con( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'" ); //explotamos la variable
				$x= mysql_fetch_array($xcons);
				$cons_sec= consultar_con( "SECCIONES", "ID='". $x["SECCION"]. "'" );
				$buf_sec= mysql_fetch_array($cons_sec);

				echo "<td>";
				switch( $buf_sec["TIPO"] )
					{
					case 'noticia':
						if( strstr($x["IMAGENES_URL"], "/imagenes2/") )
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]). '/imagenes2/'; # direccion de la carpeta
						else
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]). '/imagenes/'; # direccion de la carpeta
						
						if( !$_FILES["imagen_noticia"]["tmp_name"] ) # si no existen archivos a subir
							$trama= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" ); # obtenemos archivos actuales (ya subidos) 
						else # si existen archivos a subir
							{
							if( $_FILES["imagen_noticia"]["name"] ) # si hay datos 
								{
								$idtrack= proteger_cadena($_GET["id_src"]);
								# if( !$trama ) $trama .= ":";
								$newname= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["imagen_noticia"]["name"], 0, -4)).substr($_FILES["imagen_noticia"]["name"], -4)); # nombre con identificador 

								if( !move_uploaded_file( $_FILES["imagen_noticia"]["tmp_name"], '../'.$path.$newname ) )
									echo "Problema para subir imagenes/archivos al servidor.";
								else
									{
									# eliminar contenido antiguo
									# es imagen unica
									if( consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" ) && consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_URL" ) )
										{
										$n= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" );
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val )
											{
											unlink( '../'.$path.substr($n, 0, -4).'_'.$key.substr($n, -4) ); # eliminamos imagen
											#unlink( '../'.$path.substr($n, 0, -4).'_'.$key.".webp" ); # eliminamos imagen
											}
										unlink('../'.$path.$n); # eliminamos original
										unset($tiposarr, $key, $val, $n);
										}
									# es galeria
									else
										{
										unlink('../'.$path.proteger_cadena($_GET["id_src"]).'.zip' ); # eliminamos comprimido 
										eliminar_recursivamente( '../'.$path.proteger_cadena($_GET["id_src"]). '../' ); # eliminamos contenidos
										rmdir( '../'.$path.proteger_cadena($_GET["id_src"]). '../' ); # eliminamos carpeta 
										}
									
									# if( $trama )	$trama .= ',';
									
									#system("/usr/bin/cwebp -q 80 -m 6 ../".$path.$newname. " -o ../".$path.substr($newname, 0, -4).".webp"); # jpeg to webp
									$tipo= substr( url_cleaner($_FILES["imagen_noticia"]["name"]), -3 ); # extraemos extencion
									if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
										{
										$trama= $newname; # guardamos nombre de imagen
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$newname, $val, $key ); # redimencionamos todas las imagenes
										}
									else if( !strcmp($tipo, "zip") ) # si es comprimido zip
										{
										$zip= new ZipArchive; # abrimos clase
										if( !$zip->open('../'.$path.$newname) ) # abrimos ZIP 
											echo '<br>Archivo '. $newname. ' no se pudo extraer.';
										else if( !$zip->extractTo('../'.$path.$idtrack.'/') ) # descomprimimos
											echo '<br>Error: al extraer archivo '. $newname. '.';
										$zip->close(); # cerramos 
										
										renombrar_archivos('../'.$path.$idtrack.'/'); # renombra un archivo o todos los que estan en el directorio 
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$idtrack.'/', $val, $key ); # redimencionamos todas las imagenes
										rename('../'.$path.$newname, '../'.$path.$idtrack.'.zip' ); # renombramos zip al ID de su carpeta
										}
									unset($tipo);
									}
								}
							}

						$msg= proteger_cadena($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML

						//generamos array de insercion mysql
						$valores= array(
						"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
						"autor_mod"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
						"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
						"mensaje"=>"'". $msg. "'", //$_GET["mensaje_noticia"]
						"imagenes_url"=>"'". $path. "'",
						"imagenes_nombre"=>"'". $trama. "'",
						"fecha_mod"=>"'". time(). "'",
						"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
						"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

						unset($msg);
				
						if( actualizar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						break;
					case 'noticia_limpia':
						if( strstr($x["IMAGENES_URL"], "/imagenes2/") )
							$path= 'usuarios/'. proteger_cadena(consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "AUTOR" )). '/imagenes2/'; # direccion de la carpeta
						else
							$path= 'usuarios/'. proteger_cadena(consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "AUTOR" )). '/imagenes/'; # direccion de la carpeta
						
						if( !$_FILES["imagen_noticia"]["tmp_name"] ) # si no existen archivos a subir
							$trama= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" ); # obtenemos archivos actuales (ya subidos) 
						else # si existen archivos a subir
							{
							if( $_FILES["imagen_noticia"]["name"] ) # si hay datos 
								{
								$idtrack= proteger_cadena($_GET["id_src"]);
								if( strcmp($trama, "") ) $trama .= ":";
								$newname= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["imagen_noticia"]["name"], 0, -4)).substr($_FILES["imagen_noticia"]["name"], -4)); # nombre con identificador 

								if( !move_uploaded_file( $_FILES["imagen_noticia"]["tmp_name"], '../'.$path.$newname ) )
									echo "Problema para subir imagenes/archivos al servidor.";
								else
									{
									if( $trama )	$trama .= ',';
									
									$tipo= substr( url_cleaner($_FILES["imagen_noticia"]["name"]), -3 ); # extraemos extencion
									#system("/usr/bin/cwebp -q 80 -m 6 ../".$path.$newname. " -o ../".$path.substr($newname, 0, -4).".webp"); # jpeg to webp

									if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
										{
										$trama .= $newname; # guardamos nombre de imagen
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$newname, $val, $key ); # redimencionamos todas las imagenes
										}
									else if( !strcmp($tipo, "zip") ) # si es comprimido zip
										{
										$zip= new ZipArchive; # abrimos clase
										if( !$zip->open('../'.$path.$newname) ) # abrimos ZIP 
											echo '<br>Archivo '. $newname. ' no se pudo extraer.';
										else if( !$zip->extractTo('../'.$path.$idtrack.'/') ) # descomprimimos
											echo '<br>Error: al extraer archivo '. $newname. '.';
										$zip->close(); # cerramos 
										
										renombrar_archivos('../'.$path.$idtrack.'/'); # renombra un archivo o todos los que estan en el directorio 
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$idtrack.'/', $val, $key ); # redimencionamos todas las imagenes
										rename('../'.$path.$newname, '../'.$path.$idtrack.'.zip' ); # renombramos zip al ID de su carpeta
										}
									unset($tipo);
									}
								}
							}
							
						$msg= proteger_cadena($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML

						$valores= array(
						"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
						"autor_mod"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
						"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
						"fecha_mod"=>"'". time(). "'",
						"mensaje"=>"'". $msg. "'",
						"imagenes_url"=>"'". $path. "'",
						"imagenes_nombre"=>"'". $trama. "'",
						"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
						"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

						unset($msg);

						if( actualizar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						break;
					case 'galeria':
						if( strstr($x["IMAGENES_URL"], "/imagenes2/") )
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]). '/imagenes2/'; # direccion de la carpeta
						else
							$path= 'usuarios/'. proteger_cadena($_SESSION["log_id"]). '/imagenes/'; # direccion de la carpeta
						
						if( !$_FILES["imagen_noticia"]["tmp_name"] ) # si no existen archivos a subir
							$trama= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" ); # obtenemos archivos actuales (ya subidos) 
						else # si existen archivos a subir
							{
							if( $_FILES["imagen_noticia"]["name"] ) # si hay datos 
								{
								$idtrack= proteger_cadena($_GET["id_src"]);
								if( strcmp($trama, "") ) $trama .= ":";
								$newname= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["imagen_noticia"]["name"], 0, -4)).substr($_FILES["imagen_noticia"]["name"], -4)); # nombre con identificador 

								if( !move_uploaded_file( $_FILES["imagen_noticia"]["tmp_name"], '../'.$path.$newname ) )
									echo "Problema para subir imagenes/archivos al servidor.";
								else
									{
									# eliminar contenido antiguo
									# es una galeria comprimida
									if( consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_URL" ) && !consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" ) )
										{
										unlink($path.proteger_cadena($_GET["id_src"]).'.zip' ); # eliminamos comprimido 
										eliminar_recursivamente( '../'.$path.proteger_cadena($_GET["id_src"]). '/' ); # eliminamos contenidos
										rmdir( '../'.$path.proteger_cadena($_GET["id_src"]). '/' ); # eliminamos carpeta 
										}
									else # es imagen normal
										{
										$tmpabimg= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "IMAGENES_NOMBRE" );
										unlink( $path.$tmpabimg ); # eliminamos imagen
										#unlink( $path. substr($tmpabimg, 0, -4)."webp"); # eliminamos imagen webp
										unset($tmpabimg);
										}

									if( $trama )	$trama .= ',';
									
									$tipo= substr( url_cleaner($_FILES["imagen_noticia"]["name"]), -3 ); # extraemos extencion
									#system("/usr/bin/cwebp -q 80 -m 6 ../".$path.$newname. " -o ../".$path.substr($newname, 0, -4).".webp"); # jpeg to webp

									if( !strcmp($tipo, "jpg") || !strcmp($tipo, "gif") || !strcmp($tipo, "png") ) # si es imagen
										{
										$trama .= $newname; # guardamos nombre de imagen
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$newname.'/', $val, $key ); # redimencionamos todas las imagenes
										}
									else if( !strcmp($tipo, "zip") ) # si es comprimido zip
										{
										$zip= new ZipArchive; # abrimos clase
										if( !$zip->open('../'.$path.$newname) ) # abrimos ZIP 
											echo '<br>Archivo '. $newname. ' no se pudo extraer.';
										else if( !$zip->extractTo('../'.$path.$idtrack.'/') ) # descomprimimos
											echo '<br>Error: al extraer archivo '. $newname. '.';
										$zip->close(); # cerramos 
										
										renombrar_archivos('../'.$path.$idtrack.'/'); # renombra un archivo o todos los que estan en el directorio 
										$tiposarr= array( "medium"=>"600","otro"=>"500", "post"=>"115", "mini"=>"90", "r1"=>"200", "r2"=>"300", "r3"=>"730", "r4"=>"800", "r5"=>"900", "r6"=>"850", "r7"=>"880" );
										foreach( $tiposarr as $key=>$val ) 
											redimencionar_imagen('../'.$path.$idtrack.'/', $val, $key ); # redimencionamos todas las imagenes
										rename('../'.$path.$newname, '../'.$path.$idtrack.'.zip' ); # renombramos zip al ID de su carpeta
										}
									unset($tipo);
									}
								}
							}

							$msg= proteger_cadena($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML

							$valores= array(
									"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
									"autor_mod"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
									"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
									"mensaje"=>"'". $msg. "'",
									"imagenes_url"=>"'". $path. "'",
									"imagenes_nombre"=>"'". $trama. "'",
									"fecha_mod"=>"'". time(). "'",
									"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
									"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

							unset($path);
							unset($num);
							unset($ext);
							unset($j);
							unset($msg);

							if( actualizar_bdd( "NOTICIAS", $valores )==0 )
								echo "Error en la Insercion de los Datos.";
							else
								echo "Datos Agregados con Exito.";
						break;
					case 'descriptiva':
						//$_FILES["var"]["name"]	nombre original
						//$_FILES["var"]["tmp_name"]	nombre temporal que le asigna PHP
						//$_FILES["var"]["size"]	//tamano del archivo
						//$_FILES["var"]["type"]	//tipo de archivo

						/*
						application/zip			Archivo ZIP
						application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
						application/gzip		Archivo TAR.GZ
						application/pdf			Archivo PDF
						text/plain			Archivo TXT
						text/html			Archivo HTML
						application/x-shockwave-flash	Archivo SWF
						*/

						//comprueba que la extencion sea valida
						if( comprobar_extencion_archivo( $_FILES ) )
							{
							$num= contador_datos_tabla( "NOTICIAS", "ARCHIVOS_URL=''" ); //numero/nombre
							$path= "../uploads/noticias/"; //direccion de la carpeta
							$trama=""; //trama de archivos delimitada por ":"
							$j=0;

							$abc= consultar_con( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'" );
							$tmp_abc= mysql_fetch_array($abc);

							$trama .= $tmp_abc["ARCHIVOS_NOMBRE"];

							for( $i=1; $i<=1; $i++ )
								{
								if( strcmp($_FILES["archivo_noticia_0$i"]["name"], "") )
									{
									if( strcmp($trama, "") ) $trama .= ":";

									$ext= ".". imprimir_tipo_extencion($_FILES["archivo_noticia_0$i"]["type"], $_FILES["archivo_noticia_0$i"]["name"]);

									if( !move_uploaded_file( $_FILES["archivo_noticia_0$i"]["tmp_name"], $path.$num.$ext ) )
										echo "Problema para subir imagenes al servidor.<br>";
									else $j++;

									$trama .= $num.$ext;
									$num++;
									}
								}
							if( $j>0 ) //entonces se modificaron/actualizaron archivo tambien
								{
								$msg= proteger_cadena($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML

								$valores= array(
								"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
								"autor_mod"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". $msg. "'",
								"archivos_url"=>"'". $path. "'",
								"archivos_nombre"=>"'". $trama. "'",
								"fecha_mod"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

								unset($path);
								unset($num);
								unset($ext);
								unset($j);
								unset($msg);

								if( actualizar_bdd( "NOTICIAS", $valores )==0 )
									echo "Error en la Insercion de los Datos.";
								else
									echo "Datos Agregados con Exito.";
								}
							else //solo se actualizo la informacion, mas los archivo NO
								{
								$msg= proteger_cadena($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML

								$valores= array(
								"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
								"autor_mod"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
								"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
								"mensaje"=>"'". $msg. "'",
								"fecha_mod"=>"'". time(). "'",
								"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
								"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

								unset($path);
								unset($num);
								unset($ext);
								unset($j);
								unset($msg);

								if( actualizar_bdd( "NOTICIAS", $valores )==0 )
									echo "Error en la Insercion de los Datos.";
								else
									echo "Datos Agregados con Exito.";
								}
							}
						break;
					case 'scriptin':
						//$msg= proteger_cadena($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML
						$path= "../uploads/noticias/"; //direccion de la carpeta
						$file_edit= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["id_src"]). "'", "archivos_nombre" ); //trama de archivos delimitada por ":"
						$msg= desproteger_cadena_src($_POST["mensaje_noticia"]); //eliminamos etiquetas HTML
						
						$src_file= fopen( $path.$file_edit, "w" ); //apertura de archivo
						fwrite( $src_file, stripslashes($msg), strlen($msg) ); //escritura de archivo
						fclose($src_file); //cierre de archivo

						//generamos array de insercion mysql
						$valores= array(
						"id"=>"'". proteger_cadena($_GET["id_src"]). "'",
						"autor_mod"=>"'". consultar_datos_usuario($_SESSION["log_usr"], "ID" ). "'",
						"titulo"=>"'". proteger_cadena($_POST["titulo_noticia"]). "'",
						"fecha_mod"=>"'". time(). "'",
						"menu"=>"'". proteger_cadena($buf_sec["RELACION"]). "'",
						"seccion"=>"'". proteger_cadena($buf_sec["ID"]). "'" );

						unset($msg);
				
						if( actualizar_bdd( "NOTICIAS", $valores )==0 )
							echo "Error en la Insercion de los Datos.";
						else
							echo "Datos Agregados con Exito.";
						break;
					}
				echo "</td><tr>";

				unset($x, $xcons, $buf_sec, $cons_sec);
				}
				

			echo "<td><input type=\"submit\" value=\"Siguiente\" class=\"boton_01\"></td>";
			echo "</form>";
			break;
			
		case 'eliminar_noticia':
			if( !strcmp($_POST["resp"], "Si, Eliminar") )
				{
				$var= "";
				$var .= "id=";
				$var .= "'". proteger_cadena($_GET["id_not"]). "'";
			
				echo "<form action=\"index.php?id=noticias\" method=\"POST\">";
			
				if( eliminar_archivos( "NOTICIAS", proteger_cadena($_GET["id_not"]) ) && eliminar_imagenes( "NOTICIAS", proteger_cadena($_GET["id_not"]) ) )
					{
					if( !eliminar_bdd( "NOTICIAS", $var ) )
						echo "<td>Problemas para eliminar datos.</td><tr>";
					else
						echo "<td>Datos Eliminados con exito..</td><tr>";
					}
				else
					echo "<td>Problemas para eliminar datos adjuntos de la noticia.</td><tr>";
				
				echo "<td><input type=\"submit\" value=\"Siguiente\" class=\"boton_01\"></td>";
				echo "</form>";
				}
			else
				{
				echo "<form action=\"index.php?id=noticias&mov=". $_GET["mov"]. "&id_not=". $_GET["id_not"]. "\" method=\"POST\">";
				echo "<td colspan=\"2\">Seguro que desea eliminar la noticia seleccionada?</td><tr>";
				echo "<td><input type=\"submit\" name=\"resp\" value=\"Si, Eliminar\" class=\"boton_01\"></form></td>";
				echo "<td><form action=\"index.php?id=noticias\" method=\"POST\"> <input type=\"submit\" value=\"Cancelar\" class=\"boton_01\"></form></td>";
				}
			break;
		default:
			echo "<td style=\"padding-bottom:10px;\">";
			if( $_GET["mov"]==7 ) //entonces proviene de la seccion MENU, y desea publicar algo
				{
				if( isset($_GET["control"]) && comprobar_existencia($_GET["control"]) ) //si existe variable y seccion
					noticias_publicar( $_GET["control"] ); //muestra FORMULARIO de noticia solicitada
				else
					echo "<b>Seccion inexistente, elije una secciones existente del menu...</b>";
				}
			else
				echo "<span><b>Solo puedes publicar sobre secciones del menu...</b></span>";
			break;
		}

	if( !$_GET["mov"] )
		{
			//if( !$_GET["pagina"] )
			//	$pagina_actual=1;
			//else
			//	$pagina_actual= $_GET["pagina"];
			$minimo=16;
			$pag= paginacion( $_GET["page"], "NOTICIAS", $minimo );
			
			echo "<b>Lista de Publicaciones.</b> A continuacion se muestran las publicaciones existentes.";
			echo '<br>Existen: ';
			
			if( !strcmp($_SESSION["lod_id"], "4dm1n") ) # si es admin
				$total_not= mysql_num_rows(consultar("NOTICIAS", "*"));
			else # no es admin, es autor o editor 
				 $total_not= mysql_num_rows(consultar_con("NOTICIAS", "AUTOR='". proteger_cadena($_SESSION["log_id"]). "'"));
				 
			echo $total_not;
			
			if( $total_not==1 )
				echo " noticia.";
			else
				echo " noticias.";
			
			//echo "<table cellspacing=\"1\" cellpadding=\"0\" align=\"center\" id=\"tabla_lista_01\">";
			echo '<div id="tabla_lista_01">
				<ul>
					<li class="titulo rosa"><h2>TITULO</h2></li>
					<li class="autor rosa"><h2>AUTOR</h2></li>
					<li class="fecha rosa"><h2>FECHA</h2></li>
					<li class="controles rosa"><h2>CONTROLES</h2></li>';
			
			if( is_admin() )
				$cons= consultar_limite_enorden( "NOTICIAS", (($pag*$minimo)-$minimo). ",". $minimo, "FECHA DESC");
			else
				$cons= consultar_limite_enorden_con( "NOTICIAS", "AUTOR='". proteger_cadena($_SESSION["log_id"]). "'", (($pag*$minimo)-$minimo). ",". $minimo, "FECHA DESC");
			
			if( mysql_num_rows($cons)>0 )
				{
				while( $buf= mysql_fetch_array($cons) )
					{
					echo '
					<li class="titulo">'. noticia_cortada($buf["TITULO"], 100). '</li>
					<li class="autor">'. consultar_datos_general( "USUARIOS", "ID='". $buf["AUTOR"]. "'", "NICK"). '</li>
					<li class="fecha">'. date( "d/m/y", $buf["FECHA"] ). '</li>
					<li class="controles">
						<a href="index.php?id=noticias&mov=eliminar_noticia&id_not='. $buf["ID"]. '">
							<img src="../modulos/noticias-img/cancel.png" title="Eliminar" border="0"></a>
						<a href="index.php?id=noticias&mov=modificar_noticia&id_not='. $buf["ID"]. '">
							<img src="../modulos/noticias-img/kontact_news.png" title="Modificar" border="0"></a>
					</li>';
					}
				}
			else
				echo "<li>... no tienes publicaciones aun...</li>";
		
			echo '</ul>
			</div>';

			if( is_admin() )		selector_paginacion( "NOTICIAS", $minimo, $pag, "?id=". $_GET["id"], "0", "0" );
			else		selector_paginacion_enorden_con( "NOTICIAS", "AUTOR='". proteger_cadena($_SESSION["log_id"]). "'", "FECHA DESC", $minimo, $pag, "?id=". $_GET["id"], "0", "0" );
			
			//paginacion_noticias( contador_celdas( "NOTICIAS" ) );
			//selector de paginacion o roll out
			
			
			//echo "<td><p>Al <b>Modificar</b> el contenido aparecera en el formulario de la izquierda. 
			//Al <b>Eliminar</b> solo desaparecera la noticia de la lista y del archivo de noticias.</td><tr>";
		}
	echo "</div>";
	}

//llamadas para el PANEL DE CONTROL
if( $_GET["id"]=="noticias" ) //variable reservada para modulos
	{
	if( is_admin() || is_coadmin() || is_autor() || is_editor() )
		noticias();
	else		echo 'Area Restringida...';
	}
?>
