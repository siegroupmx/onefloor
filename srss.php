<?php
include( "admin/config.php" );
include( "modulos/base.php" );
include( "admin/tema.php" );
include( "admin/desktop_functions.php" );

function autentificacion_http()
	{
	$msg= "Feed Restringuido a Usuarios Registrados";
	header( 'HTTP/1,1 401 Unauthorized' );
	//header( 'WWW-Authenticate: Digest realm="'. $msg. '",qop="auth",nonce="'. uniqid(). '",opaque="'. md5($msg). '"' );
	header( 'WWW-Authenticate: Basic realm="'. $msg. '"' );
	
	die('<h1>'. TITULO_WEB. '</h1><h2>http://'. $_SERVER['HTTP_HOST']. '</h2><p>Registrate en el Sitio para Obtener tu Feed Personalizado.');
	# echo '<h1>TEMA WEB - NOMBRE PAGINA</h1><h2>http://'. $_SERVER['HTTP_HOST']. '</h2><p>Registrate en el Sitio para Obtener tu Feed Personalizado.';
	}

function secure_rss()
	{
	deamon_logd(); # recolector de visitas y sesiones 

	header('Content-Type: text/xml'); //indicamos al navegador que es un FeedRSS
	echo '<?xml version="1.0" encoding="UTF-8"?>
	<?xml-stylesheet type="text/xsl" media="screen" href="/~d/styles/rss2spanishfull.xsl"?>
	<?xml-stylesheet type="text/css" media="screen" href="http://feeds.feedburner.com/~d/styles/itemcontent.css"?>
	<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:creativeCommons="http://backend.userland.com/creativeCommonsRssModule" version="2.0">';

	echo '<channel>';	
	echo '<title>'. TITULO_WEB. '</title>
	<link>http://'. $_SERVER['HTTP_HOST']. '</link>
	<description>'. consultar_datos_general( "SERVER_CONFIG", "ID='1'", "META_DESCRIPCION" ). '</description>';
	//<lastBuildDate>Tue, 02 Feb 2010 09:53:15 +0000</lastBuildDate>
	echo '<generator>http://onefloor.sie-group.net/?v='. VERSION. '</generator>
	<language>es</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>

	<itunes:summary>'. consultar_datos_general( "SERVER_CONFIG", "ID='1'", "META_DESCRIPCION" ). '</itunes:summary>
	<itunes:author>'. TITULO_WEB. '</itunes:author>
	<itunes:explicit>yes</itunes:explicit>
	<itunes:image href="http://'. $_SERVER['HTTP_HOST']. '/'. TEMA_URL. '/banners/'. consultar_datos_general( "SERVER_CONFIG", "ID='1'", "BANNER" ). '" />
	<managingEditor>'. consultar_datos_general( "USUARIOS", "TIPO_USR='Administrador'", "EMAIL" ). ' ('. consultar_datos_general( "USUARIOS", "TIPO_USR='Administrador'", "NOMBRE" ). ')</managingEditor>
	<copyright>'. date( "Y", time()). '</copyright>
	<itunes:subtitle>'. noticia_cortada(consultar_datos_general( "SERVER_CONFIG", "ID='1'", "META_DESCRIPCION" ), 255). '</itunes:subtitle>
	<itunes:keywords>'. noticia_cortada(consultar_datos_general( "SERVER_CONFIG", "ID='1'", "META_CLAVES" ), 255 ). '</itunes:keywords>';

	//<image>
	//	<title>Kafelog</title>
	//	<url>http://www.kafelog.com/wp-content/themes/kafelog2/images/kafelog.jpg</url>
	//	<link>http://www.kafelog.com</link>
	//</image>
	
	//<feedburner:info xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" uri="kafelog" />
	echo '<atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="self" type="application/rss+xml" href="http://'. $_SERVER['HTTP_HOST']. '/rss.php" />';
	//<atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="hub" href="http://pubsubhubbub.appspot.com" />
	echo '<itunes:owner><itunes:email>'. consultar_datos_general( "USUARIOS", "TIPO_USR='Administrador'", "EMAIL" ). '</itunes:email>
	<itunes:name>'. TITULO_WEB. '</itunes:name></itunes:owner>
	<itunes:category text="Technology"><itunes:category text="Tech News" /></itunes:category>';
	//<itunes:category text="Comedy" /><itunes:category text="TV &amp; Film" />
	//<itunes:category text="Games &amp; Hobbies">
	//<itunes:category text="Video Games" /></itunes:category>
	echo '<itunes:category text="Technology">
	<itunes:category text="Gadgets" /></itunes:category>
	<creativeCommons:license>http://creativecommons.org/licenses/by-nc-sa/3.0/</creativeCommons:license>
	<feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://fusion.google.com/add?feedurl=http://'. $_SERVER['HTTP_HOST']. '/rss.php" src="http://buttons.googlesyndication.com/fusion/add.gif">Subscribe with Google</feedburner:feedFlare>
	';

	$cons= consultar_enorden( "NOTICIAS", "FECHA DESC" );

	while( $buf= mysql_fetch_array($cons) )
		{
		echo '<item>
			<title>'. $buf["TITULO"]. '</title>
			<link>http://'. $_SERVER['HTTP_HOST']. '/?hoja='. $buf["ID"]. '</link>
			<comments>http://'. $_SERVER['HTTP_HOST']. '/?hoja='. $buf["ID"]. '</comments>
			<pubDate>'. date( "D, d M Y h:i:s", $buf["FECHA"] ). ' GMT</pubDate>
			<dc:creator>'. consultar_datos_general( "USUARIOS", "ID='". $buf["AUTOR"]. "'", "email" ). '</dc:creator>';
			//<category><![CDATA[Kafelog Podcast]]></category>
			//<category><![CDATA[070]]></category>
			//<category><![CDATA[anime]]></category>
			//<category><![CDATA[bakuman]]></category>
			//<category><![CDATA[beck]]></category>
			//<category><![CDATA[como conoci a vuestra madre]]></category>
			//<category><![CDATA[fotos personales]]></category>
			//<category><![CDATA[frotteurismo]]></category>
			//<category><![CDATA[gallinas]]></category>
			//<category><![CDATA[kafelog]]></category>
			//<category><![CDATA[manga]]></category>
			//<category><![CDATA[redes sociales]]></category>
			//<category><![CDATA[sexo]]></category>
			echo '<guid isPermaLink="false">http://'. $_SERVER['HTTP_HOST']. '/?hoja='. $buf["ID"]. '</guid>
			<description><![CDATA['. strip_tags(desproteger_cadena_xml($buf["MENSAJE"])). ']]></description>';
			//<wfw:commentRss>http://www.kafelog.com/2010/02/podcast-070/feed/</wfw:commentRss>
			//<slash:comments>6</slash:comments>
			
			if( strcmp( $buf["ARCHIVOS_NOMBRE"], "" ) && strcmp( $buf["ARCHIVOS_NOMBRE"], "0" ) )
				{
				if( strstr( $buf["ARCHIVOS_NOMBRE"], ".mp3" ) )
					echo '<enclosure url="http://'. $_SERVER['HTTP_HOST']. '/'. $buf["ARCHIVOS_URL"].$buf["ARCHIVOS_NOMBRE"]. '" length="'. filesize($buf["ARCHIVOS_URL"].$buf["ARCHIVOS_NOMBRE"]). '" type="audio/mpeg" />';
				}
			//<itunes:keywords>070,anime,bakuman,beck,como conoci a vuestra madre,fotos personales,frotteurismo,gallinas,kafelog,Kafelog Podcast,manga,redes sociales</itunes:keywords>
			echo '<itunes:subtitle>'. strip_tags(desproteger_cadena_xml($buf["MENSAJE"])). '</itunes:subtitle>
			<itunes:summary>'. strip_tags(desproteger_cadena_xml($buf["MENSAJE"])). '</itunes:summary>
			<itunes:author>'. consultar_datos_general( "USUARIOS", "ID='". $buf["AUTOR"]. "'", "NOMBRE" ). '</itunes:author>
			<itunes:explicit>yes</itunes:explicit>';
			//<itunes:duration>1:01:41</itunes:duration>
		echo '</item>';
		}
		
	echo '</channel>';
	echo '</rss>';
	unset($buf);
	limpiar($cons);
	}

if( !isset($_SERVER['PHP_AUTH_USER']) )
	autentificacion_http();
else
	{
	$data= array( "username"=>$_SERVER['PHP_AUTH_USER'], "password"=>$_SERVER['PHP_AUTH_PW'] ); # consultamos valores y armamos array
	$cons= consultar_con( "USUARIOS", "NICK='". proteger_cadena($data["username"]). "' && PASSWORD='". proteger_cadena($data["password"]). "'" ); # consultamos
	
	if( mysql_num_rows($cons) )
		secure_rss();
	else
		autentificacion_http();
	}
?>