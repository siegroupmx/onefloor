<?php
/*
Coder: M.S.I. Angel Haniel Cantu Jauregui
Mail: angel.cantu@sie-group.net
Date: 6, Feb 2017
*/
define( RECAPTCHA_SERVER, "www.google.com" );
define( RECAPTCHA_API, "/recaptcha/api/siteverify" );
define( RECAPTCHA_PUBLIK_KEY, "6LcXmBQUAAAAAM2-OpEDvVzA2JIskAjmNNcbtLvA" );
define( RECAPTCHA_SECRET, "6LcXmBQUAAAAABZ4CB7eS1veEtupTtlWjZVJW-U9" );

/*
Como llamar Catpcha en el HTML ?
    <div class="g-recaptcha" data-sitekey="RECAPTCHA_PUBLIK_KEY"></div>

Como procesar el formulario POST ?
    $_POST["g-recaptcha-response"] :: posee la verificacion generara por la captcha, hay que validarla con: 
                                      "recaptcha_verify($_POST["g-recaptcha-response"])"
*/

function recaptcha_print()
  {
  return '<div class="g-recaptcha" data-sitekey="'. RECAPTCHA_PUBLIK_KEY. '"></div>';
  }

function recaptcha_serialize( $data )
  {
  $r=0;
  if( !is_array($data) )    return $r;
  else
    {
    $a=''; # auxiliar
    foreach( $data as $key=>$val )
      $a .= ($a ? '&':''). $key. '='. $val; # serializamos
    $r= $a; # copiamos
    }

  return $r; # retorno
  }

function recaptcha_verify( $post )
  {
  $r=0;
  $request= array( 
      'POST',   # stream
      RECAPTCHA_API,      # path
      recaptcha_serialize(array('secret'=>RECAPTCHA_SECRET, 'response'=>$post)),  # json data
      0  # json method
    );

  $json= socket_iodata( RECAPTCHA_SERVER, $request, 443 );
  $aux= explode( "\r\n", $json );
  $r= json_decode($aux[count($aux)-1]);

  return $r;
  }

?>
