<?php
function header_imagen( $formato )
	{
	if( !strcmp($formato, "image/jpeg") )
		header('Content-Type:image/jpeg');
	else if( !strcmp($formato, "image/jpg") )
		header('Content-Type:image/jpg');
	else if( !strcmp($formato, "image/png") )
		header('Content-Type:image/png');
	else if( !strcmp($formato, "image/gif") )
		header('Content-Type:image/gif');
	}
		
function formato_imagen( $formato )
	{
	if( !strcmp($formato, "image/jpeg") || !strcmp($formato, "image/png") || !strcmp($formato, "image/gif") )
		return 1;
	return 0;
	}

function getrandom_image( $buf, $muestra )
	{
	$dir= opendir($buf);
	while( ($data=readdir($dir))!==FALSE )
		{
		if( strcmp($data, ".") && strcmp($data, "..") && !is_dir($data) && strstr($data, $muestra) ) # si es distinto a patth oculto y no es directorio
			break;
		}
	closedir($dir);
	
	return $data;
	}

# retorna el numero de imagenes encontradas en un directorio
function getimages_number( $buf, $muestra )
	{
	$dir= opendir($buf);
	$cont=0;
	while( ($data=readdir($dir))!==FALSE )
		{
		if( strcmp($data, ".") && strcmp($data, "..") && !is_dir($data) && strstr($data, $muestra) ) # si es distinto a patth oculto y no es directorio
			$cont++;
		}
	closedir($dir);
	
	return $cont;
	}

# imprime imagenes en una clase especifica
function printimage( $link, $imagen, $capa )
	{
	if( $capa )
		echo '<div class="'. $capa. '">';
	echo '<a href="'. $link. '"><img src="'. $imagen. '" border="0"></a>';
	if( $capa )
		echo '</div>';
	}

# imprime imagenes segun el numero
function printallimage_number( $path, $id, $titulo, $muestra, $num, $src )
	{
	$dir= opendir($path);
	$cont=0;
	while( ($data=readdir($dir))!==FALSE )
		{
		if( strcmp($data, ".") && strcmp($data, "..") && !is_dir($data) && strstr($data, $muestra) && $num==$cont ) # si es distinto a patth oculto y no es directorio
			{
			if( $titulo ) # si envia titulo, quiere link
				$link= url_amigable($id, $titulo, "contenido", 0);
			$imagen= HTTP_SERVER.$path.'/'.$data;
			if( !is_array($src) && $src ) # se retorna con codigo HTML
				{
				if( $titulo )
					return '<a href="'. $link. '"><img src="'. $imagen. '" border="0"></a>';
				return '<img src="'. $imagen. '" border="0">';
				}
			else if( is_array($src) )		# se retorna sin codigo HTML, la imagen Full (no acortada)
				{
				if( !strcmp($src[1], "full") ) # dimencion completa
					{
					if( !strcmp( $src[0], "1") ) # solo imagen
						return substr($imagen, 0, ((strlen($imagen)-strlen($muestra.'.jpg'))-1) ). '.jpg';
					else # html etiqueta
						{
						if( $titulo ) 
							return '<a href="'. $link. '"><img src="'. substr($imagen, 0, ((strlen($imagen)-strlen($muestra.'.jpg'))-1) ). '.jpg" border="0"></a>';
						return '<img src="'. substr($imagen, 0, ((strlen($imagen)-strlen($muestra.'.jpg'))-1) ). '.jpg" border="0">';
						}
					}
				else # dimencion requerida
					{
					if( !strcmp($src[0], "1") ) # solo imagen
						return substr($imagen, 0, ((strlen($imagen)-strlen($muestra.'.jpg'))-1) ).$src[1]. '.jpg';
					else # html etiqueta 
						{
						if( $titulo )
							return '<a href="'. $link. '"><img src="'. substr($imagen, 0, ((strlen($imagen)-strlen($muestra.'.jpg'))-1) ).$src[1]. '.jpg" border="0"></a>';
						return '<img src="'. substr($imagen, 0, ((strlen($imagen)-strlen($muestra.'.jpg'))-1) ).$src[1]. '.jpg" border="0">';
						}
					}
				}
			else		return $imagen;		#  se retorna sin codigo HTML, solo imagen url
			}
		else if( strstr($data, $muestra) ) $cont++;
		}
	closedir($dir);
	unset($cont);
	}

# imprime imagenes en una clase especifica
function printallimage( $path, $id, $titulo, $muestra )
	{
	$dir= opendir($path);
	$cont=0;
	while( ($data=readdir($dir))!==FALSE )
		{
		if( strcmp($data, ".") && strcmp($data, "..") && !is_dir($data) && strstr($data, $muestra) ) # si es distinto a patth oculto y no es directorio
			{
			if( $titulo ) # si tiene algo, habra enlace 
				$link= url_amigable($id, $titulo, "contenido", 0);
			$imagen= HTTP_SERVER.$path.'/'.$data;
			echo '<div';
			if( $id ) # si hay id, habra case personalizada 
				echo ' class="'. $id. '_'. $cont. '"';
			echo ' style="';
			if( $cont==0 )
				echo 'display:block;';
			else		echo 'display:none;';
			echo '">';
			if( $titulo ) # si tiene algo, habra enlace
				echo '<a href="'. $link. '">';
			echo '<img src="'. $imagen. '" border="0">';
			if( $titulo ) # si tiene algo, habra enlace
				echo '</a>';
			echo '</div>';
			$cont++;
			}
		}
	closedir($dir);
	unset($cont);
	}
	
function crear_imagen( $formato, $archivo )
	{
	if( !strcmp($formato, "image/jpeg") )
		$img= imagecreatefromjpeg($archivo);
	else if( !strcmp($formato, "image/jpg") )
		$img= imagecreatefromjpeg($archivo);
	else if( !strcmp($formato, "image/png") )
		$img= imagecreatefrompng($archivo);
	else if( !strcmp($formato, "image/gif") )
		$img= imagecreatefromgif($archivo);
	
	unset($formato);
	return $img; 
	}

function imprimir_imagen( $img, $formato, $modo, $url, $calidad )
	{
	if( $modo==1 ) //se mostrara al tiempo
		header_imagen( $formato );
	//entonces si modo es 0 no se imprime, se GUARDA
	
	if( !strcmp($formato, "image/jpeg") )
		return imagejpeg($img, $url, $calidad );
	else if( !strcmp($formato, "image/jpg") )
		return imagejpeg($img, $url, $calidad );
	else if( !strcmp($formato, "image/png") )
		return imagepng($img, $url, $calidad );
	else if( !strcmp($formato, "image/gif") )
		return imagegif($img, $url, $calidad );
	
	return 0;
	unset($formato, $img); 
	}
	
# corrobora que la imgane no sea parte de las paletas de imagenes que comunmente
# crea el sistema, osease, las que tiene el texto de: "_post", "_mini" y "_medium".
function paleta_imagenes($buf)
	{
	$arr= array( "_post", "_medium", "_mini", "_otro", "_r1", "_r2", "_r3", "_r4", "_r5", "_r6", "_r7" );
	foreach( $arr as $key )
		{
		if( strstr($buf, $key) )
			return 1;
		}
	return 0;
	}

# redimenciona una o varias imagenes que este dentro de un directorio
# obtenido mediante el argumento y estableciendo las dimencion deseada o aproximada 
function redimencionar_imagen( $data, $sizeall, $addname )
	{
	if( is_dir($data) ) # si es directorio, entonces redimencionaremos todas las imagenes dentro 
		{
		$dir= $data;
		$d= opendir($dir); # acrimos directorio
		$arr= array(); # arreglos de archivos 
		# recorremos el directorio para meter a un array los archivos 
		while( ($buf=readdir($d))!==FALSE) #leemos
			{
			if( strcmp($buf, ".") && strcmp($buf, "..") && !is_dir($buf) && !paleta_imagenes($buf) ) # si es distinto a patth oculto y no es directorio
				$arr[]= $buf; # insertamos 
			}
		closedir($d);
		unset($buf);
		foreach( $arr as $buf )
			{
			$tipo= substr( strtolower($buf), -3 ); # extraemos extencion
			$nombre= substr( $buf, 0, -4 ); # extraemos nombre real
			$new= $nombre. '_'. $addname. '.'. $tipo; # nuevo nombre con tag y extencion
			$img= crear_imagen( "image/". $tipo, $dir.$buf ); # creamos imagen apartir de la principal
			$size= getimagesize($dir.$buf); # obtenemos dimencion de la imagen original
			$w= $size[0]; # extraemos width
			$h=$size[1]; # extraemos height
			
			# si es menor a la dimencion de thumbnails requerida
			if( $w<=$sizeall )
				{
				$width= $w;
				$height= $h;
				}
			else # entonces es mayor, se redimenciona
				{
				$extract= $w-$sizeall; # obtenemos los pixeles que se quitaran a la dimencion original
				$porcentaje= ($extract*100)/$w; # obtenemos porcentaje que se le reducira al width original
				$width= $sizeall; # establecemos width deseado
				$height= $h-(($h*$porcentaje)/100); # obtenemos dimencion height segun la reduccion del porcentaje
				}
				
			$thumb= imagecreatetruecolor($width, $height); # creamos imagen con dimenciones
			imagecopyresampled($thumb, $img, 0, 0, 0, 0, $width, $height, $w, $h ); # creamos imagen virtual
			imprimir_imagen( $thumb, "image/".$tipo, 0, $dir.$new, 70 ); # creamos imagen
			
			# system("/usr/bin/cwebp -q 80 -m 6 ". $dir.$new. " -o ".$dir.substr($new, 0, -4).".webp"); # jpeg to webp

			unset($new, $tipo, $nombre); 
			}
		}
	else # entonces es un archivo, redimencionamos la imagen
		{
		$buf= $data; # copiamos para usar el mismo code de arriba
		$dir=''; # directorio real
		$nombre=''; # nombre real 
		$x= explode("/", $buf ); # explotamos
		
		for($i=0; $i<count($x); $i++ ) # recorremos arreglo excluyendo el ultimo valor para dar con el directorio real
			{
			if( $i==(count($x)-1) ) # ultimo valor (nombre real) 
				$nombre= substr( strtolower($x[$i]), 0, -4 ). '_'. $addname.'.'. substr( strtolower($x[$i]), -3); # obtenemos el nombre real 
			else # es directorio 
				$dir .= $x[$i]. '/'; # concatenamos
			}
		
		$tipo= substr( strtolower($buf), -3 ); # extraemos extencion
		$new= $nombre; # nuevo nombre con tag y extencion
		unset($y); # eliminamos 
			
		$img= crear_imagen( "image/". $tipo, $buf ); # creamos imagen apartir de la principal
		$size= getimagesize($buf); # obtenemos dimencion de la imagen original
		$w= $size[0]; # extraemos width
		$h=$size[1]; # extraemos height
			
		# si es menor a la dimencion de thumbnails requerida
		if( $w<=$sizeall )
			{
			$width= $w;
			$height= $h;
			}
		else # entonces es mayor, se redimenciona
			{
			$extract= $w-$sizeall; # obtenemos los pixeles que se quitaran a la dimencion original
			$porcentaje= ($extract*100)/$w; # obtenemos porcentaje que se le reducira al width original
			$width= $sizeall; # establecemos width deseado
			$height= $h-(($h*$porcentaje)/100); # obtenemos dimencion height segun la reduccion del porcentaje
			}
		
		$thumb= imagecreatetruecolor($width, $height); # creamos imagen con dimenciones
		imagecopyresampled($thumb, $img, 0, 0, 0, 0, $width, $height, $w, $h ); # creamos imagen virtual
		if( !imprimir_imagen( $thumb, "image/".$tipo, 0, $dir.$new, 50 ) ) # creamos imagen
			{
			unset($new, $tipo, $nombre, $dir, $x, $i);
			return 0;
			}

		# system("/usr/bin/cwebp -q 80 -m 6 ". $dir.$new. " -o ".$dir.substr($new, 0, -4).".webp"); # jpeg to webp
			
		unset($new, $tipo, $nombre, $dir, $x, $i);
		}
	return 1;
	}

# verificador de reestricciones de una imagen, no mayor a 700 px
function imagen_restricciones( $imagen )
	{
	$size= getimagesize($imagen); # obtenemos dimencion de la imagen original
	$w= $size[0]; # extraemos width
	$h=$size[1]; # extraemos height
	
	if( $w<100 || $w>750 ) # si es menor que 100 o mayor a 750, es un error
		return 0; # error
	else
		return 1;
	}


?>