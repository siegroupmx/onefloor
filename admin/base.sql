-- phpMyAdmin SQL Dump
-- version 4.0.10.15
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-11-2018 a las 12:31:52
-- Versión del servidor: 5.1.73
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `moneybox_site`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `BANEADOS`
--

CREATE TABLE IF NOT EXISTS `BANEADOS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `ID_BAN` bigint(20) NOT NULL,
  `FECHA_BANEO` text NOT NULL,
  `FECHA_LIMITE` text NOT NULL,
  `COMENTARIO` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `BOLETIN`
--

CREATE TABLE IF NOT EXISTS `BOLETIN` (
  `ID` varchar(10) NOT NULL,
  `ID_USUARIO` varchar(10) NOT NULL,
  `FECHA` text NOT NULL,
  `STATUS` varchar(1) NOT NULL,
  `NOMBRE` text NOT NULL,
  `TELEFONO` text NOT NULL,
  `EMAIL` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `BOLETIN`
--


--
-- Estructura de tabla para la tabla `BRUTE`
--

CREATE TABLE IF NOT EXISTS `BRUTE` (
  `ID` varchar(10) NOT NULL,
  `IP` varchar(20) NOT NULL,
  `IP_PROXY` varchar(20) NOT NULL,
  `FECHA` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `BUZON`
--

CREATE TABLE IF NOT EXISTS `BUZON` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `AUTOR` varchar(30) NOT NULL,
  `MENSAJE` text NOT NULL,
  `PARA` varchar(30) NOT NULL,
  `FECHA` text NOT NULL,
  `ADJUNTOS` text NOT NULL,
  `ASUNTO` varchar(100) NOT NULL,
  `IMAGENES_URL` text NOT NULL,
  `VISIBLE` int(11) NOT NULL,
  `TRASH` int(11) NOT NULL,
  `BORRADOR` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CHAT`
--

CREATE TABLE IF NOT EXISTS `CHAT` (
  `ID` varchar(10) NOT NULL,
  `ID_CHAT` varchar(10) NOT NULL,
  `ID_SOPORTE` varchar(10) NOT NULL,
  `ID_CLIENTE` varchar(10) NOT NULL,
  `SESSION` text NOT NULL,
  `MENSAJE` text NOT NULL,
  `FECHA` text NOT NULL,
  `SENDER` varchar(10) NOT NULL,
  `DISPLAY` varchar(1) NOT NULL,
  `NAVEGADOR` text NOT NULL,
  `IP` text NOT NULL,
  `NOMBRE` text NOT NULL,
  `EMAIL` text NOT NULL,
  `TELEFONO` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CHAT_GESTION`
--

CREATE TABLE IF NOT EXISTS `CHAT_GESTION` (
  `ID` varchar(10) NOT NULL,
  `ID_SOPORTE` varchar(10) NOT NULL,
  `ID_CLIENTE` varchar(10) NOT NULL,
  `SESSION` text NOT NULL,
  `ACTIVIDAD` varchar(1) NOT NULL,
  `NAVEGADOR` text NOT NULL,
  `IP` varchar(100) NOT NULL,
  `UBICACION` varchar(100) NOT NULL,
  `NOMBRE` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `TELEFONO` varchar(100) NOT NULL,
  `FECHA` text NOT NULL,
  `FECHA_END` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `COMENTARIOS`
--

CREATE TABLE IF NOT EXISTS `COMENTARIOS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `ID_NOT` varchar(10) NOT NULL,
  `ID_USUARIO` varchar(10) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MENSAJE` text NOT NULL,
  `VISIBLE` varchar(1) NOT NULL,
  `FECHA` text NOT NULL,
  `FACEBOOK_ID` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CONTACTO`
--

CREATE TABLE IF NOT EXISTS `CONTACTO` (
  `ID` varchar(10) NOT NULL,
  `ID_SOPORTE` varchar(10) NOT NULL,
  `NAVEGADOR` text NOT NULL,
  `IP` varchar(100) NOT NULL,
  `NOMBRE` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MENSAJE` text NOT NULL,
  `TELEFONO` varchar(100) NOT NULL,
  `FECHA` text NOT NULL,
  `SESSION` text NOT NULL,
  `ID_CLIENTE` varchar(10) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CONTROL_SPAM`
--

CREATE TABLE IF NOT EXISTS `CONTROL_SPAM` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `FECHA` varchar(100) NOT NULL,
  `BUFFER_ENVIO` varchar(80) NOT NULL,
  `TITULO` varchar(100) NOT NULL,
  `EMAIL_SRC` varchar(100) NOT NULL,
  `URL_IMG` varchar(60) NOT NULL,
  `DESCRIPCION` text NOT NULL,
  `ACTION` varchar(20) NOT NULL,
  `HIT` varchar(10) NOT NULL,
  `ROLL_HIT` varchar(5) NOT NULL,
  `RELAY_CONTROL` varchar(10) NOT NULL,
  `LINK_CUSTOMER` varchar(80) NOT NULL,
  `ENVIOS` varchar(100) NOT NULL,
  `DURACION` varchar(10) NOT NULL,
  `ENVIOS_TOTALES` varchar(20) NOT NULL,
  `ACTIVO` varchar(1) NOT NULL,
  `CRON` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DOWNLOAD_RATING`
--

CREATE TABLE IF NOT EXISTS `DOWNLOAD_RATING` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `NOMBRE` varchar(100) NOT NULL,
  `TIPO` varchar(100) NOT NULL,
  `RATING` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LISTA_CORREOS`
--

CREATE TABLE IF NOT EXISTS `LISTA_CORREOS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `FECHA` varchar(100) NOT NULL,
  `NOMBRE` varchar(15) NOT NULL,
  `EMAIL` varchar(30) NOT NULL,
  `DOMINIO` varchar(20) NOT NULL,
  `CATEGORIA` varchar(1) NOT NULL,
  `GRUPO` varchar(10) NOT NULL,
  `SPAM` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LOG`
--

CREATE TABLE IF NOT EXISTS `LOG` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NICK` varchar(100) NOT NULL,
  `IP` varchar(15) NOT NULL,
  `SO` varchar(15) NOT NULL,
  `ARQUITECTURA` varchar(12) NOT NULL,
  `NAVEGADOR` varchar(15) NOT NULL,
  `NAVEGADOR_LENGUAJE` varchar(80) NOT NULL,
  `SESION` text NOT NULL,
  `UBICACION` varchar(50) NOT NULL,
  `REFERENCIA` text NOT NULL,
  `NOMBRE_HOST` varchar(80) NOT NULL,
  `HUMAN` varchar(1) NOT NULL,
  `FECHA_LOGIN` text NOT NULL,
  `FECHA_LOGOUT` text NOT NULL,
  `REF_VAR` text NOT NULL,
  `REQUEST_VAR` text NOT NULL,
  `QUERY_VAR` text NOT NULL,
  `USERAGENT_VAR` text NOT NULL,
  `ACCEPT_VAR` text NOT NULL,
  `ENCODING_VAR` text NOT NULL,
  `LENGUAJE_VAR` text NOT NULL,
  `FECHA_UPDATE` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=95691 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LOG_REPORTES`
--

CREATE TABLE IF NOT EXISTS `LOG_REPORTES` (
  `ID` varchar(10) NOT NULL,
  `NOMBRE` text NOT NULL,
  `FECHA` bigint(20) NOT NULL,
  `FECHA_INICIO` bigint(20) NOT NULL,
  `FECHA_FIN` bigint(20) NOT NULL,
  `PDF` text NOT NULL,
  `HTML` text NOT NULL,
  `MAILSEND` text NOT NULL,
  `MAILSEND_FECHA` bigint(20) NOT NULL,
  `STATUS` varchar(1) NOT NULL,
  `MENSAJE` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Estructura de tabla para la tabla `MENSAJES_NOTIFICACION`
--

CREATE TABLE IF NOT EXISTS `MENSAJES_NOTIFICACION` (
  `ID` int(11) NOT NULL,
  `MENSAJE` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `MENSAJES_NOTIFICACION`
--

INSERT INTO `MENSAJES_NOTIFICACION` (`ID`, `MENSAJE`) VALUES
(0, 'escribe notificacion cuando exista nuevo comentario'),
(1, 'escribe notificacion cuando se produsca error en el sistema'),
(2, 'escribe notificacion cuando se registre un usuario'),
(3, 'escribe notificacion cuando se publique una nueva noticia'),
(4, 'escribe notificacion cuando se utilice el sistema de recuperacion de datos del usuario'),
(5, 'escribe notificacion cuando se utilice el sistema de mensajeria privada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MENUS`
--

CREATE TABLE IF NOT EXISTS `MENUS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `NOMBRE` varchar(100) DEFAULT NULL,
  `URL_NOMBRE` varchar(100) NOT NULL,
  `VISIBILIDAD` int(11) NOT NULL,
  `FECHA` varchar(100) NOT NULL,
  `ORDEN` varchar(3) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `MENUS`
--

INSERT INTO `MENUS` (`ID`, `NOMBRE`, `URL_NOMBRE`, `VISIBILIDAD`, `FECHA`, `ORDEN`) VALUES
('kqqeerd6r', 'Firma', 'firma', 1, '1386971177', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MODULOS`
--

CREATE TABLE IF NOT EXISTS `MODULOS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `URL` varchar(50) DEFAULT NULL,
  `NOMBRE` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MUNDO`
--

CREATE TABLE IF NOT EXISTS `MUNDO` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAIS` varchar(100) NOT NULL,
  `ESTADOS` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `MUNDO`
--

INSERT INTO `MUNDO` (`ID`, `PAIS`, `ESTADOS`) VALUES
(1, 'Belice', ''),
(2, 'Costa Rica', ''),
(3, 'El Salvador', ''),
(4, 'Guatemala', ''),
(5, 'Honduras', ''),
(6, 'Nicaragua', ''),
(7, 'Panama', ''),
(8, 'Canada', ''),
(9, 'Estados Unidos', ''),
(10, 'Groenlandia', ''),
(11, 'Mexico', ''),
(12, 'Argentina', ''),
(13, 'Bolivia', ''),
(14, 'Brasil', ''),
(15, 'Chile', ''),
(16, 'Colombia', ''),
(17, 'Ecuador', ''),
(18, 'Guayana Francesa', ''),
(19, 'Guyana', ''),
(20, 'Islas Malvinas', ''),
(21, 'Paraguay', ''),
(22, 'Peru', ''),
(23, 'Surinam', ''),
(24, 'Uruguay', ''),
(25, 'Venezuela', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `NOTICIAS`
--

CREATE TABLE IF NOT EXISTS `NOTICIAS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `AUTOR` varchar(30) DEFAULT NULL,
  `TITULO` varchar(200) DEFAULT NULL,
  `MENSAJE` text NOT NULL,
  `FECHA` text NOT NULL,
  `FECHA_MOD` text NOT NULL,
  `AUTOR_MOD` varchar(30) DEFAULT NULL,
  `MENU` varchar(40) NOT NULL,
  `SECCION` varchar(40) NOT NULL,
  `IMAGENES_URL` varchar(100) NOT NULL,
  `IMAGENES_NOMBRE` text NOT NULL,
  `ARCHIVOS_URL` varchar(100) NOT NULL,
  `ARCHIVOS_NOMBRE` text NOT NULL,
  `FACEBOOK` text NOT NULL,
  `TWITTER` text NOT NULL,
  `GPLUS` text NOT NULL,
  `COMENTARIOS` bigint(20) NOT NULL,
  `NOTIFY_MAIL` varchar(1) NOT NULL,
  `NOTIFY_SMS` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PRIVILEGIOS`
--

CREATE TABLE IF NOT EXISTS `PRIVILEGIOS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `NOMBRE` varchar(30) DEFAULT NULL,
  `SECCIONES` text NOT NULL,
  `USUARIOS` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `PRIVILEGIOS`
--

INSERT INTO `PRIVILEGIOS` (`ID`, `NOMBRE`, `SECCIONES`, `USUARIOS`) VALUES
('', 'Usuarios', '0', 'vwd19i');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PUBLICIDAD`
--

CREATE TABLE IF NOT EXISTS `PUBLICIDAD` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `TITULO` varchar(100) NOT NULL,
  `EMAIL_SRC` varchar(50) NOT NULL,
  `URL_IMG` varchar(100) DEFAULT NULL,
  `DESCRIPCION` text NOT NULL,
  `URL_SITIO` varchar(90) DEFAULT NULL,
  `TIPO` int(11) NOT NULL,
  `HIT` bigint(20) NOT NULL,
  `ACTIV` int(11) NOT NULL,
  `RELAY_CONTROL` varchar(10) NOT NULL,
  `FECHA` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SECCIONES`
--

CREATE TABLE IF NOT EXISTS `SECCIONES` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `NOMBRE` varchar(100) DEFAULT NULL,
  `URL_NOMBRE` varchar(100) NOT NULL,
  `TIPO` varchar(30) DEFAULT NULL,
  `RELACION` varchar(50) DEFAULT NULL,
  `COMENTARIOS` bigint(20) NOT NULL,
  `VISIBILIDAD` int(11) NOT NULL,
  `FECHA` varchar(100) NOT NULL,
  `ORDEN` varchar(3) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `SECCIONES`
--

INSERT INTO `SECCIONES` (`ID`, `NOMBRE`, `URL_NOMBRE`, `TIPO`, `RELACION`, `COMENTARIOS`, `VISIBILIDAD`, `FECHA`, `ORDEN`) VALUES
('oc9rdjluao', 'Empresa', 'empresa', 'noticia_limpia', 'kqqeerd6r', 0, 1, '1386971206', '1'),
('ch158mu', 'Quiero ser Distribuidor', 'quiero_ser_distribuidor', 'noticia_limpia', 'kqqeerd6r', 0, 1, '1503029170', '3'),
('uckf6ij', 'API', 'api', 'noticia_limpia', 'kqqeerd6r', 0, 1, '1386971242', '4'),
('an15zg', 'Terminos de Uso', 'terminos_de_uso', 'noticia_limpia', 'kqqeerd6r', 0, 1, '1386971249', '5'),
('joj2', 'Privacidad', 'privacidad', 'noticia_limpia', 'kqqeerd6r', 0, 1, '1386971256', '6'),
('4pvezy4q', 'Blog', 'blog', 'noticia', 'kqqeerd6r', 0, 1, '1505865922', '6'),
('w5dnyry2', 'Prensa', 'prensa', 'noticia', 'kqqeerd6r', 0, 1, '1534797649', '7'),
('54yo', 'Empleo', 'empleo', 'noticia', 'kqqeerd6r', 0, 1, '1541006507', '9'),
('fhoo', 'Calendario', 'calendario', 'scriptin', 'kqqeerd6r', 0, 1, '1541437128', '8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SERVER_CONFIG`
--

CREATE TABLE IF NOT EXISTS `SERVER_CONFIG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `INDEX_DEFAULT` text NOT NULL,
  `TEMA_NAME` varchar(50) DEFAULT NULL,
  `TEMA_URL` text NOT NULL,
  `TEMA_URL_M` text NOT NULL,
  `CARITAS_NAME` varchar(50) DEFAULT NULL,
  `CARITAS_URL` text NOT NULL,
  `META_DESCRIPCION` text NOT NULL,
  `META_CLASIFICACION` text NOT NULL,
  `META_CLAVES` text NOT NULL,
  `TITULO_WEB` text NOT NULL,
  `BANNER` text NOT NULL,
  `REGISTRO_USUARIO` text NOT NULL,
  `RELAY_LIMIT` varchar(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `SERVER_CONFIG`
--

INSERT INTO `SERVER_CONFIG` (`ID`, `INDEX_DEFAULT`, `TEMA_NAME`, `TEMA_URL`, `TEMA_URL_M`, `CARITAS_NAME`, `CARITAS_URL`, `META_DESCRIPCION`, `META_CLASIFICACION`, `META_CLAVES`, `TITULO_WEB`, `BANNER`, `REGISTRO_USUARIO`, `RELAY_LIMIT`) VALUES
(1, 'index.php?ver=oc9rdjluao', 'moneybox2\n', 'templates/moneybox2', 'templates/moneybox2', 'Caritas-Default', 'caritas/default', 'Sistema erp para administrar su negocio con facturacion, nomina, clientes, cotizaciones, ventas, compras, almacen, comercio web y punto de venta', 'factura, factura electronica, sistema factura, software factura, programa factura, proveedor factura, timbre factura, nomina, sistema nomina, software nomina, programa nomina, recibos nomina, sistema checador, software checador, checador nomina, punto de venta, kit punto de venta, sistema punto de venta, software punto de venta, venta, sistema venta, software venta, programa venta, cotizaciones, cotizador, sistema cotizar, software cotizaciones, programa cotizaciones, ordenes de venta, programa ordenes de venta, software ordenes de venta, almacen, sistema almacen, software almacen, programa almacen, punto de venta abarrotes, punto de venta farmacias, sistema doctores, doctores factura, doctores honorarios, programa honorarios, sistema honorarios doctor, sistema erp, software erp, programa erp, sistema crm, software crm, programa crm, recursos humanos, sistema recursos humanos, programa recursos humanos, contabilidad, contabilidad electronica, sistema contabilidad, software contabilidad, programa contabilidad, distribuidor factura, distribuidor nomina, distribuidor software, distribuidor erp, programa para hacer facturas electronicas, software de nominas mas utilizados, programas para nominas y seguros sociales', 'factura, factura electronica, sistema factura, software factura, programa factura, proveedor factura, timbre factura, nomina, sistema nomina, software nomina, programa nomina, recibos nomina, sistema checador, software checador, checador nomina, punto de venta, kit punto de venta, sistema punto de venta, software punto de venta, venta, sistema venta, software venta, programa venta, cotizaciones, cotizador, sistema cotizar, software cotizaciones, programa cotizaciones, ordenes de venta, programa ordenes de venta, software ordenes de venta, almacen, sistema almacen, software almacen, programa almacen, punto de venta abarrotes, punto de venta farmacias, sistema doctores, doctores factura, doctores honorarios, programa honorarios, sistema honorarios doctor, sistema erp, software erp, programa erp, sistema crm, software crm, programa crm, recursos humanos, sistema recursos humanos, programa recursos humanos, contabilidad, contabilidad electronica, sistema contabilidad, software contabilidad, programa contabilidad, distribuidor factura, distribuidor nomina, distribuidor software, distribuidor erp, programa para hacer facturas electronicas, software de nominas mas utilizados, programas para nominas y seguros sociales', 'moneyBox | factura, nomina, cotizaciones, orden de compra', '', 'Establece mensaje que visualizara el usuario en el formulario cuando este por registrarse.', '1000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UPLOADS`
--

CREATE TABLE IF NOT EXISTS `UPLOADS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `ID_NICK` bigint(20) NOT NULL,
  `NICK` varchar(100) NOT NULL,
  `ARCHIVO_NOMBRE` varchar(50) NOT NULL,
  `ARCHIVO_URL` text NOT NULL,
  `TIPO_ARCHIVO` varchar(10) NOT NULL,
  `SIZE` int(11) NOT NULL,
  `FECHA` text NOT NULL,
  `LINK` varchar(60) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIOS`
--

CREATE TABLE IF NOT EXISTS `USUARIOS` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `NOMBRE` varchar(100) DEFAULT NULL,
  `NICK` varchar(100) DEFAULT NULL,
  `PASSWORD` text,
  `EMAIL` text NOT NULL,
  `PAIS` varchar(100) NOT NULL,
  `ESTADO` varchar(100) NOT NULL,
  `FIRMA` varchar(200) DEFAULT NULL,
  `TIPO_USR` varchar(30) DEFAULT NULL,
  `FECHA_REGISTRO` text NOT NULL,
  `AVATAR` varchar(100) NOT NULL,
  `FRASE_PERSONAL` varchar(20) NOT NULL,
  `FECHA_NACIMIENTO` text NOT NULL,
  `ULTIMO_LOGIN` text NOT NULL,
  `MSN` varchar(30) NOT NULL,
  `YOUTUBE` varchar(80) NOT NULL,
  `FACEBOOK` varchar(80) NOT NULL,
  `FACEBOOK_TOKEN` text NOT NULL,
  `FACEBOOK_ID` text NOT NULL,
  `TWITTER` varchar(80) NOT NULL,
  `TWITTER_ID` text NOT NULL,
  `TWITTER_TOKEN` text NOT NULL,
  `HI5` varchar(80) NOT NULL,
  `TITULO_WEB` varchar(80) NOT NULL,
  `URL_WEB` varchar(80) NOT NULL,
  `NOTIFICACION_MP` int(11) NOT NULL,
  `NOTIFICACION_COMENTARIOS` int(11) NOT NULL,
  `NOTIFICACION_NOTICIAS` int(11) NOT NULL,
  `ONLINE` varchar(1) NOT NULL,
  `CALLE` text NOT NULL,
  `COLONIA` text NOT NULL,
  `TELEFONO` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `USUARIOS`
--

INSERT INTO `USUARIOS` (`ID`, `NOMBRE`, `NICK`, `PASSWORD`, `EMAIL`, `PAIS`, `ESTADO`, `FIRMA`, `TIPO_USR`, `FECHA_REGISTRO`, `AVATAR`, `FRASE_PERSONAL`, `FECHA_NACIMIENTO`, `ULTIMO_LOGIN`, `MSN`, `YOUTUBE`, `FACEBOOK`, `FACEBOOK_TOKEN`, `FACEBOOK_ID`, `TWITTER`, `TWITTER_ID`, `TWITTER_TOKEN`, `HI5`, `TITULO_WEB`, `URL_WEB`, `NOTIFICACION_MP`, `NOTIFICACION_COMENTARIOS`, `NOTIFICACION_NOTICIAS`, `ONLINE`, `CALLE`, `COLONIA`, `TELEFONO`) VALUES
('4dm1n', 'Admin', 'root', 'A1s4r1l2n', 'noreplay@moneybox.com.mx', 'Mexico', 'Tamaulipas', NULL, '1', '1386970564', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0', '', '', ''),


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
