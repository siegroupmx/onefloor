<?php
include( "geoip/vendor/autoload.php" );
use GeoIp2\Database\Reader;

if( !$_SESSION["geoip"] || !count($_SESSION["geoip"]) ) # si no existe o no tiene contenido
	{
	# $geoipdb = new Reader('/usr/share/GeoIP/GeoLite2-City.mmdb');
	$geoipdb = new Reader('/home/angel/public_html/onefloor/admin/geoip/GeoLite2-City.mmdb');
	$record = $geoipdb->city($_SERVER['REMOTE_ADDR']);
	$_SESSION["geoip"]= array( "ciudad"=>$record->city->name, "estado"=>$record->mostSpecificSubdivision->name, "pais"=>$record->country->name );
	unset($record);
	}
?>