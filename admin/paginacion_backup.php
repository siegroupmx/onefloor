<?php
function selector_paginacion( $base, $minimo, $hoja_actual, $enlace, $ajax_enlace, $capa_ajax )
	{
	$max_paginas= 11; //maximo numero de paginas a visualizar por panel
	$cons= consultar( $base, "*" ); 
	
	//si existe el numero de registros 'minimo' +1, entonces se producira la paginacion
	if( mysql_num_rows($cons)>$minimo )
		{
		echo "<div id=\"paginacion\">";
		
		//inicio de paginacion
		if( $hoja_actual<(ceil($max_paginas/2)+1) )
			$roll_start=0;
		else
			{
			if( $hoja_actual<(ceil(mysql_num_rows($cons)/$minimo)-$max_paginas) )
				$roll_start= ($hoja_actual-ceil($max_paginas/2));
			else
				$roll_start= (ceil(mysql_num_rows($cons)/$minimo)-$max_paginas)-1;
			}
		
		//maximo de paginacion
		if( $hoja_actual<(ceil($max_paginas/2)+1) )
			$roll_end= $max_paginas;
		else
			{
			if( $hoja_actual<(ceil(mysql_num_rows($cons)/$minimo)-$max_paginas) )
				$roll_end= ($hoja_actual+(ceil($max_paginas/2)))-1;
			else
				$roll_end= ceil(mysql_num_rows($cons)/$minimo);
			}
			
		# no existen valores GET y ponemos variable page
		if( $enlace )
		$uri= $enlace. '&page=';
		else $uri= '?page=';
		
		/*##################################
		########   ENALCES NORMALES/HTML   ########    
		##################################*/
		if( !strcmp( $ajax_enlace, "0") )
			{
			if( $hoja_actual>1 )
				echo '<a href="'. $uri.($hoja_actual-1). '">';
			
			echo '<img src="'. HTTP_SERVER. 'imagenes/flecha_izquierda.png" border="0">';
			
			if( $hoja_actual>1 )
				echo "</a>";
				
			for( $i=$roll_start; $i<$roll_end; $i++ )
				{
				echo " ";
				if( $i==($hoja_actual-1) )
					echo "<span style=\"background-color:white;\">". ($i+1). "</span>";
				else	echo "<a href=\"". $uri.($i+1). "\"><span>". ($i+1). "</span></a>";
				}
			echo " ";
		
			if( $hoja_actual<$roll_end )
				echo "<a href=\"". $uri.($hoja_actual+1). "\">";
				
			echo '<img src="'. HTTP_SERVER. 'imagenes/flecha_derecha.png" border="0">';
			
			if( $hoja_actual<$roll_end )
				echo "</a>";
			}
			

		/*#########################################
		#############   ENALCES AJAX   ############    
		#########################################*/
		else
			{
			if( $hoja_actual>1 )
				echo "<a href=\"#\" onclick=\"cargar_datos( '". $ajax_enlace.$uri.($hoja_actual-1). "', '". $capa_ajax. "', 'GET', '0' );\">";
			
			echo '<img src="'. HTTP_SERVER. 'imagenes/flecha_izquierda.png" border="0">';
			
			if( $hoja_actual>1 )
				echo "</a>";
			for( $i=$roll_start; $i<$roll_end; $i++ )
				{
				echo " ";
				if( $i==($hoja_actual-1) )
					echo "<span style=\"background-color:white;\">". ($i+1). "</span>";
				else	echo "<a href=\"#\" onclick=\"cargar_datos( '". $ajax_enlace.$uri.($i+1). "', '". $capa_ajax. "', 'GET', '0' );\"><span>". ($i+1). "</span></a>";
				}
			echo " ";
		
			if( $hoja_actual<$roll_end )
				echo "<a href=\"#\" onclick=\"cargar_datos( '". $ajax_enlace.$uri.($hoja_actual+1). "', '". $capa_ajax. "', 'GET', '0' );\">";
				
			echo '<img src="'. HTTP_SERVER. 'imagenes/flecha_derecha.png" border="0">';
		
			if( $hoja_actual<$roll_end )
				echo "</a>";
			}
		
		unset($i);
		unset($roll_start);
		unset($roll_end);
		echo "</div>";
		}
	
	unset($cons);
	}

function selector_paginacion_enorden_con( $base, $valores, $regla, $minimo, $hoja_actual, $enlace, $ajax_enlace, $capa_ajax )
	{
	$max_paginas= 7; //maximo numero de paginas a visualizar por panel
	$cons= consultar_enorden_con( $base, $valores, $regla ); 
	
	//si existe el numero de registros 'minimo' +1, entonces se producira la paginacion
	if( mysql_num_rows($cons)>$minimo )
		{
		echo "<div id=\"paginacion\">";
		
		//inicio de paginacion
		if( $hoja_actual<(ceil($max_paginas/2)+1) )
			$roll_start=0;
		else
			{
			if( $hoja_actual<(ceil(mysql_num_rows($cons)/$minimo)-$max_paginas) )
				$roll_start= ($hoja_actual-ceil($max_paginas/2));
			else
				$roll_start= (ceil(mysql_num_rows($cons)/$minimo)-$max_paginas)-1;
			}
		
		//maximo de paginacion
		
		if( $hoja_actual<(ceil($max_paginas/2)+1) )
			$roll_end= $max_paginas;
		else
			{
			if( $hoja_actual<(ceil(mysql_num_rows($cons)/$minimo)-$max_paginas) )
				$roll_end= ($hoja_actual+(ceil($max_paginas/2)))-1;
			else
				$roll_end= ceil(mysql_num_rows($cons)/$minimo);
			}
		
		# no existen valores GET y ponemos variable page
		if( $enlace )
		$uri= $enlace. '&page=';
		else $uri= '?page=';

		/*#########################################
		########   ENALCES NORMALES/HTML   ########    
		#########################################*/
		if( !strcmp( $ajax_enlace, "0") )
			{
			if( $hoja_actual>1 )
				echo "<a href=\"". $uri.($hoja_actual-1). "\">";
			
			echo '<img src="'. HTTP_SERVER. '/imagenes/flecha_izquierda.png" border="0">';
			
			if( $hoja_actual>1 )
				echo "</a>";
			for( $i=$roll_start; $i<$roll_end; $i++ )
				{
				echo " ";
				if( $i==($hoja_actual-1) )
					echo "<span style=\"background-color:white;\">". ($i+1). "</span>";
				else	echo "<a href=\"". $uri.($i+1). "\"><span>". ($i+1). "</span></a>";
				}
			echo " ";
		
			if( $hoja_actual<$roll_end )
				echo "<a href=\"". $uri.($hoja_actual+1). "\">";
				
			echo '<img src="'. HTTP_SERVER. '/imagenes/flecha_derecha.png" border="0">';
			
			if( $hoja_actual<$roll_end )
				echo "</a>";
			}

		/*#########################################
		#############   ENALCES AJAX   ############    
		#########################################*/
		else
			{
			if( $hoja_actual>1 )
				echo "<a href=\"#\" onclick=\"cargar_datos( '". $ajax_enlace.$uri.($hoja_actual-1). "', '". $capa_ajax. "', 'GET', '0' );\">";
			
			echo '<img src="'. HTTP_SERVER. '/imagenes/flecha_izquierda.png" border="0">';
			
			if( $hoja_actual>1 )
				echo "</a>";
			for( $i=$roll_start; $i<$roll_end; $i++ )
				{
				echo " ";
				if( $i==($hoja_actual-1) )
					echo "<span style=\"background-color:white;\">". ($i+1). "</span>";
				else	echo "<a href=\"#\" onclick=\"cargar_datos( '". $ajax_enlace.$uri.($i+1). "', '". $capa_ajax. "', 'GET', '0' );\"><span>". ($i+1). "</span></a>";
				}
			echo " ";
		
			if( $hoja_actual<$roll_end )
				echo "<a href=\"#\" onclick=\"cargar_datos( '". $ajax_enlace.$uri.($hoja_actual+1). "', '". $capa_ajax. "', 'GET', '0' );\">";
				
			echo '<img src="'. HTTP_SERVER. 'imagenes/flecha_derecha.png" border="0">';
		
			if( $hoja_actual<$roll_end )
				echo "</a>";
			}
		
		unset($i);
		unset($roll_start);
		unset($roll_end);
		echo "</div>";
		}
	
	unset($cons);
	}
	
function paginacion( $pagina, $base, $minimo )
	{
	if( !$pagina )		return 1;
	else if( $_GET["page"]>0 && is_numeric($_GET["page"]) )
		{
		$cons= consultar( $base, "*" );
		if( mysql_num_rows($cons)>$minimo )
			{
			$max_paginas= ceil(mysql_num_rows($cons)/$minimo); //num. de paginas totales

			//si estamos en el rango, entonces la consultar es favorable y no estan alterando la consulta
			if( $pagina>1 && $pagina<($max_paginas+1) )
				return proteger_cadena($pagina);
			}
		}
	return 1;
	}

function paginacion_enorden_con( $pagina, $base, $valores, $regla, $minimo )
	{
	$cons= consultar_enorden_con( $base, $valores, $regla ); //consultamos noticias ordenadas por ID
	
	//si existen suficientes registros
	if( mysql_num_rows($cons)>$minimo )
		{
		$max_paginas= ceil(mysql_num_rows($cons)/$minimo); //num. de paginas totales
		//echo "<script language=\"JavaScript\" type=\"text/javascript\">alert('". $max_paginas. "');</script>";
		
		//si estamos en el rango, entonces la consultar es favorable y no estan alterando la consulta
		if( $pagina>1 && $pagina<($max_paginas+1) )
			return $pagina;
		}
	return 1;
	}
?>