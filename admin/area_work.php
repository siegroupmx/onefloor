<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

if( is_admin() || is_coadmin() || is_autor() || is_editor() || is_soporte() )
	{
if( $_GET["id"] ) //si invocamos variable para modulos
	{
	$fp= opendir( getcwd() ); //abrimos directorio

	while( ($buf= readdir( $fp )) !== FALSE ) //leemos
		{
		if( strchr( $buf, ".php" ) ) //comparamos que sea archivo PHP
			{
			if( strcmp($buf, ".") && strcmp($buf, "..") && !strstr($buf, "~") && strcmp( $buf, "modulos.php" ) && strcmp( $buf, "base.php" ) ) //comparamos que sea distintio
				include( $buf ); //incluimos archivo PHP
			}
		unset( $buf ); //vaciamos buffer
		}
	closedir( $fp );
	}
else
	{
	echo '<h1>Bienvenido al Sistema '. VERSION. '</h1>
	<br><br>Hemos mejorado nuestra plataforma, ahora podras seguir alimentando y editando tu sitio web desde cualquier dispositivo con internet (tablet, smatphone, laptop o pc), 
	incorporamos mejoras especiales para que las ventanas <b>se adapten</b> a tu pantalla.<br><br>Si tiene alguna duda, requieres capacitacion o simplemente hacernos una 
	recomendacion, estamos disponibles en: Tel (899) 310 1422 o <a href="mailt:contacto@sie-group.net">contacto@sie-group.net</a>';
	}
	}
?>
