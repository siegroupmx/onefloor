<?php
if( is_login() && (isset($_GET["extras"]) || isset($_GET["extras_perfil"])) )
	{
	# uploads files
	if( !strcmp($_GET["extras"], "upload" ) )
		{			
		$tipo= substr( strtolower($_FILES["campain_file"]["name"]), -3 ); # extraemos extencion
		
		if( !empty($_FILES["campain_file"]["tmp_name"]) )
			{
			$path= '../usuarios/'. proteger_cadena($_SESSION["log_id"]). '/uploads/';
			$idtrack= generar_idtrack(); //obtenemos digito aleatorio
			$_SESSION["upload"]= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["campain_file"]["name"], 0, -4)).substr($_FILES["campain_file"]["name"], -4)); # directorio
			$tipo= substr( strtolower($_FILES["campain_file"]["name"]), -3 ); # extraemos extencion
		
			/*if( !validar_tipo_de_formato( $tipo, proteger_cadena($_GET["type"])) ) # validamos formato imagen
				{
				echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error tipo de archivo...\', \'upload\' );</script>';
				$_SESSION["upload"]='';
				}*/
			if( !strcmp(leer_formato($tipo), "imagen") && ($_FILES["campain_file"]["size"]/1024)>550 ) # no exeder de 550kb en imagenes
				{
				 echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion de imagen exedida, solo 1MB\', \'upload\' );</script>';
				 $_SESSION["upload"]='';
				 }
			else if( !strcmp(leer_formato($tipo), "video") && ($_FILES["campain_file"]["size"]/1024)>(1024*5) ) # no exeder de 5MB en video
				{
				echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion de video excedida, solo 5MB\', \'upload\' );</script>';
				$_SESSION["upload"]='';
				}
			else if( !strcmp(leer_formato($tipo), "archivo") && ($_FILES["campain_file"]["size"]/1024)>(1024*5)  ) # no exeder de 5MB en archivos
				{
				echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion archivo excedida, solo 5MB\', \'upload\' );</script>';
				$_SESSION["upload"]='';
				}
			else if( !move_uploaded_file( $_FILES["campain_file"]["tmp_name"], $path.$_SESSION["upload"] ) )
				{
				echo '<script language="javascript" type="text/javascript">
				window.top.window.resultadoUpload(\'3\', \''. $_FILES["campain_file"]["name"]. '\');
				</script>';
				}
			else
				{
				if( !strcmp(leer_formato($tipo), "imagen") ) # si es imagen
					{
					$size= getimagesize($path.$_SESSION["upload"]); # obtenemos dimencion de la imagen original
					$w= $size[0]; # extraemos width
					if( $w<100 ) # verificando dimenciones de la imagen
						{
						echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion ancho de flyer requerida: minimo 100px y maximo 700px\', \'upload\' );</script>';
						uploads_del(); # eliminamos archivos subidos 
						}
					else
						{
						echo '<script language="javascript" type="text/javascript">
						window.top.window.resultadoUpload(\'1\', \''. $_FILES["campain_file"]["name"]. '\');
						</script>';
						}
					}
				else
					{
					echo '<script language="javascript" type="text/javascript">
					window.top.window.resultadoUpload(\'1\', \''. $_FILES["campain_file"]["name"]. '\');
					</script>';
					}
				unset($size, $w);
				}
			}
		else # error
			{
			echo '<script language="javascript" type="text/javascript">
			window.top.window.resultadoUpload(\'2\', \'0\');
			</script>';
			}
		}
	}
?>