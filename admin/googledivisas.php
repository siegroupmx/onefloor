<?php
define( GOOGLECALCULATOR_SERVER, "www.google.com" );
define( GOOGLECALCULATOR_PATH, "/ig/calculator?");

# retorna arreglo de dvisas
function google_listdivisa()
	{
	$divisa= array( 'AED'=>'Dirham de los Emiratos Árabes Unidos', 'AFN'=>'Afgani afgano', 
	'ALL'=>'Lek albano', 'AMD'=>'Dram armenio', 'ANG'=>'Florín de las Antillas Holandesas', 'AOA'=>'Kwanza angoleño', 'ARS'=>'Peso argentino', 
	'AUD'=>'Dólar australiano', 'AWG 	Florín arubeño'=>'AZM 	Manat azerbaiyano', 'BAM'=>'marco convertible de Bosnia-Herzegovina', 
	'BBD'=>'Dólar de Barbados', 'BDT'=>'Taka de Bangladesh', 'BGN'=>'Lev búlgaro', 'BHD'=>'Dinar bahreiní', 'BIF'=>'Franco burundés', 
	'BMD'=>'Dólar de Bermuda', 'BND'=>'Dólar de Brunei', 'BOB'=>'Boliviano', 'BOV'=>'Mvdol boliviano (código de fondos)', 'BRL'=>'Real brasileño', 
	'BSD'=>'Dólar bahameño', 'BTN'=>'Ngultrum de Bután', 'BWP'=>'Pula de Botswana', 'BYR'=>'Rublo bielorruso', 'BZD'=>'Dólar de Belice', 
	'CAD'=>'Dólar canadiense', 'CDF'=>'Franco congoleño', 'CHF'=>'Franco suizo', 'CLF'=>'Unidades de fomento chilenas (código de fondos)', 
	'CLP'=>'Peso chileno', 'CNY'=>'Yuan Renminbi de China', 'COP'=>'Peso colombiano', 'COU'=>'Unidad de valor real colombiana (añadida al COP)', 
	'CRC'=>'Colón costarricense', 'CSD'=>'Dinar serbio (It was replaced by RSD on October 25, 2006)', 'CUP'=>'Peso cubano', 'CVE'=>'Escudo caboverdiano', 
	'CYP'=>'Libra chipriota', 'CZK'=>'Koruna checo', 'DJF'=>'Franco yibutiano', 'DKK'=>'Corona danesa', 'DOP'=>'Peso dominicano', 'DZD'=>'Dinar algerino', 
	'EEK'=>'Corona estonia', 'EGP'=>'Libra egipcia', 'ERN'=>'Nakfa eritreo', 'ETB'=>'Birr etíope', 'EUR'=>'Euro', 'FJD'=>'Dólar fijiano', 'FKP'=>'Libra malvinense', 
	'GBP'=>'Libra esterlina (libra de Gran Bretaña)', 'GEL'=>'Lari georgiano', 'GHC'=>'Cedi ghanés', 'GIP'=>'Libra de Gibraltar', 'GMD'=>'Dalasi gambiano', 
	'GNF'=>'Franco guineano', 'GTQ'=>'Quetzal guatemalteco', 'GYD'=>'Dólar guyanés', 'HKD'=>'Dólar de Hong Kong', 'HNL'=>'Lempira hondureño', 
	'HRK'=>'Kuna croata', 'HTG'=>'Gourde haitiano', 'HUF'=>'Forint húngaro', 'IDR'=>'Rupiah indonesia', 'ILS'=>'Nuevo shequel israelí', 'INR'=>'Rupia india', 
	'IQD'=>'Dinar iraqí', 'IRR'=>'Rial iraní', 'ISK'=>'Króna islandesa', 'JMD'=>'Dólar jamaicano', 'JOD'=>'Dinar jordano', 'JPY'=>'Yen japonés', 'KES'=>'Chelín keniata', 
	'KGS'=>'Som kirguís (de Kirguistán)', 'KHR'=>'Riel camboyano', 'KMF'=>'Franco comoriano (de Comoras)', 'KPW'=>'Won norcoreano', 'KRW'=>'Won surcoreano', 
	'KWD'=>'Dinar kuwaití', 'KYD'=>'Dólar caimano (de Islas Caimán)', 'KZT'=>'Tenge kazajo (de Kazajstán)', 'LAK'=>'Kip lao', 'LBP'=>'Libra libanesa', 'LKR'=>'Rupia de Sri Lanka', 
	'LRD'=>'Dólar liberiano', 'LSL'=>'Loti lesothense (de Lesotho)', 'LTL'=>'Litas lituano', 'LVL'=>'Lat letón', 'LYD'=>'Dinar libio', 'MAD'=>'Dirham marroquí', 'MDL'=>'Leu moldavo', 
	'MGA'=>'Ariary malgache (de Madagascar)', 'MKD'=>'Denar macedonio', 'MMK'=>'Kyat myanmaro', 'MNT'=>'Tughrik mongol', 'MOP'=>'Pataca de Macao', 'MRO'=>'Ouguiya mauritana', 
	'MTL'=>'Lira maltesa', 'MUR'=>'Rupia mauricia', 'MVR'=>'Rufiyaa maldiva', 'MWK'=>'Kwacha malawiano', 'MXN'=>'Peso mexicano', 'MXV'=>'Unidad de Inversión (UDI) mexicana (código de fondos)', 
	'MYR'=>'Ringgit malayo', 'MZM'=>'Metical mozambiqueño', 'NAD'=>'Dólar namibio', 'NGN'=>'Naira nigeriana', 'NIO'=>'Córdoba nicaragüense', 'NOK'=>'Corona noruega', 'NPR'=>'Rupia nepalesa', 
	'NZD'=>'Dólar neozelandés', 'OMR'=>'Rial omaní (de Omán)', 'PAB'=>'Balboa panameña', 'PEN'=>'Nuevo sol peruano', 'PGK'=>'Kina de Papúa Nueva Guinea', 'PHP'=>'Peso filipino', 
	'PKR'=>'Rupia pakistaní', 'PLN'=>'zloty polaco', 'PYG'=>'Guaraní paraguayo', 'QAR'=>'Rial qatarí', 'RON'=>'Leu rumano (desde el 1 de julio de 2005)', 'RUB'=>'Rublo ruso', 'RWF'=>'Franco ruandés', 
	'SAR'=>'Riyal saudí', 'SBD'=>'Dólar de las Islas Salomón', 'SCR'=>'Rupia de Seychelles', 'SDD'=>'Dinar sudanés', 'SEK'=>'Corona sueca', 'SGD'=>'Dólar de Singapur', 'SHP'=>'Libra de Santa Helena', 
	'SKK'=>'Corona eslovaca', 'SLL'=>'Leone de Sierra Leona', 'SOS'=>'Chelín somalí', 'SRD'=>'Dólar surinamés (desde el 1 de enero de 2004)', 'STD'=>'Dobra de Santo Tomé y Príncipe', 
	'SYP'=>'Libra siria', 'SZL'=>'Lilangeni suazi (de Suazilandia)', 'THB'=>'Baht tailandés', 'TJS'=>'Somoni tayik (de Tayikistán)', 'TMM'=>'Manat turcomano', 'TND'=>'Dinar tunecino', 
	'TOP'=>'Pa’anga tongano', 'TRY'=>'Nueva lira turca', 'TTD'=>'Dólar de Trinidad y Tobago', 'TWD'=>'Dólar taiwanés', 'TZS'=>'Chelín tanzano', 'UAH'=>'Hryvnia ucraniana', 'UGX'=>'Chelín ugandés', 
	'USD'=>'Dólar estadounidense', 'USN'=>'Dólar estadounidense (Siguiente día) (código de fondos)', 'USS'=>'United States dollar (Mismo día) (código de fondos)', 'UYU'=>'Peso uruguayo', 
	'UZS'=>'Som uzbeco', 'VEB'=>'Bolívar venezolano', 'VEF'=>'Bolívar fuerte venezolano', 'VND'=>'Dong vietnamita', 'VUV'=>'Vatu vanuatense', 'WST'=>'Tala samoana', 'XAF'=>'Franco CFA', 
	'XAG'=>'Onza de plata', 'XAU'=>'Onza de oro', 'XBA'=>'European Composite Unit (EURCO) (Bonds market unit)', 'XBB'=>'European Monetary Unit (E.M.U.-6) (Bonds market unit)', 
	'XBC'=>'European Unit of Account 9 (E.U.A.-9) (Bonds market unit)', 'XBD'=>'European Unit of Account 17 (E.U.A.-17) (Bonds market unit)', 'XCD'=>'Dólar del Caribe Oriental', 
	'XDR'=>'Special Drawing Rights (FMI)', 'XFO'=>'Franco de oro (Special settlement currency)', 'XFU'=>'Franco UIC (Special settlement currency)', 'XOF'=>'Franco CFA', 'XPD'=>'Onza de paladio', 
	'XPF'=>'Franco CFP', 'XPT'=>'Onza de platino', 'XTS'=>'Reservado para pruebas', 'XXX'=>'Sin divisa', 'YER'=>'Rial yemení (de Yemen)', 'ZAR'=>'Rand sudafricano', 'ZMK'=>'Kwacha zambiano', 
	'ZWD'=>'Dólar zimbabuense' );
	return $divisa;
	}

# convierte de una divisa a otra
function google_getdivisa( $numero, $div )
	{
#tipos de divisa soportado por google
$divisa= array( 'AED'=>'Dirham de los Emiratos Árabes Unidos', 'AFN'=>'Afgani afgano', 
	'ALL'=>'Lek albano', 'AMD'=>'Dram armenio', 'ANG'=>'Florín de las Antillas Holandesas', 'AOA'=>'Kwanza angoleño', 'ARS'=>'Peso argentino', 
	'AUD'=>'Dólar australiano', 'AWG 	Florín arubeño'=>'AZM 	Manat azerbaiyano', 'BAM'=>'marco convertible de Bosnia-Herzegovina', 
	'BBD'=>'Dólar de Barbados', 'BDT'=>'Taka de Bangladesh', 'BGN'=>'Lev búlgaro', 'BHD'=>'Dinar bahreiní', 'BIF'=>'Franco burundés', 
	'BMD'=>'Dólar de Bermuda', 'BND'=>'Dólar de Brunei', 'BOB'=>'Boliviano', 'BOV'=>'Mvdol boliviano (código de fondos)', 'BRL'=>'Real brasileño', 
	'BSD'=>'Dólar bahameño', 'BTN'=>'Ngultrum de Bután', 'BWP'=>'Pula de Botswana', 'BYR'=>'Rublo bielorruso', 'BZD'=>'Dólar de Belice', 
	'CAD'=>'Dólar canadiense', 'CDF'=>'Franco congoleño', 'CHF'=>'Franco suizo', 'CLF'=>'Unidades de fomento chilenas (código de fondos)', 
	'CLP'=>'Peso chileno', 'CNY'=>'Yuan Renminbi de China', 'COP'=>'Peso colombiano', 'COU'=>'Unidad de valor real colombiana (añadida al COP)', 
	'CRC'=>'Colón costarricense', 'CSD'=>'Dinar serbio (It was replaced by RSD on October 25, 2006)', 'CUP'=>'Peso cubano', 'CVE'=>'Escudo caboverdiano', 
	'CYP'=>'Libra chipriota', 'CZK'=>'Koruna checo', 'DJF'=>'Franco yibutiano', 'DKK'=>'Corona danesa', 'DOP'=>'Peso dominicano', 'DZD'=>'Dinar algerino', 
	'EEK'=>'Corona estonia', 'EGP'=>'Libra egipcia', 'ERN'=>'Nakfa eritreo', 'ETB'=>'Birr etíope', 'EUR'=>'Euro', 'FJD'=>'Dólar fijiano', 'FKP'=>'Libra malvinense', 
	'GBP'=>'Libra esterlina (libra de Gran Bretaña)', 'GEL'=>'Lari georgiano', 'GHC'=>'Cedi ghanés', 'GIP'=>'Libra de Gibraltar', 'GMD'=>'Dalasi gambiano', 
	'GNF'=>'Franco guineano', 'GTQ'=>'Quetzal guatemalteco', 'GYD'=>'Dólar guyanés', 'HKD'=>'Dólar de Hong Kong', 'HNL'=>'Lempira hondureño', 
	'HRK'=>'Kuna croata', 'HTG'=>'Gourde haitiano', 'HUF'=>'Forint húngaro', 'IDR'=>'Rupiah indonesia', 'ILS'=>'Nuevo shequel israelí', 'INR'=>'Rupia india', 
	'IQD'=>'Dinar iraqí', 'IRR'=>'Rial iraní', 'ISK'=>'Króna islandesa', 'JMD'=>'Dólar jamaicano', 'JOD'=>'Dinar jordano', 'JPY'=>'Yen japonés', 'KES'=>'Chelín keniata', 
	'KGS'=>'Som kirguís (de Kirguistán)', 'KHR'=>'Riel camboyano', 'KMF'=>'Franco comoriano (de Comoras)', 'KPW'=>'Won norcoreano', 'KRW'=>'Won surcoreano', 
	'KWD'=>'Dinar kuwaití', 'KYD'=>'Dólar caimano (de Islas Caimán)', 'KZT'=>'Tenge kazajo (de Kazajstán)', 'LAK'=>'Kip lao', 'LBP'=>'Libra libanesa', 'LKR'=>'Rupia de Sri Lanka', 
	'LRD'=>'Dólar liberiano', 'LSL'=>'Loti lesothense (de Lesotho)', 'LTL'=>'Litas lituano', 'LVL'=>'Lat letón', 'LYD'=>'Dinar libio', 'MAD'=>'Dirham marroquí', 'MDL'=>'Leu moldavo', 
	'MGA'=>'Ariary malgache (de Madagascar)', 'MKD'=>'Denar macedonio', 'MMK'=>'Kyat myanmaro', 'MNT'=>'Tughrik mongol', 'MOP'=>'Pataca de Macao', 'MRO'=>'Ouguiya mauritana', 
	'MTL'=>'Lira maltesa', 'MUR'=>'Rupia mauricia', 'MVR'=>'Rufiyaa maldiva', 'MWK'=>'Kwacha malawiano', 'MXN'=>'Peso mexicano', 'MXV'=>'Unidad de Inversión (UDI) mexicana (código de fondos)', 
	'MYR'=>'Ringgit malayo', 'MZM'=>'Metical mozambiqueño', 'NAD'=>'Dólar namibio', 'NGN'=>'Naira nigeriana', 'NIO'=>'Córdoba nicaragüense', 'NOK'=>'Corona noruega', 'NPR'=>'Rupia nepalesa', 
	'NZD'=>'Dólar neozelandés', 'OMR'=>'Rial omaní (de Omán)', 'PAB'=>'Balboa panameña', 'PEN'=>'Nuevo sol peruano', 'PGK'=>'Kina de Papúa Nueva Guinea', 'PHP'=>'Peso filipino', 
	'PKR'=>'Rupia pakistaní', 'PLN'=>'zloty polaco', 'PYG'=>'Guaraní paraguayo', 'QAR'=>'Rial qatarí', 'RON'=>'Leu rumano (desde el 1 de julio de 2005)', 'RUB'=>'Rublo ruso', 'RWF'=>'Franco ruandés', 
	'SAR'=>'Riyal saudí', 'SBD'=>'Dólar de las Islas Salomón', 'SCR'=>'Rupia de Seychelles', 'SDD'=>'Dinar sudanés', 'SEK'=>'Corona sueca', 'SGD'=>'Dólar de Singapur', 'SHP'=>'Libra de Santa Helena', 
	'SKK'=>'Corona eslovaca', 'SLL'=>'Leone de Sierra Leona', 'SOS'=>'Chelín somalí', 'SRD'=>'Dólar surinamés (desde el 1 de enero de 2004)', 'STD'=>'Dobra de Santo Tomé y Príncipe', 
	'SYP'=>'Libra siria', 'SZL'=>'Lilangeni suazi (de Suazilandia)', 'THB'=>'Baht tailandés', 'TJS'=>'Somoni tayik (de Tayikistán)', 'TMM'=>'Manat turcomano', 'TND'=>'Dinar tunecino', 
	'TOP'=>'Pa’anga tongano', 'TRY'=>'Nueva lira turca', 'TTD'=>'Dólar de Trinidad y Tobago', 'TWD'=>'Dólar taiwanés', 'TZS'=>'Chelín tanzano', 'UAH'=>'Hryvnia ucraniana', 'UGX'=>'Chelín ugandés', 
	'USD'=>'Dólar estadounidense', 'USN'=>'Dólar estadounidense (Siguiente día) (código de fondos)', 'USS'=>'United States dollar (Mismo día) (código de fondos)', 'UYU'=>'Peso uruguayo', 
	'UZS'=>'Som uzbeco', 'VEB'=>'Bolívar venezolano', 'VEF'=>'Bolívar fuerte venezolano', 'VND'=>'Dong vietnamita', 'VUV'=>'Vatu vanuatense', 'WST'=>'Tala samoana', 'XAF'=>'Franco CFA', 
	'XAG'=>'Onza de plata', 'XAU'=>'Onza de oro', 'XBA'=>'European Composite Unit (EURCO) (Bonds market unit)', 'XBB'=>'European Monetary Unit (E.M.U.-6) (Bonds market unit)', 
	'XBC'=>'European Unit of Account 9 (E.U.A.-9) (Bonds market unit)', 'XBD'=>'European Unit of Account 17 (E.U.A.-17) (Bonds market unit)', 'XCD'=>'Dólar del Caribe Oriental', 
	'XDR'=>'Special Drawing Rights (FMI)', 'XFO'=>'Franco de oro (Special settlement currency)', 'XFU'=>'Franco UIC (Special settlement currency)', 'XOF'=>'Franco CFA', 'XPD'=>'Onza de paladio', 
	'XPF'=>'Franco CFP', 'XPT'=>'Onza de platino', 'XTS'=>'Reservado para pruebas', 'XXX'=>'Sin divisa', 'YER'=>'Rial yemení (de Yemen)', 'ZAR'=>'Rand sudafricano', 'ZMK'=>'Kwacha zambiano', 
	'ZWD'=>'Dólar zimbabuense' );

	if( !$numero || !is_numeric($numero) || !is_array($div) )		return 0;
	
	# http://www.google.com/ig/calculator?hl=en&q=1USD=?MXN
	$args= 'hl=en&q='. $numero.$div[0]. '=?'. $div[1];
	$r= socket_iodata( GOOGLECALCULATOR_SERVER, array( 'GET', GOOGLECALCULATOR_PATH.$args), 80 );
	return explode("\"", $r[1]);
	}
?>