<?php
define(FBID, "1575525202682591");
define( FBSECRET, "553a5fcd2deada70b49e8f358eeca8c0");
 
# mete (push) u obtiene (get) informacion a facebook 
# arr -> arreglo de los datos
# place -> lugar a donde iran los datos (feed, photo, note, etc.. Ver Graph App)
# user -> id usuario en el sistema (NO en facebook) o bien array( id_usuario, id_grupo|id_pagina )
# metodo -> el evento 'get' o 'post' 
function facebook_pgdata( $place, $metodo, $user, $arr )
	{
	$fb= facebook_getapi(); # obtenemos conexion
	$r=0; # incializamos 
	if( is_array($user) && count($user)==2 ) # es hacia una grupo
		{
		$fbuser= $user[1]; # obtenemos ID usuario facebook
		$fbtoken= '?access_token='. consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($user[0]). "'", "FACEBOOK_TOKEN" ); # consultamos Token Facebook
		}
	else if( is_array($user) && count($user)>2 ) # es hacia una pagina
		{
		# [0] --> id usuario, [1]--> id pagina, [2] --> page (texto), get_pageid (retornar el id de la pagina), get_all (retornar arreglo completo)
		if( !strcmp($user[2], "search") ) # buscar algo
			{
			$fbuser= $user[1];
			$fbtoken= '?access_token='. consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($user[0]). "'", "FACEBOOK_TOKEN" ); # consultamos Token Facebook
			}
		else if( !strcmp($user[2], "page") ) # publicar en la pagina
			{
			$fbuser= $user[1]; # obtenemos ID usuario facebook
			$r= facebook_pgdata( 'accounts', 'get', $user[0], 0 );
			foreach( $r[data] as $key=>$val )
				{
				if( !strcmp($val[id], strtolower($fbuser)) || strstr(strtolower($fbuser), strtolower($val[name])) ) # es el ID de la pagina
					$fbap=$val[access_token];
				}
			$fbtoken= '?access_token='. $fbap; # consultamos Token Facebook
			unset($fbap, $r, $key, $val);
			}
		else if( !strcmp($user[2], "get_pageid") ) # obtener id de la pagina
			{
			$muestra= $user[1]; # muestra a buscar
			$d=0;
			$r= facebook_pgdata( 'accounts', 'get', $user[0], 0 );
			#do {
				foreach( $r[data] as $key=>$val )
					{
					if( !strcmp(strtolower($val[id]), strtolower($muestra)) || strstr(strtolower($muestra), strtolower($val[name]) ) || 
						!strcmp(strtolower($muestra), strtolower($val[access_token]) ) )
						$d= $val[id];
					}
			#	if( !$d && $r[paging][next] ) # si no se ha obtenido algun dato y hay continuidad
			#		{
			#		echo '<br>Siguiente: '. $r[paging][next];
			#		$r= facebook_pgdata( 'accounts', 'get', $user[0], 0 ); # consultamos mas datos
			#		}
			#	}while( !$d && $r[paging][next] );
			unset($muestra, $r, $key, $val);
			$r= $d;
			}
		else if( !strcmp($user[2], "get_pageaccesstoken") ) # obtener access toke de la pagina
			{
			$muestra= $user[1]; # muestra a buscar
			$d=0;
			$r= facebook_pgdata( 'accounts', 'get', $user[0], 0 );
			foreach( $r[data] as $key=>$val )
				{
				if( !strcmp(strtolower($val[id]), strtolower($muestra)) || strstr(strtolower($muestra), strtolower($val[name])) || 
					!strcmp(strtolower($muestra), strtolower($val[access_token])) )
					$d= $val[access_token];
				}
			unset($muestra, $r, $key, $val);
			$r= $d;
			}
		else if( !strcmp($user[2], "get_pagename") ) # obtener nombre de la pagina
			{
			$muestra= $user[1]; # muestra a buscar
			$d=0;
			$r= facebook_pgdata( 'accounts', 'get', $user[0], 0 );
			foreach( $r[data] as $key=>$val )
				{
				if( !strcmp(strtolower($val[id]), strtolower($muestra)) || strstr(strtolower($muestra), strtolower($val[name])) || 
					!strcmp(strtolower($muestra), strtolower($val[access_token])) )
					$d= $val[name];
				}
			unset($muestra, $r, $key, $val);
			$r= $d;
			}
		}
	else if( $user && !is_array($user) ) # si hay conexion a facebook
		{
		$fbuser= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($user). "'", "FACEBOOK_ID" ); # obtenemos ID usuario facebook
		$fbtoken= '?access_token='. consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($user). "'", "FACEBOOK_TOKEN" ); # consultamos Token Facebook
		}
	else
		{
		$fbuser=0;
		$fbtoken=0;
		$r=0;
		}
	
	# enviando datos
	if( $fbuser && $fbtoken )
		{
		if( !strcmp($place, "delete") ) # metodo eliminacion
			$r= $fb->api( '/'. $fbuser.$fbtoken.$user[3], $metodo );
		else # los demas
			{
			$arg='';
			if( $user[3] ) # argumentos
				$arg= '&'.$user[3];
			if( !$arr )		$arr='';

			$r= $fb->api( '/'. $fbuser. '/'. $place.$fbtoken.$arg, $metodo, $arr );
			unset($arg, $arr);
			}
		unset($fbuser, $fbtoken);
		}
	unset($fb); 
	return $r;
	}

# leer el muro
function facebook_getwall()
	{
	}

# obtener una conexion a la clase 
function facebook_getapi()
	{
	# facebook init
	$facebook = new Facebook(array(
		'appId'=>FBID,
		'secret'=>FBSECRET, 
		));
		
	return $facebook;
	}

# obtener ID del usuario
function facebook_conectar()
	{
	# facebook init
	$facebook = new Facebook(array(
		'appId'=>FBID,
		'secret'=>FBSECRET, 
		));
	$user = $facebook->getUser(); # obtenemos ID del usuario
	return $user;
	}

# obtener el arreglo de datos del usuario
function facebook_get_userid()
	{
	# facebook init
	$facebook = new Facebook(array(
		'appId'=>FBID,
		'secret'=>FBSECRET, 
		));
	$user = $facebook->getUser(); # obtenemos ID del usuario

	# verificamos si el ID esta conectado a facebook 
	if( $user )
		{
		try {
			// Proceed knowing you have a logged in user who's authenticated.
			$user_profile = $facebook->api('/me');
			} catch (FacebookApiException $e) {
			error_log($e);
			$user = null; # error, no esta conectado a facebook
			}
		}
	return $user_profile;
	}

# consultar por la TOKEN del usuario conectado 
function facebook_token( $m )
	{
	# facebook init
	$facebook = new Facebook(array(
		'appId'=>FBID,
		'secret'=>FBSECRET, 
		));
	$r=0;
	
	if( !strcmp($m, "user") )		$r= $facebook->getUserAccessToken();
	else if( !strcmp($m, "app") )		$r= $facebook->getApplicationAccessToken();
	else if( !strcmp($m, "auto") ) $r= $facebook->getAccessToken();
	else		$r=0;

	return $r;
	}

?>