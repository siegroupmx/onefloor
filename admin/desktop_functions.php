<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

function onefloor_reload( $tiempo )
	{
	echo '
	<script type="text/javascript">
		setTimeout( function()	{
			location.reload();
		}, '. $tiempo. ');
	</script>
	';
	}
	
# tipo de cuentas - retorna el "nombre"
function tipo_de_cuenta( $priv )
	{
	if( $priv==1 ) # admin
		return 'Administrador Global';
	else if( $priv==2 ) # admin regional/local
		return 'Administardor Local/Regional';
	else if( $priv==3 ) # soporte tecnico 
		return 'Soporte Tecnico';
	else if( $priv==4 ) # cliente 
		return 'Cliente/Usuario';
	else if( $priv==5 ) # blogger 
		return 'Blogger';
	else if( $priv==6 ) # empresa 
		return 'Empresa';
	}

# obtener localidad del usuario
function get_geolocation()
	{
	//Geo Localizacion por IP
	# require( "admin/geoip.inc" ); //incluimos cabecera
	require( "admin/geoipcity.inc" ); //incluimos cabecera
	include( "admin/geoipregionvars.php" );
	$geoip_bd= geoip_open( "admin/geoip/GeoIPLiteCity.dat", GEOIP_STANDARD ); //abrimos archivos dat
	$ip= $_SERVER['REMOTE_ADDR']; //obtenemos IP
	$r= geoip_record_by_addr( $geoip_bd, $ip );
	$ubicacion= proteger_cadena(($r->city. '/'. $GEOIP_REGION_NAME[$r->country_code][$r->region]. '/'. $r->country_name));
	geoip_close($geoip_bd);
	return $ubicacion;
	}

# forza utilizar www 
function httpwwwforce()
	{
	# si no contiene www, redirigimos
	if( !WWWFORCE )	 	return;
	else
		{
		if( !urlparser($_SERVER['HTTP_HOST'], array("www", "0")) )
			{
			$parsehttp='';

			if( !strcmp($_SERVER['HTTPS'], "on") )
				$parsehttp= 'https://';
			else		$parsehttp= 'http://';

			$parsehttp .= 'www.'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
			header( "Location: ". $parsehttp );
			}
		}
	}

# comprubea url
# arg debe ser un arreglo, el primer valor es la opcion y el segundo, si retornamos una cadena o bit de error o exito
function urlparser( $url, $arg )
	{
	/*
	Ejm: http://username:password@hostname/path?arg=value#anchor
	Array
		(
    	[scheme] => http
    	[host] => hostname
    	[user] => username
    	[pass] => password
    	[path] => /path
    	[query] => arg=value
    	[fragment] => anchor
		)
	*/
	if( !is_array($arg) )		return 0;
	if( count($arg)!=2 )		return 0;
	
	$data= parse_url($url); # parseamos
	
	if( !strcmp($arg[0], "http") ) # forzar usar http
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( $data['scheme'] ) # si tiene scheme
				$r= $url;
			else		$r= 'http://'. $url;
			}
		else # retorna bit
			{
			if( $data['scheme'] ) # si tiene scheme
				$r=1;
			else		$r=0;
			}
		}

	else if( !strcmp($arg[0], "https") ) # forzar usar https
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( $data['scheme'] ) # si tiene scheme
				$r= $url;
			else		$r= 'https://'. $url;
			}
		else # retorna bit
			{
			if( $data['scheme'] ) # si tiene scheme
				$r=1;
			else		$r=0;
			}
		}

	else if( !strcmp($arg[0], "www") ) # forzar usar www
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( strstr($data['path'], "www") ) # si tiene www
				$r= $url;
			else		$r= 'www.'. $url;
			}
		else # retorna bit
			{
			if( strstr($data['path'], "www") ) # si tiene www
				$r=1;
			else		$r=0;
			}
		}

	else if( !strcmp($arg[0], "host") ) # forzar usar http
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( $data['host'] ) # si tiene scheme
				$r= $data['host'];
			else if( $data['path'] )
				$r= $data['path'];
			else		$r=0;
			}
		else # retorna bit
			{
			if( $data['host'] ) # si tiene scheme
				$r=1;
			else if( $data['path'] )
				$r= 1;
			else		$r=0;
			}
		}

	unset($data);
	return $r; # retornamos respuesta 
	}

# limpia una variable, eliminando salto de linea
function clearjump( $var )
	{
	$out='';
	if( strstr($var, "\n") ) # si existe un de linea
		$out= substr($var, 0, -1);
	else $out=$var;
	return $out;
	}

function url_amigable( $id, $titulo, $tipo, $arg )
	{
	$t= url_cleaner(desproteger_cadena($titulo));
	$url= HTTP_SERVER;
	
	if( !HAPPY_URL ) # si NO estan activadad
		return $id;
	
	if( !strcmp($tipo, "blog") )
		{
		$t= url_cleaner(desproteger_cadena($titulo), true);
		return HTTP_SERVER. 'blog/'. $id. '-'. $t. '.html';
		}
	else if( !strcmp($tipo, "contenido") )
		{
		$t= url_cleaner(desproteger_cadena($titulo), true);
		return HTTP_SERVER. 'hoja/'. $id. '-'. $t. '.html';
		}
	else if( !strcmp($tipo, "menu") )
		{
		$url .=  $t. '/';

		if( strstr($arg, ",") ) # contiene delimitador(es)
			{
			$x= explode(",", $arg); # explotamos
			foreach( $x as $key ) # recorremos
				$url .= $key. '/';
			unset($x);
			}
		else if( $arg ) # entonces seccion dentro del menu
			$url .= url_cleaner(desproteger_cadena($arg)). '/';
		
		return $url;
		}
	else if( !strcmp($tipo, "users") ) # calcula la profundidad con las variables
		{
		$url .= 'user/'. $t;
		
		$x= explode( "?", $id ); # explotamos
		$y= explode( "=", $x[1] ); # explotamos 
		$url .= $y[1]; # concatenamos
		unset($x, $y );
		return $url;
		}
	else if( !strcmp($tipo, "login") ) # calcula la profundidad con las variables
		{
		$url .= $t. '/'. $arg;
		return $url;
		}
	else if( !strcmp($tipo, "script") ) # pagina o script
		{
		$x= explode( ":", $arg );
		if( !strcmp($titulo, "share") ) # si son compartidos, cambiamos titulo
			$t= 'compartir';
		else if( !strcmp($titulo, "votos") ) # si son votos, cambiamos titulo
			$t= 'votar';
		else if( !strcmp($titulo, "add") ) # si son addspot, cambiamos titulo
			$t= 'add';
		
		$url .= $t;
		
		foreach($x as $key ) # ponemos argumentos
			$url .= '/'. $key;
		return $url;
		}
	else if( !strcmp($tipo, "auto") ) # calcula la profundidad con las variables
		{
		if( $arg )
			$t2= url_cleaner(desproteger_cadena($arg));
		$url .= $t. '/';
		
		if( $id )
			{
			$x= explode( "&", $id ); # explotamos
			foreach( $x as $val )
				{
				$y= explode( "=", $val ); # explotamos 
				foreach($y as $key )
					$url .= $key. '/'; # concatenamos
				}
			unset($x, $y );
			}
		if( $arg )
			$url .= $t2. '.html'; # concatenamos el titulo
		return $url;
		}
	}

function leer_formato( $tipo )
	{
	$arr= array( "jpg"=>"imagen", "jpeg"=>"imagen", "gif"=>"imagen", "flv"=>"video",  "ogg"=>"video", "doc"=>"archivo", 
			"odt"=>"archivo", "xls"=>"archivo", "pdf"=>"archivo", "txt"=>"archivo", "mp3"=>"audio" ); # formatos valido
	
	foreach( $arr as $key=>$val )
		{
		if( !strcmp($tipo, $key) )
			return $val;
		}
	return 0;
	}

# valida formato de una archivo a subir 
function validar_tipo_de_formato( $tipo, $formato )
	{
	if( !strcmp( $formato, "imagen") )
		$arr= array( "jpg", "jpeg", "gif" ); # formatos validos
	else if( !strcmp( $formato, "video") )
		$arr= array( "flv" ); # formatos validos
	else if( !strcmp( $formato, "archivo") )
		$arr= array( "doc", "odt", "xls", "pdf", "txt" ); # formatos validos
	else if( !strcmp( $formato, "comprimido") )
		$arr= array( "zip" ); # formatos validos
	else if( !strcmp( $formato, "audio") )
		$arr= array( "mp3" ); # formatos validos
	else return 0;
	
	for( $i=0; $i<count($arr); $i++ )
		{
		#if( strstr($tipo, $arr[$i]) )
		if( !strcmp($tipo, $arr[$i]) )
			return 1;
		}
	return 0;
	}

# elimina la session y uploads del directorio temporal
function uploads_del()
	{
	if( isset($_SESSION["upload"]) ) # si existe la session, existen archivos
		{
		$path= 'uploads/';
		
		$x= explode( ":", $_SESSION["upload"] ); # explotamos
		for( $i=0; $i<count($x); $i++ )
			@unlink( $path.$x[$i] ); # eliminamos
		
		unset($_SESSION["upload"]);
		}
	else		$_SESSION["upload"]='';
	}

# toma como argumento un archivo o un directorio
# si es un archivo lo pasa a minusculas (renombrandolo)
# si es directorio, pone en minusculas todos los archivos
function renombrar_archivos($data)
	{
	if( !is_dir($data) )
		return 0;
	else
		{
		$dir= opendir($data); # abrimos directorio
		while( ($buf=readdir($dir))!==FALSE )
			{
			# si es distinto a archivos '.', '..' y a directorios, lo renombramos 
			if( strcmp($buf, ".") && strcmp($buf, "..") && !is_dir($buf) )
				rename( $data.$buf, $data.strtolower(url_cleaner(substr($buf, 0, -4)).substr($buf, -4)) ); # renombramos
			}
		}
	return 1;
	}
	
function eliminar_recursivamente( $folder )
	{
	if( !is_dir($folder) ) # si es carpeta
		return 0;
	
	$dir= opendir($folder); # abrimos directorio
	$del=0;
	while( ($buf=readdir($dir))!==FALSE )
		{
		if( strcmp($buf, ".") && strcmp($buf, "..") )
			{
			if( unlink($folder.$buf) ) # eliminamos archivo
				$del++;
			}
		}
	if( $del )		return 1; # eliminados con exito
	return 0; # error  
	}

function url_cleaner( $data, $titulo=false )
	{
	$a= strtolower($data); # pasamos a minusculas 
	$s_letras= array( 'á', 'é', 'í', 'ó', 'ú', 'ñ' ); # buscar letras
	$r_letras= array( 'a', 'e', 'i', 'o', 'u', 'n' ); # sustituir letras 
	$s_signos= array( '/[^a-z0-9_]/' ); # buscar simbolos

	if( $titulo )
		$r_signo= array( '-' );
	else
		$r_signo= array( '_' );
	
	$a= str_replace( $s_letras, $r_letras, $a ); # re-emplazamos letras
	$a= preg_replace( $s_signos, $r_signo, $a ); # re-emplazamos signos
	
	return $a; 
	}

# limpia la cadena para usarla como 'keyword' content en meta tags, sustituye espacios por ','
# y elimina caracteres raros	
function url_cleaner_meta( $data )
	{
	$a= strtolower($data); # pasamos a minusculas 
	$s_letras= array( 'á', 'é', 'í', 'ó', 'ú', 'ñ' ); # buscar letras
	$r_letras= array( 'a', 'e', 'i', 'o', 'u', 'n' ); # sustituir letras 
	$s_signos= array( '/[^a-z0-9_]/' ); # buscar simbolos
	$r_signo= array( ',' );
	
	$a= str_replace( $s_letras, $r_letras, $a ); # re-emplazamos letras
	$a= preg_replace( $s_signos, $r_signo, $a ); # re-emplazamos signos
	
	return $a; 
	}

function currentURL()
	{
	return proteger_cadena($_SERVER["REQUEST_URI"]);
	}

# obtiene la IP del usuario actual y si esta por proxy tambien la IP de este 
function get_ip()
	{
	$ip= $_SERVER['REMOTE_ADDR']; //obtenemos IP
	
	if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
		$proxy_ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if( isset($_SERVER['HTTP_VIA']) )
		$proxy_ip= $_SERVER['HTTP_VIA'];
	else 	 $proxy_ip=0;
	
	if( $proxy_ip ) # si existe IP de proxy
		return $proxy_ip. ','. $ip; # retornamos ambas IPs: IP Real, IP Proxy 
	else	return $ip; # solo IP, ya que es su IP real
	}

# detecta si una IP esta haciendo bruteforcing
function bruteforcing_detect()
	{
	$brute= 5; # limite para reincidencia de bruteforcing
	$timewait= (60*15); # esperar 15 minutos 
	$ip_src= get_ip(); # obtenemos IP
	$ip_proxy=0; # variable para IP del proxy 
	if( strstr($ip_src, ",") ) # si existe delimitador, existe Ip y proxy 
		{
		$x= explode( ",", $ip_src ); # explotamos 
		$ip= $x[0];
		$ip_proxy= $x[1];
		}
	else # solo es IP
		$ip= $ip_src; # metemos IP real/normal
		
	$cons= consultar_enorden_con( "BRUTE", "IP='". proteger_cadena($ip). "'", "FECHA DESC" );
	
	if( mysql_num_rows($cons)<$brute ) # si es menor que el limite
		{
		limpiar($cons); 
		return 0; # no hay problema, puede logearse
		} 
	else
		{
		$buf= mysql_fetch_array($cons); # obtenemos el primer valor (ultimo intento de login)

		if( ($buf["FECHA"]+$timewait) < time() ) # si ha pasado el tiempo de espera por re-incider (bruteforcing)
			{
			eliminar_bdd( "BRUTE", "IP='". proteger_cadena($ip). "'" ); # eliminamos todos los registros de esta IP
			limpiar($cons);
			return 0; # puede logearse 
			}
		limpiar($cons);
		return 1; # se detectaron intentos masivos, no prodra logearse
		} 
	}

# agregue la IP a la BDD de posibles bruteforcing
function bruteforcing_add()
	{
	$ip_src= get_ip(); # obtenemos IP

	do //generamos numero aleatorio
		{
		$idtrack= generar_idtrack(); //obtenemos digito aleatorio
		}while( !strcmp( $idtrack, consultar_datos_general( "BRUTE", "ID='". $idtrack. "'", "ID" ) ) );
	
	$ip_proxy=0; # variable para IP del proxy 
	if( strstr($ip_src, ",") ) # si existe delimitador, existe Ip y proxy 
		{
		$x= explode( ",", $ip_src ); # explotamos 
		$ip= $x[0];
		$ip_proxy= $x[1];
		}
	else # solo es IP
		$ip= $ip_src; # metemos IP real/normal 
	
	$trama= array(
		"id"=>"'". $idtrack. "'", 
		"ip"=>"'". proteger_cadena($ip). "'", 
		"ip_proxy"=>"'". proteger_cadena($ip_proxy). "'", 
		"fecha"=>"'". time(). "'" 
		);
		
	insertar_bdd( "BRUTE", $trama ); # insertamos nueva IP target que hace bruteforcing 
	unset($trama);
	}

# elimina la IP a la BDD de posibles bruteforcing
function bruteforcing_del()
	{
	$ip_src= get_ip(); # obtenemos IP
	$ip_proxy=0; # variable para IP del proxy 
	if( strstr($ip_src, ",") ) # si existe delimitador, existe Ip y proxy 
		{
		$x= explode( ",", $ip_src ); # explotamos 
		$ip= $x[0];
		$ip_proxy= $x[1];
		}
	else # solo es IP
		$ip= $ip_src; # metemos IP real/normal 
	
	# si existe la IP, entonces se elimina de la BDD de Bruteforcing
	if( ($data= consultar_datos_general("BRUTE", "IP='". $ip. "' && IP_PROXY='". $ip_proxy. "'", "ID")) )
		eliminar_bdd( "BRUTE", "ID='". $data. "'" ); # eliminamos la IP
	unset($data, $ip, $ip_proxy, $x);
	}

function acento( $var )
	{
	return '&'. $var. 'acute;';
	}

function make_seed()
	{
  	list($usec, $sec) = explode(' ', microtime());
  	return (float) $sec + ((float) $usec * 100000);
	}


function generar_idtrack()
	{
	srand(make_seed());
	$dimension = rand(4, 10);

	$arr_abc123= array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 
							'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'y', 'z' );
						
	$randval="";
	for( $i=0; $i<$dimension; $i++ )
		$randval .= $arr_abc123[rand(1, count($arr_abc123) )];

	return $randval;
	}

# genera numeros/caracteres de $data["start"] a $data["end"] segun el $data["type"] de datos (caracter, numeros, mixto)
# la longitud en base a $data["longstart"] a $data["longend"] 
function generar_idtrack_rango($data)
	{
	if( !is_array($data) )		$r=0;
	else if( count($data)!=5 )		$r=0;
	else if( !$data["start"] || !$data["end"] || !$data["type"] )		$r=0;
	else if( !is_numeric($data["start"]) || !is_numeric($data["end"]) )		$r=0;
	else
		{
		srand(make_seed());
		$dimension = rand($data["longstart"], $data["longend"]);
		
		if( !strcmp($data["type"], "mixmo") )
			{
			$arr_abc123= array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 
								'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'y', 'z' );
			}
		else if( !strcmp($data["type"], "numeros") )
			{
			$arr_abc123= array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' );
			}
		else if( !strcmp($data["type"], "caracteres") )
			{
			$arr_abc123= array( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 
								'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'y', 'z' );
			}
		else		$arr_abc123= array();
						
		$r="";
		for( $i=0; $i<$dimension; $i++ )
			$r .= $arr_abc123[rand($data["start"], $data["end"])];
		}
	return $r;
	}

function generar_idnumber( $dato )
	{
	#if( $dato ) $maximo= $dato;
	#else $maximo= 10;
	srand(make_seed());
	#$dimension = strlen($maximo);

	#$arr_123= array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' );

	#return $arr_123[rand(1, count($arr_123) )];
	if( $dato==1 )	$r= 0;
	else $r= rand(1, $dato );
	return $r;
	}
	
//generar log del ultimo envio de mail
function last_sendmail_log( $email )
	{
	$file="last_sendmail_log.txt"; //archivo log
	$modo="w"; //abrimos en modo creacion nueva
	
	$fp= fopen( $file, $modo ); //abrimos stream
	fwrite( $fp, $email );
	fclose($fp);
	}

//genera, crea e incrementa log del smtp relay
function generar_log_relay_smtp( $valor )
	{
	$file="log_smtp.txt"; //archivo log

	if( !file_exists($file) ) //no existe archivo
		$modo="w"; //abrimos en modo creacion nueva
	else	$modo="r"; //abrimos en modo lectura

	$fp= fopen( $file, $modo ); //abrimos stream
	if( !strcmp($modo, "r") ) //si es en modo lectura
		{
		$buf= fgets( $fp, 1024 ); //leemos linea del archivo
		$x= explode( ":", $buf ); //explotamos 
		$tiempo= $x[0]; //tomamos tiempo 
		$cont= $x[1]; //tomamos valor contador 
		unset($x);
		unset($buf);
		fclose($fp); //cerramos stream
		unset($fp); //limpiamos
		
		$fp= fopen( $file, "w" ); //abrimos stream en modo escritura 
		}
	else //es modo escritura, nuevo log
		$tiempo= time(); //establecemos tiempo 

	//escribimos valores
	if( !strcmp( date("d/m/y", time()), date("d/m/y", $tiempo) ) ) //si la fecha d/m/y son iguales 
		$cont= ($cont+$valor); //incrementamos contador
	else
		{
		$tiempo= time();
		$cont= $valor;
		}
		
	$buf= $tiempo. ':'. $cont; //creamos buffer
	fputs( $fp, $buf ); //escribimos buffer
	
	fclose($fp);
	unset($modo);
	unset($buf);
	unset($file);
	
	return $cont; //retornamos el contador
	}

# navegador movil
function get_navegador($t)
	{
	$r=0;

	# $nav= $_SERVER['HTTP_USER_AGENT'];
	$nav= get_browser(null, true); //navegador

	if( strcmp( $_SERVER['HTTP_REFERER'], "" ) )
		$ref= strtolower($_SERVER['HTTP_REFERER']);
	else
		$ref= strtolower($_SERVER['HTTP_HOST']);

	while( list($key, $val)=each($nav) )
		{
		if( !strcmp($key, "parent" ) )
			$navegador= $val;
		else if( !strcmp($key, "platform" ) )
			$so= $val;
		}

	if( !strcmp( $t, "all" ) ) # todo el array
		$r= $nav;
	else if( !strcmp($t, "name") ) # nombre del navegador
		$r= $navegador;
	else if( !strcmp($t, "version") ) # version del navegador
		$r= $navegador;
	else if( !strcmp($t, "os") ) # sistema operativo
		$r= $so;
	else if( !strcmp($t, "tipo") ) # tipo de navegador: movil o desktop
		{
		if( strstr(strtolower($so), "and") )					$r= 'movil';
		else if( strstr(strtolower($so), "ipho") )			$r= 'movil';
		else if( strstr(strtolower($so), "symb") )			$r= 'movil';
		else if( strstr(strtolower($so), "blackberry") )	$r= 'movil';
		else if( strstr(strtolower($so), "nok") )			$r= 'movil';
		else if( strstr(strtolower($so), "sams") )			$r= 'movil';
		else		$r= 'desktop';
		}

	unset($nav, $so, $navegador);
	return $r;
	}

//Funcion para comprobar que es un usuario logeado legitimo 
function is_login()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "ID" );
	if( !strcmp( $data, $_SESSION["log_id"] ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}
	
//Funcion para comprobar que es administrador
function is_admin()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "TIPO_USR" );
	if( !strcmp( $data, "1" ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}

//Funcion para comprobar que es co-admin
function is_coadmin()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "TIPO_USR" );
	if( !strcmp( $data, "4" ) || !strcmp( $data, "1" ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}

function is_autor()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "TIPO_USR" );
	if( !strcmp( $data, "2" ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}

function is_editor()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "TIPO_USR" );
	if( !strcmp( $data, "3" ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}

function is_soporte()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "TIPO_USR" );
	if( !strcmp( $data, "5" ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}

function is_usuario()
	{
	# 1=> admin, 2=>autor, 3=>editor, 4=>coadmin, 5=>soport, 6=>usuario
	
	$data= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "TIPO_USR" );
	if( !strcmp( $data, "10" ) )
		{
		unset($data);
		return 1;
		}
	unset($data);
	return 0;
	}

//Funcion para comprobar si es un robot 
function is_a_robot( $nav )
	{
	# lista de robots 
	$r=0;
	$robots= array( 
			"google"=>"google", 
			"slurp"=>"yahoo",
			"yahoo"=>"yahoo", 
			"scooter"=>"altavista", 
			"lnktomi"=>"altavista", 
			"baidu"=>"baidu", 
			"yandex"=>"yandex", 
			"msn"=>"bing", 
			"rippers"=>"robot rippers", 
			"feeds blogs"=>"feed", 
			"sogou"=>"sogou", 
			"entireweb"=>"entireweb", 
			"general crawler"=>"general crawler", 
			"WOW64"=>"ripe", 
			"Ahrefs"=>"ahrefbot", 
			"SemrushBot"=>"semrushbot", 
			"commentreader"=>"feedcrawler", 
			"Jorgee"=>"otros", 
			"ZmEu"=>"otros", 
			"sysscan"=>"otros", 
			"MJ12bot"=>"mj12bot", 
			"wget"=>"wget", 
			"filterdb"=>"oBot", 
			"facebookexternalhit"=>"facebook", 
			"SBSearch"=>"enginelabs", 
			"DotBot"=>"botbot", 
			"Uptimebot"=>"uptimebot", 
			"CrawlBot"=>"crawlbot", 
			"Baiduspider"=>"baidu", 
			"aiHitBot"=>"hitbot", 
			"Test Certificate"=>"certester", 
			"DNSBot"=>"dnsbot", 
			"Leikibot"=>"leikibot", 
			"SV1"=>"ripe_sv1", 
			"PythonBot"=>"PythonBot", 
			"python-requests"=>"PythonBot", 
			"Python-urllib"=>"PythonBot", 
			"Laserlikebot"=>"LaserBot", 
			"evc-batch"=>"EvcBot", 
			"BamBam"=>"BamBoth", 
			"NE Crawler"=>"NEBoth", 
			"GrapeshotCrawler"=>"GrapeshotBoth",
			"BLEXBot"=>"blexbot",  
			"DomainCrawler"=>"domaincrawler", 
			"Jakarta"=>"jakarta", 
			"MegaIndex.ru"=>"megandex", 
			"Mail.RU_Bot"=>"mailRU_bot", 
			"TweetmemeBot"=>"tweetmemebot", 
			"netEstate"=>"netestate", 
			"datenbank"=>"datenbank", 
			"NetcraftSurveyAgent"=>"netcraft", 
			"Nimbostratus"=>"nimbostratus", 
			"Nimbostratus-Bot"=>"nimbostratus", 
			"CheckMarkNetwork"=>"checkmark", 
			"DareBoost"=>"dareboost", 
			"infegy.com"=>"infegy", 
			"SerendeputyBot"=>"serendeputy", 
			"FB_IAB"=>"facebook", 
			"Scoop"=>"scoop", 
			"Nuzzel"=>"amazon", 
			"Qwantify"=>"qwantify", 
			"MALNJS"=>"malnjs", 
			"VelenPublicWebCrawler"=>"velenpublic"
			);

	# crawlers por IP
	$botips= array(
		"tencent"=>array(
			"49.232.0.0"=>"49.235.255.255", 
			), 
		"eset"=>array(
			"91.228.164.0"=>"91.228.167.255", 
			), 
		"china"=>array(
			"110.240.0.0"=>"110.255.255.255", 
			"220.243.0.0"=>"220.243.255.255", 
			"171.8.0.0"=>"171.15.255.255", 
			"221.224.0.0"=>"221.231.255.255", 
			"39.96.0.0"=>"39.108.255.255"
			), 
		"kaspersky"=>array(
			"77.74.176.0"=>"77.74.179.255"
			), 
		"facebook"=>array(
			"31.13.66.0"=>"31.13.66.255", 
			"69.171.224.0"=>"69.171.255.255", 
			"157.240.0.0"=>"157.240.255.255", 
			"173.252.64.0"=>"173.252.127.255", 
			"66.220.144.0"=>"66.220.159.255", 
			"31.13.115.0"=>"31.13.115.255"
			), 
		"linkedin"=>array(
			"108.174.0.0"=>"108.174.15.255"
			), 
		"google"=>array(
			"34.64.0.0"=>"34.127.255.255", 
			"35.208.0.0"=>"35.247.255.255", 
			"104.132.0.0"=>"104.135.255.255", 
			"64.233.160.0"=>"64.233.191.255", 
			"66.102.0.0"=>"66.102.15.255", 
			"66.249.64.0"=>"66.249.95.255", 
			"74.125.0.0"=>"74.125.255.255", 
			"108.177.0.0"=>"108.177.127.255", 
			"173.194.0.0"=>"173.194.255.255", 
			"216.239.32.0"=>"216.239.63.255", 
			"216.58.192.0"=>"216.58.223.255", 
			"72.14.192.0"=>"72.14.255.255", 
			"209.85.128.0"=>"209.85.255.255", 
			"35.184.0.0"=>"35.191.255.255", 
			"35.192.0.0"=>"35.207.255.255", 
			"104.196.0.0"=>"104.199.255.255", 
			"130.211.0.0"=>"130.211.255.255"
			), 
		"bing"=>array(
			"157.54.0.0"=>"157.60.255.255", 
			"207.46.0.0"=>"207.46.255.255", 
			"40.74.0.0"=>"40.125.127.255", 
			"23.96.0.0"=>"23.103.255.255", 
			"40.74.0.0"=>"40.125.127.255", 
			"13.64.0.0"=>"13.107.255.255", 
			"137.135.0.0"=>"137.135.255.255", 
			"104.40.0.0"=>"104.47.255.255", 
			"52.145.0.0"=>"52.191.255.255", 
			"131.253.21.0"=>"131.253.47.255", 
			"65.52.0.0"=>"65.55.255.255"
			), 
		"ovh"=>array(
			"91.121.160.0"=>"91.121.175.255", 
			"69.58.176.0"=>"69.58.191.255", 
			"37.187.0.0"=>"37.187.31.255", 
			"145.239.123.0"=>"145.239.123.127", 
			"51.77.128.0"=>"51.77.131.255",  
			"149.56.0.0"=>"149.56.255.255", 
			"178.32.96.0"=>"178.32.127.255", 
			"188.165.32.0"=>"188.165.47.255", 
			"51.254.0.0"=>"51.255.255.255", 
			"37.187.160.0"=>"37.187.175.255", 
			"164.132.0.0"=>"164.132.255.255", 
			"151.80.32.0"=>"151.80.47.255", 
			"94.23.112.220"=>"94.23.112.223", 
			"91.134.0.0"=>"91.134.255.255", 
			"91.134.0.0"=>"91.134.255.255", 
			"158.69.0.0"=>"158.69.255.255", 
			"192.99.0.0"=>"192.99.255.255", 
			"142.4.192.0"=>"142.4.223.255", 
			"167.114.0.0"=>"167.114.255.255", 
			"192.99.0.0"=>"192.99.255.255", 
			"151.80.16.0"=>"151.80.31.255", 
			"198.27.64.0"=>"198.27.127.255", 
			"176.31.59.228"=>"176.31.59.231", 
			"178.33.191.168"=>"178.33.191.171", 
			"66.70.128.0"=>"66.70.255.255", 
			"149.202.0.0"=>"149.202.255.255", 
			"178.33.32.0"=>"178.33.39.255", 
			"188.165.65.128"=>"188.165.65.191", 
			"54.36.0.0"=>"54.39.255.255", 
			"144.217.0.0"=>"144.217.255.255", 
			"144.217.160.0"=>"144.217.167.255", 
			"145.239.80.0"=>"145.239.95.255", 
			"37.187.56.0"=>"37.187.57.255", 
			"176.31.224.0"=>"176.31.255.255", 
			"5.135.213.200"=>"5.135.213.203", 
			"5.39.54.232"=>"5.39.54.235", 
			"37.59.89.236"=>"37.59.89.239", 
			"80.252.171.64"=>"80.252.171.95", 
			"142.44.210.0"=>"142.44.211.255", 
			"142.44.128.0"=>"142.44.255.255", 
			"46.105.31.0"=>"46.105.31.255", 
			"46.105.172.8"=>"46.105.172.11", 
			"178.33.183.56"=>"178.33.183.59", 
			"178.33.169.4"=>"178.33.169.7", 
			"92.222.64.0"=>"92.222.95.255", 
			"213.32.0.0"=>"213.32.127.255", 
			"147.135.136.0"=>"147.135.139.255"
			), 
		"duckduckgo"=>array(
			"52.0.0.0"=>"52.31.255.255", 
			"50.16.0.0"=>"50.19.255.255"
			), 
		"ripe"=>array(
			"167.160.72.0"=>"167.160.79.255", 
			"167.160.75.0"=>"167.160.75.255", 
			"193.111.198.0"=>"193.111.199.255", 
			"191.101.96.1"=>"191.101.111.255", 
			"155.133.128.0"=>"155.133.191.255", 
			"104.168.35.128"=>"104.168.35.255", 
			"104.168.0.0"=>"104.168.127.255", 
			"46.43.192.0"=>"46.43.241.255", 
			"46.16.9.0"=>"46.16.9.255", 
			"85.234.8.0"=>"85.234.11.255", 
			"145.255.0.0"=>"145.255.15.255", 
			"178.46.128.0"=>"178.46.143.255", 
			"92.52.128.0"=>"92.52.159.255", 
			"95.167.3.32"=>"95.167.3.47", 
			"84.201.248.0"=>"84.201.255.255", 
			"37.214.24.0"=>"37.214.87.255", 
			"37.26.136.0"=>"37.26.143.255", 
			"77.93.60.0"=>"77.93.60.255", 
			"109.197.248.0"=>"109.197.255.255", 
			"217.65.214.200"=>"217.65.214.207",  
			"95.172.104.0"=>"95.172.111.255", 
			"109.68.186.0"=>"109.68.186.15", 
			"94.25.228.0"=>"94.25.228.255", 
			"31.133.48.0"=>"31.133.55.255", 
			"91.241.192.0"=>"91.241.255.255", 
			"109.194.200.0"=>"109.194.207.255", 
			"188.18.12.0"=>"188.18.13.255", 
			"89.108.99.0"=>"89.108.99.255", 
			"46.4.88.128"=>"46.4.88.159", 
			"195.154.0.0"=>"195.154.127.255", 
			"62.46.0.0"=>"62.46.255.255", 
			"85.10.192.0"=>"85.10.207.255", 
			"159.69.0.0"=>"159.69.255.255", 
			"5.9.106.192"=>"5.9.106.223", 
			"82.165.168.0"=>"82.165.172.255", 
			"188.40.131.128"=>"188.40.131.191", 
			"194.44.61.0"=>"194.44.61.255", 
			"46.165.200.0"=>"46.165.255.255", 
			"37.58.48.0"=>"37.58.55.255", 
			"185.25.34.128"=>"185.25.34.255", 
			"5.188.62.0"=>"5.188.62.255", 
			"169.45.0.0"=>"169.48.255.255", 
			"31.13.64.0"=>"31.13.127.255", 
			"185.172.148.0"=>"185.172.148.255", 
			"95.216.20.64"=>"95.216.20.127", 
			"46.229.168.0"=>"46.229.169.255", 
			"62.210.128.0"=>"62.210.255.255", 
			"144.76.0.0"=>"144.76.255.255", 
			"137.74.0.0"=>"137.74.255.255", 
			"77.67.54.0"=>"77.67.54.15", 
			"79.184.0.0"=>"79.184.255.255", 
			"195.22.124.0"=>"195.22.127.255", 
			"2.138.0.0"=>"2.139.255.255", 
			"194.60.69.0"=>"194.60.69.255", 
			"88.0.0.0"=>"88.15.255.255", 
			"84.78.0.0"=>"84.79.255.255", 
			"188.226.128.0"=>"188.226.191.255", 
			"163.172.0.0"=>"163.172.255.255", 
			"31.13.114.0"=>"31.13.114.255", 
			"93.73.0.0"=>"93.73.127.255", 
			"85.105.0.0"=>"85.105.255.255", 
			"73.0.0.0"=>"73.255.255.255", 
			"73.58.128.0"=>"73.58.255.255", 
			"31.13.114.0"=>"31.13.114.255", 
			"88.99.0.0"=>"88.99.255.255", 
			"46.165.192.0"=>"46.165.199.255", 
			"70.39.128.0"=>"70.39.255.255", 
			"70.39.157.192"=>"70.39.157.223", 
			"77.66.1.96"=>"77.66.1.103", 
			"5.9.88.96"=>"5.9.88.127", 
			"217.182.0.0"=>"217.182.255.255", 
			"194.187.168.0"=>"194.187.171.255", 
			"51.36.0.0"=>"51.36.255.255", 
			"62.210.0.0"=>"62.210.127.255", 
			"94.199.151.16"=>"94.199.151.31", 
			"148.251.0.0"=>"148.251.255.255", 
			"176.10.99.192"=>"176.10.99.223", 
			"212.62.42.0"=>"212.62.42.255", 
			"178.33.169.232"=>"178.33.169.235", 
			"213.239.210.0"=>"213.239.211.255", 
			"185.170.42.0"=>"185.170.42.255", 
			"217.69.128.0"=>"217.69.135.255", 
			"83.32.0.0"=>"83.39.255.255", 
			"79.168.0.0"=>"79.168.255.255", 
			"5.196.0.0"=>"5.196.255.255", 
			"94.228.205.64"=>"94.228.205.127", 
			"93.174.123.128"=>"93.174.123.255", 
			"94.23.168.0"=>"94.23.175.255", 
			"31.177.95.0"=>"31.177.95.255", 
			"80.248.225.128"=>"80.248.225.191", 
			"95.221.0.0"=>"95.221.255.255", 
			"89.144.0.0"=>"89.144.63.255", 
			"93.160.60.0"=>"93.160.60.255", 
			"176.10.104.239"=>"176.10.104.241", 
			"176.126.252.8"=>"176.126.252.15", 
			"188.165.192.0"=>"188.165.255.255", 
			"95.83.160.0"=>"95.83.191.255", 
			"46.105.96.0"=>"46.105.127.255", 
			"185.117.118.0"=>"185.117.118.255", 
			"37.187.32.0"=>"37.187.35.255", 
			"185.87.185.0"=>"185.87.185.255", 
			"5.135.193.212"=>"5.135.193.215", 
			"87.116.176.0"=>"87.116.191.255", 
			"64.119.157.112"=>"64.119.157.127", 
			"64.119.128.0"=>"64.119.159.255", 
			"91.247.38.0"=>"91.247.38.255", 
			"77.75.76.0"=>"77.75.76.255", 
			"138.201.0.0"=>"138.201.255.255", 
			"185.163.0.0"=>"185.163.3.255", 
			"85.102.40.0"=>"85.102.255.255", 
			"83.56.0.0"=>"83.59.255.255", 
			"51.15.0.0"=>"51.15.255.255", 
			"178.17.168.0"=>"178.17.175.255", 
			"93.159.230.0"=>"93.159.231.255", 
			"78.109.16.0"=>"78.109.31.255", 
			"5.199.130.0"=>"5.199.130.255", 
			"78.24.220.0"=>"78.24.223.255", 
			"93.115.92.0"=>"93.115.95.255", 
			"138.246.253.0"=>"138.246.253.255", 
			"37.187.72.0"=>"37.187.79.255", 
			"83.140.112.0"=>"83.140.112.31", 
			"146.185.223.0"=>"146.185.223.255", 
			"185.31.172.234"=>"185.31.172.234", 
			"185.70.8.0"=>"185.70.11.255", 
			"188.40.126.64"=>"188.40.126.127", 
			"188.40.126.64"=>"188.40.126.127", 
			"37.58.56.0"=>"37.58.63.255", 
			"195.154.128.0"=>"195.154.255.255", 
			"83.24.0.0"=>"83.24.255.255", 
			"77.222.96.0"=>"77.222.103.255", 
			"94.130.0.0"=>"94.130.255.255", 
			"109.74.10.0"=>"109.74.13.255", 
			"93.39.201.0"=>"93.39.201.255", 
			"217.69.143.0"=>"217.69.143.255", 
			"188.122.73.192"=>"188.122.73.255", 
			"217.73.208.0"=>"217.73.215.255", 
			"46.166.162.0"=>"46.166.162.255", 
			"85.255.64.0"=>"85.255.79.255", 
			"82.80.246.0"=>"82.80.255.255", 
			"80.89.203.0"=>"80.89.203.255", 
			"80.12.83.0"=>"80.12.83.255", 
			"217.91.128.0"=>"217.92.255.255", 
			"139.162.0.0"=>"139.162.255.255", 
			"94.102.49.0"=>"94.102.49.255", 
			"83.80.0.0"=>"83.87.255.255", 
			"90.160.0.0"=>"90.175.255.255", 
			"113.28.1.216"=>"113.28.1.223", 
			"194.186.107.0"=>"194.186.107.255", 
			"80.187.0.0"=>"80.187.111.255", 
			"93.94.92.0"=>"93.94.95.255", 
			"91.200.12.0"=>"91.200.15.255", 
			"80.123.68.172"=>"80.123.68.175", 
			"81.215.200.0"=>"81.215.207.255", 
			"2.34.0.0"=>"2.35.255.255", 
			"80.14.11.0"=>"80.14.11.255", 
			"183.160.0.0"=>"183.167.255.255", 
			"195.177.226.0"=>"195.177.227.255", 
			"217.174.177.128"=>"217.174.178.15", 
			"87.197.160.0"=>"87.197.191.255", 
			"178.72.240.0"=>"178.72.247.255", 
			"91.100.0.0"=>"91.101.255.255", 
			"195.62.52.0"=>"195.62.53.255", 
			"213.56.237.23"=>"213.56.237.23", 
			"188.246.96.0"=>"188.246.111.255", 
			"37.46.164.128"=>"37.46.164.255", 
			"92.220.0.0"=>"92.221.255.255", 
			"93.192.0.0"=>"93.223.255.255", 
			"169.54.233.112"=>"169.54.233.127", 
			"185.40.4.0"=>"185.40.4.255", 
			"109.236.84.0"=>"109.236.84.255", 
			"141.212.0.0"=>"141.212.255.255", 
			"141.212.122.0"=>"141.212.122.255", 
			"185.110.132.0"=>"185.110.132.255", 
			"78.129.128.0"=>"78.129.255.255", 
			"213.164.208.0"=>"213.164.223.255", 
			"185.100.87.0"=>"185.100.87.255", 
			"79.191.0.0"=>"79.191.255.255", 
			"82.238.88.0"=>"82.238.91.255", 
			"62.90.131.0"=>"62.90.131.255", 
			"213.186.0.0"=>"213.186.31.255", 
			"136.144.128.0"=>"136.144.255.255", 
			"85.195.246.0"=>"85.195.246.255", 
			"78.46.32.0"=>"78.46.63.255", 
			"5.39.30.12"=>"5.39.30.15", 
			"212.47.240.0"=>"212.47.255.255", 
			"213.239.216.0"=>"213.239.219.255", 
			"91.230.41.0"=>"91.230.41.255", 
			"178.32.0.0"=>"178.33.255.255", 
			"31.13.106.0"=>"31.13.106.255", 
			"31.13.113.0"=>"31.13.113.255", 
			"89.40.115.0"=>"89.40.115.255", 
			"94.177.248.0"=>"94.177.248.255", 
			"94.177.247.0"=>"94.177.247.255", 
			"89.36.220.0"=>"89.36.220.255", 
			"185.153.196.0"=>"185.153.199.255", 
			"185.203.242.0"=>"185.203.242.127", 
			"81.19.188.224"=>"81.19.188.239", 
			"109.92.130.0"=>"109.92.131.255", 
			"185.20.6.96"=>"185.20.6.127", 
			"46.229.170.0"=>"46.229.170.255", 
			"46.17.46.0"=>"46.17.47.255", 
			"217.69.136.0"=>"217.69.141.255", 
			"85.248.227.160"=>"85.248.227.175", 
			"5.188.10.0"=>"5.188.11.255", 
			"188.209.49.0"=>"188.209.49.255", 
			"84.19.176.0"=>"84.19.191.255", 
			"171.25.193.0"=>"171.25.193.255", 
			"83.40.0.0"=>"83.43.255.255", 
			"151.80.238.152"=>"151.80.238.159", 
			"185.129.62.0"=>"185.129.62.255", 
			"94.23.0.0"=>"94.23.255.255", 
			"81.170.140.0"=>"81.170.141.255", 
			"5.79.64.0"=>"5.79.127.255", 
			"79.137.64.0"=>"79.137.67.255", 
			"81.205.0.0"=>"81.205.255.255", 
			"183.82.104.1"=>"183.82.111.254", 
			"185.38.12.0"=>"185.38.15.255", 
			"213.183.51.0"=>"213.183.51.255", 
			"134.213.168.0"=>"134.213.168.255", 
			"80.209.252.0"=>"80.209.253.255", 
			"107.178.192.0"=>"107.178.255.255", 
			"182.224.0.0"=>"182.231.255.255", 
			"193.144.97.0"=>"193.144.97.255", 
			"90.139.0.0"=>"90.139.95.255", 
			"89.35.39.0"=>"89.35.39.127", 
			"82.223.0.0"=>"82.223.255.255", 
			"91.137.16.0"=>"91.137.31.255", 
			"188.40.120.0"=>"188.40.120.63", 
			"80.111.0.0"=>"80.111.255.255", 
			"173.212.192.0"=>"173.212.255.255", 
			"193.70.0.0"=>"193.70.127.255", 
			"87.124.192.0"=>"87.125.255.255", 
			"139.47.0.0"=>"139.47.159.255", 
			"37.59.229.24"=>"37.59.229.27", 
			"46.36.37.0"=>"46.36.37.255", 
			"158.46.200.0"=>"158.46.207.255", 
			"94.186.64.0"=>"94.186.127.255", 
			"203.81.64.0"=>"203.81.79.255", 
			"31.43.128.0"=>"31.43.159.255", 
			"87.244.44.0"=>"87.244.47.255", 
			"89.208.144.0"=>"89.208.159.255", 
			"5.165.16.0"=>"5.165.23.255", 
			"94.231.112.0"=>"94.231.119.255", 
			"87.103.204.0"=>"87.103.207.255", 
			"185.16.28.0"=>"185.16.29.255", 
			"176.215.0.0"=>"176.215.7.255", 
			"188.235.104.0"=>"188.235.111.255", 
			"176.51.0.0"=>"176.51.255.255", 
			"46.39.46.0"=>"46.39.46.255", 
			"62.183.80.0"=>"62.183.87.255", 
			"194.187.28.0"=>"194.187.28.255", 
			"194.114.144.0"=>"194.114.144.255", 
			"178.123.0.0"=>"178.123.255.255", 
			"82.208.111.0"=>"82.208.111.255", 
			"95.153.128.0"=>"95.153.143.255", 
			"94.159.17.0"=>"94.159.17.255", 
			"85.113.208.0"=>"85.113.221.255"
			), 
		"apnich"=>array(
			"36.4.0.0"=>"36.7.255.255", 
			"171.116.0.0"=>"171.119.255.255", 
			"118.81.80.0"=>"118.81.95.255", 
			"110.88.0.0"=>"110.91.255.255", 
			"118.140.0.0"=>"118.143.255.255", 
			"111.85.0.0"=>"111.85.255.255", 
			"112.66.64.0"=>"112.66.95.255", 
			"115.206.0.0"=>"115.206.255.255", 
			"60.191.32.0"=>"60.191.47.255", 
			"218.2.0.0"=>"218.4.255.255", 
			"123.16.0.0"=>"123.31.255.255", 
			"218.94.37.40"=>"218.94.37.43",
			"103.212.220.0"=>"103.212.223.255",
			"58.21.0.0"=>"58.21.255.255", 
			"36.32.0.0"=>"36.35.255.255", 
			"123.125.71.0"=>"123.125.71.255", 
			"220.181.0.0"=>"220.181.255.255", 
			"180.152.0.0"=>"180.159.255.255", 
			"180.76.0.0"=>"180.76.255.255", 
			"116.252.0.0"=>"116.253.255.255", 
			"123.144.0.0"=>"123.147.255.255", 
			"171.36.0.0"=>"171.39.255.255", 
			"114.240.0.0"=>"114.255.255.255", 
			"139.170.0.0"=>"139.170.255.255", 
			"175.16.0.0"=>"175.23.255.255", 
			"123.188.0.0"=>"123.191.255.255", 
			"125.76.0.0"=>"125.76.127.255", 
			"111.160.0.0"=>"111.167.255.255", 
			"106.45.0.0"=>"106.45.255.255", 
			"125.211.0.0"=>"125.211.255.255", 
			"61.146.178.0"=>"61.146.178.255", 
			"42.224.0.0"=>"42.239.255.255", 
			"106.37.0.0"=>"106.39.255.255", 
			"136.243.0.0"=>"136.243.255.255", 
			"110.80.0.0"=>"110.87.255.255", 
			"218.30.96.0"=>"218.30.127.255", 
			"106.11.0.0"=>"106.11.255.255", 
			"210.245.0.0"=>"210.245.15.255", 
			"202.46.32.0"=>"202.46.63.255", 
			"103.16.46.0"=>"103.16.46.255", 
			"118.99.96.0"=>"118.99.96.255", 
			"182.74.166.152"=>"182.74.166.159",
			"121.201.0.0"=>"121.201.127.255", 
			"58.19.0.0"=>"58.19.255.255", 
			"120.32.0.0"=>"120.39.255.255", 
			"60.0.0.0"=>"60.10.255.255", 
			"106.80.0.0"=>"106.95.255.255", 
			"123.51.128.0"=>"123.51.255.255", 
			"113.112.0.0"=>"113.119.255.255", 
			"119.23.0.0"=>"119.23.255.255", 
			"175.42.0.0"=>"175.42.63.255", 
			"123.112.0.0"=>"123.127.255.255", 
			"47.92.0.0"=>"47.95.255.255", 
			"27.192.0.0"=>"27.223.255.255", 
			"106.51.192.0"=>"106.51.223.255", 
			"106.128.0.0"=>"106.191.255.255", 
			"106.120.0.0"=>"106.121.255.255", 
			"117.32.0.0"=>"117.39.255.255", 
			"101.16.0.0"=>"101.31.255.255", 
			"116.113.64.0"=>"116.113.79.255", 
			"112.224.0.0"=>"112.255.255.255", 
			"43.247.156.0"=>"43.247.159.255", 
			"180.96.0.0"=>"180.127.255.255", 
			"103.198.136.0"=>"103.198.139.255", 
			"123.255.248.0"=>"123.255.251.255", 
			"183.0.0.0"=>"183.63.255.255", 
			"59.31.105.128"=>"59.31.105.255", 
			"59.0.0.0"=>"59.31.255.255", 
			"210.14.64.0"=>"210.14.95.255", 
			"101.236.0.0"=>"101.236.255.255", 
			"122.114.0.0"=>"122.114.255.255", 
			"125.137.88.224"=>"125.137.88.255", 
			"125.128.0.0"=>"125.159.255.255", 
			"114.144.0.0"=>"114.159.255.255", 
			"223.216.0.0"=>"223.219.255.255", 
			"222.184.0.0"=>"222.191.255.255", 
			"112.80.0.0"=>"112.87.255.255", 
			"112.66.104.0"=>"112.66.107.255", 
			"112.66.96.0"=>"112.66.103.255", 
			"117.50.0.0"=>"117.50.255.255", 
			"14.32.0.0"=>"14.95.255.255", 
			"202.123.177.0"=>"202.123.177.255", 
			"106.72.0.0"=>"106.73.255.255", 
			"103.6.236.0"=>"103.6.239.255", 
			"61.160.0.0"=>"61.160.255.255", 
			"123.59.0.0"=>"123.59.255.255", 
			"220.129.0.0"=>"220.143.255.255", 
			"114.30.96.0"=>"114.30.127.255", 
			"183.192.0.0"=>"183.255.255.255", 
			"103.78.132.0"=>"103.78.133.255", 
			"60.191.49.184"=>"60.191.49.191", 
			"182.128.0.0"=>"182.143.255.255", 
			"61.158.128.0"=>"61.158.255.255", 
			"164.52.0.0"=>"164.52.127.255", 
			"39.64.0.0"=>"39.95.255.255", 
			"219.128.0.0"=>"219.137.255.255", 
			"101.100.152.0"=>"101.100.159.255", 
			"101.198.0.0"=>"101.199.255.255", 
			"58.218.190.16"=>"58.218.190.23", 
			"121.97.136.0"=>"121.97.143.255", 
			"113.64.0.0"=>"113.95.255.255", 
			"106.75.0.0"=>"106.75.255.255", 
			"59.172.0.0"=>"59.173.255.255", 
			"175.152.0.0"=>"175.155.255.255"
			), 
		"amazon"=>array(
			"13.248.0.0"=>"13.251.255.255", 
			"13.250.0.0"=>"13.251.255.255", 
			"174.129.0.0"=>"174.129.255.255", 
			"18.236.0.0"=>"18.237.255.255", 
			"18.128.0.0"=>"18.255.255.255", 
			"3.128.0.0"=>"3.255.255.255", 
			"3.208.0.0"=>"3.223.255.255", 
			"3.0.0.0"=>"3.127.255.255", 
			"54.144.0.0"=>"54.159.255.255", 
			"52.32.0.0"=>"52.63.255.255", 
			"34.192.0.0"=>"34.255.255.255", 
			"54.208.0.0"=>"54.209.255.255", 
			"35.160.0.0"=>"35.167.255.255", 
			"35.152.0.0"=>"35.183.255.255", 
			"52.192.0.0"=>"52.223.255.255", 
			"54.224.0.0"=>"54.239.255.255", 
			"52.84.0.0"=>"52.95.255.255", 
			"54.160.0.0"=>"54.175.255.255", 
			"54.72.0.0"=>"54.95.255.255", 
			"54.176.0.0"=>"54.191.255.255", 
			"54.210.0.0"=>"54.211.255.255", 
			"54.208.0.0"=>"54.221.255.255", 
			"54.204.0.0"=>"54.205.255.255", 
			"54.192.0.0"=>"54.207.255.255", 
			"107.20.0.0"=>"107.23.255.255", 
			"13.52.0.0"=>"13.59.255.255", 
			"184.72.0.0"=>"184.73.255.255", 
			"54.64.0.0"=>"54.71.255.255", 
			"18.219.0.0"=>"18.228.255.255", 
			"79.125.0.0"=>"79.125.63.255", 
			"18.194.0.0"=>"18.195.255.255", 
			"18.194.0.0"=>"18.197.255.255", 
			"52.64.0.0"=>"52.79.255.255", 
			"54.240.0.0"=>"54.255.255.255", 
			"54.251.0.0"=>"54.251.255.255", 
			"204.236.128.0"=>"204.236.255.255", 
			"113.128.0.0"=>"113.129.255.255", 
			"112.0.0.0"=>"112.63.255.255"
			), 
		"alibaba"=>array(
			"47.88.0.0"=>"47.91.255.255"
			), 
		"generalcrawler"=>array(
			"192.3.205.160"=>"192.3.205.191", 
			"192.3.0.0"=>"192.3.255.255", 
			"212.47.224.0"=>"212.47.239.255", 
			"176.221.52.0"=>"176.221.52.255", 
			"173.192.0.0"=>"173.193.255.255", 
			"93.63.162.96"=>"93.63.162.103", 
			"46.246.0.0"=>"46.246.127.255",
			"107.172.0.0"=>"107.175.255.255", 
			"50.112.0.0"=>"50.112.255.255", 
			"75.126.0.0"=>"75.126.255.255", 
			"75.126.154.0"=>"75.126.154.15", 
			"37.120.158.0"=>"37.120.158.255", 
			"66.96.192.0"=>"66.96.223.0", 
			"178.63.0.192"=>"178.63.0.255", 
			"182.77.0.0"=>"182.77.63.255",
			"122.161.0.0"=>"122.161.255.255", 
			"185.145.66.0"=>"185.145.67.255", 
			"191.101.76.1"=>"191.101.79.255", 
			"206.180.160.0"=>"206.180.191.255", 
			"51.158.0.0"=>"51.158.255.255", 
			"95.216.18.192"=>"95.216.18.255", 
			"185.210.218.0"=>"185.210.218.255", 
			"104.222.32.0"=>"104.222.47.255", 
			"162.210.192.0"=>"162.210.199.255", 
			"173.249.0.0"=>"173.249.63.255", 
			"217.80.0.0"=>"217.86.127.255", 
			"104.237.192.0"=>"104.237.223.255", 
			"23.88.0.0"=>"23.89.255.255", 
			"23.89.245.0"=>"23.89.245.255", 
			), 
		"yahoo"=>array(
			"68.180.128.0"=>"68.180.255.255", 
			"72.30.0.0"=>"72.30.255.255", 
			"74.6.0.0"=>"74.6.255.255"
			), 
		"exaled"=>array(
			"178.255.208.0"=>"178.255.215.255", 
			"67.227.128.0"=>"67.227.255.255", 
			"69.39.224.0"=>"69.39.239.255"
			), 
		"twitter"=>array(
			"199.16.156.0"=>"199.16.159.255", 
			"199.59.148.0"=>"199.59.151.255"
			), 
		"apple"=>array(
			"17.0.0.0"=>"17.255.255.255"
			), 
		"yandex"=>array(
			"77.88.47.0"=>"77.88.47.255", 
			"5.255.250.0"=>"5.255.250.255", 
			"84.201.133.0"=>"84.201.133.127", 
			"141.8.144.0"=>"141.8.144.255", 
			"93.158.161.0"=>"93.158.161.255", 
			"141.8.143.128"=>"141.8.143.255", 
			"77.88.5.0"=>"77.88.5.255", 
			"79.111.0.0"=>"79.111.127.255", 
			"37.144.0.0"=>"37.147.255.255"
			), 
		"MIT"=>array(
			"128.30.0.0"=>"128.30.255.255"
			), 
		"DigitalOcean"=>array(
			"167.99.0.0"=>"167.99.255.255", 
			"104.236.0.0"=>"104.236.255.255", 
			"192.241.128.0"=>"192.241.255.255", 
			"104.131.0.0"=>"104.131.255.255", 
			"138.68.0.0"=>"138.68.255.255", 
			"138.197.0.0"=>"138.197.255.255", 
			"45.55.0.0"=>"45.55.255.255", 
			"162.243.0.0"=>"162.243.255.255", 
			"165.227.0.0"=>"165.227.255.255", 
			"174.138.0.0"=>"174.138.127.255", 
			"159.203.0.0"=>"159.203.255.255", 
			"67.207.64.0"=>"67.207.95.255", 
			"165.22.0.0"=>"165.22.255.255", 
			"159.65.0.0"=>"159.65.255.255", 
			"198.199.64.0"=>"198.199.127.255"
			), 
		"VegasNAP"=>array(
			"104.128.64.0"=>"104.128.75.255"
			), 
		"Leaseweb"=>array(
			"108.59.0.0"=>"108.59.15.255", 
			"199.58.84.0"=>"199.58.87.255"
			), 
		"Choopa"=>array(
			"45.63.14.0"=>"45.63.15.255", 
			"104.207.128.0"=>"104.207.129.255", 
			"104.207.128.0"=>"104.207.159.255", 
			"104.238.180.0"=>"104.238.181.255", 
			"104.238.128.0"=>"104.238.191.255", 
			"45.32.0.0"=>"45.32.255.255", 
			"45.32.172.0"=>"45.32.173.255", 
			"173.199.123.0"=>"173.199.123.255", 
			"173.199.64.0"=>"173.199.127.255"
			), 
		"paypal"=>array(
			"173.0.80.0"=>"173.0.95.255"
			), 
		"InternetArchive"=>array(
			"207.241.224.0"=>"207.241.239.255"
			), 
		"Otros"=>array(
			"202.182.112.0"=>"202.182.127.255",  
			"125.212.128.0"=>"125.212.255.255", 
			"128.199.0.0"=>"128.199.255.255", 
			"47.250.0.0"=>"47.254.255.255", 
			"47.252.0.0"=>"47.252.127.255", 
			"149.129.0.0"=>"149.129.255.255", 
			"149.129.128.0"=>"149.129.191.255", 
			"122.152.192.0"=>"122.152.255.255", 
			"207.102.138.0"=>"207.102.138.255", 
			"207.102.0.0"=>"207.102.255.255", 
			"50.30.32.0"=>"50.30.47.255", 
			"95.216.0.128"=>"95.216.0.191", 
			"86.44.0.1"=>"86.47.255.254", 
			"86.47.80.144"=>"86.47.80.151", 
			"89.234.64.1"=>"89.234.127.254", 
			"89.234.68.64"=>"89.234.68.127", 
			"83.70.0.1"=>"83.71.255.254", 
			"83.71.247.32"=>"83.71.247.39", 
			"46.4.0.0"=>"46.4.255.254", 
			"46.4.33.0"=>"46.4.33.63", 
			"104.129.0.0"=>"104.129.63.255", 
			"212.129.32.0"=>"212.129.63.255", 
			"192.0.64.0"=>"192.0.127.255", 
			"179.61.144.0"=>"179.61.159.254", 
			"23.231.0.0"=>"23.231.127.255", 
			"181.214.60.0"=>"181.214.63.254", 
			"204.187.12.0"=>"204.187.15.255", 
			"216.145.0.0"=>"216.145.31.255", 
			"198.240.100.0"=>"198.240.103.255", 
			"198.240.64.0"=>"198.240.127.255", 
			"172.82.160.0"=>"172.82.191.255", 
			"172.82.128.0"=>"172.82.191.255", 
			"204.12.206.0"=>"204.12.206.255", 
			"204.12.192.0"=>"204.12.255.255", 
			"199.191.56.0"=>"199.191.59.255", 
			"199.191.56.132"=>"199.191.56.135", 
			"173.205.33.16"=>"173.205.33.31", 
			"173.205.0.0"=>"173.205.127.255", 
			"104.223.112.0"=>"104.223.127.255", 
			"104.223.0.0"=>"104.223.127.255", 
			"204.79.180.0"=>"204.79.180.255", 
			"104.144.89.176"=>"104.144.89.191", 
			"104.144.0.0"=>"104.144.255.255", 
			"69.30.213.136"=>"69.30.213.143", 
			"69.30.192.0"=>"69.30.255.255", 
			"68.235.60.0"=>"68.235.63.255", 
			"68.235.32.0"=>"68.235.63.255", 
			"65.132.59.32"=>"65.132.59.47", 
			"65.128.0.0"=>"65.159.255.255", 
			"216.183.32.0"=>"216.183.63.255", 
			"68.48.0.0"=>"68.49.255.255", 
			"68.32.0.0"=>"68.63.255.255", 
			"173.160.0.0"=>"173.167.255.255", 
			"173.165.224.0"=>"173.165.255.255", 
			"64.80.115.80"=>"64.80.115.87", 
			"64.80.0.0"=>"64.80.255.255", 
			"71.234.0.0"=>"71.235.255.255", 
			"71.224.0.0"=>"71.239.255.255", 
			"209.126.128.0"=>"209.126.255.255", 
			"209.126.136.0"=>"209.126.136.31", 
			"47.52.0.0"=>"47.52.255.255", 
			"192.187.122.72"=>"192.187.122.79", 
			"192.187.96.0"=>"192.187.127.255", 
			"74.115.214.128"=>"74.115.214.159", 
			"74.115.208.0"=>"74.115.215.255", 
			"69.197.177.48"=>"69.197.177.55", 
			"69.197.128.0"=>"69.197.191.255", 
			"67.215.224.0"=>"67.215.255.255", 
			"142.0.192.0"=>"142.0.207.255", 
			"64.246.165.8"=>"64.246.165.11", 
			"64.246.160.0"=>"64.246.191.255", 
			"192.31.133.0"=>"192.31.133.255", 
			"192.31.128.0"=>"192.31.143.255", 
			"199.48.160.0"=>"199.48.167.255", 
			"77.220.205.0"=>"77.220.205.255"
			)
		);

	$m= strtolower($nav); # convertimos a minusculas 
	foreach( $robots as $key=>$val )
		{
		if( !$r && strstr($m, strtolower($key)) ) # si son iguales, es un robot 
			$r= $val; # retornamos nombre del indexador 
		}

	if( !$r ) # continua vacio, checamos por IP
		{
		foreach( $botips as $key=>$val )
			{
			if( !$r && is_array($val) && count($val) )
				{
				foreach( $val as $key2=>$val2 )
					{
					if( !$r && ip2long($nav)>=ip2long($key2) && ip2long($nav)<=ip2long($val2) )
						$r= $key; # obtenemos el bot
					}
				}
			}
		}
	
	return (!$r ? $r:'Indexado por '. $r);
	}

//Funcion para conectarse a la pagina
function autenticacion( $log )
	{
	if( $log )
		{
		if( !strcmp($log, "entrar") )
			{
			if( !strcmp( $_GET["social"], "facebook") ) # autentificado por facebook account
				include( "autentificacion/auth_facebook.php" );
			else if( !strcmp( $_GET["social"], "twitter") ) # autentificado por facebook account
				include( "autentificacion/auth_twitter.php" );
			else
				{
				$user_site= proteger_cadena($_POST["log_usr"]);
				$pass_site= proteger_cadena($_POST["log_pass"]);
				if( bruteforcing_detect() ) # si se detecta bruteforcing
					$ref= url_amigable( "?log=brute", "log", "login", "dos_prev");
				else if( login( $user_site, $pass_site ) )
					{
					$_SESSION["log_usr"]= proteger_cadena($_POST["log_usr"]);
					$_SESSION["log_pwd"]= proteger_cadena($_POST["log_pass"]);
					$_SESSION["log_id"]= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_SESSION["log_usr"]). "':PASSWORD='". proteger_cadena($_SESSION["log_pwd"]). "'", "ID" );
					bruteforcing_del(); # eliminamos la IP del Bruteforcing (si es que existe)
					if( !strcmp($_SERVER['HTTP_REFERER'], HTTP_SERVER. 'admin/index.php' ) || !strcmp($_SERVER['HTTP_REFERER'], HTTP_SERVER. 'admin/' ) )
						$ref= HTTP_SERVER. 'admin/index.php';
					else if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else		  $ref= "/"; # raiz
					}
				else
					{
					if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else		$ref= url_amigable( "?log=error", "log", "login", "error");
					}
				header( "Location: ". $ref );
				#print_r($_SESSION);
				#print_r($_POST);
				}
			}
		else if( !strcmp($log, "salir") )
			{
			# $trama= array(
			#		"SESION"=>"'". session_id(). "'",
			#		"FECHA_LOGOUT"=>"'". time(). "'"  
			#		);
			# actualizar_bdd( "USUARIOS", array("id"=>"'". $_SSSION["log_id"]. "'", "online"=>"'0'") );
			# unset($trama);
			actualizar_bdd( "USUARIOS", array("id"=>"'". $_SESSION["log_id"]. "'", "online"=>"'0'") );
			if( !strcmp( $_GET["social"], "facebook") ) # autentificado por facebook account
				include( "autentificacion/auth_facebook.php" );
			session_destroy();
			unset($_SESSION);
			header( "Location: /");
			}
		}
	}
	
function grados_temp( $num, $idioma )
	{
	if( !$num || !$idioma )		return 0;
	
	if( !strcmp($idioma, "en") ) # grados en ingles 'farenheid'
		return round((($num+32)*1.8)). ' &#186;F';
	else if( !strcmp($idioma, "es") ) # en espanol 'centigrados'
		return $num. ' &#186;C';
	return 0;
	}

# traduce dias al espanol
function dias_en2es( $dato )
	{
	$mes= array( 
			"january"=>"enero", 
			"february"=>"febrero", 
			"march"=>"marzo", 
			"april"=>"abril", 
			"may"=>"mayo", 
			"june"=>"junio", 
			"july"=>"julio", 
			"august"=>"agosto", 
			"september"=>"septiembre", 
			"october"=>"octubre", 
			"november"=>"noviembre", 
			"december"=>"diciembre"
			);
	$dias= array(
			"monday"=>"lunes", 
			"tuesday"=>"martes", 
			"wednesday"=>"miercoles", 
			"thursday"=>"jueves", 
			"friday"=>"viernes", 
			"saturday"=>"sabado", 
			"sunday"=>"domingo"
			);
	
	$m= strtolower($dato);

	# recorriendo meses	
	foreach($mes as $key=>$val)
		{
		if( !strcmp($key, $m) )
			return $val;
		}

	# recorriendo dias
	foreach($dias as $key=>$val)
		{
		if( !strcmp($key, $m) )
			return $val;
		}
	
	return '';
	}

// Funcion para limpiar consulta a MySQL
function limpiar( $con )
	{
	mysql_free_result($con);
	}
	
// Funcion que comprueba si una seccion es libre
function seccion_libre( $seccion_id )
	{
	$cons= consultar( "PRIVILEGIOS", "*" ); # consultamos tabla
	$buf= mysql_fetch_array($cons);
	
	if( mysql_num_rows($cons) ) # si existen datos
		{
		while( $buf= mysql_fetch_array($cons) ) # ciclo
			{
			if( strcmp( $buf["SECCIONES"], "0") && strcmp( $buf["SECCIONES"], "vacio") && strcmp( $buf["SECCIONES"], "") ) # si el privilegio tiene contenido
				if( strchr( $buf["SECCIONES"], $seccion_id) ) # verificamos existencia del ID
					{
					limpiar($cons);
					return 0; # si esta el ID, seccion privada :D
					}
			}
		}
		
	unset($buf);
	limpiar($cons);
	return 1; # no existio ID de la seccion, es una seccion libre
	}

function seccion_visible($id)
	{
	return ( consultar_datos_general("SECCIONES", "ID='". proteger_cadena($id). "'", "VISIBILIDAD") ? 1:0);
	}

function seccion_exists($urlname)
	{
	return ( consultar_datos_general("SECCIONES", "URL_NOMBRE='". proteger_cadena($urlname). "'", "ID") ? 1:0);
	}

//Convierte de BBCode a HTML Tags
function grupos_comprueba_miembro( $grupos, $seccion )
	{
	if( !strcmp($grupos, "") || !strcmp($grupos, "vacio") )	return 1;	
	else if( strchr( $grupos, ":" ) ) //si existe delimitador, entonces existe mas de un valor
		{
		$x= explode( ":", $grupos ); //cortamos
		
		for( $i=0; $i<sizeof($x); $i++ )
			if( !strcmp($x[$i], $seccion) ) //si la seccion existe
				return 0;	//regresamos 0, ya existe
				
		return 1; //en este punto, la seccion no se encontro y es posible insertarla
		
		unset($x);
		}
	else //solo existe un valor
		{
		if( !strcmp($grupos, $seccion) )	return 0; //entonces ya existe esta seccion en los grupos
		return 1; //no existe la seccion en los grupos
		}
	}
	
function msg2msgtags( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
   $cad_buscar= array(
   				'/\[panel\](.*?)\[\/panel\]/is',
   				'/\[googlemaps\](.*?)\[\/googlemaps\]/is',
   				'/\[googlemaps size=(.*?)\](.*?)\[\/googlemaps\]/is',
   				'/\[googlemaps\=latlong size=(.*?)\](.*?)\[\/googlemaps\]/is',
   				'/\[googlemaps\=latlong\](.*?)\[\/googlemaps\]/is', 
   				'/\[hr\]/is', 
   				'/\[amp\]/is',
   				'/\[h1\](.*?)\[\/h1\]/is',
   				'/\[h2\](.*?)\[\/h2\]/is', 
   				'/\[capa\=(.*?)\](.*?)\[\/capa\]/is',
   				'/\[clase\=(.*?)\](.*?)\[\/clase\]/is',
   				'/\[span\=(.*?)\](.*?)\[\/span\]/is',
   				'/\[center\](.*?)\[\/center\]/is',
   				'/\[table\](.*?)\[\/table\]/is',
   				'/\[th\](.*?)\[\/th\]/is',
   				'/\[td\](.*?)\[\/td\]/is',
   				'/\[tr\]/is',
   				'/\[list\](.*?)\[\/list\]/is',
   				'/\[li\](.*?)\[\/li\]/is',
   				'/\[b\](.*?)\[\/b\]/is',
   				'/\[i\](.*?)\[\/i\]/is',
   				'/\[u\](.*?)\[\/u\]/is',
   				'/\[s\](.*?)\[\/s\]/is',
   				'/\[url\=(.*?)\](.*?)\[\/url\]/is',                         
   				'/\[url\](.*?)\[\/url\]/is',                             
   				'/\[align\=(left|center|right|justify)\](.*?)\[\/align\=(left|center|right|justify)\]/is',
   				'/\[img\](.*?)\[\/img\]/is',                            
   				'/\[font\=(.*?)\](.*?)\[\/font\]/is',                    
   				'/\[size\=(.*?)\](.*?)\[\/size\]/is',                    
   				'/\[color\=(.*?)\](.*?)\[\/color\]/is',
   				'/\[code\=php\](.*?)\[\/code\]/is', 
   				'/\[code\](.*?)\[\/code\]/is',
   				'/\[quote\](.*?)\[\/quote\]/is',
   				'/\[youtube\](.*?)\[\/youtube\]/is',
   				'/\[youtube width\=(.*?) height\=(.*?)\](.*?)\[\/youtube\]/is',
   				'/\[swf\](.*?)\[\/swf\]/is',
   				'/\[swf (.*?) (.*?)\](.*?)\[\/swf\]/is',
   				'/\[tab\]/is', 
   				'/\[play_mp3\](.*?)\[\/play_mp3\]/is', 
   				'/\[video (.*?) (.*?)\](.*?)\[\/video\]/is',
   				'/\[video\](.*?)\[\/video\]/is', 
   				'/\[video_flash\](.*?)\[\/video_flash\]/is', 
   				'/\[video_flash (.*?) (.*?)\](.*?)\[\/video_flash\]/is', 
   				'/\[precio\](.*?)\[\/precio\]/is', 
   				'/\[twit\](.*?)\[\/twit\]/is', 
   				);

	$cad_remplazo= array(
					'<div class="w3-panel w3-padding w3-leftbar w3-border-yellow w3-pale-yellow w3-xlarge">$1</div>', 
					'<iframe src="'. HTTP_SERVER. '/googlemaps.php?place=$1"></iframe>',
					'<iframe src="'. HTTP_SERVER. '/googlemaps.php?place=$2&size=$1"></iframe>',
					'<iframe src="'. HTTP_SERVER. '/googlemaps.php?data=$2&size=$1"></iframe>',
					'<iframe src="'. HTTP_SERVER. '/googlemaps.php?data=$1"></iframe>', 
					'<hr>', 
					'&',
					'<h1>$1</h1>',
					'<h2>$1</h2>', 
					'<div id="$1">$2</div>', 
					'<div class="$1">$2</div>', 
					'<div style="$1">$2</div>',
					'<center>$1</center>',
					'<table id="bbcode_tabla">$1</table>',
					'<th>$1</th>',
					'<td>$1</td>',
					'<tr>',
					'<ul>$1</ul>',
					'<li>$1</li>',
   				'<strong>$1</strong>',
   				'<em>$1</em>',
   				'<u>$1</u>',
   				'<strike>$1</strike>',
   				'<a href="$1">$2</a>',
   				'<a href="$1">$1</a>',
   				'<div style="text-align:$1;">$2</div>',
   				'<img src="$1" border="0" />',
   				'<span style="font-family:$1;">$2</span>',
   				'<span style="font-size:$1;">$2</span>',
   				'<span style="color: $1;">$2</span>',
   				'Codigo PHP<br><div id="etiqueta_code">'. highlight_string('$1', 'true'). '</div>',
   				'Codigo:<br><div id="etiqueta_code">$1</div>',
   				'Cita:<br><div id="etiqueta_code">$1</div>',
   				'<span style="display:block;"><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/$1"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/$1" type="application/x-shockwave-flash" wmode="transparent" width="425" height="344"></embed></object></span>',
   				'<span style="display:block;"><object width="$1" height="$2"><param name="movie" value="http://www.youtube.com/v/$3"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/$3" type="application/x-shockwave-flash" wmode="transparent" width="$1" height="$2"></embed></object></span>',
					'<span style="display:block;"><embed src="$1" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" wmode="transparent"></embed></span>',
					'<span style="display:block;"><embed src="$3" $1 $2 type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" wmode="transparent"></embed></span>',
					proteger_cadena('&#160;&#160;&#160;&#160;'), 
					'<object type="application/x-shockwave-flash" data="admin/addons/dewplayer-mini.swf" width="160" height="20">
					<param name="wmode" value="transparent" />
					<param name="movie" value="admin/addons/dewplayer-mini.swf" />
					<param name="flashvars" value="mp3=uploads/noticias/'. mp3_file('$1'). '" />
					</object>', 
					'<video src="$3" $1 $2 controls onerror="stream_video_error(event)" type="video/ogg">Su Navegador no soporta reproduccion de video HTML5</video>', 
					'<video src="$1" controls onerror="stream_video_error(event)" type="video/ogg">Su Navegador no soporta reproduccion de video HTML5</video>',
					'<embed type="application/x-shockwave-flash" src="admin/addons/player.swf" style="" id="mpl" name="mpl" quality="high" allowfullscreen="true" allowscriptaccess="always" 
					flashvars="file=$1">',
					'<embed type="application/x-shockwave-flash" src="admin/addons/player.swf" style="" id="mpl" name="mpl" quality="high" allowfullscreen="true" allowscriptaccess="always" 
					flashvars="file=$3" height="$2" width="$1">', 
					'<div class="precio">$1</div>', 
					'$1'
                );
                #http://www.podtrac.com/pts/redirect.mp3/

	$mensaje= htmlentities($mensaje, ENT_QUOTES);
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );
	$mensaje= html_entity_decode($mensaje, ENT_QUOTES);

	return $mensaje;
	}

function google_size2width( $data )
	{
	$x= explode("x", $data);
	return 'width:'. $x[0]. 'px;height:'. $x[1]. 'px;';
	}

function r_msg2msgtags( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
   $cad_remplazo= array(
   				'/\[amp]/is',
   				'/\[capa\=(.*?)\](.*?)\[\/capa\]/is',
   				'/\[span\=(.*?)\](.*?)\[\/span\]/is',
   				'/\[center\](.*?)\[\/center\]/is',
   				'/\[table\](.*?)\[\/table\]/is',
   				'/\[th\](.*?)\[\/th\]/is',
   				'/\[td\](.*?)\[\/td\]/is',
   				'/\[tr\]/is',
   				'/\[list\](.*?)\[\/list\]/is',
   				'/\[li\](.*?)\[\/li\]/is',
   				'/\[b\](.*?)\[\/b\]/is',
   				'/\[i\](.*?)\[\/i\]/is',
   				'/\[u\](.*?)\[\/u\]/is',
   				'/\[s\](.*?)\[\/s\]/is',
   				'/\[url\=(.*?)\](.*?)\[\/url\]/is',                         
   				'/\[url\](.*?)\[\/url\]/is',                             
   				'/\[align\=(left|center|right|justify)\](.*?)\[\/align\=(left|center|right|justify)\]/is',
   				'/\[img\](.*?)\[\/img\]/is',                            
   				'/\[font\=(.*?)\](.*?)\[\/font\]/is',                    
   				'/\[size\=(.*?)\](.*?)\[\/size\]/is',                    
   				'/\[color\=(.*?)\](.*?)\[\/color\]/is',
   				'/\[code\=php\](.*?)\[\/code\]/is', 
   				'/\[code\](.*?)\[\/code\]/is',
   				'/\[quote\](.*?)\[\/quote\]/is',
   				'/\[youtube](.*?)\[\/youtube\]/is',
   				'/\[swf\](.*?)\[\/swf\]/is',
   				'/\[swf (.*?) (.*?)\](.*?)\[\/swf\]/is',
   				'/\[tab\]/is', 
   				'/\[play_mp3\](.*?)\[\/play_mp3\]/is', 
   				'/\[video (.*?) (.*?)\](.*?)\[\/video\]/is',
   				'/\[video\](.*?)\[\/video\]/is', 
   				'/\[video_flash\](.*?)\[\/video_flash\]/is', 
   				'/\[video_flash (.*?) (.*?)\](.*?)\[\/video_flash\]/is', 
   				'/\[precio\](.*?)\[\/precio\]/is'
   				);

	$cad_buscar= array(
					'&',
					'<div id="$1">$2</div>', 
					'<div style="$1">$2</div>',
					'<center>$1</center>',
					'<table id="bbcode_tabla">$1</table>',
					'<th>$1</th>',
					'<td>$1</td>',
					'<tr>',
					'<ul>$1</ul>',
					'<li>$1</li>',
   				'<strong>$1</strong>',
   				'<em>$1</em>',
   				'<u>$1</u>',
   				'<strike>$1</strike>',
   				'<a href="$1">$2</a>',
   				'<a href="$1">$1</a>',
   				'<div style="text-align:$1;">$2</div>',
   				'<img src="$1" border="0" />',
   				'<span style="font-family:$1;">$2</span>',
   				'<span style="font-size:$1;">$2</span>',
   				'<span style="color: $1;">$2</span>',
   				'Codigo PHP<br><div id="etiqueta_code">'. highlight_string('$1', 'true'). '</div>',
   				'Codigo:<br><div id="etiqueta_code">$1</div>',
   				'Cita:<br><div id="etiqueta_code">$1</div>',
   				'<span style="display:block;"><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/$1"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/$1" type="application/x-shockwave-flash" wmode="transparent" width="425" height="344"></embed></object></span>',
					'<span style="display:block;"><embed src="$1" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" wmode="transparent"></embed></span>',
					'<span style="display:block;"><embed src="$3" $1 $2 type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" wmode="transparent"></embed></span>',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 
					'<object type="application/x-shockwave-flash" data="admin/addons/dewplayer-mini.swf" width="160" height="20">
					<param name="wmode" value="transparent" />
					<param name="movie" value="admin/addons/dewplayer-mini.swf" />
					<param name="flashvars" value="mp3=uploads/noticias/'. mp3_file('$1'). '" />
					</object>', 
					'<video src="$3" $1 $2 controls onerror="stream_video_error(event)" type="video/ogg">Su Navegador no soporta reproduccion de video HTML5</video>', 
					'<video src="$1" controls onerror="stream_video_error(event)" type="video/ogg">Su Navegador no soporta reproduccion de video HTML5</video>',
					'<embed type="application/x-shockwave-flash" src="admin/addons/player.swf" style="" id="mpl" name="mpl" quality="high" allowfullscreen="true" allowscriptaccess="always" 
					flashvars="file=$1">',
					'<embed type="application/x-shockwave-flash" src="admin/addons/player.swf" style="" id="mpl" name="mpl" quality="high" allowfullscreen="true" allowscriptaccess="always" 
					flashvars="file=$3" height="$2" width="$1">', 
					'<div class="precio">$1</div>'
                );
                #http://www.podtrac.com/pts/redirect.mp3/

	# $mensaje= htmlentities($mensaje, ENT_QUOTES);
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );
	# $mensaje= html_entity_decode($mensaje, ENT_QUOTES);

	return $mensaje;
	}

//elimina algunos BBCodes
function eliminar_bbcode( $mensaje )
	{
   $cad_buscar= array(
   				'/\[center\](.*?)\[\/center\]/is',
   				'/\[b\](.*?)\[\/b\]/is',
   				'/\[i\](.*?)\[\/i\]/is',
   				'/\[u\](.*?)\[\/u\]/is',
   				'/\[s\](.*?)\[\/s\]/is',
   				'/\[align\=(left|center|right|justify)\](.*?)\[\/align\=(left|center|right|justify)\]/is',
   				'/\[img\](.*?)\[\/img\]/is',                            
   				'/\[youtube](.*?)\[\/youtube\]/is',
   				'/\[swf\](.*?)\[\/swf\]/is',
   				'/\[swf (.*?) (.*?)\](.*?)\[\/swf\]/is',
   				'/\[url\=(.*?)\](.*?)\[\/url\]/is',                         
   				'/\[url\](.*?)\[\/url\]/is'
   				);

	$cad_remplazo= array(
					'<center>$1</center>',
   				'<strong>$1</strong>',
   				'<em>$1</em>',
   				'<u>$1</u>',
   				'<strike>$1</strike>',
   				'$2',
   				'$1',
   				'$1',
					'$1',
					'$3',
					'$1',
					'$1',
                );

	$mensaje= htmlentities($mensaje, ENT_QUOTES);
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );
	$mensaje= html_entity_decode($mensaje, ENT_QUOTES);

	return $mensaje;
	}

function eliminar_bbcode_all( $mensaje )
	{
   $cad_buscar= array(
   				'/\[center\](.*?)\[\/center\]/is',
   				'/\[b\](.*?)\[\/b\]/is',
   				'/\[i\](.*?)\[\/i\]/is',
   				'/\[u\](.*?)\[\/u\]/is',
   				'/\[s\](.*?)\[\/s\]/is',
   				'/\[align\=(left|center|right|justify)\](.*?)\[\/align\=(left|center|right|justify)\]/is',
   				'/\[img\](.*?)\[\/img\]/is',                            
   				'/\[youtube](.*?)\[\/youtube\]/is',
   				'/\[swf\](.*?)\[\/swf\]/is',
   				'/\[swf (.*?) (.*?)\](.*?)\[\/swf\]/is',
   				'/\[url\=(.*?)\](.*?)\[\/url\]/is',                         
   				'/\[url\](.*?)\[\/url\]/is'
   				);

	$cad_remplazo= array(
					'',
   				'',
   				'',
   				'',
   				'',
   				'',
   				'',
   				'',
					'',
					'',
					'',
					'',
                );

	$mensaje= htmlentities($mensaje, ENT_QUOTES);
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );
	$mensaje= html_entity_decode($mensaje, ENT_QUOTES);

	return $mensaje;
	}

//busca archivo MP3 a reproducir
function mp3_file( $id )
	{
	return $id. '.mp3';
	}


//Convierte de CODIGO a Caritas
function msg2caritas( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
	if( strchr( $mensaje, ":D" ) )
		$mensaje= str_replace( ":D", "<img src=\"". CARITAS_URL. "/001.gif\" border=\"0\" alt=\":D\" title=\":D\">", $mensaje );

	if( strchr( $mensaje, ":(" )  )
		$mensaje= str_replace( ":(", "<img src=\"". CARITAS_URL. "/002.gif\" border=\"0\" alt=\":(\" title=\":(\">", $mensaje );

	if( strchr( $mensaje, ";)" ) )
		$mensaje= str_replace( ";)", "<img src=\"". CARITAS_URL. "/009.gif\" border=\"0\" alt=\";)\" title=\";)\">", $mensaje );

	if( strchr( $mensaje, "^_^" ) )
		$mensaje= str_replace( "^_^", "<img src=\"". CARITAS_URL. "/003.gif\" border=\"0\" alt=\"^_^\" title=\"^_^\">", $mensaje );

	if( strchr( $mensaje, "o_O" ) )
		$mensaje= str_replace( "o_O", "<img src=\"". CARITAS_URL. "/004.gif\" border=\"0\" alt=\"o_O\" title=\"o_O\">", $mensaje );

	if( strchr( $mensaje, ":S" ) )
		$mensaje= str_replace( ":S", "<img src=\"". CARITAS_URL. "/005.gif\" border=\"0\" alt=\":S\" title=\":S\">", $mensaje );

	if( strchr( $mensaje, "-.-" ) )
		$mensaje= str_replace( "-.-", "<img src=\"". CARITAS_URL. "/008.gif\" border=\"0\" alt=\"-.-\" title=\"-.-\">", $mensaje );	

	if( strchr( $mensaje, "(H)" ) )
		$mensaje= str_replace( "(H)", "<img src=\"". CARITAS_URL. "/006.gif\" border=\"0\" alt=\"(H)\" title=\"(H)\">", $mensaje );

	if( strchr( $mensaje, ":O" ) )
		$mensaje= str_replace( ":O", "<img src=\"". CARITAS_URL. "/011.gif\" border=\"0\" alt=\":O\" title=\":O\">", $mensaje );
		 
	if( strchr( $mensaje, ":|" ) )
		$mensaje= str_replace( ":|", "<img src=\"". CARITAS_URL. "/010.gif\" border=\"0\" alt=\":|\" title=\":|\">", $mensaje );
	/*if( strchr( $mensaje, "" ) &&  strchr( $mensaje, "" ) )
		{
		$mensaje= str_replace();
		$mensaje= str_replace();
		}*/
	return $mensaje;
	}
	
function msg2caritas_extra( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
	if( strchr( $mensaje, ":ass_suck:" ) )
		$mensaje= str_replace( ":ass_suck:", "<img src=\"". CARITAS_URL. "/012.gif\" border=\"0\" alt=\":ass_suck:\" title=\":ass_suck:\">", $mensaje );
	if( strchr( $mensaje, ":laser:" ) )
		$mensaje= str_replace( ":laser:", "<img src=\"". CARITAS_URL. "/013.gif\" border=\"0\" alt=\":laser:\" title=\":laser:\">", $mensaje );
	if( strchr( $mensaje, ":vomito:" ) )
		$mensaje= str_replace( ":vomito:", "<img src=\"". CARITAS_URL. "/014.gif\" border=\"0\" alt=\":vomito:\" title=\":vomito:\">", $mensaje );
	if( strchr( $mensaje, ":nerdo:" ) )
		$mensaje= str_replace( ":nerdo:", "<img src=\"". CARITAS_URL. "/015.gif\" border=\"0\" alt=\":nerdo:\" title=\":nerdo:\">", $mensaje );
	if( strchr( $mensaje, ":juez:" ) )
		$mensaje= str_replace( ":juez:", "<img src=\"". CARITAS_URL. "/016.gif\" border=\"0\" alt=\":juez:\" title=\":juez:\">", $mensaje );
	if( strchr( $mensaje, ":vomitando:" ) )
		$mensaje= str_replace( ":vomitando:", "<img src=\"". CARITAS_URL. "/017.gif\" border=\"0\" alt=\":vomitando:\" title=\":vomitando:\">", $mensaje );
	if( strchr( $mensaje, ":metralleta:" ) )
		$mensaje= str_replace( ":metralleta:", "<img src=\"". CARITAS_URL. "/018.gif\" border=\"0\" alt=\":metralleta:\" title=\":metralleta:\">", $mensaje );
	if( strchr( $mensaje, ":asesino:" ) )
		$mensaje= str_replace( ":asesino:", "<img src=\"". CARITAS_URL. "/019.gif\" border=\"0\" alt=\":asesino:\" title=\":asesino:\">", $mensaje );
	if( strchr( $mensaje, ":ak_rojo:" ) )
		$mensaje= str_replace( ":ak_rojo:", "<img src=\"". CARITAS_URL. "/020.gif\" border=\"0\" alt=\":ak_rojo:\" title=\":ak_rojo:\">", $mensaje );
	if( strchr( $mensaje, ":ak_asalto:" ) )
		$mensaje= str_replace( ":ak_asalto:", "<img src=\"". CARITAS_URL. "/021.gif\" border=\"0\" alt=\":ak_asalto:\" title=\":ak_asalto:\">", $mensaje );
	if( strchr( $mensaje, ":sniper:" ) )
		$mensaje= str_replace( ":sniper:", "<img src=\"". CARITAS_URL. "/022.gif\" border=\"0\" alt=\":sniper:\" title=\":sniper:\">", $mensaje );
	if( strchr( $mensaje, ":rambo:" ) )
		$mensaje= str_replace( ":rambo:", "<img src=\"". CARITAS_URL. "/023.gif\" border=\"0\" alt=\":rambo:\" title=\":rambo:\">", $mensaje );
	if( strchr( $mensaje, ":metralleta2:" ) )
		$mensaje= str_replace( ":metralleta2:", "<img src=\"". CARITAS_URL. "/024.gif\" border=\"0\" alt=\":metralleta2:\" title=\":metralleta2:\">", $mensaje );
	if( strchr( $mensaje, ":tanque:" ) )
		$mensaje= str_replace( ":tanque:", "<img src=\"". CARITAS_URL. "/025.gif\" border=\"0\" alt=\":tanque:\" title=\":tanque:\">", $mensaje );
	if( strchr( $mensaje, ":sangrado:" ) )
		$mensaje= str_replace( ":sangrado:", "<img src=\"". CARITAS_URL. "/026.gif\" border=\"0\" alt=\":sangrado:\" title=\":sangrado:\">", $mensaje );
	if( strchr( $mensaje, ":num1:" ) )
		$mensaje= str_replace( ":num1:", "<img src=\"". CARITAS_URL. "/027.gif\" border=\"0\" alt=\":num1:\" title=\":num1:\">", $mensaje );
	if( strchr( $mensaje, ":ok:" ) )
		$mensaje= str_replace( ":ok:", "<img src=\"". CARITAS_URL. "/028.gif\" border=\"0\" alt=\":ok:\" title=\":ok:\">", $mensaje );
	if( strchr( $mensaje, ":platanin:" ) )
		$mensaje= str_replace( ":platanin:", "<img src=\"". CARITAS_URL. "/029.gif\" border=\"0\" alt=\":platanin:\" title=\":platanin:\">", $mensaje );
	if( strchr( $mensaje, ":fuck1:" ) )
		$mensaje= str_replace( ":fuck1:", "<img src=\"". CARITAS_URL. "/030.gif\" border=\"0\" alt=\":fuck1:\" title=\":fuck1:\">", $mensaje );
	if( strchr( $mensaje, ":fuck2:" ) )
		$mensaje= str_replace( ":fuck2:", "<img src=\"". CARITAS_URL. "/031.gif\" border=\"0\" alt=\":fuck2:\" title=\":fuck2:\">", $mensaje );
	if( strchr( $mensaje, ":varazo:" ) )
		$mensaje= str_replace( ":varazo:", "<img src=\"". CARITAS_URL. "/032.gif\" border=\"0\" alt=\":varazo:\" title=\":varazo:\">", $mensaje );
	if( strchr( $mensaje, ":golpe:" ) )
		$mensaje= str_replace( ":golpe:", "<img src=\"". CARITAS_URL. "/033.gif\" border=\"0\" alt=\":golpe:\" title=\":golpe:\">", $mensaje );
	if( strchr( $mensaje, ":ban:" ) )
		$mensaje= str_replace( ":ban:", "<img src=\"". CARITAS_URL. "/034.gif\" border=\"0\" alt=\":ban:\" title=\":ban:\">", $mensaje );
	if( strchr( $mensaje, ":dedo:" ) )
		$mensaje= str_replace( ":dedo:", "<img src=\"". CARITAS_URL. "/035.gif\" border=\"0\" alt=\":dedo:\" title=\":dedo:\">", $mensaje );
	if( strchr( $mensaje, ":pared:" ) )
		$mensaje= str_replace( ":pared:", "<img src=\"". CARITAS_URL. "/036.gif\" border=\"0\" alt=\":pared:\" title=\":pared:\">", $mensaje );
	if( strchr( $mensaje, ":fuck3:" ) )
		$mensaje= str_replace( ":fuck3:", "<img src=\"". CARITAS_URL. "/037.gif\" border=\"0\" alt=\":fuck3:\" title=\":fuck3:\">", $mensaje );
	if( strchr( $mensaje, ":besamela:" ) )
		$mensaje= str_replace( ":besamela:", "<img src=\"". CARITAS_URL. "/038.gif\" border=\"0\" alt=\":besamela:\" title=\":besamela:\">", $mensaje );
	if( strchr( $mensaje, ":borracho:" ) )
		$mensaje= str_replace( ":borracho:", "<img src=\"". CARITAS_URL. "/039.gif\" border=\"0\" alt=\":borracho:\" title=\":borracho:\">", $mensaje );
	if( strchr( $mensaje, ":borracho2:" ) )
		$mensaje= str_replace( ":borracho2:", "<img src=\"". CARITAS_URL. "/040.gif\" border=\"0\" alt=\":borracho2:\" title=\":borracho2:\">", $mensaje );
	if( strchr( $mensaje, ":agente_007:" ) )
		$mensaje= str_replace( ":agente_007:", "<img src=\"". CARITAS_URL. "/041.gif\" border=\"0\" alt=\":agente_007:\" title=\":agente_007:\">", $mensaje );
	if( strchr( $mensaje, ":boxeador:" ) )
		$mensaje= str_replace( ":boxeador:", "<img src=\"". CARITAS_URL. "/042.gif\" border=\"0\" alt=\":boxeador:\" title=\":boxeador:\">", $mensaje );
	if( strchr( $mensaje, ":chacos:" ) )
		$mensaje= str_replace( ":chacos:", "<img src=\"". CARITAS_URL. "/043.gif\" border=\"0\" alt=\":chacos:\" title=\":chacos:\">", $mensaje );
	if( strchr( $mensaje, ":muletas:" ) )
		$mensaje= str_replace( ":muletas:", "<img src=\"". CARITAS_URL. "/044.gif\" border=\"0\" alt=\":muletas:\" title=\":muletas:\">", $mensaje );
	if( strchr( $mensaje, ":granada_estupida:" ) )
		$mensaje= str_replace( ":granada_estupida:", "<img src=\"". CARITAS_URL. "/045.gif\" border=\"0\" alt=\":granada_estupida:\" title=\":granada_estupida:\">", $mensaje );
	//if( strchr( $mensaje, ":camilla:" ) )
		//$mensaje= str_replace( ":camilla:", "<img src=\"". CARITAS_URL. "/046.gif\" border=\"0\" alt=\":camilla:\" title=\":camilla:\">", $mensaje );
		
	return $mensaje;
	}
	
function lista_caritas()
	{
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':D', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/001.gif\" border=\"0\" alt=\":D\" title=\":D\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':(', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/002.gif\" border=\"0\" alt=\":(\" title=\":(\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ';)', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/009.gif\" border=\"0\" alt=\";)\" title=\";)\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( '^_^', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/003.gif\" border=\"0\" alt=\"^_^\" title=\"^_^\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( 'o_O', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/004.gif\" border=\"0\" alt=\"o_O\" title=\"o_O\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':S', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/005.gif\" border=\"0\" alt=\":S\" title=\":S\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( '-.-', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/008.gif\" border=\"0\" alt=\"-.-\" title=\"-.-\"></a> ";	
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( '(H)', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/006.gif\" border=\"0\" alt=\"(H)\" title=\"(H)\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':O', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/011.gif\" border=\"0\" alt=\":O\" title=\":O\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':|', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/010.gif\" border=\"0\" alt=\":|\" title=\":|\"></a> ";
	//echo "[><a onclick=\"caritas_extra('caritas_extra')\">Ver Caritas Extra</a><] ";
	//echo "[><a onclick=\"caritas_extra('caritas_tuki')\">Ver Caritas Tuki</a><] ";
	}
	
function lista_cartas_extra()
	{
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ass_suck:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/012.gif\" border=\"0\" alt=\":ass_suck:\" title=\":ass_suck:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':laser:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/013.gif\" border=\"0\" alt=\":laser:\" title=\":laser:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':vomito:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/014.gif\" border=\"0\" alt=\":vomito:\" title=\":vomito:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':nerdo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/015.gif\" border=\"0\" alt=\":nerdo:\" title=\":nerdo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':juez:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/016.gif\" border=\"0\" alt=\":juez:\" title=\":juez:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':vomitando:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/017.gif\" border=\"0\" alt=\":vomitando:\" title=\":vomitando:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':metralleta:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/018.gif\" border=\"0\" alt=\":metralleta:\" title=\":metralleta:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':asesino:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/019.gif\" border=\"0\" alt=\":asesino:\" title=\":asesino:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ak_rojo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/020.gif\" border=\"0\" alt=\":ak_rojo:\" title=\":ak_rojo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ak_asalto:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/021.gif\" border=\"0\" alt=\":ak_asalto:\" title=\":ak_asalto:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':sniper:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/022.gif\" border=\"0\" alt=\":sniper:\" title=\":sniper:\"> </a>";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':rambo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/023.gif\" border=\"0\" alt=\":rambo:\" title=\":rambo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':metralleta2:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/024.gif\" border=\"0\" alt=\":metralleta2:\" title=\":metralleta2:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':tanque:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/025.gif\" border=\"0\" alt=\":tanque:\" title=\":tanque:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':sangrado:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/026.gif\" border=\"0\" alt=\":sangrado:\" title=\":sangrado:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':num1:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/027.gif\" border=\"0\" alt=\":num1:\" title=\":num1:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ok:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/028.gif\" border=\"0\" alt=\":ok:\" title=\":ok:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':platanin:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/029.gif\" border=\"0\" alt=\":platanin:\" title=\":platanin:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':fuck1:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/030.gif\" border=\"0\" alt=\":fuck1:\" title=\":fuck1:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':fuck2:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/031.gif\" border=\"0\" alt=\":fuck2:\" title=\":fuck2:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':varazo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/032.gif\" border=\"0\" alt=\":varazo:\" title=\":varazo:\"></a> "; 
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':golpe:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/033.gif\" border=\"0\" alt=\":golpe:\" title=\":golpe:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ban:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/034.gif\" border=\"0\" alt=\":ban:\" title=\":ban:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':dedo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/035.gif\" border=\"0\" alt=\":dedo:\" title=\":dedo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':pared:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/036.gif\" border=\"0\" alt=\":pared:\" title=\":pared:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':fuck3:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/037.gif\" border=\"0\" alt=\":fuck3:\" title=\":fuck3:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':besamela:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/038.gif\" border=\"0\" alt=\":besamela:\" title=\":besamela:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':borracho:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/039.gif\" border=\"0\" alt=\":borracho:\" title=\":borracho:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':borracho2:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/040.gif\" border=\"0\" alt=\":borracho2:\" title=\":borracho2:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':agente_007:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/041.gif\" border=\"0\" alt=\":agente_007:\" title=\":agente_007:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':boxeador:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/042.gif\" border=\"0\" alt=\":boxeador:\" title=\":boxeador:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':chacos:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/043.gif\" border=\"0\" alt=\":chacos:\" title=\":chacos:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':muletas:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/044.gif\" border=\"0\" alt=\":muletas:\" title=\":muletas:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':granada_estupida:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/045.gif\" border=\"0\" alt=\":granada_estupida:\" title=\":granada_estupida:\"></a> ";
	}

function lista_controles()
	{
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[img]', '[/img]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/imagen.gif\" border=\"0\" title=\"Imagen\" alt=\"Imagen\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[b]', '[/b]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/bold.gif\" border=\"0\" title=\"Negrita\" alt=\"Negrita\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[i]', '[/i]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/italic.gif\" border=\"0\" title=\"Italica\" alt=\"Italica\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[url]', '[/url]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/url.gif\" border=\"0\" title=\"Link\" alt=\"Link\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[youtube]', '[/youtube]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/youtube.gif\" border=\"0\" title=\"Youtube Video\" alt=\"Youtube Video\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[swf]', '[/swf]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/swf.png\" border=\"0\" title=\"Archivo SWF\" alt=\"Archivo SWF\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[swf width= height=]', '[/swf]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/swf-ajustable.png\" border=\"0\" title=\"SWF Ajustable\" alt=\"SWF Ajustable\"></a>";
	}
	
//obtener tipo de archivo apartir del nombre y extencion
function obtener_extencion_stream_archivo( $archivo )
	{
	/*
	image/jpeg	Imagen JPEG, JPG
	image/png	Imagen PNG
	image/gif	Imagen GIF
	application/zip			Archivo ZIP
	application/gzip		Archivo TAR.GZ
	application/pdf			Archivo PDF
	application/doc			Archivo DOC
	text/plain			Archivo TXT
	text/html			Archivo HTML
	application/x-shockwave-flash	Archivo SWF
	application/octet-stream
	*/
	if( strpos( $archivo, ".jpg" ) || strpos( $archivo, ".jpeg" ) )
		return "image/jpeg";
	else if( strpos( $archivo, ".png" ) )
		return "image/png";
	else if( strpos( $archivo, ".gif" ) )
		return "image/gif";
	else if( strpos( $archivo, ".zip" ) )
		return "application/zip";
	else if( strpos( $archivo, ".rar" ) )
		return "application/rar";
	else if( strpos( $archivo, ".tar.gz" ) )
	return "application/gzip";
	else if( strpos( $archivo, ".doc" ) )
		return "application/doc";
	else if( strpos( $archivo, ".txt" ) )
		return "application/plain";
	else if( strpos( $archivo, ".html" ) )
		return "application/html";
	else if( strpos( $archivo, ".pdf" ) )
		return "application/pdf";
	else if( strpos( $archivo, ".swf" ) )
		return "application/x-shockwave-flash";
	else
		return "application/octet-stream";
	}


//comprueba que la extencion del archivo a subir coincida con los establecidos
function comprobar_extencion_imagen( $archivo )
	{
	/*
	image/jpeg	Imagen JPEG, JPG
	image/png	Imagen PNG
	image/gif	Imagen GIF
	*/
	while( list($g, $h)=each($archivo) ) //recorremos todos los FILES
		{
		$i=0; //inicializamos foco "apagado"=0
		while( list($w, $z)=each($h) )
			{
			if( !strcmp($w, "name") && strcmp( $z, "" ) ) //si existe nombre de archivo
				$i=1; //encendemos foco
			if( !strcmp($w, "type") && strcmp( $z, "" ) ) //si existe tipo
				{
				if( !strpos( $z, "jpg" ) && !strpos( $z, "jpeg" ) && !strpos( $z, "png" ) && !strpos( $z, "gif" ) )
					{
					echo "Tipo de extencion/archivo inaceptable.<br>";
					return 0;
					}
				$i=0; //apagamos foco
				}
			}
		}

	unset($i);
	return 1;
	}

//comprueba que la extencion del archivo a subir coincida con los establecidos
function comprobar_extencion_archivo( $archivo )
	{
	/*
	application/zip			Archivo ZIP
	application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
	application/gzip		Archivo TAR.GZ
	application/pdf			Archivo PDF
	text/plain			Archivo TXT
	text/html			Archivo HTML
	application/x-shockwave-flash	Archivo SWF
	*/

	while( list($g, $h)=each($archivo) ) //recorremos todos los FILES
		{
		$i=0; //inicializamos foco "apagado"=0
		while( list($w, $z)=each($h) )
			{
			if( !strcmp($w, "name") && strcmp( $z, "" ) ) //si existe nombre
				$i=1; //encendemos foco
			if( !strcmp($w, "type") && strcmp( $z, "" ) ) //sie xiste tipo
				{
				if( !strpos( $z, "jpg" ) && !strpos( $z, "mp3" )  && !strpos( $z, "zip" ) && !strpos( $z, "gzip" ) && !strpos( $z, "pdf" ) && !strpos( $z, "plain" ) && !strpos( $z, "html" ) && !strpos( $z, "flash" ) && !strpos( $z, "octet-stream" ) && !strpos( $z, "opendocument" ) )
					{
					echo "Tipo de extencion/archivo inaceptable.<br>";
					return 0;
					}
				$i=0; //apagamos foco
				}
			}
		}
	unset($i);
	return 1;
	}

//comprueba que la extencion del archivo a subir coincida con los establecidos
function comprobar_extencion_archivo_scriptin( $archivo )
	{
	/*
	application/zip			Archivo ZIP
	application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
	application/gzip		Archivo TAR.GZ
	application/pdf			Archivo PDF
	text/plain			Archivo TXT
	text/html			Archivo HTML
	application/x-shockwave-flash	Archivo SWF
	*/

	while( list($g, $h)=each($archivo) ) //recorremos todos los FILES
		{
		$i=0; //inicializamos foco "apagado"=0
		while( list($w, $z)=each($h) )
			{
			if( !strcmp($w, "name") && strcmp( $z, "" ) ) //si existe nombre
				$i=1; //encendemos foco
			if( !strcmp($w, "type") && strcmp( $z, "" ) ) //si existe tipo
				{
				if( !strpos( $z, "php" ) && strcmp($z, "application/octet-stream") )
					{
					echo "Tipo de extencion/archivo inaceptable.<br>";
					return 0;
					}
				$i=0; //apagamos foco
				}
			}
		}
	unset($i);
	return 1;
	}

function lista_grupos( $modo )
	{
	$grupos= array( "1"=>"Administrador", "2"=>"Autor", "3"=>"Editor", "4"=>"CoAdmin", "5"=>"Soporte Tecnico", "10"=>"Usuario" );
	$cons= consultar_enorden( "PRIVILEGIOS", "NOMBRE" );
	$r= array();
	if( !strcmp($modo, "select") )
		{
		if( mysql_num_rows($cons)==0 )
			echo "<option>No existen grupos aun...</option>";
		else
			{
			while( $buf= mysql_fetch_array($cons) )
				$r[]= $buf["NOMBRE"];
			unset($buf);
			}
		}
	else if( !strcmp($modo, "getmyname") )	# retorna NOMBRe del grupo del usuario
		{
		$gr= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["log_id"]. "'", "TIPO_USR" ); # el id del grupo
		foreach( $grupos as $key=>$val )
			{
			if( !strcmp($key, $gr) )	# si es el mismo ID
				$r= $val;	# ponemos nombre
			}
		unset($gr);
		}
	else if( !strcmp($modo, "getmynameid") )	# retorna ID del grupo del usuario
		$r= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["log_id"]. "'", "TIPO_USR" ); # el id del grupo
	limpiar($cons);
	unset($cons);
	return $r;
	}

function lista_paises( $modo )
	{
	$cons= consultar_enorden( "MUNDO", "PAIS" );
	switch( $modo )
		{
		case 'select':
			while( $buf= mysql_fetch_array($cons) )
				{
				echo "<option value=\"". $buf["PAIS"]. "\">";
				echo $buf["PAIS"];
				echo "</option>";
				}
			unset($buf);
			break;
		}
	unset($cons);
	}


function mes_esp( $dato )
	{
	if( !strcmp( $dato, "01" ) )
		return "Enero";
	else if( !strcmp( $dato, "02" ) )
		return "Febrero";
	else if( !strcmp( $dato, "03" ) )
		return "Marzo";
	else if( !strcmp( $dato, "04" ) )
		return "Abril";
	else if( !strcmp( $dato, "05" ) )
		return "Mayo";
	else if( !strcmp( $dato, "06" ) )
		return "Junio";
	else if( !strcmp( $dato, "07" ) )
		return "Julio";
	else if( !strcmp( $dato, "08" ) )
		return "Agosto";
	else if( !strcmp( $dato, "09" ) )
		return "Septiembre";
	else if( !strcmp( $dato, "10" ) )
		return "Octubre";
	else if( !strcmp( $dato, "11" ) )
		return "Noviembre";
	else
		return "Diciembre";
	}
	
function proteger_cadena( $cadena )
	{
	return htmlentities($cadena, ENT_QUOTES, "UTF-8");
	}

function desproteger_cadena_src( $cadena )
	{
	$out= html_entity_decode($cadena, ENT_QUOTES, "UTF-8");

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );

	$out= msg2msgtags($out);
	#$out= msg2caritas($out);
	#$out= msg2caritas_extra($out);

	return $out;
	}
	
function desproteger_cadena( $cadena )
	{
	$out=$cadena;
	$out= html_entity_decode( $out, ENT_QUOTES);

	if( strchr( $out, "<" ) )
		$out= str_replace( "<", htmlentities("<", ENT_QUOTES ), $out );
	if( strchr( $out, ">" ) )
		$out= str_replace( ">", htmlentities(">", ENT_QUOTES), $out );

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );
	
	$out= utf8_encode( str_replace("|","/",$out) );
	$out= msg2msgtags($out);
	#$out= msg2caritas($out);
	#$out= msg2caritas_extra($out);
	
	return $out;
	}

function desproteger_cadena_xml( $cadena )
	{
	$out= desproteger_cadena($cadena);
	$out= utf8_decode($out);
	$arr= array( "\n"=>"<br />", "&nbsp;"=>"&#160;", "&ntilde;"=>"&#241;" );
	
	foreach( $arr as $key=>$val )
		{
		if( strstr($out, $key ) )
			$out= str_replace( $key, $val, $out );
		}
	
	$out= utf8_encode( $out );
	return $out;
	}

function desproteger_cadena_comment( $cadena )
	{
	$out=$cadena;
	$out= html_entity_decode( $out, ENT_QUOTES);

	if( strchr( $out, "<" ) )
		$out= str_replace( "<", htmlentities("<", ENT_QUOTES), $out );
	if( strchr( $out, ">" ) )
		$out= str_replace( ">", htmlentities(">", ENT_QUOTES), $out );

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );
	
	$out= msg2msgtags($out);
	#$out= msg2caritas($out);
	#$out= msg2caritas_extra($out);
	
	return $out;
	}
	
function eliminar_acentos( $cadena )
	{
	if( strchr( $out, "á" ) )
		$out= str_replace( "á", "a", $out );
	if( strchr( $out, "é" ) )
		$out= str_replace( "é", "e", $out );
	if( strchr( $out, "í" ) )
		$out= str_replace( "í", "i", $out );
	if( strchr( $out, "ó" ) )
		$out= str_replace( "ó", "o", $out );
	if( strchr( $out, "ú" ) )
		$out= str_replace( "ú", "u", $out );
	}

function comprobar_variables( $base_dd, $base_dd2, $valor )
	{
	if( strchr($valor, ",") )
		{
		$x= explode( ",", $valor );
		
		$a= consultar_con( $base_dd, "ID='". $x[0]. "'" );
		$b= consultar_con( $base_dd2, "ID='". $x[1]. "':RELACION='". $x[0]. "'" );
		
		if( mysql_num_rows($a)==0 || mysql_num_rows($b)==0 )
			{
			unset($a);
			unset($b);
			unset($x);
			return 0;
			}
			
		$buf= mysql_fetch_array($b);

		unset($a);
		unset($b);
		unset($x);

		/*##################################
		#########		NOTICIAS		#########
		####################################*/
		if( !strcmp($buf["TIPO"], "noticia") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "" ) && strcmp($_POST["mensaje_noticia"], "" ) )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "noticia_limpia") )
			{
			unset($buf);

			if( strcmp( $_POST["titulo_noticia"], "" ) && strcmp($_POST["mensaje_noticia"], "") )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "galeria") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "") && strcmp($_POST["mensaje_noticia"], "") )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "descriptiva") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "") && strcmp($_POST["mensaje_noticia"], "") && ( strcmp($_FILES["archivo_noticia_01"]["name"], "" ) || $_POST["manual_file"] ==1) )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "scriptin") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "") && strcmp($_FILES["archivo_noticia_01"]["name"], "" ) )
				return 1;
			}
		}

	/*########################################
	#########	USUARIOS MENSAJERIA	#########
	##########################################*/
	if( !strcmp($valor, "enviar_mensaje") )
		{
		if( strcmp($_POST["mensajeria_usuario"], "") && strcmp($_POST["mensajeria_asunto"], "") && strcmp($_POST["mensajeria_msg"], "" ) )
			return 1;
		}
		
	return 0;
	}

function evitar_repeticion( $tipo, $dato, $bdd )
	{
	//comprobar registros ID
	if( !strcmp($tipo, "registro") )
		{
		$cons= consultar_con( $bdd, "ID='". $dato. "'" );
		
		if( mysql_num_rows($cons)>0 )
			echo "<td>Error: El Identificador (ID) que intentas insertar ya existe...<br>";
		}
		
	//comprobar imagenes
	else if( !strcmp($tipo, "imagenes") )
		{
		$cons= consultar( $bdd, "*" );
		while( $buf= mysql_fetch_array($cons) ) //recorriendo registros
			{
			if( strcmp( $buf["IMAGENES_URL"], "") ) //si no esta vacio, accedemos
				{
				$tmp="";
				
				//Obtenemos nombres de imagenes existentes
				if( strchr( $buf["IMAGENES_NOMBRE"], ":" ) ) //si existe concatenador, son varias imagenes
					$tmp= explode( ":", $buf["IMAGENES_NOMBRE"] ); //dividimos
				else //solo es una imagen
					$tmp= $buf["IMAGENES_NOMBRE"]; //copiamos nombre
					
				//Comparacion de nombres para evitar imagenes duplicadas
				if( strchr($buf["IMAGENES_NOMBRE"], ":") ) //si existe concatenador, son varias imagenes
				if( strchr($buf["IMAGENES_NOMBRE"], ":") ) //si existe concatenador, son varias imagenes
					{
					for( $i=0; $i<count($tmp); $i++ )
						{
						if( !strcmp($buf["IMAGENES_NOMBRE"], $dato) ) //si tienen el mismo nombre
							{
							echo "<td>La imagen <b>". $tmp[$i]. "</b> ya existe, asi que no sera subida al servidor.<br>";
							return 1; //error
							}
						}
					}
				else //solo es una imagen
					{
					if( !strcmp($buf["IMAGENES_NOMBRE"], $dato) ) //si tienen el mismo nombre
						{
						echo "<td>La imagen <b>". $dato. "</b> ya existe, asi que no sera subida al servidor.<br>";
						return 1; //error
						}
					}
				}
			} 
		}

	//comprobar archivos
	else if( !strcmp($tipo, "archivos") )
		{
		$cons= consultar( $bdd, "*" );
		while( $buf= mysql_fetch_array($cons) ) //recorriendo registros
			{
			if( strcmp( $buf["ARCHIVOS_URL"], "") ) //si no esta vacio, accedemos
				{
				$tmp="";
				
				//Obtenemos nombres de imagenes existentes
				if( strchr( $buf["ARCHIVOS_NOMBRE"], ":" ) ) //si existe concatenador, son varias imagenes
					$tmp= explode( ":", $buf["ARCHIVOS_NOMBRE"] ); //dividimos
				else //solo es una imagen
					$tmp= $buf["ARCHIVOS_NOMBRE"]; //copiamos nombre
					
				//Comparacion de nombres para evitar imagenes duplicadas
				if( strchr($buf["ARCHIVOS_NOMBRE"], ":") ) //si existe concatenador, son varias imagenes
					{
					for( $i=0; $i<count($tmp); $i++ )
						{
						if( !strcmp($buf["ARCHIVOS_NOMBRE"], $dato) ) //si tienen el mismo nombre
							{
							echo "<td>El archivo</td> <b>". $tmp[$i]. "</b> ya existe, asi que no sera subida al servidor.<br>";
							return 1; //error
							}
						}
					}
				else //solo es una imagen
					{
					if( !strcmp($buf["ARCHIVOS_NOMBRE"], $dato) ) //si tienen el mismo nombre
						{
						echo "<td>El archivo <b>". $dato. "</b> ya existe, asi que no sera subida al servidor.<br>";
						return 1; //error
						}
					}
				}
			} 
		}
		
	//comprobar imagenes del buzon
	else if( !strcmp($tipo, "buzon") )
		{
		$cons= consultar( $bdd, "*" );
		while( $buf= mysql_fetch_array($cons) ) //recorriendo registros
			{
			if( strcmp( $buf["IMAGENES_URL"], "") ) //si no esta vacio, accedemos
				{
				$tmp="";
				
				//Obtenemos nombres de imagenes existentes
				if( strchr( $buf["ADJUNTOS"], ":" ) ) //si existe concatenador, son varias imagenes
					$tmp= explode( ":", $buf["ADJUNTOS"] ); //dividimos
				else //solo es una imagen
					$tmp= $buf["ADJUNTOS"]; //copiamos nombre
					
				//Comparacion de nombres para evitar imagenes duplicadas
				if( strchr($buf["ADJUNTOS"], ":") ) //si existe concatenador, son varias imagenes
					{
					for( $i=0; $i<count($tmp); $i++ )
						{
						if( !strcmp($buf["ADJUNTOS"], $dato) ) //si tienen el mismo nombre
							{
							echo "<td>La imagen <b>". $tmp[$i]. "</b> ya existe, asi que no sera subida al servidor.<br>";
							return 1; //error
							}
						}
					}
				else //solo es una imagen
					{
					if( !strcmp($buf["ADJUNTOS"], $dato) ) //si tienen el mismo nombre
						{
						echo "<td>La imagen <b>". $dato. "</b> ya existe, asi que no sera subida al servidor.<br>";
						return 1; //error
						}
					}
				}
			} 
		}
	
	unset($cons);
	return 0; //exito
	}

	
function rating_download( $muestra )
	{
	$cons= consultar_con( "DOWNLOAD_RATING", "ID='". $muestra. "'" );
	
	if( mysql_num_rows($cons)==0 )
		{
		unset($cons);
		return 0;
		}
	else
		{
		$buf= mysql_fetch_array($cons);
		return $buf["RATING"];
		unset($buf);
		unset($cons);
		}
	}

//limpia la cadena dependiendo lo que se desee eliminar
function limpiar_cadena( $cadena, $modo )
	{
	if( !strcmp($modo, "espacios") ) //limpia espacios
		{
		$tmp="";
		for( $i=0; $i<strlen(proteger_cadena($cadena)); $i++ )
			{
			if( strcmp($cadena[$i], " ") ) //si no es un espacio
				$tmp .= $cadena[$i]; //concatenamos
			}
		return $tmp;
		}
	else if( !strcmp($modo, "comas") ) //limpia comas
		{
		$tmp="";
		for( $i=0; $i<strlen(proteger_cadena($cadena)); $i++ )
			{
			if( strcmp($cadena[$i], ",") ) //si no es un espacio
				$tmp .= $cadena[$i]; //concatenamos
			}
		return $tmp;
		}
	}
	
function verificar_correos_grupos( $mail_dest )
	{
	$mails="";
	
	if( strchr( $mail_dest, " ") ) //si existen espacios
		$mail_dest= limpiar_cadena($mail_dest, "espacios" ); //limpiamos espacios

	if( strchr( $mail_dest, ",") ) //si existen comas
		{
		$xmails= explode( ",", $mail_dest ); //dividimos variable
			
		for( $i=0; $i<sizeof($xmails); $i++ )
			{
			if( strcmp( $mails, "" ) )
				$mails .= ",";
					
			if( strchr( $xmails[$i], "@" ) ) //si existe un arroba
				{
				if( !validar_email( $xmails[$i] ) )
					{
					echo 'E-Mail que se intento enviar es invalido.<br>';
					return 0;
					}
				else	$mails .= $xmails[$i];
				}
			else //entonces es un grupo de usuarios
				{
				if( mysql_num_rows(consultar_con("PRIVILEGIOS", "NOMBRE='". $xmails[$i]. "'"))==0 )
					return 0;
				else
					$mails .= $xmails[$i];
				}
			}
		}
	else //es un unico valor
		{
		if( strchr( $mail_dest, "@" ) ) //si existe un arroba
			{
			if( !validar_email( $mail_dest ) )
				{
				echo 'E-Mail que se intento enviar es invalido.<br>';
				return 0;
				}
			else	$mails .= $mail_dest;
			}
		else //entonces es un grupo de usuarios
			{
			if( mysql_num_rows(consultar_con("PRIVILEGIOS", "NOMBRE='". $mail_dest. "'"))==0 )
				{
				echo 'No se encuentra el grupo ['. $mail_dest. ']<br>';
				return 0;
				}
			else
				$mails .= $mail_dest;
			}
		}
	return $mails;
	}

function email_tipo_marketing($email_tipo)
	{
	if( !strcmp($email_tipo, "E") ) return 'Empresa';
	else if( !strcmp($email_tipo, "P") ) return 'Persona';
	else if( !strcmp($email_tipo, "P") ) return 'Persona';
	else return '';
	}
	
# funcion para evitar repeticion de envio, retorn un 1 si NO se repite, y un 0 si se repite
function evitar_repeticion_mails( $mails_db, $email )
	{
	if( !$mails_db || !$email )
		return 1;
	if( strstr($mails_db, $email) )
		return 0; //el mail se ha repetido
	return 1; //el mail NO se esta repitiendo
	}
	
# funcion para envio de correos
function enviar_correo( $to, $asunto, $modo, $enlace, $adjunto, $from, $log, $link_custom )
	{
	$subject=$asunto; //titulo del correo	
	$mail = new PHPMailer;
	$mail->CharSet = 'utf-8';

	if( SENDINBLUE ) # servicio sendinblue.com
		{
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = false;
		$mail->Host = SENDINBLUE_SMTP;
		$mail->Port = SENDINBLUE_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SENDINBLUE_USER;
		$mail->Password = SENDINBLUE_PASS;
		}
	else 	$mail->isSendmail();

	# from - de
	if( strstr($from, "<") && strstr($from, ">") )
		{
		$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
		preg_match_all($patron, $from, $buf, PREG_OFFSET_CAPTURE );
		$mail->setFrom($buf[2][0][0], $buf[1][0][0]);
		unset($patron, $buf);
		}
	else 	$mail->setFrom($from, TITULO_WEB );

	# replay_to - responder a/hacia
	#$repl= consultar_datos_general( "USUARIOS", "ID='4dm1n'", "EMAIL" );
	#$mail->addReplyTo( ($repl ? $repl:NULL), NULL );
	#unset($repl);

	# to - para
	if( strstr($to, ",") ) # si son varios
		{
		$x= explode(",", $to);
		foreach( $x as $key )
			{
			if( strstr($key, "<") && strstr($key, ">") )
				{
				$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
				preg_match_all($patron, $key, $buf, PREG_OFFSET_CAPTURE );
				$mail->addAddress($buf[2][0][0], $buf[1][0][0]);
				unset($patron, $buf);
				}
			else 	$mail->addAddress($key);
			}
		unset($x);
		}
	else # solo uno
		{
		if( strstr($to, "<") && strstr($to, ">") )
			{
			$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
			preg_match_all($patron, $to, $buf, PREG_OFFSET_CAPTURE );
			$mail->addAddress($buf[2][0][0], $buf[1][0][0]);
			unset($patron, $buf);
			}
		else 	$mail->addAddress($to);
		}

	$mail->isHTML(true);
	$mail->Subject = $subject;
	$html= file_get_contents((CWD ? CWD.'/':'').'newsletter.html'); # obtenemos el stream del HTML Plantilla
	$html= preg_replace( '/\[EMPRESA\]/', desproteger_cadena_src(SLOGAN), $html );
	$html= preg_replace( '/\[EMPRESA_URL\]/', HTTP_SERVER, $html );
	$html= preg_replace( '/\[BLOG_URL\]/', HTTP_SERVER.'blog/#blog', $html );
	$html= preg_replace( '/\[TERMINOSDEUSO_URL\]/', HTTP_SERVER.'terminos_de_uso/#terminos_de_uso', $html );
	$html= preg_replace( '/\[PRIVACIDAD_URL\]/', HTTP_SERVER.'privacidad/#privacidad', $html );
	$html= preg_replace( '/\[PRIVACIDAD_URL\]/', HTTP_SERVER.'privacidad/#privacidad', $html );
	$html= preg_replace( '/\[TITULO\]/', $asunto, $html );
	$html= preg_replace( '/\[IMG_POST\]/', 'data:image/'. substr($log, -3). ';base64,'.base64_encode(file_get_contents($log)), $html );
	$html= preg_replace( '/\[BOTONES\]/', $link_custom, $html );
	$html= preg_replace( '/\[LOGO\]/', 'data:image/'. substr(LOGO_PNG, -3). ';base64,'. base64_encode(file_get_contents(LOGO_PNG)), $html );


		if( $modo==0 ) //enviar correo para comentarios
			{
			//Cuerpo o contexto del mensaje, la esencia del correo, el todo ;) 
			$cuerpo= "Han publicado un nuevo comentario en <b>http://". $_SERVER['HTTP_HOST']. "</b>.<br>Enlace al tema: ";
			$cuerpo .= "<b></b><a href=\"". $enlace. "\" target=\"_blank\">". $enlace. "</a></b>";
			}
		else if( $modo==1 ) //error en correo para comentarios
			{
			$trama_deficiente= $_POST;
			//Cuerpo o contexto del mensaje, la esencia del correo, el todo ;) 
			$cuerpo= "Se produjo un error en el servidor <b>http://". $_SERVER['HTTP_HOST']. "</b> al interntar enviar aviso de notificacion a las bandejas, ";
			$cuerpo .= "la notificacion se intento enviar en blanco o sin u enlace hacia la noticia donde se publico el comentario.";
			$cuerpo .= "<p>A continuacion de muestra la trama deficiente obtenida: <br>". $trama_deficiente;
			unset($trama_deficiente);
			}
		else if( $modo==2 ) //enviar correo de nuevo usuario
			{
			$cuerpo= "Un nuevo usuario se ha registrado al sitio <b>http://". $_SERVER['HTTP_HOST']. "</b>, los datos del usuario son:";
			$cuerpo .= "<p>Usuario <b>". $enlace. "</b></p>";
			}
		else if( $modo==3 ) //enviar correo de noticia nueva
			{
			$cuerpo= "Te escribimos para informarte que se a publicado una nueva noticia en el sitio <b>". $_SERVER['HTTP_HOST']. "</b>, ";
			$cuerpo .= "gracias por preferir nuestra comunidad y esperamos que este nuevo <b>servicio informativo</b> te sea de ayuda para enterarte ";
			$cuerpo .= "de una forma rapida y facil de las novedades de nuestro sitio web.";
			}
		else if( $modo==4 ) //enviar correo de recuperacion de datos del usuario
			{
			$cuerpo= "<b>Sistema de Recuperacion de Datos.</b>";
			$cuerpo .= "<p>Te informamos que hemos obtenido una solicitud de datos de tu cuenta en nuestra pagina <b>". $_SERVER['HTTP_HOST']. "</b>, con motivos de ";
			$cuerpo .= "<b>recuperacion de usuario y password</b> dicha solicitud provino de:";
			$cuerpo .= "<p>IP: <b>". $_SERVER['REMOTE_ADDR']. "</b><br>";
			$cuerpo .= "Nombre Host: <b>";
			//obteniendo nombre del host
			$info= gethostbyaddr($_SERVER['REMOTE_ADDR']);
			if( strcmp( $info, $_SERVER['REMOTE_ADDR']) )
				$cuerpo .= $info;
			else $cuerpo.= "<b>no se pudo obtener</b>";
			unset($info);
			
			$cuerpo .= "</b><br>";
			$cuerpo .= "Fecha: <b>". date( "d/m/y", time() ). " a las ". date( "g:i a", time() ). "</b>";
			$cuerpo .= "<p>Los datos solicitados referentes a tu cuenta son:";
			
			$x= explode( "|", $enlace );
			$cuerpo .= "<p><b>Username: </b>". $x[0];
			$cuerpo .= "<br><b>Password: </b>". $x[1];
			unset($x);
			}
		else if( $modo==5 ) //enviar correo de Mensaje Privado recivido
			{
			$cuerpo= "Te escribimos para informarte que has recivido un Mensaje Privado de <b>". $enlace. "</b> en tu cuenta con <b>". $_SERVER['HTTP_HOST']. "</b>, ";
			$cuerpo .= "gracias por preferir nuestra comunidad y esperamos que este <b>servicio informativo</b> te sea de utilidad.";
			}
		else if( $modo==6 ) //publicidad, no existe cuerpo este biene explicitamente creado en $enlace
			{
			if( strcmp($log, "0") ) //si existe registo del log en la BDD
				{
				$mailing_log='<center><img src="http://'. $_SERVER['HTTP_HOST']. '/monitor.php?id='. $log. '"><center><br>';
				$cuerpo .= $mailing_log;
				unset($mailing_log);
				}
			//opciones de compartimiento en redes sociales
			$cuerpo= '<br><center>Comparte este Anuncio:<br>';
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=digg">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/digg.png" border="0" alt="Digg" title="Digg"></a>'; # Digg
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=delicious">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/delicious.png" border="0" alt="del.icio.us" title="del.icio.us"></a>'; # del.icio.us
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=facebook">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/facebook.png" border="0" alt="Facebook" title="Facebook"></a>'; # facebook 
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=google_bookmarks">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/google_bookmarks.png" border="0" alt="Google Bookmarks" title="Google Bookmarks"></a>'; # google bookmarks
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=barrapunto">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/barrapunto.png" border="0" alt="Barrapunto" title="Barrapunto"></a>'; # Barrapunto
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=meneame">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/meneame.png" border="0" alt="Meneame" title="Meneame"></a>'; # meneame
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=technorati">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/technorati.png" border="0" alt="Technorati" title="Technorati"></a>'; # Technorati
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=twitter">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/twitter.png" border="0" alt="Twitter" title="Twitter"></a>'; # Twitter
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=yahoo_bookmarks">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/yahoo_bookmarks.png" border="0" alt="Yahoo! Bookmarks" title="Yahoo! Bookmarks"></a>'; # Yahoo! Bookmarks
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=identica">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/identica.png" border="0" alt="identi.ca" title="identi.ca"></a>'; # identi.ca
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=live">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/live.png" border="0" alt="Live" title="Live"></a>'; # Live
			$cuerpo .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=bitacoras">
				<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/bitacoras.png" border="0" alt="Bitacoras.com" title="Bitacoras.com"></a>'; # Bitacoras.com
			# $cuerpo .= '<a href="'. $_SERVER['HTTP_HOST']. '/share.php?id='. $log. '&net=">
			#	<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/redes_sociales/" border="0" alt="" title=""></a>';
			$cuerpo .= '</center><br>';
			
			//si no ves el mail, enlace 
			//$cuerpo .= '<center>Si no puede ver la imagen da <a hre=""><b>CLICK AQUI</b></a></center><br>';
				
			if( strcmp( $link_custom, "" ) && strcmp( $link_custom, "0" ) ) //agregamos link del anuncio/contenido
				$cuerpo .= "<a href=\"http://". $_SERVER['HTTP_HOST']. "/monitor.php?id=". $log. "&x=a\">";
				
			$cuerpo .= desproteger_cadena($enlace);

			if( strcmp( $link_custom, "" ) && strcmp( $link_custom, "0" ) ) //agregamos cierre de link del anuncio/contenido
				$cuerpo .= "</a>";
			}
		else if( $modo==7 ) // el usuario manda el contenido
			$cuerpo= $enlace;

		$html= preg_replace( '/\[MENSAJE\]/', $cuerpo, $html );
	
	$mail->msgHTML($html, NULL);
	# Replace the plain text body with one created manually
	$mail->AltBody = NULL;

	//archivos adjuntos
	if( strcmp($adjunto, "0") && strcmp($adjunto, "vacio") && strcmp($adjunto, "") )
		$mail->addAttachment($adjunto);

	
	if (!$mail->send())
		return 0;
	else
		return 1;
	}

function validar_dominio( $dominio )
	{
	$dom='';
	# si tiene arroba, hay que validar dominio de un correo
	if( strstr( $dominio, "@") ) # si tiene arroba, hay que validar dominio de un correo
		{
		$x= explode("@", $dominio);
		$dom= $x[1]; # tomamos dominio 
		}
	else		$dom= $dominio;
	
	if( gethostbynamel($dom) ) # comprobando dominio con www
		return 1; # exito
	else if( gethostbynamel('www.'.$dom) ) # comprobando dominios sin www
		return 1;
	return 0;
	}

function validar_usuario( $data )
	{
	$invalid= ',./;\'[]`<>?:"{}\\|+_)()*&^%$#@!~';
	$fl=0;

	for( $i=0; $i<strlen($invalid); $i++ )
		{
		if( !$fl && strstr($data, $invalid[$i]) )
			$fl=1;
		}

	return $fl;
	}

function validar_email( $mail )
	{
	if( !strstr($mail, "@") ) # si no contiene la arroba
		return 0;
	else
		{
		$validinbox= array("hotmail.com", "gmail.com", "yahoo.com"); #mails comerciales
		$x= explode( "@", $mail );
		
		# comprobaremos si lo que esta la izquierda del @ no tenga caracteres invalidos
		if( !$x[0] )	return 0; # si no existe dato, entonces esta erroneo el mail
		$novalid= array( "|", "/", "\\", ",", "+", "?", "\'", "(", ")", "&", "%", "$", "\"", "@", "!", "~", "{", "}", "[", "]", "<", ">" );
		foreach( $novalid as $key )
			{
			if( strstr($x[0], $key) ) # si tiene el caracter
				return 0; # error
			}
		
		# comprobamos que el dominio tenga si quiera un punto
		if( !strstr($x[1], ".") )		return 0;

		# comprobamos que no sea un proveedor de correo publico conocido 
		foreach($validinbox as $key)
			{
			if( !strcmp($key, $x[1]) )
				return 1;
			}
			
		# comprobamos lo que esta a la derecha del @
		if( !validar_dominio($x[1]) )
			return 0;
		return 1;
		}
	}

function ver_contenido_directorio( $target_dir, $arg, $tipo )
	{
	$abc= "../".$target_dir;
	if( $dir= opendir( $abc ) )
		{
		switch($tipo)
			{
			case 'select':
				if( strcmp($arg, "" ) )
					echo "<option value=\"". $arg. "\">". $arg. " <i>(cargado)</i></option>";
					
				while( ($buf_dir=readdir($dir))!==FALSE )
					{
					if( strcmp( $target_dir.$buf_dir, $arg ) && strcmp( $buf_dir, "." ) && strcmp( $buf_dir, ".." ) )
						echo "<option value=\"". $target_dir.$buf_dir. "\">". $buf_dir. "</option>";
					unset($buf_dir);
					}
				break;
			}
		}
	else
		echo "<option>Error al leer carpeta: ". $target_dir. "...</option>";
	
	closedir($dir);
	}
	
//Funcion que corta el contenido de un mensaje a un numero limitado de caracteres
function noticia_cortada( $msg, $limite )
	{
	$total= strlen($msg); //tomamos longitud del mensaje
	
	if( $total>$limite ) //si el mensaje es mayor al limite establecido
		return substr( $msg, 0, $limite );
	else //retornamos el mensaje
		return $msg;
	}

# proporciona imagenes de un directorio o bien solo una
function get_image( $path, $m )
	{
	$dir= opendir($path);
	if( $m==0 ) # solo la primer imagen
		{
		$fl=0;
		do
			{
			$buf=readdir($dir);
			if( strcmp($buf, ".") && strcmp($buf, "..") )
				$fl=1;
			}while( $fl==0 );
		closedir($dir);
		return $path.$buf;
		}
	else if( $m==1 ) # todas las imagenes en un array
		{
		$data= array();
		while( ($buf=readdir($dir)) !== FALSE)
			{
			if( strcmp($buf, ".") && strcmp($buf, "..") )
				$data[]= $buf;
			}
		closedir($dir);
		return $data;
		}
	return 0;
	}

//Funcion para visualizar imagenes
function imagenes_rollout( $path, $img_nom, $modo )
	{
	if( $modo==0 ) # se visualiza una imagen
		{
		echo '<img src="'. get_image(HTTP_SERVER. $path.$img_nom, 0). '">';
		}
	else if( $modo==1 ) # se visualizan todas las imagenes
		{
		$x= explode(",", $img_nom); # explotamos
		foreach($x as $key )
			{
			if( is_dir($path.$key) ) # si es directorio
				{
				$dir= opendir($path.$key); # abrimos directorio
				while(($buf=readdir($dir))!==FALSE)
					{
					if( strcmp($buf, ".") && strcmp($buf, "..") && strstr($buf, "_resized.") )
						{
						echo '<a href="#" onclick="cargar_datos( \'own=me&op=verimagen&id_not='. $img_nom. '&img='. proteger_cadena($buf). '\', \'supercapa\', \'GET\', \'0\'); 
						supercapa(\'supercapa\', \'#000000\', \'50\');">
						<img src="'. $path.$key.'/'.$buf. '" border="0"></a>';
						}
					}
				closedir($dir); # cerramos
				}
			else		# es una imagen
				echo '<img src="'. HTTP_SERVER.$path.$key. '" border="0">';
			}
		}
	else if( $modo==2 ) # se visualizan todas dentro de un div (roll out)
		{
		echo 'hola';
		}
	
	unset($buf);
	}

function imprimir_numeros( $inicio, $maximo, $modo )
	{
	switch( $modo )
		{
		case 'select':
			for( $i=$inicio; $i<($maximo+1); $i++ )
				echo "<option value=\"". $i. "\">". $i. "</option>";
			break;
		default:
			for( $i=$inicio; $i<($maximo+1); $i++ )
				echo $i. "<br>";
			break;
		}
	}





 
?>
