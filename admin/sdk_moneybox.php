<?php
#
# Version 2.0
#
# MONEYBOX.COM.MX
# Web http://www.moneybox.com.mx
# Autor: M.S.I. Angel Cantu Jauregui
# Mail angel.cantu@sie-group.net
# Fecha Enero 19 2014, 08:30:00
# Software by www.sie-group.net
#
# Está obra está sujeta a la licencia Reconocimiento-CompartirIgual 3.0 Unported de Creative Commons. 
# Para ver una copia de esta licencia, visite http://creativecommons.org/licenses/by-sa/3.0/.
/*
Link Licencia:
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES"><img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">src.mx - mas que un simple acortador</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="www.sie-group.net" property="cc:attributionName" rel="cc:attributionURL">sie-group.net</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">Creative Commons Reconocimiento-CompartirIgual 3.0 Unported License</a>.
*/

#
# El SDK de moneybox.com.mx te ayudara a realizar las peticiones de forma transparente a nuestros 
# servidores, con solo incluir este PHP en tus proyectos de software.
#

# definicion de variables
define( MONEYBOX_API, 'api.moneybox.com.mx' );
# define( MONEYBOX_API, 'apimoneybox.yo.com' );
define( MONEYBOX_PATH, '/index.php' ); # servidor de api
define( MONEYBOX_AUTH, 'siegroup,A1s4r1l2n' ); # en un futuro

# libreria de sockets - DESCOMENTA EL INCLUDE
#
# inclue( "iosockets.php" );
#

#
# funcion principal del SDK
# argumentos:
#		$metodo --> el servicio que se demanda
#		$bigdata --> parametros varios
#			:: 
function moneybox( $metodo, $bigdata )
	{
	$x= explode(",", MONEYBOX_AUTH);
	$jsondata= array( 
		"user"=>($x[0] ? $x[0]:0),				# usuario 
		"pass"=>($x[1] ? $x[1]:0), 			# password
		"secret"=>($bigdata["secret"] ? $bigdata["secret"]:0),	# si tienes, agregalo
		"root"=>($bigdata["root"] ? $bigdata["root"]:0),	# si tienes, agregalo
		"metodo"=>$metodo, 					# el servicio
		"xml"=>$bigdata["xml"],			# el XML a timbrar
		"uuid"=>$bigdata["uuid"], 		# el UUID a cancelar o consultar
		"cliente"=>$bigdata["cliente"], # Datos del Cliente para registro, editar o eliminacion
		"extra"=>($bigdata["extra"] ? $bigdata["extra"]:0),		# si tienes extras
		"data"=>($bigdata["data"] ? $bigdata["data"]:0)		# la data
		 );

	$jsontrama= json_encode($jsondata); # url encodeada

	# usando curl
	$q= curl_iodata( MONEYBOX_API, array( 'POST', MONEYBOX_PATH, $jsontrama, "json" ), 443 ); # enviamos consulta
	$a= explode( "\r\n\r\n", $q);
	$r= json_decode($a[1]);

	unset($jsondata, $jsontrama, $a, $x, $q);
	return $r;
	}
?>