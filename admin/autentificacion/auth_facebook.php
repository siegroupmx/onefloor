<?php
if( !strcmp($log, "salir") && !strcmp( $_GET["social"], "facebook") )
	{
	if( !is_login() ) # si no esta logeado, entonces posiblemente aun no ha accedido a nuestro portal
		{
		$fbid= facebook_conectar(); # obtenemos el ID del usuario 
		if( !$fbid ) # si no hay ID, es un bruteforcing o ataque 
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		else # si existe sesion o conexion con nuestro portal 
			{
			$iduser= consultar_datos_general( "USUARIOS", "FACEBOOK_ID='". proteger_cadena($fbid). "'", "ID" ); # obtenemos ID
			$trama= array( "id"=>"'". proteger_cadena($iduser). "'", "facebook_id"=>"'0'", "facebook_token"=>"'0'", "online"=>"'0'" );
			if( actualizar_bdd( "USUARIOS", $trama ) )
				echo 'Desconexion con Facebook exitosa...';
			else	echo 'Problemas para desconectar tu sesion de facebook...';
			
			unset($trama, $iduser);
			header( "Location: /" );
			}
		}
	else # si sta logeado
		{
		$fbid= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "FACEBOOK_ID" ); # obtenemos id facebook
		
		if( !$fbid  ) # si no existe sesion conexion o previa sincronizacion a facebook 
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		else
			{
			$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", "facebook_id"=>"'0'", "facebook_token"=>"'0'", "online"=>"'0'" );
			if( actualizar_bdd( "USUARIOS", $trama ) )
				echo 'Desconexion con Facebook exitosa...';
			else	echo 'Problemas para desconectar tu sesion de facebook...';
			
			unset($trama);
			header( "Location: ". url_amigable( "?my=perfil", consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "NICK"), "users", 0 ) );
			}
		}
	}
else if( !strcmp($log, "entrar") && !strcmp( $_GET["social"], "facebook") )
	{
	if( !strcmp($_GET["m"], "sync") ) # sincronizando porque no tiene cuenta o cerro sesion de facebook
		{
		# no esta logeado, entonces tal vez:
		# - es usuario nuevo
		# - es usuario haciendo login por red social 
		if( !is_login() )
			{
			$user_id= facebook_conectar();
			if( !$user_id )
				{
				echo '<h1>Error: no se pudo consultar tu informaci'. acento("o"). 'n en Facebook.</h1>';
				unset($user_id); 
				header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
				}
			else
				{
				# si ya tiene cuenta, es un relogin por red social 
				if( consultar_datos_general( "USUARIOS", "FACEBOOK_ID='". proteger_cadena($user_id). "'", "ID") )
					{
					$_SESSION["log_usr"]= consultar_datos_general( "USUARIOS", "FACEBOOK_ID='". proteger_cadena($user_id). "'", "NICK");
					$_SESSION["log_pwd"]= consultar_datos_general( "USUARIOS", "FACEBOOK_ID='". proteger_cadena($user_id). "'", "PASSWORD");
					$_SESSION["log_id"]= consultar_datos_general( "USUARIOS", "FACEBOOK_ID='". proteger_cadena($user_id). "'", "ID");

					$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
					"facebook_id"=>"'". proteger_cadena($user_id). "'", "facebook_token"=>"'". facebook_token( "auto" ). "'", 
					"online"=>"'1'" );
					# actualizamos 
					actualizar_bdd( "USUARIOS", $trama ); # actualizamos datos 
					unset($trama);
					
					bruteforcing_del(); # eliminamos la IP del Bruteforcing (si es que existe)
					unset($trama);
					if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else		$ref= '/';
					header( "Location: ". $ref );
					}
				# no tiene cuenta, crearemos nueva cuenta
				else
					{
					$userface= facebook_get_userid(); # consultamos informacion del usuario a facebook
					# generamos valores aleatorios de PASSWORD y ID
					do //generamos numero aleatorio
						{
						$idtrack= generar_idtrack(); //obtenemos digito aleatorio
						}while( !strcmp( $idtrack, consultar_datos_general( "USUARIOS", "ID='". $idtrack. "'", "ID" ) ) );
					
					# creacion de sesiones 
					$a= explode("@", $userface["email"] );
					$nickusr= ($userface["username"] ? $userface["username"]:$a[0]);
					$_SESSION["log_usr"]= $nickusr;
					$_SESSION["log_pwd"]= $idtrack;
					$_SESSION["log_id"]= $idtrack;

					# Geo Localizacion por IP
					require( "admin/geoipcity.inc" ); //incluimos cabecera
					include( "admin/geoipregionvars.php" );
					$geoip_bd= geoip_open( "admin/geoip/GeoIPLiteCity.dat", GEOIP_STANDARD ); //abrimos archivos dat
					$ip= $_SERVER['REMOTE_ADDR']; //obtenemos IP
					$r= geoip_record_by_addr( $geoip_bd, $ip );
					$ubicacion= proteger_cadena(($r->city. '/'. $GEOIP_REGION_NAME[$r->country_code][$r->region]. '/'. $r->country_name));
					geoip_close($geoip_bd); # cerramos stream
					$mundo= explode( "/", $ubicacion ); # ciudad/estado/pais

					/*
					$x= explode( "@", $userface["email"] ); # obtenemos dominio del mail
					# si no existe correo en la lista de correos, lo agregamos 
					if( !r_consultar_datos_general( "LISTA_CORREOS", "EMAIL='". proteger_cadena($userface["email"]). "'", "EMAIL" ) ) # si el mail NO existe, lo agregamos
						{
						# ceacion de cuenta en el sistema
						do //generamos numero aleatorio
							{
							$idtrack2= generar_idtrack(); //obtenemos digito aleatorio
							}while( !strcmp( $idtrack2, consultar_datos_general( "LISTA_CORREOS", "ID='". $idtrack2. "'", "ID" ) ) );
						 
						$trama2= array(
							"id"=>"'". $idtrack2. "'", 
							"fecha"=>"'". time(). "'", 
							"nombre"=>"'0'", 
							"email"=>"'". proteger_cadena($userface["email"]). "'", 
							"dominio"=>"'". $x[1]. "'", 
							"categoria"=>"'0'", 
							"grupo"=>"'0'", 
							"spam"=>"'0'", 
							"empresa"=>"'0'", 
							"ciudad"=>"'". r_consultar_datos_general( "MUNDO_CIUDAD", "CIUDAD='". proteger_cadena($mundo[0]). "'", "ID"). "'", 
							"estado"=>"'". r_consultar_datos_general( "MUNDO_ESTADO", "ESTADO='". proteger_cadena($mundo[1]). "'", "ID"). "'", 
							"pais"=>"'". r_consultar_datos_general( "MUNDO_PAIS", "PAIS='". proteger_cadena($mundo[2]). "'", "CODIGO" ). "'", 
							"colonia"=>"'0'", 
							"calle"=>"'0'"
							);
						r_insertar_bdd( "LISTA_CORREOS", $trama2 ); # insertamos
						unset($trama2);
						}

					$trama_foro= array(
						"member_name"=>"'". proteger_cadena($userface["username"]). "'", "date_registered"=>"'". time(). "'", 
						"posts"=>"'0'", "id_group"=>"'0'", "last_login"=>"'0'", "real_name"=>"'". proteger_cadena($userface["username"]). "'", 
						"instant_messages"=>"'0'", "unread_messages"=>"'0'", "new_pm"=>"'0'", "pm_prefs"=>"'0'", 
						"passwd"=>"'". md5($idtrack). "'", "email_address"=>"'". proteger_cadena($userface["email"]). "'", "gender"=>"'0'", 
						"hide_email"=>"'0'", "show_online"=>"'1'", "time_offset"=>"'0'", "pm_email_notify"=>"'1'", 
						"karma_bad"=>"'0'", "karma_good"=>"'1'", "notify_announcements"=>"'1'", "notify_regularity"=>"'1'", 
						"notify_send_body"=>"'0'", "notify_types"=>"'2'", "id_theme"=>"'0'", "is_activated"=>"'1'", 
						"id_msg_last_visit"=>"'0'", "id_post_group"=>"'4'", "total_time_logged_in"=>"'0'", "warning"=>"'0'", 
						"pm_receive_from"=>"'1'" 
						);
					
					insertar_bdd( "smf_members", $trama_foro ); # si inserta con exito al usuario
					*/
						
					# creamos cuenta en el sistema 
					$trama= array(
						"id"=>"'". $idtrack. "'", 
						"nick"=>"'". proteger_cadena($nickusr). "'", 
						"password"=>"'". $idtrack. "'", 
						"email"=>"'". proteger_cadena($userface["email"]). "'", 
						"fecha_registro"=>"'". time(). "'", 
						#"ciudad"=>"'". r_consultar_datos_general( "MUNDO_CIUDAD", "CIUDAD='". proteger_cadena($mundo[0]). "'", "ID"). "'", 
						#"estado"=>"'". r_consultar_datos_general( "MUNDO_ESTADO", "ESTADO='". proteger_cadena($mundo[1]). "'", "ID"). "'", 
						#"pais"=>"'". r_consultar_datos_general( "MUNDO_PAIS", "PAIS='". proteger_cadena($mundo[2]). "'", "CODIGO" ). "'", 
						"tipo_usr"=>"'10'",  
						"facebook_id"=>"'". proteger_cadena($user_id). "'", 
						"facebook_token"=>"'". facebook_token( "auto" ). "'", 
						"online"=>"'1'"
						);
						
					insertar_bdd( "USUARIOS", $trama ); # si inserta con exito al usuario
					inicializar_espacio_personal($idtrack); # si hay error inicializando carpetas

					/*
					$arr= array(
							"message"=>"", 
							"name"=>"", 
							"description"=>"". $userface["username"]. " Bienvenido a ". HTTP_SERVER. " - ". TITULO_WEB. "", 
							"caption"=>"". HTTP_SERVER. "", 
							"picture"=>"". TEMA_URL. "/imagenes/logo.jpg", 
							"link"=>"". HTTP_SERVER. ""  
							);
					facebook_pgdata( 'feed', 'post', $_SESSION["log_id"], $arr ); # enviando informacion a facebook
					*/
					
					bruteforcing_del(); # eliminamos la IP del Bruteforcing (si es que existe)
					unset($userface, $ubicacion, $mundo, $trama, $arr );
					if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else		$ref= '/';
					
					header( "Location: ". $ref );
					}
				}
			}
		}
	else # si esta logeado, esta sincronizando desde cuenta ya existente
		{
		if( !is_login() ) # si no esta logeado, esta haciendo fuerza bruta 
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		else # esta logeado, esta agregando su facebook 
			{
			$user_id= facebook_conectar();
			if( !$user_id )
				{
				echo '<h1>Error: no se pudo consultar tu informaci'. acento("o"). 'n en Facebook.</h1>';
				unset($user_id);
				}
			else
				{
				if( $_SESSION["ref"] )
					$ref= $_SESSION["ref"];
				else		$ref= url_amigable( 0, $_SESSION["log_usr"], "users", 0 );
				#trama 
				$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
				"facebook_id"=>"'". proteger_cadena($user_id). "'", "facebook_token"=>"'". facebook_token( "auto" ). "'" );
				# actualizamos 
				if( !actualizar_bdd( "USUARIOS", $trama ) ) # error
					{ 
					unset($trama, $user_id);
					header( "Location: ". $ref );
					# url_amigable( "?my=perfil", consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "NICK"), "users", 0 )
					}
				else # exito 
					{
					echo '<div class="exito_izq_nf">Perfil sincronizado con '. acento("e"). 'xito...</div>';
					unset($trama, $user_id); 
					header( "Location: ". $ref );
					}
				}
			}
		}
	}
?>