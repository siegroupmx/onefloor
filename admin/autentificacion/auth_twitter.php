<?php
if( !strcmp($log, "salir") && !strcmp( $_GET["social"], "twitter") )
	{
	if( !is_login() ) # si no esta logeado, entonces posiblemente aun no ha accedido a nuestro portal
		{
		$twid= twitter_conectar(); # obtenemos el ID del usuario 
		if( !$twid ) # si no hay ID, es un bruteforcing o ataque 
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		else # si existe sesion o conexion con nuestro portal 
			{
			$iduser= consultar_datos_general( "USUARIOS", "TWITTER_ID='". proteger_cadena($twid). "'", "ID" ); # obtenemos ID
			$trama= array( "id"=>"'". proteger_cadena($iduser). "'", "twitter_id"=>"'0'", "twitter_token"=>"'0'", "online"=>"'0'" );
			if( actualizar_bdd( "USUARIOS", $trama ) )
				echo 'Desconexion con Twitter exitosa...';
			else	echo 'Problemas para desconectar tu sesi'. acento("o"). 'n de twitter...';
			
			unset($trama, $iduser);
			header( "Location: /" );
			}
		unset($twid);
		}
	else # si sta logeado
		{
		$twid= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "TWITTER_ID" ); # obtenemos id twitter
		
		if( !$twid  ) # si no existe sesion conexion o previa sincronizacion a twitter
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		else
			{
			$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", "twitter_id"=>"'0'", "twitter_token"=>"'0'", "online"=>"'0'" );
			if( actualizar_bdd( "USUARIOS", $trama ) )
				echo 'Desconexion con Twitter exitosa...';
			else	echo 'Problemas para desconectar tu sesi'. acento("o"). 'n de twitter...';
			
			unset($trama);
			header( "Location: ". url_amigable( "?my=perfil", consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "USUARIO"), "users", 0 ) );
			}
		unset($twid);
		}
	}
else if( !strcmp($log, "entrar") && !strcmp( $_GET["social"], "twitter") )
	{
	if( !strcmp($_GET["m"], "sync") ) # sincronizando porque no tiene cuenta o cerro sesion de twitter
		{
		# no esta logeado, entonces tal vez:
		# - es usuario nuevo
		# - es usuario haciendo login por red social 
		if( !is_login() )
			{
			$user_id= twitter_conectar();
			if( !$user_id && !$_SESSION["access_token"] ) # no existen sesiones 
				header( "Location: ". HTTP_SERVER. "admin/twitter/redirect.php" ); # enviamos a sincronizacion
			else # entonces creamos cuenta nueva o relogin
				{
				# si la cuenta twitter existe, esta haciendo login por la red social twitter 
				if( consultar_datos_general( "USUARIOS", "TWITTER_ID='". $_SESSION["access_token"]["user_id"]. "' && TWITTER_TOKEN='". $_SESSION["access_token"]["oauth_token"]. "'", "ID") )
					{
					$_SESSION["log_usr"]= consultar_datos_general( "USUARIOS", "TWITTER_ID='". $_SESSION["access_token"]["user_id"]. "' && TWITTER_TOKEN='". $_SESSION["access_token"]["oauth_token"]. "'", "NICK"); # pendiente 
					$_SESSION["log_pwd"]= consultar_datos_general( "USUARIOS", "TWITTER_ID='". $_SESSION["access_token"]["user_id"]. "' && TWITTER_TOKEN='". $_SESSION["access_token"]["oauth_token"]. "'", "PASSWORD");
					$_SESSION["log_id"]= consultar_datos_general( "USUARIOS", "TWITTER_ID='". $_SESSION["access_token"]["user_id"]. "' && TWITTER_TOKEN='". $_SESSION["access_token"]["oauth_token"]. "'", "ID");

					$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
						"twitter_id"=>"'". $_SESSION["access_token"]["user_id"]. "'", 
						"twitter_token"=>"'". $_SESSION["access_token"]["oauth_token"]. "'",
						"twitter_token_secret"=>"'". $_SESSION["access_token"]["oauth_token_secret"]. "'", 
						"twitter_access"=>"'". $_SESSION["oauth_token"]. "'", 
						"online"=>"'1'" );

					actualizar_bdd( "USUARIOS", $trama ); # actualizamos informacion

					# siguiendo a los perfiles
					$seguirestos= array( "sie_group"=>consultar_datos_general( "USUARIOS", "NICK='sie_group'", "TWITTER_ID" ), 
								"turundusmx"=>consultar_datos_general( "USUARIOS", "NICK='turundusmx'", "TWITTER_ID" ), 
								"armaturadio"=>consultar_datos_general( "USUARIOS", "NICK='armaturadio'", "TWITTER_ID" ), 
								"radiomotionmx"=>consultar_datos_general( "USUARIOS", "NICK='radiomotionmx'", "TWITTER_ID" ), 
								"0day3"=>consultar_datos_general( "USUARIOS", "NICK='0day'", "TWITTER_ID" ), 
								"diabliyo"=>consultar_datos_general( "USUARIOS", "NICK='diabliyo'", "TWITTER_ID" ) );
					
					foreach( $seguirestos as $key=>$val )
						{
						if( $val ) #si hay datos
							$ob= $val;
						else		$ob= $key;

							if( $val ) # si hay id
								$arr1= array( 'target_id'=>$ob, 'source_id'=>$_SESSION["access_token"]["user_id"] );
							else # el screen name
								$arr1= array( 'target_screen_name'=>$ob, 'source_screen_name'=>$_SESSION["access_token"]["screen_name"] );
							$rs= twitter_get( array( "GET", "friendships/show", $arr1 ), $_SESSION["log_id"]);
	
							# si no me sigue
							if( !($rs->relationship->source->following) )
								twitter_get( array("POST", "seguir", $ob ), $_SESSION["log_id"] );
						unset($rs, $arr1, $ob);
						}

					unset($r, $seguirestos);
					if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else		$ref= '/';
					header( "Location: ". $ref );
					}
				else  # esta creando cuenta nueva
					{
					do //generamos numero aleatorio
						{
						$idtrack= generar_idtrack(); //obtenemos digito aleatorio
						}while( !strcmp( $idtrack, consultar_datos_general( "USUARIOS", "ID='". $idtrack. "'", "ID" ) ) );

					if( !consultar_datos_general("USUARIOS", "NICK='". proteger_cadena($_SESSION["access_token"]["screen_name"]). "'", "ID") )
						$username= $_SESSION["access_token"]["screen_name"]; # tomamos el username actual
					else # ya existe, le pondremos un numero
						$username= $_SESSION["access_token"]["screen_name"]. '_'. generar_idtrack(); # tomamos el username actual

					# creacion de sesiones  
					$_SESSION["log_pwd"]= $idtrack;
					$_SESSION["log_id"]= $idtrack;
					$_SESSION["log_usr"]=$username;

					# Geo Localizacion por IP
					require( "admin/geoipcity.inc" ); //incluimos cabecera
					include( "admin/geoipregionvars.php" );
					$geoip_bd= geoip_open( "admin/geoip/GeoIPLiteCity.dat", GEOIP_STANDARD ); //abrimos archivos dat
					$ip= $_SERVER['REMOTE_ADDR']; //obtenemos IP
					$r= geoip_record_by_addr( $geoip_bd, $ip );
					$ubicacion= proteger_cadena(($r->city. '/'. $GEOIP_REGION_NAME[$r->country_code][$r->region]. '/'. $r->country_name));
					geoip_close($geoip_bd); # cerramos stream
					$mundo= explode( "/", $ubicacion ); # ciudad/estado/pais
					# $x= explode( "@", $userface["email"] ); # obtenemos dominio del mail
					
					# creamos cuenta en el sistema 
					$trama= array(
						"id"=>"'". $idtrack. "'", 
						"nick"=>"'". $username. "'", 
						"password"=>"'". $idtrack. "'", 
						"email"=>"'0'", 
						"fecha_registro"=>"'". time(). "'", 
						#"ciudad"=>"'". r_consultar_datos_general( "MUNDO_CIUDAD", "CIUDAD='". proteger_cadena($mundo[0]). "'", "ID"). "'", 
						#"estado"=>"'". r_consultar_datos_general( "MUNDO_ESTADO", "ESTADO='". proteger_cadena($mundo[1]). "'", "ID"). "'", 
						#"pais"=>"'". r_consultar_datos_general( "MUNDO_PAIS", "PAIS='". proteger_cadena($mundo[2]). "'", "CODIGO" ). "'",  
						"tipo_usr"=>"'10'", 
						"url_web"=>"'0'", 
						"twitter"=>"'0'", 
						#"trash"=>"'0'", 
						#"susp"=>"'0'", 
						"twitter_id"=>"'". $_SESSION["access_token"]["user_id"]. "'", 
						"twitter_token"=>"'". $_SESSION["access_token"]["oauth_token"]. "'",
						"twitter_token_secret"=>"'". $_SESSION["access_token"]["oauth_token_secret"]. "'", 
						"twitter_access"=>"'". $_SESSION["oauth_token"]. "'", 
						"online"=>"'1'"
						);

					# si no existe correo en la lista de correos, lo agregamos 
					#if( !consultar_datos_general( "LISTA_CORREOS", "EMAIL='". proteger_cadena($userface["email"]). "'", "EMAIL" ) ) # si el mail NO existe, lo agregamos
					#	{
					#	# ceacion de cuenta en el sistema
					#	do //generamos numero aleatorio
					#		{
					#		$idtrack2= generar_idtrack(); //obtenemos digito aleatorio
					#		}while( !strcmp( $idtrack2, consultar_datos_general( "LISTA_CORREOS", "ID='". $idtrack2. "'", "ID" ) ) );
					 
					#	$trama2= array(
					#		"id"=>"'". $idtrack2. "'", 
					#		"fecha"=>"'". time(). "'", 
					#		"nombre"=>"'0'", 
					#		"email"=>"'". proteger_cadena($userface["email"]). "'", 
					#		"dominio"=>"'". $x[1]. "'", 
					#		"categoria"=>"'0'", 
					#		"grupo"=>"'0'", 
					#		"spam"=>"'0'", 
					#		"empresa"=>"'0'", 
					#		"ciudad"=>"'". consultar_datos_general( "MUNDO_CIUDAD", "CIUDAD='". proteger_cadena($mundo[0]). "'", "ID"). "'", 
					#		"estado"=>"'". consultar_datos_general( "MUNDO_ESTADO", "ESTADO='". proteger_cadena($mundo[1]). "'", "ID"). "'", 
					#		"pais"=>"'". consultar_datos_general( "MUNDO_PAIS", "PAIS='". proteger_cadena($mundo[2]). "'", "CODIGO" ). "'", 
					#		"colonia"=>"'0'", 
					#		"calle"=>"'0'"
					#		);
					#	insertar_bdd( "LISTA_CORREOS", $trama2 ); # insertamos
					#	unset($trama2);
					#	}

			/*
				$trama_foro= array(
					"member_name"=>"'". $username. "'", "date_registered"=>"'". time(). "'", 
					"posts"=>"'0'", "id_group"=>"'0'", "last_login"=>"'0'", "real_name"=>"'". $username. "'", 
					"instant_messages"=>"'0'", "unread_messages"=>"'0'", "new_pm"=>"'0'", "pm_prefs"=>"'0'", 
					"passwd"=>"'". md5($idtrack). "'", "email_address"=>"'nomail@nodomain.com'", "gender"=>"'0'", 
					"hide_email"=>"'0'", "show_online"=>"'1'", "time_offset"=>"'0'", "pm_email_notify"=>"'1'", 
					"karma_bad"=>"'0'", "karma_good"=>"'1'", "notify_announcements"=>"'1'", "notify_regularity"=>"'1'", 
					"notify_send_body"=>"'0'", "notify_types"=>"'2'", "id_theme"=>"'0'", "is_activated"=>"'1'", 
					"id_msg_last_visit"=>"'0'", "id_post_group"=>"'4'", "total_time_logged_in"=>"'0'", "warning"=>"'0'", 
					"pm_receive_from"=>"'1'" 
					);
			*/

					insertar_bdd( "USUARIOS", $trama ); # si inserta con exito al usuario
					# insertar_bdd( "smf_members", $trama_foro ); # si inserta con exito al usuario
					inicializar_espacio_personal($idtrack); # si hay error inicializando carpetas
					bruteforcing_del(); # eliminamos la IP del Bruteforcing (si es que existe)

					# publicando en twitter
					#$r= twitter_get( array( "POST", "tweet", "Bienvenido a ". HTTP_SERVER. " - ". TITULO_WEB), $_SESSION["log_id"] );
					#$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
					#	"twitter_id"=>"'". $_SESSION["access_token"]["user_id"]. "'", 
					#	"twitter_token"=>"'". $_SESSION["access_token"]["oauth_token"]. "'",
					#	"twitter_token_secret"=>"'". $_SESSION["access_token"]["oauth_token_secret"]. "'", 
					#	"twitter_access"=>"'". $_SESSION["oauth_token"]. "'" );

					# siguiendo a los perfiles
					#$seguirestos= array( "sie_group"=>consultar_datos_general( "USUARIOS", "NICK='sie_group'", "TWITTER_ID" ), 
					#			"turundusmx"=>consultar_datos_general( "USUARIOS", "NICK='turundusmx'", "TWITTER_ID" ), 
					#			"armaturadio"=>consultar_datos_general( "USUARIOS", "NICK='armaturadio'", "TWITTER_ID" ), 
					#			"radiomotionmx"=>consultar_datos_general( "USUARIOS", "NICK='radiomotionmx'", "TWITTER_ID" ), 
					#			"0day3"=>consultar_datos_general( "USUARIOS", "NICK='0day'", "TWITTER_ID" ), 
					#			"diabliyo"=>consultar_datos_general( "USUARIOS", "NICK='diabliyo'", "TWITTER_ID" ) );

					#foreach( $seguirestos as $key=>$val )
					#	{
					#	if( $val ) #si hay datos
					#		$ob= $val;
					#	else		$ob= $key;

					#		if( $val ) # si hay id
					#			$arr1= array( 'target_id'=>$ob, 'source_id'=>$_SESSION["access_token"]["user_id"] );
					#		else # el screen name
					#			$arr1= array( 'target_screen_name'=>$ob, 'source_screen_name'=>$_SESSION["access_token"]["screen_name"] );
					#		$rs= twitter_get( array( "GET", "friendships/show", $arr1 ), $_SESSION["log_id"]);
	
							# si no me sigue
					#		if( !($rs->relationship->source->following) )
					#			twitter_get( array("POST", "seguir", $ob ), $_SESSION["log_id"] );
					#	unset($rs, $arr1, $ob);
					#	}
					unset($seguirestos, $ubicacion, $mundo, $trama, $trama_foro, $r);

					if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else		$ref= '/';
					header( "Location: ". $ref );
					}
				}
			}
		else # no se sincronizan usuarios logeado
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		}
	else # esta logeado, esta sincronizando desde cuenta ya existente
		{
		if( !is_login() ) # si no esta logeado, esta haciendo fuerza bruta 
			header( "Location: ". url_amigable( "?log=brute", "log", "login", "dos_prev") );
		else # esta logeado, esta agregando su twitter 
			{
			$user_id= twitter_conectar();
			if( !$user_id && !$_SESSION["access_token"]) # no existen sesiones 
				header( "Location: ". HTTP_SERVER. "admin/twitter/redirect.php" ); # enviamos a sincronizacion
			else # si esta en linea con twitter
				{	
				$trama= array( "id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
					"twitter_id"=>"'". $_SESSION["access_token"]["user_id"]. "'", 
					"twitter_token"=>"'". $_SESSION["access_token"]["oauth_token"]. "'",
					"twitter_token_secret"=>"'". $_SESSION["access_token"]["oauth_token_secret"]. "'", 
					"twitter_access"=>"'". $_SESSION["oauth_token"]. "'" );

				# siguiendo a los perfiles
					$seguirestos= array( "sie_group"=>consultar_datos_general( "USUARIOS", "NICK='sie_group'", "TWITTER_ID" ), 
								"turundusmx"=>consultar_datos_general( "USUARIOS", "NICK='turundusmx'", "TWITTER_ID" ), 
								"armaturadio"=>consultar_datos_general( "USUARIOS", "NICK='armaturadio'", "TWITTER_ID" ), 
								"radiomotionmx"=>consultar_datos_general( "USUARIOS", "NICK='radiomotionmx'", "TWITTER_ID" ), 
								"0day3"=>consultar_datos_general( "USUARIOS", "NICK='0day'", "TWITTER_ID" ), 
								"diabliyo"=>consultar_datos_general( "USUARIOS", "NICK='diabliyo'", "TWITTER_ID" ) );

					foreach( $seguirestos as $key=>$val )
						{
						if( $val ) #si hay datos
							$ob= $val;
						else		$ob= $key;

							if( $val ) # si hay id
								$arr1= array( 'target_id'=>$ob, 'source_id'=>$_SESSION["access_token"]["user_id"] );
							else # el screen name
								$arr1= array( 'target_screen_name'=>$ob, 'source_screen_name'=>$_SESSION["access_token"]["screen_name"] );
							$rs= twitter_get( array( "GET", "friendships/show", $arr1 ), $_SESSION["log_id"]);
	
							# si no me sigue
							if( !($rs->relationship->source->following) )
								twitter_get( array("POST", "seguir", $ob ), $_SESSION["log_id"] );
						unset($rs, $arr1, $ob);
						}
					
				actualizar_bdd( "USUARIOS", $trama ); # actualizamos informacion
				unset($trama, $seguirestos);
				
				if( $_SESSION["ref"] ) # si existe referencia
					$ref= $_SESSION["ref"];
				else		$ref= '/';
				# $ref= url_amigable( "?my=perfil", consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "USUARIO"), "users", 0 ); # original
				header( "Location: ". $ref );
				}
			}
		}
	}
?>