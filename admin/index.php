<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

# header ('Content-type: text/html; charset=utf-8');
chdir( "../" ); //cambiamos al directorio raiz
include( "modulos/modulos.php" ); //cargamos modulos
autenticacion( $_GET["log"] );

echo '
<html>
	<head>
	<title>Panel de Control -- Administracion del Sitio - '. file_get_contents("VERSION"). ' -- '. (is_login() ? 'Logeado':'No Logeado'). '</title>
	<meta charset="UTF-8">
	<link type="text/css" rel="stylesheet" href="'. HTTP_SERVER. '/css/w3.min.css">
	<link type="text/css" rel="stylesheet" href="'. HTTP_SERVER. 'admin/css/estiloadmin.css">
	<script language="JavaScript" type="text/javascript" src="'. HTTP_SERVER. 'admin/js/script.js"></script>
	<script language="JavaScript" type="text/javascript">
	function add_file()
		{
		alert( "Link disponible proximamente..." );
		}
	</script>
	</head>
	
	<body onload="document.accesar.log_usr.focus()">
';

	echo '<div id="main">';

	//si las SESSIONES existen, entonces esta logeado.
	if( is_admin() || is_coadmin() || is_autor() || is_editor() || is_soporte() )
		{
		include( "push_modulos.php" );
		echo '<div id="centro">';
			include( "area_work.php" );
		echo '</div>';
		}
	else if( is_login() ) # logeado cliente
		echo "Acceso Restringido a solo Colaboradores/Administradores";
	else # sin logear
		{
		echo '
	<div id="login_box">
		<div class="login_logo">
			<img src="'. HTTP_SERVER. 'logo.jpg">
		</div>
		<div class="login_inputs">
			<div class="txt">Ingresa sus datos de acceso.';
			
			if( !strcmp($_GET["log"], "error") )
				echo '<br><div class="msg_error">Error datos de acceso incorrectos.</div>';
			echo '
			</div>

			<form action="'. url_amigable( "log=entrar", "log", "login", "in"). '" method="POST" name="accesar">
			<ul>
				<li><input type="text" value="usuario" name="log_usr" id="log_usr" onkeyup="detectar_entradas(\'log_usr\', event, \'login\', \''. currentURL(). '\');" 
					onclick="if(this.value==\'usuario\') this.value=\'\';" onblur="if(this.value==\'\') this.value=\'usuario\';"></li>
				<li><input type="password" value="password" name="log_pass" id="log_pass" onkeyup="detectar_entradas(\'log_pass\', event, \'login\', \''. currentURL(). '\');" 
					onclick="if(this.value==\'password\') this.value=\'\';" onblur="if(this.value==\'\') this.value=\'password\';"></li>
				<li><a href="javascript:document.accesar.submit()"><div class="boton azul">Entrar</div></a></li>
			</ul>
			</form>
		</div>
	</div>';
		}

	echo '</div>';
	
	?>
	</body>
</html>
