<?php
function proteger_cadena( $cadena )
	{
	return htmlentities($cadena, ENT_QUOTES);
	}

function generar_xml( $vars, $index )
	{
	if( !strcmp($index, "bitacoras" ) ) //xml de bitacoras.com
		{
		$x= explode( ";", $vars ); //explotamos variable 
		return '<?xml version="1.0"?>
									<methodCall>
 										<methodName>weblogUpdates.ping</methodName>
 										<params>
  											<param>
   										<value>'. $x[0]. '</value>
  											</param>
  											<param>
   										<value>'. $x[1]. '</value>
  											</param>
 										</params>
									</methodCall>';
		}
		
	return 0;
	}
	
function ping( $host, $url, $referer, $vars, $live, $type_connection, $accion, $formato )
	{
	$port= "80"; // puerto
	$trama=""; 
	
	//verificando stream 
	if( !strcmp($accion, "POST" ) || !strcmp( $accion, "GET" ) )
		{
		$stream= $accion. ' '. $url. ' HTTP/1.1\r\n';
		echo $stream;
		}
	else
		return "Error-StreamInvalid";
	
	//verificando host
	if( strcmp($host, "") )
		$trama .= 'Host: '. $host. '\r\n';
	else
		return 'Error-HostDesconocido';
	
	//incializando User-Agent
	//$trama .= 'User-Agent: OneFloor\r\n';
	//$trama .= 'Mozilla/5.0 (X11; U; Linux x86_64; es-MX; rv:1.9.1.8) Gecko/20100214 Ubuntu/9.10 (karmic) Firefox/3.5.8 GTB7.0\r\n';
	
	//verificando si la conexion se re-utilizara o cerrara
	if( $live==1 ) //mantenemos la conexion
		{
		$trama .= 'Keep-Alive: 300\r\n';
		$trama .= 'Connection: keep-alive\r\n';
		}
	else if( $live==0 ) //se cierra al instante
		$trama .= 'Connection: close\r\n';
	else //error, no especifico
		return 'Error-DisponibilidadConexion';
	
	//verificando si se usara referer 
	if( strcmp($referer, "") ) //si contiene informacion
		$trama .= 'Referer: '. $referer. '\r\n';
		
	//variables
	if( strcmp($vars, "") && !strcmp($accion, "POST") ) //si existen variables 
		{
		if( !strcmp($formato, "html") )
			{
			$trama .= 'Content-Type: application/x-www-form-urlencoded\r\n';
			$trama .= 'Content-Lenght: '. strlen($vars). '\r\n\r\n';
			$trama .= $vars;
			}
		else if( !strcmp($formato, "xml") )
			{
			$trama .= 'Content-Type: text/xml\r\n';
			if( $gen_vars= generar_xml( $vars, "bitacoras" ) )
				{
				$trama .= 'Content-Lenght: '. strlen($gen_vars). '\r\n\r\n';
				$trama .= $gen_vars;
				}
			else
				return 'Error-XMLMake['. $gen_vars. ']';
			}
		else
			return 'Errir-BadFormatSelected';
		}
	else
		$trama .= 'Accept: text/html,application/xhtml+xml,application/xml\r\n\r\n';
	
	/* Tipos de Conexion - $type_connection.
	1 = Funciones scoket.
	2 = Funciones fsocket.
	3 = Funciones pfsocket.
	*/
	
	# conexion mediante sockets
	if( !strcmp($type_connection, "1") ) //socket
		{
		if( $sd= socket_create( AF_INET, SOCK_STREAM, SOL_TCP ) ) //creamos socket
			{
			if( socket_connect( $sd, gethostbyname($host), $port ) ) //conectamos
				{
				if( socket_write( $sd, $stream.$trama, strlen($trama) ) )
					{
					$buf=""; //buffer que contendra los datos a retornar
					do
						{
						if( ($aux= socket_read( $sd, 1024, PHP_NORMAL_READ )) !== FALSE ) //se lee hacia el auxiliar
							$buf .= $aux;
						else
							return 'Error-Lectura['. strlen($aux). '/'. strlen($buf). ']';
						}while( strlen($aux)>0 || $aux!="" ); //leer mientras sea distinto a cero
						
					return $buf;
					}
				else
					return "Error-Escritura";
				}
			else
				return "Error-Conectar";
			
			socket_close($sd);
			unset($sd);
			}
		else
			return "Error-CrearSocket";
		}
	else if( !strcmp($type_connection, "2") ) //fsocket
		{
		echo 'fsocket';
		}
	else if( !strcmp($type_connection, "3") ) //pfsocket
		{
		echo $trama;
		$fp= pfsockopen( $host, $port, $errno, $errstr );
		fputs( $fp, $stream ); //ponemos flujo
		fputs( $fp, $trama ); //ponemos trama
		$buf="";
		while ( !feof($fp) )
			$buf .= fgets( $fp, 1024 ).'<br>';
		fclose($fp);
		return $buf;
		}
	else if( !strcmp($type_connection, "4") ) //pfsocket
		{
		# Using the XML-RPC extension to format the XML package
		//$request = xmlrpc_encode_request("weblogUpdates.ping", array("nombre_del_blog", "http://blog.com") );
		
		# Using the cURL extension to send it off,
		echo $trama; 
		$cd = curl_init();
		curl_setopt( $cd, CURLOPT_URL, $host ); # URL to post to
		curl_setopt( $cd, CURLOPT_RETURNTRANSFER, 1 ); # return into a variable
		
		if( strcmp($vars, "") && !strcmp($accion, "POST") )
			{
			curl_setopt( $cd, CURLOPT_HTTPHEADER, $trama ); # custom headers, see above
			curl_setopt( $cd, CURLOPT_CUSTOMREQUEST, 'POST' ); # This POST is special, and uses its specified Content-type
			}
		$buf = curl_exec( $cd ); # run
		curl_close($cd);

		return $buf;
		}
	
	unset($port);
	unset($trama);
	unset($stream);
	return 'Error-SocketBadMode['. $type_connection. ']';
	}
	
echo '<form action="ping_index.php" method="POST">
	Host: <input type="text" name="host">
	<br>URL: <input type="text" name="url">
	<br>Referer: <input type="text" name="referer">
	<br>variables: <input type="text" name="vars">
	<br>Mantener conexion: <select name="live"><option value="0"/>0<option value="1"/>1</select>
	<br>Tipo de Socket: <select name="tipo_socket"><option value="1"/>Socket<option value="2"/>FSocket<option value="3"/>pFSocket<option value="4"/>cURL</select>
	<br>Flujo de Datos: <select name="stream"><option value="POST"/>POST<option value="GET"/>GET</select>
	<br>Formato: <select name="formato"><option value="http"/>HTTP<option value="xml"/>XML</select>
	<br><input type="submit" value="Enviar" name="boton"> 
</form>';

if( isset($_POST["boton"]) && !strcmp( $_POST["boton"], "Enviar") )
	{
	$data= ping( proteger_cadena($_POST["host"]), proteger_cadena($_POST["url"]), proteger_cadena($_POST["referer"]), proteger_cadena($_POST["vars"]), proteger_cadena($_POST["live"]), proteger_cadena($_POST["tipo_socket"]), proteger_cadena($_POST["stream"]), proteger_cadena($_POST["formato"]) );
	
	echo '<p>Recivido ['. strlen($data). ']<br>';
	echo '<p>------ Encodec ------<br>'. proteger_cadena($data);
	echo '<p>INFO ---<br>'. $data;
	}
?>