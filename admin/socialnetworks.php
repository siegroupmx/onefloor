<?php
function socialnetworks()
	{
	# facebook
	if( $_GET["state"] && $_GET["code"] )
		{
		# facebook init
		$facebook = new Facebook(array(
					'appId'=>FBID,
					'secret'=>FBSECRET, 
			));
		$user = $facebook->getUser(); # obtenemos ID del usuario

		$buf='';
		if( is_login() )
			$buf=url_amigable( "?log=entrar", "log", "login", "in/facebook");
			
		# lo enviamos a log/in/facebook/sync en donde puede suceder:
		# - El usuario esta ingresando como nuevo por medio de la red social
		# - El usuario esta haciendo "login" por medio de la red social 
		else 
			$buf=url_amigable( "?log=entrar", "log", "login", "in/facebook/sync");
		
		header( "Location: ". $buf );
		}
		
	# twitter
	#
	# cuando api.twitter.com nos autentifica correctamente (que el usuario acepto nuestra APP), entonces el server
	# retorna valores que el api twitter con esto genera 2 sessiones: oauth_token y oauth_token_secret
	# estas sesiones debemos guardarlas
	#
	# autorizacion
	else if( $_GET["oauth_token"] && $_GET["oauth_verifier"] )
		{
		# verificando credenciales twitter
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
		$_SESSION['access_token'] = $access_token;

		# si esta logeado entonces esta agegando su cuenta de twitter
		if( is_login() )
			$buf=url_amigable( "?log=entrar", "log", "login", "in/twitter");
			
		# lo enviamos a log/in/twitter/sync en donde puede suceder:
		# - El usuario esta ingresando como nuevo por medio de la red social
		# - El usuario esta haciendo "login" por medio de la red social 
		else 
			$buf=url_amigable( "?log=entrar", "log", "login", "in/twitter/sync");
			
		header( "Location: ". $buf );
		}
	}
?>