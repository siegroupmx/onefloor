<?php
# inicializa el carrito
# coloca en la variable $_SESSION["car"]["items"] el arreglo de los items
function carrito_init()
	{
	if( !count($_SESSION["car"]) ) # si no existe session carrito
		$_SESSION["car"]= array( "cfdi"=>0, "init"=>0, "items"=>array(), "paqueteria"=>array(), "envio"=>0 ); # creamos

	if( !is_login() )	return 0; # no esta logeado
	else if( $_SESSION["car"]["init"]==1 ) # ya se inicializo
		return 1;
	else
		{
		$aux= array();
		if( is_array($_SESSION["car"]["items"]) && count($_SESSION["car"]["items"]) ) # si hay items en memoria
			{
			foreach( $_SESSION["car"]["items"] as $key=>$val )
				$aux[$key]=$val; # respaldamos
			$_SESSION["car"]["items"]=0; # reseteamos
			}

		$cons= consultar_con( "CARRITO", "ID_USUARIO='". $_SESSION["log_id"]. "' && TIPO='4'" );
		if( mysql_num_rows($cons) )
			{
			$buf= mysql_fetch_array($cons);
			$_SESSION["car"]["items"]= carrito_convert( "string2arr", $buf["PEDIDO"] );
			limpiar($cons);
			}

		if( count($aux) ) # si hay valores de respaldo
			{
			foreach($aux as $key=>$val ) # recorremos
				$_SESSION["car"]["items"][$key]= ( count($_SESSION["car"]["items"][$key]) ? $_SESSION["car"]["items"][$key]:$val ); # agregamos o actualizamos
			unset($aux);
			}

		$_SESSION["car"]["init"]=1; # ya inicializamos
		}
	return 1; # todo bien
	}

function carrito_convert( $modo, $data )
	{
	if( !$modo ) 	$r=0;
	if( !$data || !count($data) ) 	$r=0;
	else if( !strcmp($modo, "string2arr") ) # de Cadena a Arreglo
		{
		# string recibida: [id_producto,cantidad,precio,imagen,nombre,ancho,alto,largo,peso]....
		# arreglo a generarse: array( [id_producto]=array(cantidad"=>x, "precio"=>x, "imagen"=>x, "importe"=>x, "nombre"=>x) , ..... )
		preg_match_all( '\[([0-9a-zA-Z]{1,})\,([0-9]{1,})\,([0-9.,]{1,})\,([a-zA-Z0-9.:/-_]{1,})\,([a-zA-Z0-9.,-_*.?]{1,})\,([0-9]{1,})\,([0-9]{1,})\,([0-9]{1,})\,([0-9]{1,})\]', $data, $out );
		if( !$out[0][0] )
			$r=0;
		else # patron encontrado
			{
			$aux= array();
			foreach( $out[0] as $key=>$val )
				$aux[$out[1][$key]]= array( "cantidad"=>$aux[$out[2][$key]], "precio"=>$aux[$out[3][$key]], "imagen"=>$aux[$out[4][$key]], "importe"=>($aux[$out[2][$key]]*$aux[$out[3][$key]]), "nombre"=>$aux[$out[5][$key]], "ancho"=>$aux[$out[6][$key]], "alto"=>$aux[$out[7][$key]], "largo"=>$aux[$out[8][$key]], "peso"=>$aux[$out[9][$key]] ); # ordenando
			$r= $aux; # arreglo formado
			}
		}
	else if( !strcmp($modo, "arr2string") ) # de Arreglo a Cadena
		{
		# recibimos arreglo: array( [id_producto]=array("cantidad"=>x, "precio"=>x, "imagen"=>x, "importe"=>x, "nombre"=>x) , ..... )
		# string a convertirse: [id_producto,cantidad,precio,imagen,nombre]... 
		$out=''; # inicializamos
		foreach( $data as $key=>$val )
			$out .= '['. $key. ','. $val["cantidad"]. ','. $val["precio"]. ','. $val["imagen"]. ','. $val["nombre"]. ','. $val["ancho"]. ','. $val["alto"]. ','. $val["largo"]. ','. $val["peso"]. ']'; # ordenamos
		$r= $out; # string formado
		}

	return $r; # retornamos resultado
	}
?>