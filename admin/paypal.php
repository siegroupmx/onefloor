<?php
define( PAYPALSAND_ACCOUNT, "angel.cantu-facilitator@sie-group.net" );
define( PAYPALSAND_ID, "AQLaarJ8KKl1XvYvcP5viP70e2ejyNiC39G-BGPNa5ci_4_eMVnPehofdweq9N0NtJawXra4KmEe42iH" );
define( PAYPALSAND_SECRET, "EF3rTr0I6ZGujrtCtK5apCtjpK5BACT09lAPvIhPLLGHIuSiEscP-8A2Nsa2FJog9fBsdPRocz7kq3i1" );

define( PAYPAL_ACCOUNT, "angel.cantu@sie-group.net" );
define( PAYPAL_ID, "AZ1GSLgEFnMFBHEBqex-vHt47Nv-5awsnYX2zrHzMcBhh9YilK82UMeqD6yTb6Kvfzwn8D_NnLCDVD0r" );
define( PAYPAL_SECRET, "EEsBjSGU1W1T7npCSWNxBjWTMHkspUea3rMwVc1nmoRcDOW0a3XABW61GFj3LXhi6Q3-XwZ7S11QVZH6" );

function paypal( $op, $amount=NULL, $moneda="USD" )
	{
	$r=0;
	if( !strcmp( $op, "get_button") )
		$r= '<div id="paypal-button-container"></div>';
	else if( !strcmp($op, "payment_sandbox") )
		{
		$r='<script>
    paypal.Button.render({
        env: \'sandbox\',
        style: {
            layout: \'vertical\',
            size:   \'medium\',
            shape:  \'rect\',
            color:  \'gold\'
        },
        funding: {
            allowed: [ paypal.FUNDING.CARD, paypal.FUNDING.CREDIT ],
            disallowed: [ ]
        },
        client: {
            sandbox:    \''. PAYPALSAND_ID. '\',
            production: \''. PAYPAL_ID. '\'
        },

        payment: function(data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: \''. number_format($amount, 2, '.', ''). '\', currency: \''. $moneda. '\' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function() {
                cargar_datos( \'own=me&op=payment&brand=paypal&status=save&paymentToken=\'+data.paymentToken+\'&paymentID=\'+data.paymentID+\'&payerID=\'+data.payerID, \'statuspayment\', \'GET\', \'0\');
            });
        }

    }, \'#paypal-button-container\');

</script>';
		}
	else if( !strcmp($op, "payment") )
		{
		$r='<script>
    paypal.Button.render({
        env: \'production\',
        style: {
            layout: \'vertical\',
            size:   \'medium\',
            shape:  \'rect\',
            color:  \'gold\'
        },
        funding: {
            allowed: [ paypal.FUNDING.CARD, paypal.FUNDING.CREDIT ],
            disallowed: [ ]
        },
        client: {
            production: \''. PAYPAL_ID. '\'
        },

        payment: function(data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: \''. number_format($amount, 2, '.', ''). '\', currency: \''. $moneda. '\' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function() {
                cargar_datos( \'own=me&op=payment&brand=paypal&status=save\', \'statuspayment\', \'GET\', \'0\');
            });
        }

    }, \'#paypal-button-container\');

</script>';
		}

	return $r;
	}
?>