<?php
/*#############################################
#####    AJAX PARA CONTROL PANEL ADMIN    #####
#############################################*/

session_start();
include( "../admin/config.php" );
include( "../admin/desktop_functions.php" );
include( "../admin/handled_images.php" );
include( "../modulos/base.php" );
include( "../admin/tema.php" );
include( "../admin/paginacion.php" );
include( "../admin/recaptchalib.php" );
include( "../admin/recaptcha.php" );

if( is_admin() ) //si es admin
	{
	if( !strcmp($_GET["id"], "publicidad" ) )
		include( "ajax/admin-ajax-publicidad.php" );
	else if( isset($_GET["extras"]) || isset($_GET["extras_perfil"]) )
		include( "ajax/ajax_extras.php" ); 
		
	/*
	else if( !strcmp($_GET["id"], "noticias" ) )
		include( "ajax/admin-ajax-noticias.php" );
	else if( !strcmp($_GET["id"], "menus" ) )
		include( "ajax/admin-ajax-menus.php" );
	else if( !strcmp($_GET["id"], "configuracion" ) )
		include( "ajax/admin-ajax-configuracion.php" );
	else if( !strcmp($_GET["id"], "usuarios" ) )
		include( "ajax/admin-ajax-usuarios.php" );
	*/
	
	else
		echo "Error o_O [Codigo: AP1]";
	}
else
	echo 'Error o_O [Codigo: AP0]';
?>
