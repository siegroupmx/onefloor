<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

if( is_admin() || is_coadmin() || is_autor() || is_editor() || is_soporte() )
	{
	chdir( "modulos/" );	//cambiamos al directorio 'modulos'
	$fp= opendir( getcwd() ); //tomamos directorio actual
	$bm=array();
	while( ($buf= readdir( $fp )) !==FALSE ) //leemos mientra existan archivos en el directorio
		{
		if( !strstr($buf, "~") && !strstr($buf, "modulos.php") && !strstr($buf, "base") && strstr( $buf, ".php" ) ) //buscamos que tengan la extencion '.php'
				$bm[]= '<li><a href="index.php?id='. substr($buf, 0, -4). '">'. ucfirst(substr($buf, 0, -4)). '</a></li>'; //imprimimos
		}

	unset( $buf ); //eliminamos buffer
	closedir( $fp ); //cerramos directorio

	echo '
	<div id="menu">
		<div class="menu_big">
			<ul>
				<li><a href="index.php">Inicio</a></li>';

			foreach( $bm as $key=>$val )
				echo $val;

	echo '
				<li><a href="'.	url_amigable( "?log=entrar", "log", "login", "out"). '">Salir</a></li>
			</ul>
		</div>

		<div class="menu_movil">
			<div class="sprite home"></div>
			<ul class="minilista">';

			foreach( $bm as $key=>$val )
				echo $val;

		echo '</ul>
		</div>

		<div class="version_info">'. VERSION. '</div>
	</div>';

	unset($bm);
	}
?>
