<?php
include( "admin/config.php" );
include( "modulos/base.php" );
include( "admin/tema.php" );
include( "admin/desktop_functions.php" );

# deamon_logd(); # recolector de visitas y sesiones 

header('Content-Type: text/xml'); //indicamos al navegador que es un FeedRSS
header( 'Content-Type: text/xml; charset=UTF-8');
# header( 'Content-Type: text/html; charset=ISO-8859-4');
echo '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:media="http://search.yahoo.com/mrss/">';

//echo "<rss xmlns:atom='http://www.w3.org/2005/Atom' xmlns:openSearch='http://a9.com/-/spec/opensearchrss/1.0/' version='2.0'>";

	echo '
	<channel>
		<title>'. desproteger_cadena_src(TITULO_WEB). '</title>
		<atom:link href="'. HTTP_SERVER. 'feed/" rel="self" type="application/rss+xml" />
		<link>'. HTTP_SERVER. '</link>
		<description>'. desproteger_cadena_src(consultar_datos_general( "SERVER_CONFIG", "ID='1'", "META_DESCRIPCION" )). '</description>
		<lastBuildDate>'. date( "D, d M Y H:i:s", time() ). ' +0000</lastBuildDate>
		<language>es-ES</language>
		<sy:updatePeriod>hourly</sy:updatePeriod>
		<sy:updateFrequency>1</sy:updateFrequency>
		<generator>'. HTTP_SERVER. '?v='. VERSION. '</generator>
		<image>
			<url>'. HTTP_SERVER. 'favicon-32x32.jpg</url>
			<title>'. desproteger_cadena_src(TITULO_WEB). '</title>
			<link>'. HTTP_SERVER. '</link>
			<width>32</width>
			<height>32</height>
		</image> 
	';

	# $cons= consultar_enorden( "NOTICIAS", "FECHA DESC" );
	$cons= consultar_limite_enorden( "NOTICIAS", "0, 24", "FECHA DESC");

	while( $buf= mysql_fetch_array($cons) )
		{
		echo '
		<item>
			<title>'. desproteger_cadena_src($buf["TITULO"]). '</title>
			<link>'. url_amigable($buf["ID"], $buf["TITULO"], "contenido", 0). '</link>
			<comments>'. url_amigable($buf["ID"], $buf["TITULO"], "contenido", 0). '#comentarios</comments>
			<pubDate>'. date( "D, d M Y H:i:s", $buf["FECHA"] ). ' +0000</pubDate>
			<dc:creator><![CDATA['. consultar_datos_general( "USUARIOS", "ID='". $buf["AUTOR"]. "'", "NOMBRE" ). ']]></dc:creator>
			<category><![CDATA[Blog]]></category>
			<guid isPermaLink="false">'. HTTP_SERVER. '?hoja='. $buf["ID"]. '</guid>
			<description><![CDATA['. noticia_cortada(strip_tags(desproteger_cadena_src($buf["MENSAJE"])), 200). ']]></description>';

		if( $buf["IMAGENES_NOMBRE"] ) # si tiene imagen, noticia con imagen
			{
			$ext= substr( strtolower($buf["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
			$img= substr( $buf["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'. $ext; # extraemos nombre real
			echo '<content:encoded><![CDATA['. '<p><img src="'. HTTP_SERVER.$buf["IMAGENES_URL"].$img. '" alt="'. desproteger_cadena_src($buf["TITULO"]). '" title="'. desproteger_cadena_src($buf["TITULO"]). '"></p>'. desproteger_cadena_src($buf["MENSAJE"]). ']]></content:encoded>
			<image>
				<url>'. HTTP_SERVER.$buf["IMAGENES_URL"].$img. '</url>
			</image>';
			}
		else 	echo '<content:encoded><![CDATA['. desproteger_cadena_src($buf["MENSAJE"]). ']]></content:encoded>'; # solo noticia texto
			
			#if( strcmp( $buf["ARCHIVOS_NOMBRE"], "" ) && strcmp( $buf["ARCHIVOS_NOMBRE"], "0" ) )
			#	{
			#	if( strstr( $buf["ARCHIVOS_NOMBRE"], ".mp3" ) )
			#		echo '<enclosure url="http://'. $_SERVER['HTTP_HOST']. '/'. $buf["ARCHIVOS_URL"].$buf["ARCHIVOS_NOMBRE"]. '" length="'. filesize($buf["ARCHIVOS_URL"].$buf["ARCHIVOS_NOMBRE"]). '" type="audio/mpeg" />';
			#	}
		echo '
			<wfw:commentRss>'. url_amigable($buf["ID"], $buf["TITULO"], "contenido", 0). '/feed/</wfw:commentRss>
			<slash:comments>0</slash:comments>
		</item>';
		}
		
echo '</channel>
</rss>';
unset($buf);
limpiar($cons);
?>
