<?php
include( "modulos/modulos.php" );

if( isset($_GET["file_id"]) )
	{
	//comprobar si se trata de uno o varios archivos
	if( strchr( $_GET["file_id"], "," ) ) //si existe delimitador son varios archivos
		{
		$x= explode( ",", $_GET["file_id"] );
		$file= proteger_cadena($x[0]);
		}
	else # tomamos nombre ID
		$file= proteger_cadena($_GET["file_id"]);
		
	$cons= consultar_con( "NOTICIAS", "ID='". $file. "'" ); //consultamos ID descarga
	
	if( mysql_num_rows($cons)==0 )//la descarga no existe
		{
		header( 'HTTP/1.0 404 Not Found' );
		echo "<h1><b>Error:</b> el archivo que has intentado descargar no existe.</h1>";
		}
	else
		{
		$tmp= mysql_fetch_array($cons); //obtenemos informacion de archivo a descargar
		
		//verificar el tipo de archivo demandado (descargar)
		if( strcmp($tmp["ARCHIVOS_NOMBRE"], "") && strcmp($tmp["ARCHIVOS_NOMBRE"], "0") ) //entonces es un ARCHIVO el que se intenta descargar
			{
			if( strchr($_GET["file_id"], ",") ) //si existe delimitador son varios archivos
				{
				$y= explode( ":", $tmp["ARCHIVOS_NOMBRE"] );
				$put_file= $y[$x[1]];
				$put_url= $tmp["ARCHIVOS_URL"];
				unset($y);
				unset($x);
				}
			else # es un solo archivo 
				{
				$put_file= $tmp["ARCHIVOS_NOMBRE"]; # toma nombe del archivo
				$put_url= $tmp["ARCHIVOS_URL"]; # toma direccion URL del archivo 
				}
			}
		else //es una imagen
			{
			if( strchr($_GET["file_id"], ",") ) //si existe delimitador
				{
				$y= explode( ":", $tmp["IMAGENES_NOMBRE"] );
				$put_file= $y[$x[1]];
				$put_url= $tmp["IMAGENES_URL"];
				unset($y);
				}
			else
				{
				$put_file= $tmp["IMAGENES_NOMBRE"];
				$put_url= $tmp["IMAGENES_URL"];
				}
			}
		
		$rat_cons= consultar_con( "DOWNLOAD_RATING", "ID='". $file. "'" ); //consultamos el DOWNLOAD RATING
		
		if( mysql_num_rows($rat_cons)==0 ) //aun no recive RATING
			{
			$buf_rating= explode( ".", $put_file );
			
			$trama_rating= array(
								"ID"=>"'". $file. "'",
								"NOMBRE"=>"'". $put_file. "'",
								"TIPO"=>"'". $buf_rating["1"]. "'", 
								"RATING"=>"'1'" );
								
			insertar_bdd( "DOWNLOAD_RATING", $trama_rating ); //insertamos primer rating
			}
		else //ya existe RATING
			{
			$buf_rating= mysql_fetch_array($rat_cons);
			
			$trama_rating= array(
								"ID"=>"'". $file. "'",
								"RATING"=>"'". ($buf_rating["RATING"]+1). "'" );
								
			actualizar_bdd( "DOWNLOAD_RATING", $trama_rating ); //actualizamos RATING +1
			}
		unset($buf_rating);
		unset($trama_rating);
		limpiar($rat_cons);

		$fp= fopen( "download_log.txt", "w" );
		$dato= '> GetCwd: '. getcwd(). '<br>File Url: '. $put_url.$put_file; 
		fputs( $fp, $dato );
		fclose($fp);
		unset($dato);

		
		if( strstr( $put_file, ".mp3" ) ) # si es mp3
			$type_file= 'audio/mpeg';
		else $type_file= 'application/octet-stream'; # desconocido 
		
		//header( 'Content-Type: application/octet-stream' );
		header( 'Content-Type: '. $type_file);
		header( 'Content-Disposition: attachment; filename='. $put_file );
		echo file_get_contents( $put_url.$put_file );
		
		unset($tmp);
		
		header( 'Location: ?' );
		}
	
	limpiar($cons);
	}
else
	header( 'Location: ?' );
?>