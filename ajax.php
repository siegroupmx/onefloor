<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

# funcion para comprimir pagina web
function comprimir_web($buffer)
	{
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer); 
	}

ob_start("comprimir_web"); # calcular peso web y compresion

session_start();
header ('Content-type: text/html; charset=utf-8');
include( "modulos/modulos.php" );

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">';


if( is_login() )
	{
	/*#########################################
	######				USUARIOS				#######
	#########################################*/
	if( !strcmp( $_GET["ver"], "usuarios") )
		include( "ajax/ajax_usuario.php" );

	/*#########################################
	######				COMENTARIOS			#######
	#########################################*/
	else if( !strcmp($_GET["id"], "comentarios") )
		include( "ajax/ajax-comentarios.php" );
	
	/*##################################################
	######				LOGIN/RECUPERACION			#######
	##################################################*/
	else if( !strcmp($_GET["id"], "recuperar_datos" ) )
		include( "ajax/ajax-login.php" );

	else if( !strcmp($_GET["id"], "registrarme" ) )
		include( "ajax/ajax-registro.php" );
	
	else if( !strcmp($_GET["ver"], "usuarios_account" ) )
		include( "ajax/ajax-usuarios-account.php" );
	
	else if( !strcmp($_GET["id"], "publicidad_ctl" ) )
		include( "ajax/ajax-publicidad-ctl.php" );
	
	else if( !strcmp($_GET["my"], "perfil") ) # mi perfil
		include( "ajax/ajax_perfil.php" );
	}

# variable especial para que cargues tu propio AJAX de tu tema web
if( !strcmp($_GET["own"], "me") )
	include( INCLUDES_TEMA_URL.'/ajax/ajax.php' );

# chat online
else if( !strcmp($_GET["my"], "chatsupport") )
	include( "admin/chat_client.php" );

else //error, ninguno coincidio :D
	echo "No puedes usar este AJAX :O";

ob_end_flush(); # fin objeto

?>