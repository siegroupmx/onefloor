<?php
/*
create table LOG (
	ID BIGINT AUTO_INCREMENT not NULL PRIMARY KEY, 
	NICK VARCHAR(100) not NULL, 
	IP VARCHAR(15) not NULL, 
	SO VARCHAR(15) not NULL, 
	HUMAN VARCHAR(1) not NULL, 
	ARQUITECTURA VARCHAR(12) not NULL, 
	NAVEGADOR VARCHAR(15) not NULL, 
	NAVEGADOR_LENGUAJE VARCHAR(80) not NULL,
	SESION TEXT not NULL, 
	UBICACION VARCHAR(20) not NULL, 
	REFERENCIA TEXT not NULL, 
	NOMBRE_HOST VARCHAR(80) not NULL,
	FECHA_LOGIN TEXT not NULL, 
	FECHA_LOGOUT TEXT not NULL, 
	REF_VAR TEXT not NULL, 
	REQUEST_VAR TEXT not NULL, , 
	QUERY_VAR TEXT not NULL
	USERAGENT_VAR TEXT not NULL, 
	ACCEPT_VAR TEXT not NULL, 
	ENCODING_VAR TEXT not NULL, 
	LENGUAJE_VAR TEXT not NULL, 
	FECHA_UPDATE BITINT not NULL
	);

create table LOG_REPORTES (
	ID VARCHAR(10) not NULL UNIQUE PRIMARY KEY, 	// identificador unico
	NOMBRE TEXT not NULL, 			// nombre visual del reporte
	FECHA BIGINT not NULL, 			// fecha en que se genero el reporte
	FECHA_INICIO BIGINT not NULL, 	// fecha lapso inicio de generacion
	FECHA_FIN BIGINT not NULL, 		// fecha lapso fin de generacion
	PDF TEXT not NULL, 				// ruta al documento reporte pdf
	HTML TEXT not NULL, 			// ruta al documento reporte html
	MAILSEND TEXT not NULL, 		// correo al que se envio reporte
	MAILSEND_FECHA BIGINT not NULL, // hora de envio del correo
	MENSAJE TEXT not NULL, 			// mensaje que se envia por correo
	STATUS VARCHAR(1) not NULL 		// 1=generado, 0/2= no generado
	);
*/

function comprimir_web($buffer)
	{
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer); 
	}

ob_start( 'comprimir_web'); # calcular peso web y compresion

include( "modulos/modulos.php" );

echo '
<!DOCTYPE html>
<html>
	<head>
	<title>Sistema Reportes de Trafico</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="'. HTTP_SERVER. 'css/w3.css">
	<style type="text/css">
	<!--
	#pad4{padding:4px;font-size:8px;}
	.cleandiv{overflow:hidden;}
	.nd{text-decoration:none;}
	.nd a{text-decoration:none;}
	.nd a:link{text-decoration:none;}
	.bnaranja{border-color:orange!important;}
	.bazul2{border-color:#8085e9!important;}
	div{word-wrap:break-word;}
	//-->
	</style>
	<script src="https://code.highcharts.com/highcharts.src.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta charset="UTF-8">
    <meta http-equiv="Vary" content="Accept-Encoding" />
	<meta name="robots" content="index,follow" />
	</head>

	<body>
		<div class="w3-container">
			<h1>Sistema de Reportes del Monitor de Trafico</h1>
		</div>
		<div class="w3-container w3-small">';

			//paginacion
			$minimo= 30; //selector minimo del numero de registros a mostrar por consulta
			$pag= paginacion( $_GET["page"], "LOG", $minimo );
			
			if( isset($_GET["muestra"]) && $_GET["muestra"]==1 )
				$cons_log= consultar_indexados( "LOG", "NICK", proteger_cadena($_POST["usuarios_buscar"]) );
			else
				$cons_log= consultar_limite_enorden( "LOG", (($pag*$minimo)-$minimo). ','. $minimo, "FECHA_LOGIN DESC");

			echo '
			<div class="w3-container">
				<form class="w3-padding" action="index.php?id='. $_GET["id"]. '&mov='. $_GET["mov"]. '&muestra=1" method="POST">
					<div class="w3-threequarter">
					<input type="text" class="w3-input w3-border" placeholder="registro a buscar..." id="q" name="q"></div>
					<div class="w3-quarter"><button class="w3-margin-left w3-button w3-padding w3-purple">Buscar</button></div>
				</form>
			</div>
			<div class="w3-container w3-padding-24">
				<div class="w3-row">
					<div class="w3-bar w3-small">
						<a href="'. HTTP_SERVER. 'reportes.php" class="w3-bar-item nd'. ((!$_GET["id"] || strcmp($_GET["id"], "reportes")) ? ' w3-blue':' w3-hover-gray'). '">Trafico</a>
						<a href="'. HTTP_SERVER. 'reportes.php?id=reportes" class="w3-bar-item nd'. (!strcmp($_GET["id"], "reportes") ? ' w3-blue':' w3-hover-gray'). '">Reportes</a>
					</div>';

		if( !strcmp($_GET["id"], "reportes") )
			{
			if( !strcmp($_GET["m"], "init") ) # inicializar base LOG_REPORTES
				{
				$incon= consultar_enorden( "LOG", "FECHA_LOGIN ASC"); # del mas antigo al nuevo
				if( !mysql_num_rows($incon) || mysql_num_rows($incon)<100 )
					echo '<div class="w3-panel w3-padding w3-pale-red w3-leftbar w3-border-red">No tienes suficiente trafico en el sitio aun..</div>';
				else
					{
					$trama= array();
					$tmp=0;
					$aux= array();
					while( $inbuf=mysql_fetch_array($incon) )
						{
						if( !count($trama) || !$trama[date("m", $inbuf["FECHA_LOGIN"])] ) # si no existe en el array
							{
							do //generamos numero aleatorio de 4 a 10 digitos
								{
								$id_track= generar_idtrack(); //obtenemos digito aleatorio
								}while( count($aux[$id_track]) );

							$finicio= strtotime(date("Y-m", $inbuf["FECHA_LOGIN"]).'-01T00:00:00');
							$fend= 0;
							$aux[$id_track]=1; # guardamos el ID

							if( $tmp )
								{
								$x= explode("/", $tmp);
								$trama[$tmp]["fecha_fin"]= $finicio;
								}

							$tmp= date("m/Y", $inbuf["FECHA_LOGIN"]);

							$trama[date("m/Y", $inbuf["FECHA_LOGIN"])]= array(
								"id"=>"'". $id_track. "'", 
								"nombre"=>"'". mes_esp(date( "m", $inbuf["FECHA_LOGIN"])). " ". date("Y", $inbuf["FECHA_LOGIN"]). "'", 
								"fecha"=>time(), 
								"fecha_inicio"=>$finicio, 
								"fecha_fin"=>$fend, 
								"pdf"=>"'0'", 
								"html"=>"'0'", 
								"mailsend"=>"'0'", 
								"mailsend_fecha"=>"'0'", 
								"status"=>"'0'"
								); # creamos
							}
						}
					# print_r($trama);

					$exito=0;
					$err=0;
					foreach( $trama as $key=>$val )
						{
						if( $val["fecha_fin"] && strcmp(date("d/m/Y", time()), date("d/m/Y", $val["fecha_inicio"])) ) 	# si hay fecha termino
							{
							if( !insertar_bdd( "LOG_REPORTES", $val ) )
								$err++;
							else 	$exito++;
							}
						else 	$err++;
						}

					echo ($exito ? '<div class="w3-panel w3-padding w3-pale-blue w3-leftbar w3-border-blue">Se agregaron con exito '. $exito. ' colas de reporte de sus metricas completas.. <a href="'. HTTP_SERVER. 'reportes.php?id=reportes"><button class="w3-button w3-padding w3-green">Continuar</button></a></div>':'');
					echo ( $err ? ($exito ? '<br>':'').'<div class="w3-panel w3-padding w3-pale-red w3-leftbar w3-border-red">Se obtuvier'. acento("o"). 'n '. $err. ' errores en las metricas..</div>':'');
					unset($inbuf);
					# onefloor_reload(2000);
					}
				unset($incon);
				}
			else if( $_GET["m"] )
				{
				if( !consultar_datos_general( "LOG_REPORTES", "ID='". proteger_cadena($_GET["idr"]). "'", "NOMBRE") )
					echo '<div class="w3-panel w3-padding w3-pale-red w3-leftbar w3-border-red">El repote indicado no existe..</div>';
				else if( !strcmp($_GET["m"], "ver") ) # mirar el reporte
					{
					echo 'Mirando reporte...';
					print_r($_GET);
					}
				else if( !strcmp($_GET["m"], "regen") ) # regenerar reporte
					{
					echo 'Regenerando el reporte..';
					print_r($_GET);
					}
				else if( !strcmp($_GET["m"], "gen") ) # generando reporte de forma manual
					{
					$finicio= consultar_datos_general( "LOG_REPORTES", "ID='". proteger_cadena($_GET["idr"]). "'", "FECHA_INICIO");
					$fend= consultar_datos_general( "LOG_REPORTES", "ID='". proteger_cadena($_GET["idr"]). "'", "FECHA_FIN");

					# echo 'Rango: '. date( "d/m/Y, g:i a", $finicio ). ' al '. date( "d/m/Y, g:i a", $fend);

					$rcon= consultar_rango_enorden( "LOG", "FECHA_LOGIN", $finicio, $fend, "FECHA_LOGIN ASC");
					if( !mysql_num_rows($rcon) )
						echo '<div class="w3-panel w3-padding w3-pale-red w3-leftbar w3-border-red">No hay datos suficientes para generar reporte..</div>';
					else 
						{
						$metricas= array( 
							"contador"=>0, 
							"usuarios"=>array(
								"total"=>0,
								"total_ref"=>0, 
								"total_unico"=>0,
								"ref"=>array(), 
								"unico"=>array()
								), 
							"robot"=>array(
								"total"=>0,
								"marcas"=>array()
								), 
							"paginas"=>array(
								"total"=>0, 
								"total_indexadas"=>0, 
								"total_visitadas"=>0, 
								"total_referencias"=>0, 
								"indexadas"=>array(), 
								"visitadas"=>array(), 
								"referencias"=>array(), # total=conteo totales, index= indexaciones totales, visita= conteo, ref= conteo
								"pagina"=>array( "robot"=>array(), "humano"=>array() ), 
								), 
							"calendario"=>array()
							);

						while( $rbuf=mysql_fetch_array($rcon) )
							{
							# inicializando calendario
							if( !count($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]) ) # si no existe el dia
								{
								$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]=0;
								$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]=0;
								$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]=0;
								$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]=0;
								}

							$metricas["contador"] += 1;
							$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]= (!($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]+1)); # calendario conteo del dia totales

							if( is_a_robot($rbuf["IP"]) || is_a_robot($rbuf["USERAGENT_VAR"]) ) # si es robot
								{
								$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]= (!($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]+1)); # calendario conteo dia indexaciones 
								$metricas["robot"]["total"] += 1; # incrementamos robot visita
								$nbot= (is_a_robot($rbuf["IP"]) ? is_a_robot($rbuf["IP"]):is_a_robot($rbuf["USERAGENT_VAR"]));
								$nbot= substr( $nbot, strlen('Indexado por '), strlen($nbot) ); # obtenemos nombre del bot

								if( !count($metricas["robot"]["marcas"]) || !$metricas["robot"]["marcas"][$nbot] )
									$metricas["robot"]["marcas"][$nbot]= 1; # inicializamos
								else 	$metricas["robot"]["marcas"][$nbot] += 1; # incrementamos

								$metricas["paginas"]["total"] += 1; # incrementamos contador de pagina visitada
								$metricas["paginas"]["total_indexadas"] += 1; # incrementamos contador de pagina indexada
								$metricas["paginas"]["indexadas"][]= array( "robot"=>$nbot, "fecha"=>$rbuf["FECHA_LOGIN"], "ruta"=>$rbuf["REQUEST_VAR"]);
								}
							else # es acceso humano
								{
								$metricas["usuarios"]["total"] += 1; # incrementamos visitas totales
								$metricas["paginas"]["total"] +=1; # incrementamos total
								$tmp= parse_url($rbuf["REF_VAR"]);

								# contabilizando pagina visitada
								if( count($metricas["paginas"]["pagina"]["humano"][($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/')]) )
									$metricas["paginas"]["pagina"]["humano"][($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/')] += 1;
								else 	$metricas["paginas"]["pagina"]["humano"][($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/')]= 1;

								# no hay referencia, es visita directa
								# if( !$rbuf["REF_VAR"] )
								if( !$rbuf["REF_VAR"] || !strcmp($rbuf["REF_VAR"], "/") || strstr(substr(HTTP_SERVER, strlen('https://'), strlen(HTTP_SERVER)), $tmp["host"]) )
									{
									$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]= (!($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]+1));
									$metricas["usuarios"]["total_unico"] += 1; # incrementamos unico
									if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] )
										$metricas["usuarios"]["unico"][$rbuf["UBICACION"]]=1; # inicializamos
									else 	$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] += 1; # incrementamos

									$metricas["paginas"]["total_visitadas"] +=1; # incrementamso referencia
									$metricas["paginas"]["visitadas"][]= array( "ruta"=>($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/'), "fecha"=>$rbuf["FECHA_LOGIN"], "ubicacion"=>$rbuf["UBICACION"] );
									}
								else # SI tenemos referencia, viene de otro sitio o buscador
									{
									$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]= ( !($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]+1));
									$tmp= parse_url( $rbuf["REF_VAR"]);
									$nref= $tmp["host"];
									$nquery= $tmp["query"];
									$npath= $tmp["path"];
									$metricas["usuarios"]["total_ref"] += 1; # incrementamos referencia
									if( !count($metricas["usuarios"]["ref"]) || !$metricas["usuarios"]["ref"][$nref] )
										$metricas["usuarios"]["ref"][$nref]=1;
									else 	$metricas["usuarios"]["ref"][$nref] += 1;

									# referencia Geografica
									if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] )
										$metricas["usuarios"]["unico"][$rbuf["UBICACION"]]=1; # inicializamos
									else 	$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] += 1; # incrementamos

									$metricas["paginas"]["total_referencias"] += 1;
									$metricas["paginas"]["referencias"][]= array( "ref"=>$nref, "ruta"=>($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/'), "fecha"=>$rbuf["FECHA_LOGIN"], "ubicacion"=>$rbuf["UBICACION"], "consulta"=>($nquery ? $nquery:""), "loader"=>($npath ? $npath:"") );
									}
								}
							}

						#
						# ordenamiento
						#

						# referencias
						$aux= array();
						foreach( $metricas["usuarios"]["ref"] as $key=>$val )
							$aux[$key]= $val;
						arsort($aux);
						$metricas["usuarios"]["ref"]= $aux;
						unset($aux);

						# usuarios
						$aux= array();
						foreach( $metricas["usuarios"]["unico"] as $key=>$val )
							$aux[$key]= $val;
						arsort($aux);
						$metricas["usuarios"]["unico"]= $aux;
						unset($aux);

						# pagina
						$aux= array();
						foreach( $metricas["paginas"]["pagina"]["humano"] as $key=>$val )
							$aux[$key]= $val;
						arsort($aux);
						$metricas["paginas"]["pagina"]["humano"]= $aux;
						unset($aux);

						# $metricas["paginas"]["referencias"][]= array( "ref"=>$nref, "ruta"=>($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/'), "consulta"=>($nquery ? $nquery:""), "loader"=>($npath ? $npath:"") );

						echo '<div class="w3-row w3-padding-32">
						<h2 class="w3-leftbar w3-border-blue w3-padding">Metrica de Trafico del Mes: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>

						<div class="w3-panel w3-container w3-padding w3-pale-yellow">
							Conoce el trafico entrante a tu sitio web, distingue el trafico de usuarios del comercial e identifica a tus referenciadores.
						</div>
						<div class="w3-row w3-container w3-padding-16">
							<div class="w3-card-2 w3-padding w3-left  w3-bottombar w3-border-black">
								<h3>'. number_format($metricas["contador"]). '</h3>
								<p>Visitas totales.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar w3-border-blue">
								<h3>'. number_format($metricas["usuarios"]["total_unico"]). '</h3>
								<p>Visitas de Usuario.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar w3-border-green">
								<h3>'. number_format($metricas["usuarios"]["total_ref"]). '</h3>
								<p>Visitas de Referenciadas.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar bnaranja">
								<h3>'. number_format($metricas["robot"]["total"]). '</h3>
								<p>Visitas de Indexadores.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar bazul2">
								<h3>'. number_format($metricas["usuarios"]["total"]). '</h3>
								<p>Usuario+Referencia.</p>
							</div>
						</div>
						<div class="w3-container" id="trafico">
						</div>
						</div>';


# valores para la grafica
$i=0;
$gr_cat='';
$gr_vi='';
$gr_to='';
$gr_re='';
$gr_in='';
foreach( $metricas["calendario"] as $key=>$val )
	{
	$gr_cat .= ($i ? ',':''). '\''. $key. '\'';
	$gr_vi .= ($i ? ',':''). $val["visita"];
	$gr_to .= ($i ? ',':''). $val["total"];
	$gr_re .= ($i ? ',':''). $val["ref"];
	$gr_in .= ($i ? ',':''). $val["index"];
	$gr_ur .= ($i ? ',':''). ($val["visita"]+$val["ref"]);
	$i++;
	}

echo '<script type="text/javascript">

Highcharts.chart(\'trafico\', {
    chart: {
        type: \'line\'
    },
    title: {
        text: \''. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '\'
    },
    subtitle: {
        text: \'Source: '. HTTP_SERVER. '\'
    },
    xAxis: {
        categories: ['. $gr_cat. ']
    },
    yAxis: {
        title: {
            text: \'Metrica Trafico '. mes_esp(date( "m", $finicio)). '\'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: true
        }
    },
    series: [{
        name: \'Usuarios\',
        data: ['. $gr_vi. ']
    }, {
        name: \'V.Totales\',
        data: ['. $gr_to. ']
    }, {
        name: \'Referencias\',
        data: ['. $gr_re. ']
    }, {
        name: \'Indexacion\',
        data: ['. $gr_in. ']
    }, {
        name: \'Usr+Ref\',
        data: ['. $gr_ur. ']
    }]
});
		</script>';
unset($gr_cat, $gr_vi, $gr_to, $gr_re, $gr_in, $gr_ur );

						echo '<div class="w3-row w3-padding-32">
						<h2 class="w3-leftbar w3-border-blue w3-padding">Metrica de Referencias: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>
						<div class="w3-panel w3-container w3-padding w3-pale-yellow">
							Identifica los sitios que estan proporcionandote trafico de interes.
						</div>
						<div class="w3-container" id="referers">
						</div>
						</div>
						';

# print_r($metricas["paginas"]["referencias"]);

$i=0;
$gr_br='';
$gr_mt='';
foreach( $metricas["usuarios"]["ref"] as $key=>$val )
	{
	$gr_br .=  ($i ? ',':''). '\''. ($key ? $key:'unknown'). '\'';
	$gr_mt .=  ($i ? ',':''). $val;
	$i++;
	}

echo '
<script type="text/javascript">

Highcharts.chart(\'referers\', {
    chart: {
        type: \'bar\'
    },
    title: {
        text: \''. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '\'
    },
    subtitle: {
        text: \'Source: '. HTTP_SERVER. '\'
    },
    xAxis: {
        categories: ['. $gr_br. '],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: \'Metrica de Referencias\',
            align: \'high\'
        },
        labels: {
            overflow: \'justify\'
        }
    },
    tooltip: {
        valueSuffix: \' visitas\'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: \'vertical\',
        align: \'right\',
        verticalAlign: \'top\',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || \'#FFFFFF\'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        data: ['. $gr_mt. ']
    }]
});
		</script>';
unset($gr_br, $gr_mt);

						echo '<div class="w3-row w3-padding-32">
						<h2 class="w3-leftbar w3-border-blue w3-padding">Metrica Geografica: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>
						<div class="w3-panel w3-container w3-padding w3-pale-yellow">
							Conoce la ubicacion geografica donde generas mayor presencia e interes, este es tu publico.
						</div>
						<div class="w3-container" id="geogra">
						</div>
						</div>
';


$i=0;
$gr_br='';
$gr_mt='';
foreach( $metricas["usuarios"]["unico"] as $key=>$val )
	{
	if( $val>1 )
		{
		$gr_br .=  ($i ? ',':''). '\''. ($key ? $key:'unknown'). '\'';
		$gr_mt .=  ($i ? ',':''). $val;
		$i++;
		}
	}

echo '
<script type="text/javascript">

Highcharts.chart(\'geogra\', {
    chart: {
        type: \'bar\'
    },
    title: {
        text: \''. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '\'
    },
    subtitle: {
        text: \'Source: '. HTTP_SERVER. '\'
    },
    xAxis: {
        categories: ['. $gr_br. '],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: \'Metrica Geografica\',
            align: \'high\'
        },
        labels: {
            overflow: \'justify\'
        }
    },
    tooltip: {
        valueSuffix: \' visitas\'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: \'vertical\',
        align: \'right\',
        verticalAlign: \'top\',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || \'#FFFFFF\'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        data: ['. $gr_mt. ']
    }]
});
		</script>';

unset($gr_br, $gr_mt);

						echo '
						<div class="w3-row w3-padding-32">
							<h2 class="w3-leftbar w3-border-blue w3-padding">Top 10: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>
							<div class="w3-panel w3-container w3-padding w3-pale-yellow">
								Identifica el top 10 de lo mas interesante para tu portal.
							</div>
							<div class="w3-container" id="top10">
								<div class="w3-row">
									<div class="w3-quarter w3-padding">
										<h5 class="w3-border-blue w3-border-bottom">Referencias</h5>
										<div class="w3-row">';
										$i=0;
										foreach( $metricas["usuarios"]["ref"] as $key=>$val )
											{
											if( $i<10 )
												{
												echo '<div class="cleandiv w3-hover-light-gray"><div class="w3-threequarter">'. desproteger_cadena_src($key). '</div>
												<div class="w3-quarter w3-right-align">'. number_format($val, 0, '', ','). '</div></div>';
												}
											else 	break;
											$i++;
											}
									echo '</div>
									</div>
									<div class="w3-quarter w3-padding">
										<h5 class="w3-border-blue w3-border-bottom">Geografica</h5>
										<div class="w3-row">';
										$i=0;
										foreach( $metricas["usuarios"]["unico"] as $key=>$val )
											{
											if( $i<10 )
												{
												echo '<div class="cleandiv w3-hover-light-gray"><div class="w3-threequarter">'. desproteger_cadena_src($key). '</div>
												<div class="w3-quarter w3-right-align">'. number_format($val, 0, '', ','). '</div></div>';
												}
											else 	break;
											$i++;
											}
									echo '</div>
									</div>
									<div class="w3-rest w3-padding">
										<h5 class="w3-border-blue w3-border-bottom">Paginas</h5>
										<div class="w3-row">';
										$i=0;
										foreach( $metricas["paginas"]["pagina"]["humano"] as $key=>$val )
											{
											if( $i<10 )
												{
												echo '<div class="cleandiv w3-hover-light-gray"><div class="w3-threequarter">'. desproteger_cadena_src($key). '</div>
												<div class="w3-quarter w3-right-align">'. number_format($val, 0, '', ','). '</div></div>';
												}
											else 	break;
											$i++;
											}
									echo '</div>
									</div>
								</div>
								<div class="w3-row">
									<div class="w3-half w3-padding">
										<h5 cass="w3-border-blue w3-border-bottom">Busquedas</h5>
										<div class="w3-row">
											<div class="w3-threequarter">Dato</div>
											<div class="w3-quarter">Num</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						';



						echo '<div class="w3-row w3-padding-32">
						<h2 class="w3-leftbar w3-border-blue w3-padding">Detalle de Trafico: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>
						<div class="w3-panel w3-container w3-padding w3-pale-yellow">
							Observa acceso por acceso tus visitas, segmentado por situaciones de interes.
						</div>

						<div class="w3-bar w3-light-gray w3-bottombar w3-border-black">
							<a href="#full_ref" class="w3-bar-item w3-padding w3-hover-green nd">Referenciado</a>
							<a href="#full_graf" class="w3-bar-item w3-padding w3-hover-green nd">Geografico</a>
							<a href="#full_usr" class="w3-bar-item w3-padding w3-hover-green nd">Usuarios</a>
							<a href="#full_ind" class="w3-bar-item w3-padding w3-hover-green nd">Indexado</a>
							<a href="#full_all" class="w3-bar-item w3-padding w3-hover-green nd">Todo</a>
						</div>

						<div class="w3-row" id="full_ref">
							<div class="w3-bar w3-black w3-small">
								<div href="#full_ref" class="w3-bar-item w3-padding" style="width:15%">Marca</div>
								<div href="#full_graf" class="w3-bar-item w3-padding" style="width:20%">Ruta</div>
								<div href="#full_usr" class="w3-bar-item w3-padding" style="width:10%">Fecha</div>
								<div href="#full_ind" class="w3-bar-item w3-padding" style="width:15%">Lugar</div>
								<div href="#full_all" class="w3-bar-item w3-padding" style="width:20%">Consulta</div>
								<div href="#full_all" class="w3-bar-item w3-padding style="width:20%"">Desde</div>
							</div>';
			$i=0;
			foreach( $metricas["paginas"]["referencias"] as $key=>$val )
				{
				echo '<div class="w3-bar w3-tiny'. ( !($i%2) ? ' w3-light-gray':''). '" style="word-wrap:break-word;">
				<li class="w3-bar-item" style="width:15%">'. $val["ref"]. '</li>
				<li class="w3-bar-item" style="width:20%">'. $val["ruta"]. '</li>
				<li class="w3-bar-item" style="width:10%">'. date( "d/m/Y, g:i a", $val["fecha"]). '</li>
				<li class="w3-bar-item" style="width:15%">'. $val["ubicacion"]. '</li>
				<li class="w3-bar-item w3-right-align" style="width:20%">'. $val["consulta"]. '</li>
				<li class="w3-bar-item w3-right-align" style="width:20%">'. $val["loader"]. '</li>
				</div>';
				$i++;
				}
					echo '</div>

						</div>
';

/*
									$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]= ( !($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]+1));
									$tmp= parse_url( $rbuf["REF_VAR"]);
									$nref= $tmp["host"];
									$nquery= $tmp["query"];
									$npath= $tmp["path"];
									$metricas["usuarios"]["total_ref"] += 1; # incrementamos referencia
									if( !count($metricas["usuarios"]["ref"]) || !$metricas["usuarios"]["ref"][$nref] )
										$metricas["usuarios"]["ref"][$nref]=1;
									else 	$metricas["usuarios"]["ref"][$nref] += 1;

									# referencia Geografica
									if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] )
										$metricas["usuarios"]["unico"][$rbuf["UBICACION"]]=1; # inicializamos
									else 	$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] += 1; # incrementamos

									$metricas["paginas"]["total_referencias"] += 1;
									$metricas["paginas"]["referencias"][]= array( "ref"=>$nref, "ruta"=>($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/'), "fecha"=>$rbuf["FECHA_LOGIN"], "ubicacion"=>$rbuf["UBICACION"], "consulta"=>($nquery ? $nquery:""), "loader"=>($npath ? $npath:"") );
									*/

						}
					unset($rcon);
					}
				}
			else
				{
				echo '
					<div class="w3-bar w3-small w3-red">
						<button class="w3-bar-item w3-red w3-left-align" style="width:55%;">Reporte</button>
						<button class="w3-bar-item w3-red" style="width:10%;">Generado</button>
						<button class="w3-bar-item w3-red" style="width:20%;">Lapso Metrica</button>
						<button class="w3-bar-item w3-red" style="width:15%;">Controles</button>
					</div>
				';

				$cons= consultar_enorden( "LOG_REPORTES", "FECHA_INICIO DESC" );
				if( !mysql_num_rows($cons) )
					{
					echo '<div class="w3-container">Click para inicializar tu sistema <a href="'. HTTP_SERVER. 'reportes.php?id=reportes&m=init"><button class="w3-button w3-orange w3-padding w3-margin-top">Inicializar</button></a></div>';
					}
				else
					{
					while( $buf=mysql_fetch_array($cons) )
						{
						echo '
						<div class="w3-bar">
							<div class="w3-bar-item" style="width:55%">'. desproteger_cadena_src($buf["NOMBRE"]). '</div>
							<div class="w3-bar-item w3-center" style="width:10%">'. desproteger_cadena_src(($buf["FECHA"] ? date("d/m/Y", $buf["FECHA"]):'')). '</div>
							<div class="w3-bar-item w3-center" style="width:20%">'. desproteger_cadena_src((($buf["FECHA_INICIO"] && $buf["FECHA_FIN"]) ? date("d/m/Y", $buf["FECHA_INICIO"]). ' al '. date("d/m/Y", $buf["FECHA_FIN"]):'')). '</div>
							<div class="w3-bar-item nd" style="width:15%"><a href="'. HTTP_SERVER. 'reportes.php?id=reportes&m=gen&idr='. $buf["ID"]. '"><button class="w3-input w3-small w3-blue">Generar</button></a></div>
						</div>';
						}
					}
				}
			}
		else
			{
			echo '
					<div class="w3-bar w3-small w3-red">
						<button class="w3-bar-item w3-red" style="width:3%;">::</button>
						<button class="w3-bar-item w3-red" style="width:6%;">Usuario</button>
						<button class="w3-bar-item w3-red" style="width:15%;">Geo</button>
						<button class="w3-bar-item w3-red" style="width:8%;">IP</button>
						<button class="w3-bar-item w3-red" style="width:10%;">S.O/NAV</button>
						<button class="w3-bar-item w3-red" style="width:10%;">Fecha</button>
						<button class="w3-bar-item w3-red" style="width:10%;">Fecha Update</button>
						<button class="w3-bar-item w3-red" style="width:38%;">Referencia</button>
					</div>
					<div class="w3-rows" id="pad4">';
			if( mysql_num_rows($cons_log)==0 )
				echo '<div class="w3-bar" style="width:100%;">No hay datos aun...</div>';
			else
				{
				$a=0;
				while( $buf_log= mysql_fetch_array($cons_log) )
					{
						/*
								$tmp= parse_url($rbuf["REF_VAR"]);

								# no hay referencia, es visita directa
								# if( !$rbuf["REF_VAR"] )
								if( !$rbuf["REF_VAR"] || strstr(substr(HTTP_SERVER, strlen('https://'), strlen(HTTP_SERVER)), $tmp["host"]) )
						*/

					$ref= parse_url($buf_log["REF_VAR"]); # ( (strstr($_SERVER['HTTP_HOST'], $parse["path"]) || strstr($_SERVER['HTTP_HOST'], $parse["host"])) ? 0:1);
					$quer= $buf_log["REQUEST_VAR"]; # ( (strstr($_SERVER['HTTP_HOST'], $parse["path"]) || strstr($_SERVER['HTTP_HOST'], $parse["host"])) ? 0:1);
					$parse= ((!$buf_log["REF_VAR"] || strstr($ref["host"], substr(HTTP_SERVER, strlen('https://'), strlen(HTTP_SERVER)))) ? 0:1); # si encontramos

					$robot= (is_a_robot($buf_log["IP"]) ? is_a_robot($buf_log["IP"]):is_a_robot($buf_log["USERAGENT_VAR"]));
					echo '
					<div class="w3-bar'. ($robot ? ' w3-pale-yellow':(($quer && strstr($quer, "/?ads=")) ? ' w3-green':(($parse && strcmp($quer, "/")) ? ' w3-pale-green':''))). '">
							<li id="pad4" class="w3-bar-item" style="width:3%;">';
					if( $robot )
						echo '<img src="../admin/imagenes/letra_i.png" alt="'. $robot. '" title="'. $robot. '" border="0">';
					else if( $parse ) //entonces fue una busqueda o referido
						echo '<img src="../admin/imagenes/letra_r.png" alt="'. $buf_log["REFERENCIA"]. '" title="'. $buf_log["REFERENCIA"]. '" border="0">';
					else
						echo '<img src="../admin/imagenes/letra_u.png" alt="'. $buf_log["REFERENCIA"]. '" title="'. $buf_log["REFERENCIA"]. '" border="0">';

					echo '</li>
							<li id="pad4" class="w3-bar-item" style="width:6%;">'. desproteger_cadena($buf_log["NICK"]). '</li>
							<li id="pad4" class="w3-bar-item" style="width:15%;">'. desproteger_cadena($buf_log["UBICACION"]). '</li>
							<li id="pad4" class="w3-bar-item" style="width:8%;">'. desproteger_cadena($buf_log["IP"]). '</li>
							<li id="pad4" class="w3-bar-item" style="width:10%;">'. desproteger_cadena( ($robot ? $robot:$buf_log["SO"].'/'.$buf_log["NAVEGADOR"]) ). '</li>
							<li id="pad4" class="w3-bar-item" style="width:10%;">'. date( "d/m/Y, g:i a", $buf_log["FECHA_LOGIN"]). '</li>
							<li id="pad4" class="w3-bar-item" style="width:10%;">'. date( "d/m/Y, g:i a", $buf_log["FECHA_UPDATE"]). '</li>
							<li id="pad4" class="w3-bar-item" style="width:38%;">'. ($buf_log["REF_VAR"] ? 'Referer: '. desproteger_cadena($buf_log["REF_VAR"]).'<br>':''). ($buf_log["REQUEST_VAR"] ? 'Request: '. desproteger_cadena($buf_log["REQUEST_VAR"]).'<br>':''). ($buf_log["QUERY_VAR"] ? 'Query: '. desproteger_cadena($buf_log["QUERY_VAR"]).'<br>':''). '
								'. ($buf_log["USERAGENT_VAR"] ? '<br>UserAgent: '. desproteger_cadena_src($buf_log["USERAGENT_VAR"]):''). '
								'. ($buf_log["ACCEPT_VAR"] ? '<br>Accept: '. desproteger_cadena_src($buf_log["ACCEPT_VAR"]):''). '
								'. ($buf_log["ENCODING_VAR"] ? '<br>Encoding: '. desproteger_cadena_src($buf_log["ENCODING_VAR"]):''). '
								'. ($buf_log["LENGUAJE_VAR"] ? '<br>Lenguaje: '. desproteger_cadena_src($buf_log["LENGUAJE_VAR"]):''). '
							</li>
						</div>
					';
					unset($robot);
					}
				//selector de paginacion o roll out
				echo '<div class="container w3-padding w3-small">';
					selector_paginacion( "LOG", $minimo, $pag, '?id='. $_GET["id"]. '&mov='. $_GET["mov"], "0", "0" );
				echo '</div>';
				}
			unset($buf_log, $cons_log);
			echo '</div>';


	$finicio= strtotime(date("Y-m", time()).'-01T00:00:00');
	$ffin= strtotime((date("m", time())==12) ? (date("Y-", time())+1):date("Y-", time()).((date("m", time())==12) ? '01':(date("m", time())+1) ).'-01T00:00:00');
	$fl=0;
	$rtcons= consultar_rango_enorden( "LOG", "FECHA_LOGIN", $finicio, $ffin, "FECHA_LOGIN ASC" );
	if( !mysql_num_rows($rtcons) )
		$fl=0;
	else
		{
						$metricas= array( 
							"contador"=>0, 
							"usuarios"=>array(
								"total"=>0,
								"total_ref"=>0, 
								"total_unico"=>0,
								"ref"=>array(), 
								"unico"=>array()
								), 
							"robot"=>array(
								"total"=>0,
								"marcas"=>array()
								), 
							"paginas"=>array(
								"total"=>0, 
								"total_indexadas"=>0, 
								"total_visitadas"=>0, 
								"total_referencias"=>0, 
								"indexadas"=>array(), 
								"visitadas"=>array(), 
								"referencias"=>array() # total=conteo totales, index= indexaciones totales, visita= conteo, ref= conteo
								), 
							"calendario"=>array()
							);
		$fl=1;
		while( $rtbuf=mysql_fetch_array($rtcons) )
			{
							# inicializando calendario
							if( !count($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]) ) # si no existe el dia
								{
								$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["index"]=0;
								$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["ref"]=0;
								$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["visita"]=0;
								$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["total"]=0;
								}

							$metricas["contador"] += 1;
							$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["total"]= (!($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["total"]) ? 1:($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["total"]+1)); # calendario conteo del dia totales

							if( is_a_robot($rtbuf["IP"]) || is_a_robot($rtbuf["USERAGENT_VAR"]) ) # si es robot
								{
								$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["index"]= (!($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["index"]) ? 1:($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["index"]+1)); # calendario conteo dia indexaciones 
								$metricas["robot"]["total"] += 1; # incrementamos robot visita
								$nbot= (is_a_robot($rtbuf["IP"]) ? is_a_robot($rtbuf["IP"]):is_a_robot($rtbuf["USERAGENT_VAR"]));
								$nbot= substr( $nbot, strlen('Indexado por '), strlen($nbot) ); # obtenemos nombre del bot

								if( !count($metricas["robot"]["marcas"]) || !$metricas["robot"]["marcas"][$nbot] )
									$metricas["robot"]["marcas"][$nbot]= 1; # inicializamos
								else 	$metricas["robot"]["marcas"][$nbot] += 1; # incrementamos

								$metricas["paginas"]["total"] += 1; # incrementamos contador de pagina visitada
								$metricas["paginas"]["total_indexadas"] += 1; # incrementamos contador de pagina indexada
								$metricas["paginas"]["indexadas"][]= array( "robot"=>$nbot, "fecha"=>$rtbuf["FECHA_LOGIN"], "ruta"=>$rtbuf["REQUEST_VAR"]);
								}
							else # es acceso humano
								{
								$metricas["usuarios"]["total"] += 1; # incrementamos visitas totales
								$metricas["paginas"]["total"] +=1; # incrementamos total
								$tmp= parse_url($rtbuf["REF_VAR"]);

								# no hay referencia, es visita directa
								# if( !$rtbuf["REF_VAR"] )
								if( !$rtbuf["REF_VAR"] || !strcmp($rtbuf["REF_VAR"], "/") || strstr(substr(HTTP_SERVER, strlen('https://'), strlen(HTTP_SERVER)), $tmp["host"]) )
									{
									$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["ref"]= (!($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["ref"]) ? 1:($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["ref"]+1));
									$metricas["usuarios"]["total_unico"] += 1; # incrementamos unico
									if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rtbuf["UBICACION"]] )
										$metricas["usuarios"]["unico"][$rtbuf["UBICACION"]]=1; # inicializamos
									else 	$metricas["usuarios"]["unico"][$rtbuf["UBICACION"]] += 1; # incrementamos

									$metricas["paginas"]["total_visitadas"] +=1; # incrementamso referencia
									$metricas["paginas"]["visitadas"][]= array( "ruta"=>($rtbuf["REQUEST_VAR"] ? $rtbuf["REQUEST_VAR"]:'/'), "fecha"=>$rtbuf["FECHA_LOGIN"], "ubicacion"=>$rtbuf["UBICACION"] );
									}
								else # SI tenemos referencia, viene de otro sitio o buscador
									{
									$metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["visita"]= ( !($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["visita"]) ? 1:($metricas["calendario"][date("d", $rtbuf["FECHA_LOGIN"])]["visita"]+1));
									$tmp= parse_url( $rtbuf["REF_VAR"]);
									$nref= $tmp["host"];
									$nquery= $tmp["query"];
									$npath= $tmp["path"];
									$metricas["usuarios"]["total_ref"] += 1; # incrementamos referencia
									if( !count($metricas["usuarios"]["ref"]) || !$metricas["usuarios"]["ref"][$nref] )
										$metricas["usuarios"]["ref"][$nref]=1;
									else 	$metricas["usuarios"]["ref"][$nref] += 1;

									# referencia Geografica
									if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rtbuf["UBICACION"]] )
										$metricas["usuarios"]["unico"][$rtbuf["UBICACION"]]=1; # inicializamos
									else 	$metricas["usuarios"]["unico"][$rtbuf["UBICACION"]] += 1; # incrementamos

									$metricas["paginas"]["total_referencias"] += 1;
									$metricas["paginas"]["referencias"][]= array( "ref"=>$nref, "ruta"=>($rtbuf["REQUEST_VAR"] ? $rtbuf["REQUEST_VAR"]:'/'), "fecha"=>$rtbuf["FECHA_LOGIN"], "ubicacion"=>$rtbuf["UBICACION"], "consulta"=>($nquery ? $nquery:""), "loader"=>($npath ? $npath:"") );
									}
								}
			}
		}

		echo '
						<div class="w3-row w3-container w3-padding-16">
							<div class="w3-card-2 w3-padding w3-left  w3-bottombar w3-border-black">
								<h3>'. number_format($metricas["contador"]). '</h3>
								<p>Visitas totales.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar w3-border-blue">
								<h3>'. number_format($metricas["usuarios"]["total_unico"]). '</h3>
								<p>Visitas de Usuario.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar w3-border-green">
								<h3>'. number_format($metricas["usuarios"]["total_ref"]). '</h3>
								<p>Visitas de Referenciadas.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar bnaranja">
								<h3>'. number_format($metricas["robot"]["total"]). '</h3>
								<p>Visitas de Indexadores.</p>
							</div>
							<div class="w3-card-2 w3-padding w3-left w3-margin-left w3-bottombar bazul2">
								<h3>'. number_format($metricas["usuarios"]["total"]). '</h3>
								<p>Usuario+Referencia.</p>
							</div>
						</div>
				<div class="w3-row">
					<h2 class="w3-panel w3-leftbar w3-border-red w3-teal w3-padding">Metrica Tiempo Real '. mes_esp(date("m"), time()). ' '.  date("Y"). '</h2>
					<div class="w3-container" id="mreal"></div>
				</div>';

if( $fl )
	{
# valores para la grafica
$i=0;
$gr_cat='';
$gr_vi='';
$gr_to='';
$gr_re='';
$gr_in='';
foreach( $metricas["calendario"] as $key=>$val )
	{
	$gr_cat .= ($i ? ',':''). '\''. $key. '\'';
	$gr_vi .= ($i ? ',':''). $val["visita"];
	$gr_to .= ($i ? ',':''). $val["total"];
	$gr_re .= ($i ? ',':''). $val["ref"];
	$gr_in .= ($i ? ',':''). $val["index"];
	$gr_ur .= ($i ? ',':''). ($val["visita"]+$val["ref"]);
	$i++;
	}

echo '<script type="text/javascript">

Highcharts.chart(\'mreal\', {
    chart: {
        type: \'line\'
    },
    title: {
        text: \''. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '\'
    },
    subtitle: {
        text: \'Source: '. HTTP_SERVER. '\'
    },
    xAxis: {
        categories: ['. $gr_cat. ']
    },
    yAxis: {
        title: {
            text: \'Metrica Trafico '. mes_esp(date( "m", $finicio)). '\'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: true
        }
    },
    series: [{
        name: \'Usuarios\',
        data: ['. $gr_vi. ']
    }, {
        name: \'V.Totales\',
        data: ['. $gr_to. ']
    }, {
        name: \'Referencias\',
        data: ['. $gr_re. ']
    }, {
        name: \'Indexacion\',
        data: ['. $gr_in. ']
    }, {
        name: \'Usr+Ref\',
        data: ['. $gr_ur. ']
    }]
});
		</script>';
unset($gr_cat, $gr_vi, $gr_to, $gr_re, $gr_in, $gr_ur );
	}
			echo '
				<div class="w3-row">
					<h2 class="w3-panel w3-leftbar w3-border-red w3-teal w3-padding">Metrica de Referencias: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>
					<div class="w3-container" id="mrealref"></div>
				</div>';
$i=0;
$gr_br='';
$gr_mt='';
foreach( $metricas["usuarios"]["ref"] as $key=>$val )
	{
	$gr_br .=  ($i ? ',':''). '\''. ($key ? $key:'unknown'). '\'';
	$gr_mt .=  ($i ? ',':''). $val;
	$i++;
	}

echo '
<script type="text/javascript">

Highcharts.chart(\'mrealref\', {
    chart: {
        type: \'bar\'
    },
    title: {
        text: \''. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '\'
    },
    subtitle: {
        text: \'Source: '. HTTP_SERVER. '\'
    },
    xAxis: {
        categories: ['. $gr_br. '],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: \'Metrica de Referencias\',
            align: \'high\'
        },
        labels: {
            overflow: \'justify\'
        }
    },
    tooltip: {
        valueSuffix: \' visitas\'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: \'vertical\',
        align: \'right\',
        verticalAlign: \'top\',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || \'#FFFFFF\'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        data: ['. $gr_mt. ']
    }]
});
		</script>';
unset($gr_br, $gr_mt);


						echo '<div class="w3-row w3-padding-32">
						<h2 class="w3-leftbar w3-border-blue w3-padding">Detalle de Trafico: '. mes_esp(date( "m", $finicio)). ' '. date("Y", $finicio). '</h2>
						<div class="w3-panel w3-container w3-padding w3-pale-yellow">
							Observa acceso por acceso tus visitas, segmentado por situaciones de interes.
						</div>

						<div class="w3-bar w3-light-gray w3-bottombar w3-border-black">
							<a href="#full_ref" class="w3-bar-item w3-padding w3-hover-green nd">Referenciado</a>
							<a href="#full_graf" class="w3-bar-item w3-padding w3-hover-green nd">Geografico</a>
							<a href="#full_usr" class="w3-bar-item w3-padding w3-hover-green nd">Usuarios</a>
							<a href="#full_ind" class="w3-bar-item w3-padding w3-hover-green nd">Indexado</a>
							<a href="#full_all" class="w3-bar-item w3-padding w3-hover-green nd">Todo</a>
						</div>

						<div class="w3-row" id="full_ref">
							<div class="w3-bar w3-black w3-small">
								<div href="#full_ref" class="w3-bar-item w3-padding" style="width:15%">Marca</div>
								<div href="#full_graf" class="w3-bar-item w3-padding" style="width:20%">Ruta</div>
								<div href="#full_usr" class="w3-bar-item w3-padding" style="width:10%">Fecha</div>
								<div href="#full_ind" class="w3-bar-item w3-padding" style="width:15%">Lugar</div>
								<div href="#full_all" class="w3-bar-item w3-padding" style="width:20%">Consulta</div>
								<div href="#full_all" class="w3-bar-item w3-padding style="width:20%"">Desde</div>
							</div>';
			$i=0;
			foreach( $metricas["paginas"]["referencias"] as $key=>$val )
				{
				echo '<div class="w3-bar w3-tiny'. ( !($i%2) ? ' w3-light-gray':''). '" style="word-wrap:break-word;">
				<li class="w3-bar-item" style="width:15%">'. $val["ref"]. '</li>
				<li class="w3-bar-item" style="width:20%">'. $val["ruta"]. '</li>
				<li class="w3-bar-item" style="width:10%">'. date( "d/m/Y, g:i a", $val["fecha"]). '</li>
				<li class="w3-bar-item" style="width:15%">'. $val["ubicacion"]. '</li>
				<li class="w3-bar-item w3-right-align" style="width:20%">'. $val["consulta"]. '</li>
				<li class="w3-bar-item w3-right-align" style="width:20%">'. $val["loader"]. '</li>
				</div>';
				$i++;
				}
					echo '</div>

						</div>
';
			}

		echo '
			</div>
		</div>
	</body>
</html>
';

ob_end_flush(); # fin objeto
?>