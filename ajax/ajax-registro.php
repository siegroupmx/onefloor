<?php
if( !strcmp($_GET["id"], "registrarme" ) )
	{
	if( !strcmp($_GET["m"], "yes") )
		{
		echo "<h3>Procedimiento de Registro</h3>";
		echo "<p align=\"center\">";
			
		//checamos variables
		if( !$_POST["registro_nombre"] || consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena($_POST["registro_nick"]). "'", "ID") )
			echo '<b>Error: </b>el nickname que desea usar, ya existe.';
		else if( !validar_email($_POST["registro_email"]) )
			echo '<b>Error:</b> la direccion de correo electronico no es valida.';
		else if( consultar_datos_general( "USUARIOS", "EMAIL='". proteger_cadena($_POST["registro_email"]). "'", "ID") )
			echo '<b>Error: </b>la direccion de correo electronico ya esta registrada.';
		else if( !$_POST["registro_password"] || strlen($_POST["registro_password"])<6 )
			echo '<b>Error:</b> la clave debe ser mayor a 6 caracteres.';
		else if( !$_POST["registro_pais"] )
			echo '<b>Error:</b> debe especificar un pais.';
		else if( !$_POST["registro_estado"] )
			echo '<b>Error:</b> debe especificar un estado.';
		else
			{
				do //generamos numero aleatorio
					{
					$idtrack= generar_idtrack(); //obtenemos digito aleatorio
					}while( !strcmp( $idtrack, consultar_datos_general( "USUARIOS", "ID='". $idtrack. "'", "ID" ) ) );
					
			$trama= array(
				"id"=>"'". $idtrack. "'", 
				"nombre"=>"'". proteger_cadena($_POST["registro_nombre"]). "'",
				"nick"=>"'". proteger_cadena($_POST["registro_nick"]). "'", 
				"password"=>"'". proteger_cadena($_POST["registro_password"]). "'", 
				"email"=>"'". proteger_cadena($_POST["registro_email"]). "'",
				"pais"=>"'". proteger_cadena($_POST["registro_pais"]). "'",
				"estado"=>"'". proteger_cadena($_POST["registro_estado"]). "'", 
				"fecha_registro"=>"'". time(). "'", 
				"avatar"=>"'default.png'", 
				"tipo_usr"=>"'Usuarios'" );
					
			if( !insertar_bdd( "USUARIOS", $trama ) ) //insertamos
				echo "Error en la Insercion de los Datos.";
			else //exito al insertar datos
				{
				echo "<b>". TITULO_WEB. " te da la Bienvenida.</b><p>Te haz registrado con exito, ahora podras conectarte a la pagina.";
					
				if( !inicializar_espacio_personal( $idtrack ) )
					{
					echo "<p><b>IMPORTANTE:</b> tu usuario a sido registrado con exito, pero tu <b>EspacioWeb</b> no ha podido ser inicializado, porfavor da aviso ";
					echo "de esto al <b>Administrador</b>.</p>";
					}
				enviar_correo( consultar_datos_general( "USUARIOS", "TIPO_USR='Administrador'", "email"), consultar_datos_base( "MENSAJES_NOTIFICACION", 2, "mensaje" ), 2, $_POST["registro_nick"]."<". $_POST["registro_email"]. ">", 0, 0, 0, 0 );
				}
			}
		unset($tmp); //limpiamos
		echo "</p>";
		}
	else
		{
		echo "<h3>Formulario de Registro</h3>";
		//echo "<div id=\"rcontent\">";
		//echo "<div id=\"rcol\">";
		echo "<div id=\"noticia_div\">";
		echo "En el siguiente formulario rellena <b>todos los datos</b> para continuar con tu registro.<br>";
		echo "<br>";
			
		if( strcmp( consultar_datos_base( "SERVER_CONFIG", 1, "registro_usuario" ), "") )
			echo desproteger_cadena(consultar_datos_base( "SERVER_CONFIG", 1, "registro_usuario" ));
		else echo "<i>Aun no se ha establecido mensaje para el registro de usuarios</i>.";
			
		echo "<table>";
		echo "<td>Nombre:</td><td><input type=\"text\" name=\"registro_nombre\" id=\"registro_nombre\" style=\"font-size:11px;margin-bottom:4px;padding:2px;\"></td><tr>";
		echo "<td>Nick:</td><td><input type=\"text\" name=\"registro_nick\" id=\"registro_nick\" style=\"font-size:11px;margin-bottom:4px;padding:2px;\"></td><tr>";
		echo "<td>Password:</td><td><input type=\"password\" name=\"registro_password\" id=\"registro_password\" style=\"font-size:11px;margin-bottom:4px;padding:2px;\"></td><tr>";
		echo "<td>Correo Electronico:</td><td><input type=\"text\" name=\"registro_email\" id=\"registro_email\" style=\"font-size:11px;margin-bottom:4px;padding:2px;\"></td><tr>";
		echo "<td>Pais:</td>";
		echo "<td><select name=\"registro_pais\" id=\"registro_pais\">";
			lista_paises("select");
		echo "</select><tr>";
		echo "<td>Estado:</td><td><input type=\"text\" name=\"registro_estado\" id=\"registro_estado\"></td><tr>";
		echo "</table>";
		echo "<center>";
		echo '<input type="submit" value="Registrarme" style="padding:2px 3px 2px 3px;margin-bottom:10px;" 
		onclick="cargar_datos(\'id=registrarme&m=yes\', \'content\', \'POST\', \'registro_nombre:registro_nick:registro_password:registro_email:registro_pais:registro_estado\');"></center>';
		echo "</div>";
		}
	}
?>