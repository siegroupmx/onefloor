<?php
if( !strcmp($_GET["id"], "comentarios") && is_login() )
	{
	if( !strcmp($_GET["mov"], "moderar") && is_autor() )
		{
		if( !strcmp($_GET["opcion"], "eliminar") )
			{
			if( $_GET["id_com"] && consultar_datos_general("COMENTARIOS", "ID='". proteger_cadena($_GET["id_com"]). "'", "ID_USUARIO") )
				{
				# si esta en facebook
				/*
				if( ($fbobj=consultar_datos_general("COMENTARIOS", "ID='". proteger_cadena($_GET["id_com"]). "'", "FACEBOOK_ID")) )
					{
					$user= array(
						consultar_datos_general("COMENTARIOS", "ID='". proteger_cadena($_GET["id_com"]). "'", "ID_USUARIO"), 
						$fbobj,  
						'search', 
						'&method=delete'
						);
					$arr=array();

					$r= facebook_pgdata( 'delete', 'post', $user, $arr );
					print_r($r);
					}
				unset($fbobj);
				*/
				
				# quitamos de la BDD
				if( !eliminar_bdd( "COMENTARIOS", "ID='". proteger_cadena($_GET["id_com"]). "'" ) )
					echo "Problemas para eliminar comentario";
				else	echo "Eliminado con exito (recarga la hoja).";
				}
			else		echo 'Error: el comentario no existe...';
			}
		else if( !strcmp($_GET["opcion"], "esconder") )
			{
			$tr= array( "id"=>"'". proteger_cadena($_GET["id_com"]). "'", "visible"=>"'0'" );
			echo '--> ';
			if( actualizar_bdd( "COMENTARIOS", $tr )=="0" )
				echo 'Problemas para <u>hacer invisible</u> el comentario...';
			else
				echo 'Ahora el comentario <u>es invisible</u>...';
			unset($tr);
			}
		else if( !strcmp($_GET["opcion"], "mostrar") )
			{
			echo '--> ';
			$tr= array( "id"=>"'". proteger_cadena($_GET["id_com"]). "'", "visible"=>"'1'" );
			if( actualizar_bdd( "COMENTARIOS", $tr )=="0" )
				echo 'Problemas para <u>hacer visible</u> el comentario...';
			else
				echo 'Ahora el comentario <u>es visible</u>...';
			unset($tr);
			}
		}
	else if(	!strcmp($_GET["mov"], "agregar") )
		{
		if( !consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["idnot"]). "'", "AUTOR" ) ) # si no existe noticia
				echo '<h1>Error: Intentas publicar en un noticia que no existe.['. $_GET["hoja"]. ']</h1>';
		else if( !$_POST["comentario_mensaje"] )
			echo '<h1>No haz escrito mensaje aun...</h1>';
		#else if( !get_recaptcha_check($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]) )
		#	echo '<b>Error:</b> La comprobacion de Re-Captcha fallo, vuelva a intentarlo.';
		else
			{
			/*
			###################
			# codigo viejo :( #
			###################
			if( usuario_legitimo() ) # si es usuario registrado
				{ 
				$com_nombre= consultar_datos_general( "USUARIOS", "NICK='". proteger_cadena( $_SESSION["log_usr"] ). "'", "NOMBRE" );
				$com_email= proteger_cadena( consultar_datos_usuario( $_SESSION["log_usr"], "email" ) );
				}
			else # es Anonimo 
				{
				$com_nombre= "Anonimo"; # establecemos nombre 
				if( !empty($_POST["comentario_email"]) ) # si el e-mail no esta vacio 
					$com_email= proteger_cadena( $_POST["comentario_email"] ); # establecemos e-mail para notificaciones 
				else $com_email= "0"; # esta vacio, no se notificara 
				}
			*/
			$com_user= proteger_cadena($_SESSION["log_id"]);
			$com_email= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "EMAIL");
			$com_mensaje= proteger_cadena( $_POST["comentario_mensaje"] ); # tomamos mensaje 
		
			#if( is_admin() || is_admingrp() ) # si es administrador o del grupo 
				$visible="1"; # visible por defecto 
			#else	$visible="0"; # no_visible se esperara vista por admin / moderador

			do //generamos numero aleatorio de 4 a 10 digitos
				{
				$idtrack= generar_idtrack(); //obtenemos digito aleatorio
				}while( !strcmp( $idtrack, consultar_datos_general( "COMENTARIOS", "ID='". $idtrack. "'", "ID" ) ) );

			$trama= array(
				"ID"=>"'". $idtrack. "'", 
				"ID_NOT"=>"'". proteger_cadena($_GET["idnot"]). "'",
				"ID_USUARIO"=>"'". $com_user. "'",
				"MENSAJE"=>"'". $com_mensaje. "'",
				"EMAIL"=>"'". $com_email. "'",
				"VISIBLE"=>"'". $visible. "'",  
				"FECHA"=>"'". time(). "'" 
				);

			if( insertar_bdd( "COMENTARIOS", $trama )==0 )
				echo "<h1>Error en la Insercion de los Datos.</h1>";
			else
				{
				echo '<img src="http://'. $_SERVER['HTTP_HOST']. '/admin/imagenes/palomita.png" style="float:left;" border="0">';
				#if( is_admin() || is_admingrp() )	
					echo 'Tu mensaje ha sido publicado (Refresca la Pagina).';
				#else
				#	echo 'Tu mensaje ha sido recivido con exito.<br>En lo mas pronto posible revisaran tu mesaje';

				# notificacion por email
				/*
				$link= "http://". $_SERVER['HTTP_HOST']. "/?hoja=". $_GET["hoja"];
				$rsend= consultar_con( "COMENTARIOS", "ID_NOT='". proteger_cadena($_GET["hoja"]). "'" );
				$big_buf_rep="";
			
				while( $rsend_buf= mysql_fetch_array($rsend) )
					{
					# si el correo no se repite y es distinto de Vacio y 0
					if( evitar_repeticion_mails($big_buf_rep, $rsend_buf["EMAIL"]) && strcmp($rsend_buf["EMAIL"], "") && strcmp($rsend_buf["EMAIL"], "0") )
						{
						# si esta activado recivir notificaciones o si es anonimo, se le envia correo
						if( !strcmp( $rsend_buf["AUTOR"], "Anonimo") || !strcmp( consultar_datos_general( "USUARIOS", "NOMBRE='". $rsend_buf["AUTOR"]. "'", "NOTIFICACION_COMENTARIOS" ), "1") )
							{
							if( strcmp( $big_buf_rep, "" ) ) //si no contiene datos
								$big_buf_rep .= ","; //agregamos coma separadora
							$big_buf_rep .= $rsend_buf["EMAIL"]; //concatenamos mail
					
							if( !enviar_correo( $rsend_buf["EMAIL"], consultar_datos_base( "MENSAJES_NOTIFICACION", 0, "mensaje" ), 0, $link, 0, 0, 0, 0 ) )
								echo "<h1>Error al Enviar Correo.</h1>";
							}
						}
					}
				unset($big_buf_rep);
				limpiar($rsend);
				*/
				
				# actualizar comentarios
				$contador= consultar_datos_general( "NOTICIAS", "ID='". proteger_cadena($_GET["idnot"]). "'", "COMENTARIOS");
				if( !$contador )		$contador=1;
				else		$contador += 1;
				$carr=array( "id"=>"'". proteger_cadena($_GET["idnot"]). "'", 
					"comentarios"=>"'". $contador. "'" 
					);
				actualizar_bdd( "NOTICIAS", $carr ); # actualizamos contador de comentarios
				unset($carr, $contador);
				
				# redes sociales
				#
				# facebook -- se coloca el comentario en la publicacion e FanPage.
				if( ($fbobj=consultar_datos_general("NOTICIAS", "ID='". proteger_cadena($_GET["idnot"]). "'", "FACEBOOK")) && 
					consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "FACEBOOK_ID" ) )
					{
					$user= array(
						$_SESSION["log_id"], 
						$fbobj,  
						'search'
						);
					$arr= array( "message"=>$com_mensaje );

					$r= facebook_pgdata( 'comments', 'post', $user, $arr );
					if( $r["id"] ) # si se inserto con exito
						{
						$newtrama= array( "id"=>"'". $idtrack. "'", "facebook_id"=>"'". $r["id"]. "'" );
						actualizar_bdd( "COMENTARIOS", $newtrama );
						unset($newtrama);
						}
					}
				unset($fbobj);
				}
			unset($visible, $idtrack);
			}
		}
	else
		echo "No puedes usar este AJAX :P";
	}
else
		echo "No puedes usar este AJAX :P";
?>