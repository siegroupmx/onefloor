<?php
if( is_login() && !strcmp($_GET["my"], "perfil") )
	{
	if( $_GET["op"] && !is_array($_GET["op"]) && !strcmp($_GET["op"], "upload_file") ) # subir imagen de perfil
		{
		$tipo= substr( strtolower($_FILES["upload_perfil"]["name"]), -3 ); # extraemos extencion
		# echo '<script type="text/javascript">alert( \'Formato: '. $tipo. ' / Tmp Name: '. $_FILES["upload_perfil"]["tmp_name"]. ' / Name: '. $_FILES["upload_perfil"]["name"]. '\');</script>';
		echo 'Entre';
		
		if( !empty($_FILES["upload_perfil"]["tmp_name"]) )
			{
			$path= 'uploads/';
			$idtrack= generar_idtrack(); //obtenemos digito aleatorio
			$_SESSION["upload"]= $idtrack. '_'. strtolower(url_cleaner(substr($_FILES["upload_perfil"]["name"], 0, -4)).substr($_FILES["upload_perfil"]["name"], -4)); # directorio
			$tipo= substr( strtolower($_FILES["upload_perfil"]["name"]), -3 ); # extraemos extencion
		
			if( !$_GET["type"] || is_array($_GET["type"]) )
				echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error no se especifico formato...\', \'upload\' );</script>';
			else if( !validar_tipo_de_formato( $tipo, proteger_cadena($_GET["type"])) ) # validamos formato imagen
				echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error tipo de archivo...\', \'upload\' );</script>';
			else if( !strcmp(leer_formato($tipo), "imagen") && ($_FILES["upload_perfil"]["size"]/1024)>550 ) # no exeder de 550kb en imagenes
				 echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion de imagen excedida, solo 1MB\', \'upload\' );</script>';
			else if( !strcmp(leer_formato($tipo), "archivo") && ($_FILES["campain_file"]["size"]/1024)>(1024*5) && !is_coadmin() ) # no exeder de 5MB en archivos
				 echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion archivo excedida, solo 5MB\', \'upload\' );</script>';
			else if( !move_uploaded_file( $_FILES["upload_perfil"]["tmp_name"], $path.$_SESSION["upload"] ) )
				{
				echo '<script language="javascript" type="text/javascript">
				window.top.window.resultadoUpload(\'3\', \''. $_FILES["upload_perfil"]["name"]. '\');
				</script>';
				}
			else
				{
				$size= getimagesize($path.$_SESSION["upload"]); # obtenemos dimencion de la imagen original
				$w= $size[0]; # extraemos width
				if( $w>550 ) # verificando dimenciones de la imagen
					{
					echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: dimencion ancho de imagen maximo 550px\', \'upload\' );</script>';
					uploads_del(); # eliminamos archivos subidos 
					}
				else
					{
					 if( !strcmp(leer_formato($tipo), "imagen") ) # si es imagen
						$path_user= 'usuarios/'. proteger_cadena($_SESSION["log_id"]). '/imagenes/'; # path de imagenes
					else if(  !strcmp(leer_formato($tipo), "archivo") ) # si es archivo
						$path_user= 'usuarios/'. proteger_cadena($_SESSION["log_id"]). '/archivos/'; # path de archivos 					
					copy( $path.$_SESSION["upload"], $path_user.$_SESSION["upload"]); # copiamos al directorio del propietario 
					unlink( $path.$_SESSION["upload"] ); # eliminamos temporal

					 if( !strcmp(leer_formato($tipo), "imagen") ) # si es imagen
						{
						$trama= array(
							"id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
							"avatar"=>"'". proteger_cadena($path_user.$_SESSION["upload"]). "'"
							);
						}
					else if(  !strcmp(leer_formato($tipo), "archivo") ) # si es archivo
						{
						$trama= array(
							"id"=>"'". proteger_cadena($_SESSION["log_id"]). "'", 
							"curriculum"=>"'". proteger_cadena($path_user.$_SESSION["upload"]). "'"
							);
						}
					
					if( !strcmp(leer_formato($tipo), "imagen") ) # si es imagen
						{
						# si existe imagen de perfil, la eliminamos 
						if( consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "IMAGEN" ) )
							{
							$tmp_img= consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "IMAGEN" );
							unlink( $tmp_img ); # eliminamo imagen normal
							unlink( substr($tmp_img, 0, -4).'_mini'.substr($tmp_img, -4) ); # eliminamo imagen mini
							unlink( substr($tmp_img, 0, -4).'_web'.substr($tmp_img, -4) ); # eliminamo imagen web
							unset($tmp);
							}
						
						# redimencionando la imagen 
						if( !redimencionar_imagen( $path_user.$_SESSION["upload"], "45", "mini" ) ) # redimencion 1 para mini-spot (catalogo de anuncios)
							{
							echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: al re-dimencionar imagen mini.\', \'upload\' );</script>';
							$path.$_SESSION["upload"];
							$path_user.$_SESSION["upload"];
							}
						else if( !redimencionar_imagen( $path_user.$_SESSION["upload"], "150", "web" ) ) # redimencion 2 para spot-web (contenido del anuncio)
							{
							echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: al re-dimencionar imagen web.\', \'upload\' );</script>';
							$path.$_SESSION["upload"];
							$path_user.$_SESSION["upload"];
							}
						else if( !actualizar_bdd("USUARIOS", $trama ) )
							{
							echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: problema para guardar cambios.\', \'upload\' );</script>';
							$path.$_SESSION["upload"];
							$path_user.$_SESSION["upload"];
							}
						else
							{
							echo '<script language="javascript" type="text/javascript">
							window.top.window.resultadoUpload(\'1\', \''. $_FILES["upload_perfil"]["name"]. '\');
							</script>';
							}
						}
					else
						{
						if( !actualizar_bdd("USUARIOS", $trama ) )
							{
							echo '<script language="javascript" type="text/javascript">window.top.window.escribirCapa( \'Error: problema para guardar cambios.\', \'upload\' );</script>';
							$path.$_SESSION["upload"];
							$path_user.$_SESSION["upload"];
							}
						else
							{
							echo '<script language="javascript" type="text/javascript">
							window.top.window.resultadoUpload(\'1\', \''. $_FILES["upload_perfil"]["name"]. '\');
							</script>';
							}
						}
					}
				unset($size, $w);
				}
			unset($_SESSION["upload"]);
			}
		else # error
			{
			echo '<script language="javascript" type="text/javascript">
			window.top.window.resultadoUpload(\'2\', \'0\');
			</script>';
			}
		}
	else
		{
		if( !consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_GET["user"]). "'", "USUARIO" ) )
			echo '<div class="error_izq">El perfil que intenta actualizar no existe.</div>';
		else if( !$_GET["user"] && strcmp($_SESSION["log_usr"], $_GET["user"]) && !is_coadmin() )
			echo '<div class="error_izq">El perfil que intenta actualizar no es suyo.</div>';
		else if( !strcmp( $_POST["perfil_clave"], "") || strlen($_POST["perfil_clave"])<5 ) # comprobando calve
			echo '<div class="error_izq">La clave debe contener <b>minimo 8 caracteres</b>.</div>';
		else if( is_admin() && !$_POST["perfil_privilegio"] || !strcmp($_POST["perfil_privilegio"], "error") )
			echo '<div class="error_izq">Error en el tipo de Privilegio.</div>';
		else if( is_admin() && !$_POST["perfil_perfil"] || !strcmp($_POST["perfil_perfil"], "error") )
			echo '<div class="error_izq">Error en el tipo de Perfil.</div>';
		#else if( !strcmp($_POST["perfil_pais"], "error") || !strcmp($_POST["perfil_estado"], "error") || !strcmp($_POST["perfil_ciudad"], "error") )
		#	echo '<div class="error_izq">Especifica pais, ciudad y estado <b>validos</b>.</div>';
		else if( strcmp($_POST["perfil_web"], "") && !validar_enlaceweb($_POST["perfil_web"]) )
			echo '<div class="error_izq">La direccion web de su sitio <b>no es valida</b>.</div>';
		#else if( strcmp($_POST["perfil_podcast"], "") && !validar_enlaceweb($_POST["perfil_podcast"]) )
		#	echo '<div class="error_izq">La direccion web de su podcast <b>no es valida</b>.</div>';
		else if( !validar_email($_POST["perfil_email"]) && is_admin() )
			echo '<div class="error_izq">La direccion de correo electronico <b>no es valida</b>.</div>';
		else
			{
			if( strcmp($_POST["perfil_nacimiento"], "") ) # si contiene informacion
				{
				if( !strstr($_POST["perfil_nacimiento"], "/") )	$nacimiento=''; # fecha incorrecta
				else if( count( $x=explode("/", $_POST["perfil_nacimiento"]) )!=3 ) $nacimiento=''; # fecha incorrecta
				else # ahora veremos cada valor delimitado por '/'
					{
					$fl=0;
					if( !is_numeric($x[0]) )	$fl=1;
					if( !is_numeric($x[1]) ) $fl=1;
					if( !is_numeric($x[2]) ) $fl=1;
					# se ha establecido bien la fecha de nacimiento
					if( $fl==0 ) $nacimiento= $_POST["perfil_nacimiento"]; # fecha correcta
					else $nacimiento='';
					}
				unset($x);
				}
			else $nacimiento='';
				
			# resolviendo la pagina web
			if( !validar_enlaceweb($_POST["perfil_web"]) )  $web=''; # la URL no es valida 
			else # es valida, ahora verificamos http://
				{
				if( !strstr(strtolower($_POST["perfil_web"]), "http://") ) # si no tiene 'http://'
					$web= 'http://'. proteger_cadena( strtolower($_POST["perfil_web"]) );
				else		$web= proteger_cadena( strtolower($_POST["perfil_web"]) );
				}
			# resolviendo web del podcast
			if( !validar_enlaceweb($_POST["perfil_podcast"]) )  $pod=''; # la URL no es validavalida 
			else # es valida, ahora verificamos http://
				{
				if( !strstr(strtolower($_POST["perfil_podcast"]), "http://") ) # si no tiene 'http://'
					$pod= 'http://'. proteger_cadena( strtolower($_POST["perfil_podcast"]) );
				else		$pod= proteger_cadena( strtolower($_POST["perfil_podcast"]) );
				}
		
			# resolviendo correo, si es admin lo puede editar
			if( is_admin() )
				{
				$email= $_POST["perfil_email"];
				$perfil= $_POST["perfil_perfil"];
				$privilegio= $_POST["perfil_privilegio"];
				}
			# entonces es cliente, admin regional o soporte, se pone el mismo mail 
			else
				{
				$email= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_GET["user"]). "'", "EMAIL" ); # obtenemos email
				$perfil= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_GET["user"]). "'", "PERFIL" ); # obtenemos perfil
				$privilegio= consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_GET["user"]). "'", "PRIVILEGIO" ); # obtenemos privilegio
				}
			
			$trama= array( 
				"id"=>"'". proteger_cadena($_GET["user"]). "'", 
				"password"=>"'". proteger_cadena($_POST["perfil_clave"]). "'",
				"email"=>"'". proteger_cadena($email). "'",   
				"nombre"=>"'". proteger_cadena($_POST["perfil_nombre"]). "'", 
				"fecha_nacimiento"=>"'". proteger_cadena($nacimiento). "'", 
				"telefono"=>"'". proteger_cadena($_POST["perfil_telefono"]). "'", 
				"calle"=>"'". proteger_cadena($_POST["perfil_calle"]). "'", 
				"colonia"=>"'". proteger_cadena($_POST["perfil_colonia"]). "'", 
				"pais"=>"'". proteger_cadena($_POST["perfil_pais"]). "'", 
				"estado"=>"'". proteger_cadena($_POST["perfil_estado"]). "'", 
				"ciudad"=>"'". proteger_cadena($_POST["perfil_ciudad"]). "'", 
				#"empresa"=>"'". proteger_cadena($_POST["perfil_empresa"]). "'", 
				#"rfc"=>"'". proteger_cadena($_POST["perfil_rfc"]). "'", 
				"web"=>"'". proteger_cadena($web). "'"
				#"perfil"=>"'". proteger_cadena($perfil). "'", 
				#"privilegio"=>"'". proteger_cadena($privilegio). "'", 
				#"podcast"=>"'". proteger_cadena($pod). "'" 
				);
				
			if( !actualizar_bdd("USUARIOS", $trama) ) # si no se actualiza el perfil
				echo '<div class="error_izq"><b>Error: </b>el sistema no ha podido guardar tus datos '. help_admin(). '.</div>';
			else # entonces se actualizo, ahora guardamos modificaciones tambien en la LISTA DE MAILS
				{
				echo '<div class="exito_izq">Los datos fueron Guardados con exito.</div>';
	
				$trama_lista= array(
					"id"=>"'". consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_GET["user"]). "'", "LISTA_DB"). "'", 
					"nombre"=>"'". proteger_cadena($_POST["perfil_nombre"]). "'",
					"email"=>"'". proteger_cadena($email). "'", 
					"pais"=>"'". proteger_cadena($_POST["perfil_pais"]). "'", 
					"estado"=>"'". proteger_cadena($_POST["perfil_estado"]). "'", 
					"ciudad"=>"'". proteger_cadena($_POST["perfil_ciudad"]). "'",
					"calle"=>"'". proteger_cadena($_POST["perfil_calle"]). "'", 
					"colonia"=>"'". proteger_cadena($_POST["perfil_colonia"]). "'",
					"empresa"=>"'". proteger_cadena($_POST["perfil_empresa"]). "'"
					); 
				actualizar_bdd( "LISTA_CORREOS", $trama_lista ); # actualizamos
				
				# redes sociales
				$arr= array(
						"message"=>"", 
						"name"=>"". desproteger_cadena(consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "NOMBRE" )). " ha actualizado la informaci". acento("o"). "n de su perfil en Turundus.", 
						"description"=>"T". acento("u"). " as". acento("i"). " como ". desproteger_cadena(consultar_datos_general( "USUARIOS", "ID='". proteger_cadena($_SESSION["log_id"]). "'", "NOMBRE" )). " conectate a Turundus, el sistema de marketing en linea mas popular y funcional en el mundo... Vende, anuncia y gana !", 
						"caption"=>"". HTTP_SERVER. "", 
						"picture"=>"". HTTP_SERVER. "flyer/isologo.jpg", 
						"link"=>"". HTTP_SERVER. ""  
						);
				facebook_pgdata( 'feed', 'post', $_SESSION["log_id"], $arr );
				unset($arr); 
				}
			unset($trama, $nacimiento, $trama_lista);
			}
		}
	}
?>