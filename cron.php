#!/usr/bin/php
<?php
/*
:: CronJob para Onefloor

Coder 	M.S.I. Angel Cantu Jauregui
Date 	07/11/2017, 10:00:00 +GMT
Web 	https://www.sie-group.net
Mail 	angel.cantu@sie-group.net

Este script es para colocarle como parte de los trabajos en ejecucion de cron, se recomienda seguir instrucciones de la tarea a realizar y establecer tiempos de ejecucion a como se recomienda a continuacion:

# generar reporte estadistica mensual sitio
Objetivo: cada dia 1 del mes generara estadistica, generar PDF y enviara al correo del admin

# generar reporte estadistica mensual paginas
Objetivo: cada dia 1 del mes generara estadistica, generar PDF y enviara al correo del admin

# envia por mail reporte de una metrica 
Objetivo: enviar por correo reporte de la metrica deseada

# enviar felicitacion cumplea~os
Objetivo: cada dia a las 6am buscara quienes cumplen a~os

# usuarios inactivos, invitarlos a regresar
Objetivo: cada primer Lunes del Mes, aquellos con ausencia mayor a 30 dias, le enviara invitacion a regresar al sitio

# enviar NOTICIA nueva por correo a usuarios registrados
Objetivo: cada 5 minutos buscara nueva noticia

*/

# cron regla general
# * 6 1 * * root /usr/bin/php /var/www/sitio.com/web/cron.php user pass msite


# debes ubicar en la raiz del sitio dentro de "public_html" o de "web"
define( CRON_USER, "siegroup" ); # Importante: configura tu usuario
define( CRON_PASS, "A1s4r1l2n" ); # Importante: configura tu password
define( HTTP_SERVER, "https://www.sie-group.net/");
define( SLOGAN, "www.sie-group.net" );	# slogan que se imprime en el NAV de la plantilla del correo
define( CWD, '/var/www/sie-group.net/web' ); # directorio actual
define( PATHR, CWD.'/reportes' ); # directorio donde guardaremos los reportes
define( HTML_REPORTE, CWD.'/reporte_tmp.html' ); # plantilla HTML del reporte
define( SENDINBLUE, 1 ); # 1= activado, los mails saldran por este servicio
define( SENDINBLUE_USER, "noreplay@crver.net");
define( SENDINBLUE_PASS, "A1s4r1l2n#");
define( SENDINBLUE_SMTP, "smtp.crver.net" );
define( SENDINBLUE_PORT, 587 );
define( FROM_TITLE, 'SIEGroup' );
define( FROM_MAIL, 'noreply@sie-group.net' );
define( LOGO_PNG, CWD.'/logo.png' );
define( LOGO_JPG, CWD.'/logo.jpg' );
include( CWD."/admin/config.php" );
include( CWD."/modulos/base.php" );
include( CWD."/admin/desktop_functions.php" );
include( CWD. "/admin/phpmailer/PHPMailerAutoload.php" );

# autentificacion por linea de comandos (argc, argv)
function autentificacion_cmd( $argc, $argv )
	{
	if( $argc<3 )
		echo "\nError CMD-Login [". $argc. "] [". $argv. "][". $argv[0]. "]\n\n";
	else
		{
		for($i=0; $i<count($argv); $i++ )
			{
			if( $i==1 ) # username
				$user= $argv[$i];
			else if( $i==2 ) # password
				$pass= $argv[$i];
			else if( $i==3 ) # deamon command 
				$get= $argv[$i];
			}
		if( !strcmp($user, CRON_USER) && !strcmp($pass, CRON_PASS) )
			{
			if( $get )
				return $get;
			else		return 1;
			}
		}
	return 0;
	}

if( !($get=autentificacion_cmd( $argc, $argv )) )
	echo "\n\nError CMD-Login\nUso: php cron.php USUARIO CLAVE opcion\n";
else # accedio a tipear usuario y clave 
	cron_main( $get ); # inciamos sistema cron 

function cron_main($deamon)
	{
	if( !strcmp($deamon, "msite" ) ) # generar reporte estadistica mensual sitio
		cron_msite();
	else if( !strcmp($deamon, "mpages" ) ) # generar reporte estadistica mensual paginas
		cron_mpages();
	else if( !strcmp($deamon, "metricmail" ) ) # envia por mail reporte de una metrica 
		cron_metricmail();
	else if( !strcmp($deamon, "felicidades" ) ) # enviar felicitacion cumplea~os
		cron_felicidades();
	else if( !strcmp($deamon, "regresa" ) ) # usuarios inactivos, invitarlos a regresar
		cron_regresa();
	else if( !strcmp($deamon, "news" ) ) # enviar NOTICIA nueva por correo a usuarios registrados
		cron_news();
	}

function cron_msite()
	{
	echo "\n## Iniciando Metricas de Sitio";
	$log_inicio= strtotime(((date("m", time())=="01") ? ((date("Y", time()))-1):date("Y", time())).'-'.((date("m", time())=="01") ? '12':((date("m", time()))-1)).'-01T00:00:00'); #inicio del mes pasado
	$log_end= strtotime(date("Y-m", time()).'-01T00:00:00'); # fin del mes pasado (dia 1 del mes actual)
	$incon= consultar_rango_enorden( "LOG", "FECHA_LOGIN", $log_inicio, $log_end, "FECHA_LOGIN ASC"); # consultamos el rango
	$from= FROM_TITLE. '<'. FROM_MAIL. '>';

	if( !mysql_num_rows($incon) || mysql_num_rows($incon)<100 )
		echo "\nNo hay suficiente trafico para generar reporte..";
	else
		{
		limpiar($incon); # limpiamos consulta
		do //generamos numero aleatorio de 4 a 10 digitos
			{
			$id_track= generar_idtrack(); //obtenemos digito aleatorio
			}while( !strcmp( $id_track, consultar_datos_general( "LOG_REPORTES", "ID='". $id_track. "'", "ID" ) ) );

		$trama= array(
					"id"=>"'". $id_track. "'", 
					"nombre"=>"'". mes_esp(date( "m", $log_inicio)). " ". date("Y", $log_inicio). "'", 
					"fecha"=>time(), 
					"fecha_inicio"=>$log_inicio, 
					"fecha_fin"=>$log_end, 
					"pdf"=>"'0'", 
					"html"=>"'0'", 
					"mailsend"=>"'0'", 
					"mailsend_fecha"=>"'0'", 
					"status"=>"'0'", 
					"mensaje"=>"'0'"
					); # creamos

		# print_r($trama);

		# $to= consultar_datos_general( "USUARIOS", "ID='4dm1n'", "EMAIL" ); # correo del admin
		$to= 'darkdiabliyo@gmail.com';
		$imgpost= HTTP_SERVER. 'reportes.png';
		if( !insertar_bdd( "LOG_REPORTES", $trama ) )
			{
			$sub= 'Fallo reporte de metricas de '. mes_esp(date("m", $log_inicio)). ' '. date("Y", $log_inicio);
			echo "\nProblemas para guardar reporte..";
			$msg= 'Hola.<br><br>Fallo al realizarse el reporte de las m&eacute;tricas del mes actual.';
			enviar_correo( $to, $sub, 7, $msg, 0, $from, $imgpost, NULL );
			}
		else
			{
			$msg= 'Hola.<br><br>Listo ya esta disponible el reporte de m&eacute;tricas del sitio, esperamos esta informaci&oacute;n te sea de mucha ayuda.';
			$sub= 'Tus estadisticas de '. mes_esp(date("m", $log_inicio)). ' '. date("Y", $log_inicio). ' esta disponible';
			$modo= 7; # modo 7 es personalizado
			$file= generar_pdf( array("url", $id_track) );

			if( is_array($file) && $file["url"] && $file["html"] )
				actualizar_bdd( "LOG_REPORTES", array("id"=>"'". $id_track. "'", "pdf"=>"'". proteger_cadena($file["pdf"]). "'", "html"=>"'". proteger_cadena($file["html"]). "'") );
			else 	$msg .= '<br><br><b>IMPORTANTE</b>:<br>* Problemas para generar los formatos HTML/PDF.';

			$botones= '<a href="'. HTTP_SERVER. 'reportes/'. $id_track. '.pdf"><button style="cursor:pointer;border:none;display:inline-block;outline:0;padding:8px 16px;vertical-align:middle;overflow:hidden;text-decoration:none;color:inherit;background-color:inherit;text-align:center;cursor:pointer;white-space:nowrap;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none; color:#fff!important;background-color:#f44336!important; border-radius:8px;">Descargar Reporte en PDF</button></a>
				<a href="'. HTTP_SERVER. 'reportes/'. $id_track. '.html"><button style="cursor:pointer;border:none;display:inline-block;outline:0;padding:8px 16px;vertical-align:middle;overflow:hidden;text-decoration:none;color:inherit;background-color:inherit;text-align:center;cursor:pointer;white-space:nowrap;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none; color:#fff!important;background-color:#f44336!important; border-radius:8px; margin-left:16px!important;">Ir al Reporte en HTML</button></a>';

			if( enviar_correo( $to, $sub, $modo, $msg, $file["pdf"], $from, $imgpost, $botones ) )
				{
				echo "\nReporte: ". $id_track. " [Enviado]";
				actualizar_bdd( "LOG_REPORTES", array("id"=>"'". $id_track. "'", "mailsend"=>"'". proteger_cadena($to). "'", "mailsend_fecha"=>"'". time(). "'", "status"=>"'1'", "mensaje"=>"'". proteger_cadena($msg). "'") );
				}
			else 	echo "\nError al enviar Reporte de: ". $id_track;
			}

		unset($inbuf, $trama, $id_track, $to, $sub, $modo, $msg, $file, $imgpost);
		}
	echo "\n## Fin Metricas de Sitio";
	unset($incon);
	}

function cron_mpages()
	{
	}

function cron_metricmail($id)
	{
	echo "\n## Iniciando MetriMail";
	$cons= consultar_enorden_con( "LOG_REPORTE", "STATUS='0'". ($id ? " && ID='". proteger_cadena($id). "'":""), "FECHA_INICIO ASC" );
	$from= FROM_TITLE. '<'. FROM_MAIL. '>';
	if( !mysql_num_rows($cons) )
		echo "\nNo hay contenido a enviar..";
	else
		{
		$cont=0;
		$imgpost= HTTP_SERVER. 'reportes.png';
		while( $buf=mysql_fetch_array($cons) )
			{
			# $to= consultar_datos_general( "USUARIOS", "ID='4dm1n'", "EMAIL" ); # correo del admin
			$to= 'darkdiabliyo@gmail.com';
			$sub= 'Tu reporte de metricas de '. mes_esp(date("m", $log_inicio)). ' '. date("Y", $log_inicio). ' esta disponible';
			$modo= 7; # modo 7 es personalizado
			$msg= 'Hola.<br><br>Listo ya esta disponible el reporte de m&eacute;tricas del sitio, descargalo adjunto o bien visita tu pagina para mirarlo.';

			if( !$buf["pdf"] || !$buf["html"] ) # si falta alguno
				{
				$file= generar_pdf( array("url", $buf["ID"]) ); # genera archivos

				if( is_array($file) && $file["url"] && $file["html"] )
					actualizar_bdd( "LOG_REPORTES", array("id"=>"'". $buf["ID"]. "'", "pdf"=>"'". proteger_cadena($file["pdf"]). "'", "html"=>"'". proteger_cadena($file["html"]). "'") );
				else 	$msg .= '<br><br><b>IMPORTANTE</b>:<br>* Problemas para generar los formatos HTML/PDF.';
				}
			else 	$file=array( "pdf"=>$buf["PDF"], "html"=>$buf["HTML"] ); # seteamos existentes

			if( enviar_correo( $to, $sub, $modo, $msg, $file["pdf"], $from, $imgpost, NULL ) ) # enviamos mail
				{
				$cont++;
				echo "\nReporte: ". $buf["ID"]. " [Enviado]";
				actualizar_bdd( "LOG_REPORTES", array("id"=>"'". $buf["ID"]. "'", "mailsend"=>"'". proteger_cadena($to). "'", "mailsend_fecha"=>"'". time(). "'", "status"=>"'1'") ); # actualizamos
				}
			else 
				echo "\nError al enviar Reporte de: ". $buf["ID"];

			unset($file, $to, $sub, $modo, $msg);
			}
		echo "\n\n== Se enviaron ". $cont. " reporte". (($cont==1) ? "":"s");
		limpiar($cons);
		unset($imgpost);
		}
	echo "\n## Fin MetriMail";
	unset($cons);
	}

function cron_felicidades()
	{

	}

function cron_regresa()
	{

	}

function cron_news()
	{
	$cons= consultar_enorden_con( "BOLETIN", "STATUS='1'", "FECHA DESC");
	$user= array();

	if( mysql_num_rows($cons) )
		{
		while( $buf=mysql_fetch_array($cons) )
			$user[]= array( "nombre"=>($buf["NOMBRE"] ? $buf["NOMBRE"]:""), "email"=>$buf["EMAIL"], "active"=>$buf["STATUS"], "sms"=>$buf["TELEFONO"] );
		limpiar($cons);
		}
	unset($cons);

	if( is_array($user) && count($user) ) # si hay 
		{
		$err=0;
		$nc= consultar_enorden_con("NOTICIAS", "NOTIFY_MAIL='0'", "FECHA DESC" ); # consultar noticias aun sin notificar
		if( mysql_num_rows($nc) )
			{
			$from= FROM_TITLE. "<".FROM_MAIL. ">";
			$modo=7;
			$bn= mysql_fetch_array($nc); # consultamos

			foreach( $user as $key=>$val )
				{
				if( $val["email"] && strstr($val["email"], "@") ) # $val["active"]
					{
					echo "\nEnviando Noticia por correo a: ". $val["email"];
					$to= $val["email"];
					$sub= 'Nueva Noticia: '. desproteger_cadena_src($bn["TITULO"]); 
					$imgpost=CWD.'/'.(($bn["IMAGENES_NOMBRE"] && $bn["IMAGENES_URL"]) ? $bn["IMAGENES_URL"].$bn["IMAGENES_NOMBRE"]:'boletin.jpg');
					$msg= noticia_cortada(strip_tags(desproteger_cadena_src($bn["MENSAJE"])), 200). ((strlen($bn["MENSAJE"])>200) ? '&#8230;':'');
					$botones= '<a href="'. url_amigable($bn["ID"], $bn["TITULO"], "contenido", 0). '#'. $bn["ID"]. '"><button style="cursor:pointer;border:none;display:inline-block;outline:0;padding:8px 16px;vertical-align:middle;overflow:hidden;text-decoration:none;color:inherit;background-color:inherit;text-align:center;cursor:pointer;white-space:nowrap;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none; color:#fff!important;background-color:#f44336!important; border-radius:8px;">Continuar Leyendo..</button></a>';
					if( !enviar_correo( $to, $sub, $modo, $msg, 0, $from, $imgpost, $botones ) )
						$err++;

					echo "\nTitulo: ". $sub;
					unset($to, $sub, $imgpost, $msg, $botones);
					}
				}

			actualizar_bdd( "NOTICIAS", array("id"=>"'". $bn["ID"]. "'", "NOTIFY_MAIL"=>"'1'") ); # actualizamos que ya se envio

			if( $err ) # si hubo errores
				enviar_correo( $from, "Problemas para enviar Notificaciones..", $modo, "Hay que revisar el sistema, problemas con envio de correos <b>". $err. " errores</b>", 0, $from, CWD."/boletin.jpg", 0 );
			unset($bn);
			limpiar($nc);
			}
		unset($nc);
		}

	echo "\n\nSe enviaron ". count($user). " notificaciones..\n\n";
	}

# crea reporte PDF
function generar_pdf( $call )
	{
	$r=0;
	$path= PATHR;
	if( !file_exists($path) )	mkdir($path); # si no existe, lo creamos

	if( !is_array($call) || count($call)!=2 ) 	return $r;
	else 
		{
		$filename= $path. '/'.$call[1]. '.pdf';
		$filehtml= $path. '/'.$call[1]. '.html';
		$html= file_get_contents(HTML_REPORTE); # obtenemos el stream del HTML Plantilla
		$a= consultar_con( "LOG_REPORTES", "ID='". $call[1]. "'" );
		$buf= mysql_fetch_array($a);
		$finicio=$buf["FECHA_INICIO"];
		$fend= $buf["FECHA_FIN"];
		$botones= '';

		$html= preg_replace( '/\[EMPRESA\]/', desproteger_cadena_src(SLOGAN), $html );
		$html= preg_replace( '/\[EMPRESA_URL\]/', HTTP_SERVER, $html );
		$html= preg_replace( '/\[BLOG_URL\]/', HTTP_SERVER.'blog/#blog', $html );
		$html= preg_replace( '/\[TERMINOSDEUSO_URL\]/', HTTP_SERVER.'terminos_de_uso/#terminos_de_uso', $html );
		$html= preg_replace( '/\[PRIVACIDAD_URL\]/', HTTP_SERVER.'privacidad/#privacidad', $html );
		$html= preg_replace( '/\[PRIVACIDAD_URL\]/', HTTP_SERVER.'privacidad/#privacidad', $html );
		$html= preg_replace( '/\[TITULO\]/', 'Tus estadisticas de '. mes_esp(date("m", $buf["FECHA_INICIO"])). ' del '. date( "Y", $buf["FECHA_INICIO"]), $html );
		$html= preg_replace( '/\[MENSAJE\]/', 'Ya tenemos listas tus estadisticas del sitio web, esperamos esta informaci&oacute; te sea de mucha ayuda.', $html );
		$html= preg_replace( '/\[BOTONES\]/', $botones, $html );
		$html= preg_replace( '/\[IMG_POST\]/', HTTP_SERVER.'reportes.png', $html );
		$html= preg_replace( '/\[METRICA_MES\]/', mes_esp(date("m", $buf["FECHA_INICIO"])). ' del '. date( "Y", $buf["FECHA_INICIO"]), $html );
		$html= preg_replace( '/\[METRICA_MES_SIMPLE\]/', mes_esp(date("m", $buf["FECHA_INICIO"])). ' '. date( "Y", $buf["FECHA_INICIO"]), $html );

		# obteniendo calculo de valores
		$rcon= consultar_rango_enorden( "LOG", "FECHA_LOGIN", $finicio, $fend, "FECHA_LOGIN ASC");
		$metricas= array( 
			"contador"=>0, 
			"usuarios"=>array(
				"total"=>0,
				"total_ref"=>0, 
				"total_unico"=>0,
				"ref"=>array(), 
				"unico"=>array()
				), 
			"robot"=>array(
				"total"=>0,
				"marcas"=>array()
				), 
			"paginas"=>array(
				"total"=>0, 
				"total_indexadas"=>0, 
				"total_visitadas"=>0, 
				"total_referencias"=>0, 
				"indexadas"=>array(), 
				"visitadas"=>array(), 
				"referencias"=>array() # total=conteo totales, index= indexaciones totales, visita= conteo, ref= conteo
				), 
			"calendario"=>array()
			);
		while( $rbuf=mysql_fetch_array($rcon) )
			{
			# inicializando calendario
			if( !count($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]) ) # si no existe el dia
				{
				$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]=0;
				$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]=0;
				$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]=0;
				$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]=0;
				}

			$metricas["contador"] += 1;
			$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]= (!($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["total"]+1)); # calendario conteo del dia totales

			if( is_a_robot($rbuf["IP"]) || is_a_robot($rbuf["USERAGENT_VAR"]) ) # si es robot
				{
				$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]= (!($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["index"]+1)); # calendario conteo dia indexaciones 
				$metricas["robot"]["total"] += 1; # incrementamos robot visita
				$nbot= (is_a_robot($rbuf["IP"]) ? is_a_robot($rbuf["IP"]):is_a_robot($rbuf["USERAGENT_VAR"]));
				$nbot= substr( $nbot, strlen('Indexado por '), strlen($nbot) ); # obtenemos nombre del bot

				if( !count($metricas["robot"]["marcas"]) || !$metricas["robot"]["marcas"][$nbot] )
					$metricas["robot"]["marcas"][$nbot]= 1; # inicializamos
				else 	$metricas["robot"]["marcas"][$nbot] += 1; # incrementamos

				$metricas["paginas"]["total"] += 1; # incrementamos contador de pagina visitada
				$metricas["paginas"]["total_indexadas"] += 1; # incrementamos contador de pagina indexada
				$metricas["paginas"]["indexadas"][]= array( "robot"=>$nbot, "fecha"=>$rbuf["FECHA_LOGIN"], "ruta"=>$rbuf["REQUEST_VAR"]);
				}
			else # es acceso humano
				{
				$metricas["usuarios"]["total"] += 1; # incrementamos visitas totales
				$metricas["paginas"]["total"] +=1; # incrementamos total
				$tmp= parse_url($rbuf["REF_VAR"]);

				# no hay referencia, es visita directa
				# if( !$rbuf["REF_VAR"] )
				if( !$rbuf["REF_VAR"] || !strcmp($rbuf["REF_VAR"], "/") || strstr(substr(HTTP_SERVER, strlen('https://'), strlen(HTTP_SERVER)), $tmp["host"]) )
					{
					$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]= (!($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["ref"]+1));
					$metricas["usuarios"]["total_unico"] += 1; # incrementamos unico
					if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] )
						$metricas["usuarios"]["unico"][$rbuf["UBICACION"]]=1; # inicializamos
					else 	$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] += 1; # incrementamos

					$metricas["paginas"]["total_visitadas"] +=1; # incrementamso referencia
					$metricas["paginas"]["visitadas"][]= array( "ruta"=>($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/'), "fecha"=>$rbuf["FECHA_LOGIN"], "ubicacion"=>$rbuf["UBICACION"] );
					}
				else # SI tenemos referencia, viene de otro sitio o buscador
					{
					$metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]= ( !($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]) ? 1:($metricas["calendario"][date("d", $rbuf["FECHA_LOGIN"])]["visita"]+1));
					$tmp= parse_url( $rbuf["REF_VAR"]);
					$nref= $tmp["host"];
					$nquery= $tmp["query"];
					$npath= $tmp["path"];
					$metricas["usuarios"]["total_ref"] += 1; # incrementamos referencia
					if( !count($metricas["usuarios"]["ref"]) || !$metricas["usuarios"]["ref"][$nref] )
						$metricas["usuarios"]["ref"][$nref]=1;
					else 	$metricas["usuarios"]["ref"][$nref] += 1;

					# referencia Geografica
					if( !count($metricas["usuarios"]["unico"]) || !$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] )
						$metricas["usuarios"]["unico"][$rbuf["UBICACION"]]=1; # inicializamos
					else 	$metricas["usuarios"]["unico"][$rbuf["UBICACION"]] += 1; # incrementamos

					$metricas["paginas"]["total_referencias"] += 1;
					$metricas["paginas"]["referencias"][]= array( "ref"=>$nref, "ruta"=>($rbuf["REQUEST_VAR"] ? $rbuf["REQUEST_VAR"]:'/'), "fecha"=>$rbuf["FECHA_LOGIN"], "ubicacion"=>$rbuf["UBICACION"], "consulta"=>($nquery ? $nquery:""), "loader"=>($npath ? $npath:"") );
					}
				}
			}

		$html= preg_replace( '/\[METRICA_TOTAL\]/', number_format($metricas["contador"]), $html );
		$html= preg_replace( '/\[METRICA_USUARIOS\]/', number_format($metricas["usuarios"]["total_unico"]), $html );
		$html= preg_replace( '/\[METRICA_REFERENCIAS]/', number_format($metricas["usuarios"]["total_ref"]), $html );
		$html= preg_replace( '/\[METRICA_INDEXADOS\]/', number_format($metricas["robot"]["total"]), $html );
		$html= preg_replace( '/\[METRICA_USRREF\]/', number_format($metricas["usuarios"]["total"]), $html );

		$i=0;
		$gr_cat='';
		$gr_vi='';
		$gr_to='';
		$gr_re='';
		$gr_in='';
		foreach( $metricas["calendario"] as $key=>$val )
			{
			$gr_cat .= ($i ? ',':''). '\''. $key. '\'';
			$gr_vi .= ($i ? ',':''). $val["visita"];
			$gr_to .= ($i ? ',':''). $val["total"];
			$gr_re .= ($i ? ',':''). $val["ref"];
			$gr_in .= ($i ? ',':''). $val["index"];
			$gr_ur .= ($i ? ',':''). ($val["visita"]+$val["ref"]);
			$i++;
			}

		$html= preg_replace( '/\[MET_TR_CAT\]/', $gr_cat, $html );
		$html= preg_replace( '/\[MET_TR_VI\]/', $gr_vi, $html );
		$html= preg_replace( '/\[MET_TR_TO\]/', $gr_to, $html );
		$html= preg_replace( '/\[MET_TR_RE\]/', $gr_re, $html );
		$html= preg_replace( '/\[MET_TR_IN\]/', $gr_in, $html );
		$html= preg_replace( '/\[MET_TR_UR\]/', $gr_ur, $html );
		unset($gr_cat, $gr_vi, $gr_to, $gr_re, $gr_in, $gr_ur );

		$i=0;
		$gr_br='';
		$gr_mt='';
		foreach( $metricas["usuarios"]["ref"] as $key=>$val )
			{
			$gr_br .=  ($i ? ',':''). '\''. ($key ? $key:'unknown'). '\'';
			$gr_mt .=  ($i ? ',':''). $val;
			$i++;
			}

		$html= preg_replace( '/\[MET_RE_BR\]/', $gr_br, $html );
		$html= preg_replace( '/\[MET_RE_MT\]/', $gr_mt, $html );
		unset($gr_br, $gr_mt);

		$i=0;
		$gr_br='';
		$gr_mt='';
		foreach( $metricas["usuarios"]["unico"] as $key=>$val )
			{
			if( $val>1 )
				{
				$gr_br .=  ($i ? ',':''). '\''. ($key ? $key:'unknown'). '\'';
				$gr_mt .=  ($i ? ',':''). $val;
				$i++;
				}
			}

		$html= preg_replace( '/\[MET_GEO_BR\]/', $gr_br, $html );
		$html= preg_replace( '/\[MET_GEO_MT\]/', $gr_mt, $html );
		unset($gr_br, $gr_mt);

		$i=0;
		$aux='';
		foreach( $metricas["paginas"]["referencias"] as $key=>$val )
			{
			$aux.= '<div class="w3-bar w3-tiny'. ( !($i%2) ? ' w3-light-gray':''). '" style="word-wrap:break-word;">
			<li class="w3-bar-item" style="width:15%">'. $val["ref"]. '</li>
			<li class="w3-bar-item" style="width:20%">'. $val["ruta"]. '</li>
			<li class="w3-bar-item" style="width:10%">'. date( "d/m/Y, g:i a", $val["fecha"]). '</li>
			<li class="w3-bar-item" style="width:15%">'. $val["ubicacion"]. '</li>
			<li class="w3-bar-item w3-right-align" style="width:20%">'. $val["consulta"]. '</li>
			<li class="w3-bar-item w3-right-align" style="width:20%">'. $val["loader"]. '</li>
			</div>';
			$i++;
			}
		$html= preg_replace( '/\[METRICAS_DATA\]/', $aux, $html );
		unset($aux, $i);
		limpiar($a);
		limpiar($rcon);
		unset($a, $buf, $rcon);

		$fp= fopen( $filehtml, "w" ); # creamos archivo html
		fwrite($fp, $html );	# guardamos datos html
		fclose($fp);	# cerramos stream
		echo "\nEscribiendo Archivos:\n[PDF] ". $filename. "\n[HTML] ". $filehtml;
		system( '/usr/bin/wkhtmltopdf '. $filehtml. ' '. $filename );	# creamos PDF

		if( !strcmp($call[0], "pdf") ) # regresamos el PDF (output stream)
			return file_get_contents($filename);
		else if( !strcmp($call[0], "html") ) # regresamos el HTML (output stream)
			return file_get_contents($filehtml);
		else if( !strcmp($call[0], "url") ) # regersamos ruta donde esta el pdf y html guardado
			return array( "html"=>$filehtml, "pdf"=>$filename );
		unset($html, $filename, $filehtml, $fp);
		}

	return $r;
	}

?>
