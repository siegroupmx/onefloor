<?php
include( "modulos/modulos.php" );

# continente 
if( $_POST["continente"] )		$args= proteger_cadena($_POST["continente"]);
else if( $_POST["pais"] )		$args= proteger_cadena($_POST["pais"]);
else if( $_POST["provincia"] )		$args= proteger_cadena($_POST["provincia"]);
else if( $_POST["localidad"] )		$args= proteger_cadena($_POST["localidad"]);
else	$args= 'continente=0';

$arr= array(
				"GET", #stream
				CLIMA_API_KEY.$args, # args
				""# data post 
				);
$r= socket_iodata( CLIMA_URL, $arr, 80 );

#$patron='|\<div class\=\"ListasLeft"\>(.*?)\<\/div\>|is';
#$patron='|\<li\>(.*?)\<\/li\>|is';
#$patron='/\<a href\=\"\s*([^\s]*)\s*\"\>(.*?)\<\/a\>/m';
#$patron= '/<a href=\s*([^\s]*)\s*>/m';
#$patron= '/\<form\>(.*?)\<\/form\>/is';
# preg_match( $patron, $r[1], $buf);

#
# icono de nube
# http://css.meteored.mx/widget/css/galeria19/g8.png
#
# icono del viento
# http://css.meteored.mx/widget/css/galeria1/simbolo_viento/5.png
#

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/DTD/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="'. HTTP_SERVER. 'js/jquery.js"></script>
<style type="text/css" rel="stylesheet">
<!--
BODY{margin:auto;padding:0px;font:11px Arial, Helvetica, sans-serif;}
select{font:11px Arial, Helvetica, sans-serif;}
.boton {font:11px Arial, Helvetica, sans-serif;padding:2px 4px 2px 4px;margin-left:4px;}
#clima{float:left;margin:auto;padding:0px;margin-right:5px;width:150px;}
#clima .min{float:left;width:75px;text-align:left;color:orange;font-weight:bold;font-size:16px;margin-bottom:10px;}
#clima .min span{font-size:9px;}
#clima .max{float:left;width:75px;text-align:left;color:green;font-weight:bold;font-size:16px;margin-bottom:10px;}
#clima .max span{font-size:9px;}
#clima .lanube{float:left;width:100px;padding-bottom:10px;}
#clima .elviento{float:left;width:50px;padding-bottom:10px;}
#clima .dia{color:gray;font-size:15px;}
#clima .pronostico{padding: 5px 0px 10px 0px;}
#clima .pre{margin:auto;padding:0px;}
#clima .nav{margin:auto;padding:0px;overfloe:hidden;margin-top:5px;}
#clima .nav a{color:gray;text-decoration:none;border:solid 1px blue;padding:2px 4px 2px 4px;border:solid 1px #dcf57b;background-color:#f0fab8;margin-right:10px;}
#clima .nav a:link,a:active{color:gray;text-decoration:none;}
#clima .nav a:hover{background-color:#daf461;text-decoration:none;color:black;}
.iconoclima{background:transparent url(imagenes/clima.png) no-repeat;width:37px;height:30px;overflow:hidden;}
.iconoclima2{background:transparent url(imagenes/clima.png) no-repeat;width:20px;height:20px;overflow:hidden;}
.nube1{background-position:-1px -5px;}
.nube2{background-position:-41px -5px;}
.nube3{background-position:-79px -5px;}
.nube4{background-position:-118px -5px;}
.nube5{background-position:-1px -44px;}
.nube6{background-position:-4px -44px;}
.nube7{background-position:-79px -44px;}
.nube8{background-position:-118px -44px;}
.nube9{background-position:-1px -84px;}
.nube10{background-position:-41px -84px;}
.nube11{background-position:-79px -84px;}
.nube12{background-position:-118px -84px;}
.nube13{background-position:-1px -122px;}
.nube14{background-position:-40px -122px;}
.nube15{background-position:-79px -122px;}
.nube16{background-position:-118px -122px;}
.nube17{background-position:0px -163px;}
.nube18{background-position:-39px -163px;}
.nube19{background-position:-79px -163px;}
.nube20{background-position:-118px -163px;}

.viento1{background-position:-2px -197px;}
.viento2{background-position:-24px -197px;}
.viento3{background-position:-46px -197px;}
.viento4{background-position:-69px -197px;}
.viento5{background-position:-91px -198px;}
.viento6{background-position:-113px -197px;}
.viento7{background-position:-135px -197px;}
.viento8{background-position:-2px -220px;}
.viento9{background-position:-24px -220px;}
.viento10{background-position:-46px -220px;}
.viento11{background-position:-69px -220px;}
.viento12{background-position:-91px -220px;}
.viento13{background-position:-113px -220px;}
.viento14{background-position:-136px -220px;}
.viento15{background-position:-1px -242px;}
.viento16{background-position:-24px -242px;}
.viento17{background-position:-46px -242px;}
.viento18{background-position:-68px -242px;}
.viento19{background-position:-91px -242px;}
.viento20{background-position:-113px -242px;}
.viento21{background-position:-135px -242px;}
.viento22{background-position:-2px -265px;}
.viento23{background-position:-23px -265px;}
.viento24{background-position:-47px -265px;}
.viento25{background-position:-68px -265px;}
.viento26{background-position:-91px -265px;}
.viento27{background-position:-114px -265px;}
.viento28{background-position:-135px -265px;}
.viento29{background-position:-1px -287px;}
.viento30{background-position:-24px -287px;}
.viento31{background-position:-46px -287px;}
.viento32{background-position:-69px -287px;}
.viento33{background-position:-91px -287px;}
//-->
</style>
</head>
<body>';
$xml= simplexml_load_string($r[1]);

if( !is_object($xml) )
	echo 'Error de XML...';
else
	{
	$fl=0;
	$y=0;
	if( !$_POST["continente"] && !$_POST["pais"] && !$_POST["provincia"] && !$_POST["localidad"] )
		$y= 'continente';
	else if( $_POST["continente"] && !$_POST["pais"] && !$_POST["provincia"] && !$_POST["localidad"] )
		$y= 'pais';
	else if( $_POST["pais"] && !$_POST["provincia"] && !$_POST["localidad"] )
		$y= 'provincia';
	else if( $_POST["provincia"] && !$_POST["localidad"] )
		$y= 'localidad';
	else
		$y=1;
	
	if( $y==1 )
		{
		# print_r($xml);
		# echo $r[1];
		$clima=array();
		foreach( $xml->location->var as $key )
			{
			# echo $key->name. ' / icono '. $key->icon;
			$aux= array();
			if( is_object($key->data->forecast) )
				{
				foreach( $key->data->forecast as $val )
					{
					$aux[]= $val[value];
					# echo ' / valor '. $val[value]. ' [sec '. $val[data_sequence]. ']';
					}
				}
			$clima[]= array( "titulo"=> $key->name, "icono"=>$key->icon, 
									"valores"=>$aux  );
			unset($aux);
			}
		# echo '<h1>'. $xml->location->interesting->url. '</h1>';
		for( $i=0; $i<5; $i++ )
			{
			echo '<div id="clima" class="'. $i. '"';
			if( $i>0 )		echo 'style="display:none;"';
			echo '><div class="pre">';
			$c=0;
			foreach( $clima as $key )
				{
				if( $c==0 ) # temperatura minima (numero)
					echo '<div class="min"><span>min</span> '. grados_temp($key["valores"][$i], "en"). '</div>';
				else if( $c==1 ) # temperatura maxima (numero)
					echo '<div class="max"><span>max</span> '. grados_temp($key["valores"][$i], "en"). '</div>';
				else if( $c==2 ) # simbolo viento (flechita)
					echo '<div class="elviento"><a title="'. $key["valores"][$i]. '"><div class="iconoclima2 viento'. $key["icono"]. '"></div></a></div>'; 
				else if( $c==3 ) # variable simbolo (nuve)
					echo '<div class="lanube"><a title="'. $key["valores"][$i]. '"><div class="iconoclima nube'. $key["icono"]. '"></div></a></div>';
				else if( $c==4 ) # el dia 
					echo '<div class="dia">>> '. dias_es2en($key["valores"][$i], "en"). '</div>';
				#else if( $c==5 ) # pronostico textual
				#	echo '<div class="pronostico">'. $key["valores"][$i]. '</div>';
				# echo '['. $key["icono"]. '] |'. $c. '| La '. $key["titulo"]. ' es: '. $key["valores"][$i]. '<br>';
				$c++;
				}
			unset($c);
			echo '</div><div class="nav">';
			if( $i>0 )
				echo '<a href="#" class="'. ($i-1). '_clic"><< Back</a>';
			if( ($i+1)<5 )
				{
				if( ($i+1)<5 )
					$clic= ($i+1);
				else		$clic=0; 
				echo '<a href="#" class="'. $clic. '_clic">Next >></a>';
				}
			echo '</div></div>';
			}

			echo '<script type="text/javascript">
			$(document).ready(function(){';
			for($i=0; $i<5; $i++ )
				{
				echo '
				$(\'.'. $i. '_clic\').click( function(event) {
				';
				echo 'if( $(\'.'. $i. '\').is(\':hidden\') )
							{
							';
							for($y=0; $y<5; $y++ )
								{
								if( $y==$i )
									{
									echo '$(\'.'. $y. '\').css( "display", "block" );
									';
									}
								else
									{
									echo '$(\'.'. $y. '\').css( "display", "none" );
									';
									}
								}
							echo '
							}
						});';
					}
			echo '
			});
			</script>';
		echo '<p><a href="clima.php?new=1">Other City...</a></p>';
		}
	else
		{
		$lugar= explode( "/", get_geolocation() ); # ciudad / estado / pais
		if( !$_SESSION["new"] )
			$_SESSION["new"]='0';
		else		$_SESSION["new"]='1';
		
		if( !strcmp($_GET["new"], "1") )
			$_SESSION["new"]='1';
		else if( !strcmp($_GET["new"], "2") )
			$_SESSION["new"]='0';
		
		if( $lugar[0] && $lugar[1] && $lugar[2] && !$_SESSION["new"] )
			{
			echo '
			<div id="cont_fdd31247c3ccadfd244ec3fdd31594be">
<span id="h_fdd31247c3ccadfd244ec3fdd31594be"><a id="a_fdd31247c3ccadfd244ec3fdd31594be" href="http://www.meteored.mx/" target="_blank" style="color:#656565;font-family:1;font-size:14px;">Clima</a> Ciudad de M&eacute;xico</span><script type="text/javascript" src="http://www.meteored.mx/wid_loader/fdd31247c3ccadfd244ec3fdd31594be"></script>
</div>';
			}
		else
			{
			echo '<form action="clima.php" method="POST"><select name="'. $y. '">';
			foreach( $xml->location->data as $data )
				{
				$x= explode( "?", $data->url );
				echo '<option value="'. $x[1]. '" />'. $data->name;
				unset($x);
				}
			echo '</select>
			<br><input type="submit" value="Continue" class="boton"></form>';
			if( $_POST["continente"] || $_POST["pais"] || $_POST["provincia"] || $_POST["localidad"] )
				echo '<form action="clima.php" method="POST"><input type="submit" value="Back" class="boton"></form>';
			echo '</form>';
			}
		echo '<p><a href="clima.php?new=1">Other City...</a></p>';
		# | <a href="clima.php?new=2">Auto-Detectar</a></p>';
		}
	}
echo '</body></html>';
?>