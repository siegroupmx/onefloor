<?php
include( "modulos/modulos.php" );

$lista= google_listdivisa(); # obtenemos lista de divisas

echo '<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style typ="text/css" rel="stylesheet">
BODY{margin:auto;padding:0px;}
#divisa{font:12px Arial, Helvetica, sans-serif;}
#divisa select{font:12px Arial, Helvetica, sans-serif;width:150px;}
#divisa .accion h1{font:15px Arial, Helvetica, sans-serif;font-weight:bold;}
#divisa .boton{font-size:12px;}
</style>
</head>
<body>
<div id="divisa">';
if( $_POST["tipo_from"] && $_POST["tipo_to"] )
	{
	$divc= array( proteger_cadena($_POST["tipo_from"]), proteger_cadena($_POST["tipo_to"]) ); # creamos arreglo
	$v= google_getdivisa(1, $divc ); # consultamos API calculadora 
	
	echo '<div class="accion"><h1>'. $v[1]. ' = '. number_format($v[3], 2). ' '. $divc[1]. '</h1></div>';
	}
else
	{
	$divc= array( "USD", "MXN" ); # creamos arreglo
	$v= google_getdivisa(1, $divc ); # consultamos API calculadora 
	
	echo '<div class="accion"><h1>'. $v[1]. ' = '. number_format($v[3], 2). ' '. $divc[1]. '</h1></div>';
	}
	
echo '<form action="divisas.php" method="POST">
<select name="tipo_from" id="tipo_from">
	<option value="error" /></option>';

foreach( $lista as $key=>$val )
	echo '<option value="'. $key. '" />'. $val. ' - '. $key;
echo '</select> a 
<select name="tipo_to" id="tipo_to">
	<option value="error" /></option>';

foreach( $lista as $key=>$val )
	echo '<option value="'. $key. '" />'. $val. ' - '. $key;
echo '</select>
<br><input type="submit" value="Consult" class="boton"></form>
</div>
</body>
</html>';
?>