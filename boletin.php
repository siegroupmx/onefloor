<?php
function comprimir_web($buffer)
	{
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer); 
	}

ob_start( 'comprimir_web'); # calcular peso web y compresion

include( "modulos/modulos.php" );

echo '<!DOCTYPE html>
<html lang="es">
	<head>
	<title>Boletines Gestionador</title>
	<link rel="stylesheet" href="'. HTTP_SERVER. 'css/w3.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	<body class="w3-container">
	<h1>Base de Boletines.</h1>

	<form name="form" action="?op=upload" method="POST" enctype="multipart/form-data">
	<div class="w3-container w3-pale-yellow w3-padding">
		<div class="w3-row">
		 <div class="w3-half">
		 	Proporcione archivo para subir a la base.
		 </div>
		 <div class="w3-half">
		 	<input type="file" class="w3-input" name="file" id="file" value="Subir">
		 </div>
		</div>
		<div class="w3-row">
			<button class="w3-button w3-green w3-large" onclick="document.form.submit();">Subir</div>
		</div>
	</div>
	</form>';

if( !strcmp($_GET["op"], "disable") )
	{
	if( !$_GET["id_mail"] || !consultar_datos_general( "BOLETIN", "ID='". proteger_cadena($_GET["id_mail"]). "'", "EMAIL" ) )
		echo '<div class="w3-panel w3-pale-red w3-leftborder w3-border-red w3-small">el correo a deshabilitar no existe.</div>';
	else
		{
		if( !actualizar_bdd( "BOLETIN", array( "id"=>"'". proteger_cadena($_GET["id_mail"]). "'", "status"=>"'2'") ) )
			echo '<div class="w3-panel w3-pale-red w3-leftborder w3-border-red w3-small w3-padding">problemas para realizar consulta a la bdd..</div>';
		else
			echo '<div class="w3-panel w3-pale-green w3-leftborder w3-border-green w3-small w3-padding">Correo deshabilitado con exito..</div>';
		}
	}
else if( !strcmp($_GET["op"], "upload") )
	{
	if( !$_FILES["file"]["name"] || !$_FILES["file"]["tmp_name"] ) # si no existe
		echo '<div class="w3-panel w3-pale-red w3-leftborder w3-border-red w3-small">no se detecto archivo a subir..</div>';
	else if( strcmp( substr($_FILES["file"]["name"], -4), ".txt" ) || strcmp($_FILES["file"]["type"], "text/plain") ) # tipo de archivo incorrecto
		echo '<div class="w3-panel w3-pale-red w3-leftborder w3-border-red w3-small">tipo de archivo incorrecto..</div>';
	else
		{
		$tmpfile= 'uploads/tmp.txt';

		if( !move_uploaded_file($_FILES["file"]["tmp_name"], $tmpfile) ) # moviendo archivo
			{
			echo '<div class="w3-panel w3-pale-red w3-leftborder w3-border-red w3-small">no logramos guardar el archivo..</div>';
			print_r($_FILES);
			}
		else
			{
			$msg='';
			$cont= array( "exito"=>0, "error"=>0, "rep"=>0, "total"=>0 );
			$fp= fopen($tmpfile, "r" );
			while( ($buf=fgets($fp, 2048))!==FALSE )
				{
				$aux= substr($buf, 0, -1);
				$dbMails=array();

				if( strstr($aux, ",") ) # comilla separador
					{
					$x= explode(",", $aux);

					foreach( $x as $m )
						{
						if( strstr($m, "@") ) # mail
							{
							$auxM='';
							for( $i=0; $i<strlen($m); $i++ ) # limpiamos caracteres raros
								$auxM .= ((strcmp($m[$i], " ") && strcmp($m[$i], "\n") && strcmp($m[$i], "\t")) ? $m[$i]:'');

							$dbMails[]= strtolower($auxM); # agregamos
							unset($auxM);
							}
						}
					unset($x);
					}
				else if( strstr($aux, "@") ) # unico correo
					{
					$m= $aux;
					$auxM='';
					for( $i=0; $i<strlen($m); $i++ ) # limpiamos caracteres raros
						$auxM .= ((strcmp($m[$i], " ") && strcmp($m[$i], "\n") && strcmp($m[$i], "\t")) ? $m[$i]:'');

					$dbMails[]= strtolower($auxM); # agregamos
					unset($auxM, $m);
					}

				# si existen datos a insertar
				if( count($dbMails) )
					{
					foreach( $dbMails as $key=>$val )
						{
						if( consultar_datos_general("BOLETIN", "EMAIL='". proteger_cadena($val). "'", "ID" ) )
							$cont["rep"]++;
						else
							{
							do //generamos numero aleatorio de 4 a 10 digitos
								{
								$idtrack= generar_idtrack(); //obtenemos digito aleatorio
								}while( !strcmp( $idtrack, consultar_datos_general( "BOLETIN", "ID='". $idtrack. "'", "ID" ) ) );

							$trama= array(
								"id"=>"'". $idtrack. "'", 
								"email"=>"'". proteger_cadena($val). "'", 
								"status"=>"'1'", 
								"fecha"=>"'". time(). "'"
							);
							if( !insertar_bdd("BOLETIN", $trama ) )
								$cont["error"]++;
							else 	$cont["exito"]++;
							unset($trama, $idtrack);
							}
						}
					}
				unset($aux, $buf, $dbMails);
				}

			echo '<div class="w3-panel w3-pale-yellow w3-leftborder w3-border-yellow w3-small">
			Archivo: '. desproteger_cadena_src($_FILES["file"]["name"]). '
			<br>Registros encontrados: '. $cont["total"]. '
			<br>Registros Repetidos: '. $cont["rep"]. '
			<br>Registros Errados: '. $cont["error"]. '
			<br>Registros Agregados: '. $cont["exito"]. '
			<p>
				<a href="boletin.php" class="w3-button w3-large w3-blue">Continuar</a>
			</p>
			</div>';

			unlink($tmpfile);
			}
		}
	}

$cons= consultar_enorden_con( "BOLETIN", "STATUS='1'", "FECHA DESC" );

if( !mysql_num_rows($cons) )
	echo '<div class="w3-panel w3-pale-red w3-leftborder w3-border-red w3-small">No hay usuarios registrados aun..</div>';
else
	{
	echo '<div class="w3-panel w3-pale-green w3-leftborder w3-border-green w3-small">Existen <b>'. mysql_num_rows($cons). '</b> registros..</div>
	
	<div class="w3-row w3-black w3-padding">
		<div class="w3-third">Mail</div>
		<div class="w3-third">Estatus</div>
		<div class="w3-third">Opciones</div>
	</div>
	';

	while( $buf=mysql_fetch_array($cons) )
		{
		echo '
		<div class="w3-row w3-padding w3-small w3-hover-purple">
			<div class="w3-third">'. desproteger_cadena($buf["EMAIL"]). '</div>
			<div class="w3-third"><span class="w3-card '. ($buf["STATUS"]==1 ? 'w3-green':'w3-red'). ' w3-padding-small">'. desproteger_cadena($buf["STATUS"]==1 ? 'activo':'inactivo'). '</span></div>
			<div class="w3-third w3-right-align">
				<a href="javascript:;" title="Modificar"><i class="material-icons w3-large">create</i></a>
				<a href="'. HTTP_SERVER. 'boletin.php?op=disable&id_mail='. $buf["ID"]. '" title="Deshabilitar"><i class="material-icons w3-large">cancel</i></a>
			</div>
		</div>
		';
		}
	}

echo '</body>
</html>';

ob_end_flush(); # fin objeto
?>