<?php
include( "modulos/modulos.php" );

if( $_GET["place"] || $_GET["data"] ) # si hay lugar a identificar
	{
	if( !$_GET["data"] &&  $_GET["place"] ) # si no es latitud y longitud
		{
		$args= 'address='. urlencode($_GET["place"]). '&sensor='. GOOGLEMAPS_SENSOR;
		$datos= array( 'GET', GOOGLEMAPS_GEOCODEXML.$args 	);
		$r= socket_iodata( GOOGLEMAPS_SERVER, $datos, 80 );
		$xml= simplexml_load_string($r[1]);
		unset($r);
		if( !is_object($xml) )
			echo 'Error XML.';
		else
			{
			$latitud= $xml->result->geometry->location->lat;
			$longitud= $xml->result->geometry->location->lng;
			}
		}
	else if( $_GET["data"] && !$_GET["place"] )# entonces si hay latitude y longitud
		{
		if( strstr($_GET["data"], ",") )
			{
			$x= explode( ",", $_GET["data"]);
			$latitud= $x[0];
			$longitud= $x[1];
			}
		else
			echo 'Error data...';
		}
		
	if( $latitud && $longitud)
		{
		if( $_GET["size"] )		$dimencion= $_GET["size"]; # dimencion de la imagen
		else		$dimencion= '640x316'; # dimencion de la imagen 
		$args= 'zoom=15&size='. $dimencion. '&maptype=roadmap&markers=color:blue|label:S|'. $latitud. ','. $longitud. '&sensor='. GOOGLEMAPS_SENSOR;
		$datos= array( 'GET', GOOGLEMAPS_STATICMAP.$args );
		$r= socket_iodata( GOOGLEMAPS_SERVER, $datos, 80);

		header_imagen( "image/png" );
		echo $r[1];
		}
	else
		echo 'Error Latitud y Longitud';
	}
else
	echo 'Error Args...';
?>