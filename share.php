<?php
/*
Script para Compartir informacion en las diversas redes sociales y de bloggin

Autor M.S.I Angel Cantu Jauregui <angel.cantu@sie-group.net>
Date 01 01 2006, 08:00:00 PM +GTM
Blog http://lab.sie-group.net/
*/

include( "modulos/modulos.php" );

if( isset( $_GET["id"] ) )
	{
	$id_pub= proteger_cadena($_GET["id"]); // protegermos cadena
	$cons= consultar_con( "NOTICIAS", "ID='". $id_pub. "'" ); //consultamos valores segun el ID
	
	if( mysql_num_rows($cons) )
		{
		$buf= mysql_fetch_array($cons);
		$net= proteger_cadena($_GET["net"]); //protegemos variable
		
		if( !strcmp($net, "digg") || !strcmp($net, "delicious") || !strcmp($net, "facebook") || !strcmp($net, "google_bookmarks") || !strcmp($net, "barrapunto") || !strcmp($net, "meneame") || 
				!strcmp($net, "technorati") || !strcmp($net, "twitter") || !strcmp($net, "yahoo_bookmarks") || !strcmp($net, "identica") || !strcmp($net, "live") || !strcmp($net, "bitacoras") || 
				!strcmp($net, "buzz") )
			{
			$contenido= is_gd(url_amigable($buf["ID"], $buf["TITULO"], "contenido", 0 ), null);
			$titulo= proteger_cadena($buf["TITULO"]);
			
			if( !strcmp($net, "digg") )
				$url= 'http://digg.com/submit?phase=2&url='. $contenido. '&bodytext='. $titulo;
			else if( !strcmp($net, "delicious") )
				$url= 'http://delicious.com/post?url='. $contenido. '&title='. $titulo. '&notes='. $contenido;
			else if( !strcmp($net, "facebook") )
				$url= 'http://www.facebook.com/share.php?u='. $contenido. '&title='. $titulo. '&annotation='. $contenido;
			else if( !strcmp($net, "google_bookmarks") )
				$url= 'http://www.google.com/bookmarks/mark?op=edit&bkmk='. $contenido. '&title='. $titulo. '&annotation='. $contenido;
			else if( !strcmp($net, "barrapunto") )
				$url= 'http://barrapunto.com/submit.pl?subj='. $titulo. '&story='. $contenido;
			else if( !strcmp($net, "meneame") )
				$url= 'http://meneame.net/submit.php?url='. $contenido;
			else if( !strcmp($net, "technorati") )
				$url= 'http://technorati.com/faves?add='. $contenido;
			else if( !strcmp($net, "twitter") )
				$url= 'http://twitter.com/?status='. $titulo. ' - '. $contenido;
			else if( !strcmp($net, "yahoo_bookmarks") )
				$url= 'http://bookmarks.yahoo.com/toolbar/savebm?u='. $contenido. '&t='. $titulo. '&opener=bm&ei=UTF-8&d='. $contenido;
			else if( !strcmp($net, "identica") )
				$url= 'http://identi.ca/notice/new?status_textarea='. $contenido;
			else if( !strcmp($net, "live") )
				$url= 'https://favorites.live.com/quickadd.aspx?marklet=1&url='. $contenido. '&title='. $titulo;
			else if( !strcmp($net, "bitacoras") )
				$url= 'http://bitacoras.com/anotaciones/'. $contenido;
			else if( !strcmp($net, "buzz") )
				$url= 'http://www.google.com/reader/link?url='. $contenido. '&title='. $titulo;
				
			header( "Location: ". $url );
			
			unset($buf);
			unset($net);
			}
		else
			echo 'La red social elejida no existe.';
		}
	else
		echo "Este anuncio no esta regitrado en nuestra base de datos.";
		
	limpiar($cons);
	unset($cons);
	unset($id_pub);
	}
else
	{
	$msg='<h1>IMPORTANTE</h1><p>Debido a que nuestros clientes y la informacion que los mismos comparten 
	con nosotros son de <b>caracter privado</b>, nos reservamos el derecho de capturar su <b>Direccion IP</b> por un lapso de <i>2 dias</i> por cuestiones de seguridad.
	<p>La direccion IP que capturamos de su equipo nos servira para efectos de investigacion en caso que nuestro sistema de mailing e informacion de nuestros clientes se viesen comprometidos 
	por esta razon le aconsejamos <b>por favor</b> no intente buscar bugs o burlar nuestros sistemas.<p>Atentamente.<p><h2>SIE-Group
	<br>servicios@sie-group.net<br>http://www.sie-group.net/';
	echo $msg;
	}
?>