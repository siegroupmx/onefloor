<?php
/*
OneFloor-PHP v1.1 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/
# funcion para comprimir pagina web
function comprimir_web($buffer)
	{
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer); 
	}

ob_start( 'comprimir_web'); # calcular peso web y compresion

# date_default_timezone_set('America/Monterrey');
$offset= 3600*24;
header ('Content-type: text/html; charset=utf-8');
header( 'Expires: '. gmdate("D, d M Y H:i:s", time() + $offset). ' GMT' );
header( 'Last-Modified: '. gmdate("D, d M Y H:i:s", time()). ' GMT' );
header( 'Cache-Control: public, max-age=3600' );
header( 'Vary: Accept-Encoding' );
unset($offset);

include( "modulos/modulos.php" );

#if( BASE_USR && BASE_PASS && BASE )
#	deamon_logd();
	
# httpwwwforce();
# socialnetworks(); 
autenticacion( $_GET["log"] );

# <meta name="verify-v1" content="9RkdHyFpE6q/IBB8Z/nVs54fhHuLn67+9UhaP3sDN8U=">
# <meta http-equiv="Content-Type" content= "text/html; charset=UTF-8">
# <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

echo '<!DOCTYPE html>
<html lang="es">
	<head>
	<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Vary" content="Accept-Encoding" />
	<meta name="robots" content="index,follow" />
	<meta name="msvalidate.01" content="3D0F88A5EF206A0AFDF7A11C8A3CA8F8" />
	<script type="text/javascript" src="'. HTTP_SERVER. 'js/script.js"></script>
	<script type="text/javascript" src="'. HTTP_SERVER. 'js/jquery.js"></script>
	';
	if( file_exists( INCLUDES_TEMA_URL. "/js/script.js" ) ) # si existe script personal del Template 
		echo '<script type="text/javascript" src="'. TEMA_URL. '/js/script.js"></script>';

if( $_GET["hoja"] && !is_array($_GET["hoja"]) ) # si no es arreglo y existe
	{
	if( strstr($_GET["hoja"], "-") )
		{
		$abcHoja= explode("-", $_GET["hoja"]);
		$xHoja= $abcHoja[0];
		}
	else 
		$xHoja= $_GET["hoja"];

	$cmeta= consultar_con( "NOTICIAS", "ID='". proteger_cadena($xHoja). "'" );
	$bmeta= mysql_fetch_array($cmeta);
	$img= HTTP_SERVER.$bmeta["IMAGENES_URL"].$bmeta["IMAGENES_NOMBRE"];
	$canonical= url_amigable($bmeta["ID"], $bmeta["TITULO"], "contenido", 0);

	echo '<title>'. desproteger_cadena_src($bmeta["TITULO"]). '</title>
	<link rel="canonical" href="'. $canonical. '" />
	<meta name="description" content="'. noticia_cortada( strip_tags(desproteger_cadena($bmeta["MENSAJE"])), 150). '">
	<meta name="keywords" content="'. desproteger_cadena(url_cleaner_meta($bmeta["TITULO"])). '">
	<meta property="og:title" content="'. desproteger_cadena($bmeta["TITULO"]). '"/>
	<meta property="og:description" content="'. noticia_cortada( strip_tags(desproteger_cadena($bmeta["MENSAJE"])), 150). '">
	<meta property="og:url" content="'. desproteger_cadena(url_amigable($bmeta["ID"], $bmeta["TITULO"], "contenido", 0)). '"/>
	<meta property="og:type" content="article"/>
	<meta property="og:language" content="ES"/>
	<meta property="og:author" content="'. consultar_datos_general( "USUARIOS", "ID='". $bmeta["AUTOR"]. "'", "NOMBRE" ). '"/>
	<meta itemprop="name" content="'. desproteger_cadena($bmeta["TITULO"]). '">
	<meta itemprop="description" content="'. noticia_cortada( strip_tags(desproteger_cadena($bmeta["MENSAJE"])), 150). '">
	';
	if( $bmeta["IMAGENES_URL"] && $bmeta["IMAGENES_NOMBRE"] ) # si son imagenes subidas
		{
		echo '
		<link rel="image_src" href="'. $img. '">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@moneyboxlatam">
		<meta name="twitter:title" content="'. desproteger_cadena_src($bmeta["TITULO"]). '">
		<meta name="twitter:description" content="'. noticia_cortada( strip_tags(desproteger_cadena_src($bmeta["MENSAJE"])), 150). '">
		<meta name="twitter:image" content="'. $img. '">	
		<meta property="og:image" content="'. $img. '">
		<meta itemprop="image" content="'. $img. '">
		';
		# <meta name="twitter:creator" content="@SarahMaslinNir">
		}
	else if( $bmeta["FACEBOOK"] )
		{
		echo '
		<link rel="image_src" href="'. $bmeta["IMAGENES_URL"]. '">
		<meta property="og:image" content="'. $bmeta["IMAGENES_URL"]. '">
		<meta itemprop="image" content="'. $bmeta["IMAGENES_URL"]. '">
		';
		}

	unset($bmeta);
	limpiar($cmeta);
	}
else if( $_GET["ver"] && $_GET["sec"] )
	{
	$t= desproteger_cadena(str_replace( "_", " ", $_GET["ver"] ));
	$t2= desproteger_cadena(str_replace( "_", " ", $_GET["sec"] ));
	$canonical= (url_amigable( "x", $t, "menu", $t2 ).($_GET["page"] ? 'page/'. desproteger_cadena($_GET["page"]):''));
	echo '<title>'. $t.' - '. $t2. '</title>
	<link rel="canonical" href="'. $canonical. '" />
	<meta name="description" content="'. META_DESCRIPCION. '">
	<meta name="keywords" content="'. META_CLAVES. '">
	';
	unset($t);
	}
else if( $_GET["ver"] && isset($_GET["ver"]) )
	{
	$t= desproteger_cadena(str_replace( "_", " ", $_GET["ver"] ));
	$canonical= (url_amigable( "x", $t, "menu", 0 ).($_GET["page"] ? 'page/'. desproteger_cadena($_GET["page"]):''));
	echo '<title>'. $t. '</title>
	<link rel="canonical" href="'. $canonical. '" />
	<meta name="description" content="'. META_DESCRIPCION. '">
	<meta name="keywords" content="'. META_CLAVES. '">
	';
	unset($t);
	}
else
	{
	$canonical= HTTP_SERVER;
	echo '<title>'. TITULO_WEB. '</title>
	<link rel="canonical" href="'. $canonical. '" />
	<meta name="description" content="'. META_DESCRIPCION. '">
	<meta name="keywords" content="'. META_CLAVES. '">
	';
	}

echo '
	<meta http-equiv="Content-Language" content="ES">
	<meta name="classification" content="'. desproteger_cadena(META_CLASIFICACION). '">
	<link rel="stylesheet" href="'. HTTP_SERVER. 'css/w3.css">
	<link rel="stylesheet" href="'. TEMA_URL. '/css/estilo.css">
	<link rel="alternate" type="application/rss+xml" title="Feed-RSS" href="'. HTTP_SERVER. 'rss.php">
	';
?>
	
	<?php	
	if( !BASE_USR && !BASE_PASS && !BASE )
		include( "install.php" );
		
	else
		{
		include( INCLUDES_TEMA_URL. "/cuerpo/logos.php" );
		include( INCLUDES_TEMA_URL. "/cuerpo/intermedio.php" );
		include( INCLUDES_TEMA_URL. "/cuerpo/firma.php" );
		}

# chat support
# if( CHAT_SUPPORT )		chat_init();


echo '
</html>
';

ob_end_flush(); # fin objeto
?>
