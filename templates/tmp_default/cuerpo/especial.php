<?php

function ver_noticia_tema( $id, $print_full )
	{
	if( $print_full=="0" )
		{
		$a= consultar_con( "SECCIONES", "ID='". proteger_cadena($id). "'"); //consultamos secciones
		$a_tmp= mysql_fetch_array($a);
		
		$minimo=20; //maximo 6 noticias por hoja
		$page= paginacion_enorden_con( proteger_cadena($_GET["page"]), "NOTICIAS", "SECCION='". $a_tmp["ID"]. "':MENU='". $a_tmp["RELACION"]. "'", "FECHA DESC", $minimo );
		//consultamos noticias ordenadas por ID, en orden DESCendiente y siguiendo un LIMITE por hoja
		$b= consultar_limite_enorden_con( "NOTICIAS", "SECCION='". $a_tmp["ID"]. "':MENU='". $a_tmp["RELACION"]. "'", (($page*$minimo)-$minimo). ",". $minimo, "FECHA DESC" );

		# si es busqueda en catalogo, cabiamos la cosnulta
		if( !is_array($_GET["ver"]) && !is_array($_POST["q"]) && $_POST["q"] )
			{
			# echo '<h1>Aqui</h1>';
			$b= consultar_indexados_enorden( "NOTICIAS", "MENU != '". consultar_datos_general("MENUS", "URL_NOMBRE='inicio'", "ID"). "' && MENU != '". consultar_datos_general("MENUS", "URL_NOMBRE='firma'", "ID"). "' && TITULO", proteger_cadena(strtolower($_POST["q"])), "TITULO ASC" );
			$b_tmp= mysql_fetch_array($b);
			mysql_data_seek($b, 0); # reiniciar puntero

			$a= consultar_con( "SECCIONES", "ID='". $b_tmp["SECCION"]. "':RELACION='". $b_tmp["MENU"]. "'"); //consultamos secciones
			$a_tmp= mysql_fetch_array($a);
			# echo '<h1>Resultado: '. mysql_num_rows($q). '</h1>';
			}
		/*
		else if( !strcmp($_GET["ver"], "blog") )		# ver el el blog
			{
			$a= consultar_con( "SECCIONES", "RELACION='". proteger_cadena($id). "'"); //consultamos secciones
			$a_tmp= mysql_fetch_array($a);
		
			$page= paginacion_enorden_con( proteger_cadena($_GET["page"]), "NOTICIAS", "SECCION='". $a_tmp["ID"]. "':MENU='". $a_tmp["RELACION"]. "'", "FECHA DESC", $minimo );
			//consultamos noticias ordenadas por ID, en orden DESCendiente y siguiendo un LIMITE por hoja
			$b= consultar_limite_enorden_con( "NOTICIAS", "SECCION='". $a_tmp["ID"]. "':MENU='". $a_tmp["RELACION"]. "'", (($page*$minimo)-$minimo). ",". $minimo, "FECHA DESC" );
			}
		*/
		}
	else
		{
		$b= consultar_con( "NOTICIAS", "ID='". $print_full. "'" ); //consultamos noticia
		$b_tmp= mysql_fetch_array($b);
		
		$a= consultar_con( "SECCIONES", "ID='". $b_tmp["SECCION"]. "':RELACION='". $b_tmp["MENU"]. "'"); //consultamos secciones
		$a_tmp= mysql_fetch_array($a);
		}
	
	if( mysql_num_rows($b)==0 )
		echo '<p><strong><h3>Seccion/Noticia Vacia ['. $id. ']</h3></strong>';
	else
		{
		if( !strcmp($a_tmp["TIPO"], "noticia" ) )
			ver_noticiapost_tema($b, $print_full );
		else if( !strcmp($a_tmp["TIPO"], "noticia_limpia" ) )
			ver_noticialimpia_tema($b, $print_full );
		else if( !strcmp($a_tmp["TIPO"], "galeria" ) )
			ver_noticiagaleria_tema($b, $print_full );
		else if( !strcmp($a_tmp["TIPO"], "descriptiva" ) )
			ver_noticiadescriptiva_tema($b, $print_full );
		else if( !strcmp($a_tmp["TIPO"], "scriptin" ) )
			ver_noticiascriptin_tema($b, $print_full );
			
		if( $print_full=="0" && strcmp($a_tmp["TIPO"], "scriptin" ) && strcmp($a_tmp["TIPO"], "noticia_limpia" ) && !$_POST["q"] )
			{
			$minimo= 20;
			if( isset($_GET["page"]) )
				$page= proteger_cadena($_GET["page"]); //obtenemos pagina
			else
				$page=1;

			if( $_GET["ver"] && $_GET["sec"] ) # seccion y menu
				$link= url_amigable( '?ver='. proteger_cadena($_GET["ver"]). '&sec='. proteger_cadena($_GET["sec"]), proteger_cadena($_GET["ver"]), "menu", proteger_cadena($_GET["sec"]). ",page" );
			else if( $_GET["ver"] ) # menu solamente
				$link= url_amigable( '?ver='. proteger_cadena($_GET["ver"]), proteger_cadena($_GET["ver"]), "menu", "page" );
			else $link=0;
			
			selector_paginacion_enorden_con( "NOTICIAS", "SECCION='". $a_tmp["ID"]. "':MENU='". $a_tmp["RELACION"]. "'", "FECHA DESC", $minimo, $page, $link, 0, 0, "#".($_GET["sec"] ? $_GET["sec"]:($_GET["ver"] ? $_GET["ver"]:'')) );
			}
		limpiar($b);
		limpiar($a);
		}
	unset($a_tmp);
	}

//Muestra noticia POST para TEMA
function ver_noticiapost_tema( $q, $id_tmp )
	{
	if( $id_tmp=="0" )
		{
		$i=0;
		while( $tmp=mysql_fetch_array($q) )
			{
			# fecha 
			if(  date("y", $tmp["FECHA"])<date("y", time()) ) # del a~o pasado
				$fecha= date( "d/m/y, g:i a", $tmp["FECHA"] );
			else if( date("m", $tmp["FECHA"])<date("m", time()) ) # del mes pasado
				$fecha= dias_en2es(date("F", $tmp["FECHA"])). ' '. date("d", $tmp["FECHA"]);
			else if( date("d", $tmp["FECHA"])<date("d", time()) )
				{
				$cd= date("d", time())-date("d", $tmp["FECHA"]);
				if( $cd==1 )		$fecha= 'Ayer';
				else if( $cd==2 )		$fecha= 'Antier';
				else		$fecha= 'Hace '. $cd. ' d'. acento("i"). 'as';
				unset($cd);
				}
			else		$fecha= 'hoy a las '. date("g:i a", $tmp["FECHA"]);
		
			$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
			$comm= mysql_fetch_array($com_con);

			$nombre= consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "NOMBRE" );
			$email= proteger_cadena( consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "EMAIL" ) );
			$pais= consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "PAIS" );

	# dimenciones oficiales del sistema
	# 600  -  medium" ); # redimencion para medium
	# 500  -  otro" ); # redimencion para medium
	# 90		- mini" ); # redimencion para medium
	# 115	- post" ); # redimencion para post
	# 200	- r1" ); # redimencion para post
	# 300	- r2" ); # redimencion para post
	# 730", "r3" ); # redimencion para post
	# 800", "r4" ); # redimencion para post
	# 900", "r5" ); # redimencion para post

/*
echo '<div class="w3-container w3-padding">
				<h1>'. desproteger_cadena_src($tmp["TITULO"]). '</h1>';
				
				$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
				$nombre= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_post.'. $ext; # extraemos nombre real
							
				if( $tmp["IMAGENES_NOMBRE"] )
					echo '<div class="imagen"><img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$nombre. '"></div>';
				echo '<p class="w3-justify">'. desproteger_cadena_src($tmp["MENSAJE"]). '</p>
	</div>';
*/

			//titulo noticia
			$jsonschema= array(
				"@context"=>"https://schema.org", 
				"@type"=>"BlogPosting", 
				"name"=>desproteger_cadena_src($tmp["TITULO"]), 
				"url"=>url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0), 
				"datePublished"=>date( "Y-m-d", $tmp["FECHA"] ). 'T'. date( "H:i:s", $tmp["FECHA"]), 
				"dateModified"=>($tmp["FECHA_MOD"] ? (date( "Y-m-d", $tmp["FECHA_MOD"] ). 'T'. date( "H:i:s", $tmp["FECHA_MOD"])):(date( "Y-m-d", $tmp["FECHA"] ). 'T'. date( "H:i:s", $tmp["FECHA"]))), 
				"headline"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 100 ), 
				"image"=>($tmp["IMAGENES_NOMBRE"] ? (HTTP_SERVER.$tmp["IMAGENES_URL"].substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'.substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 )):''), 
				"thumbnailUrl"=>($tmp["IMAGENES_NOMBRE"] ? (HTTP_SERVER.$tmp["IMAGENES_URL"].substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'.substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 )):''), 
				"description"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 ), 
				"articleBody"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 ), 
				"author"=>array( "@type"=>"Person", "name"=>$nombre), 
				"publisher"=>array( "@type"=>"Organization", "name"=>HTTP_SERVER, "logo"=>array( "@type"=>"ImageObject", "url"=>HTTP_SERVER. 'logo.jpg') ), 
				"mainEntityOfPage"=>array( "@type"=>"WebPage", "@id"=>url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0))
			);

			echo '
			<article class="w3-container'. ($i ? ' w3-padding-32':''). '">
			<script type="application/ld+json">'. json_encode($jsonschema). '</script>
			<div class="w3-half w3-padding">
			<h2 class="blogtitulo"><a href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '#'. $tmp["ID"]. '">'. desproteger_cadena_src($tmp["TITULO"]). '</a></h2>
			<div class="w3-container w3-center w3-hide-medium w3-hide-large">';
			$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
			$img= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'. $ext; # extraemos nombre real
			if( $tmp["IMAGENES_NOMBRE"] )
				echo '<a href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '#'. $tmp["ID"]. '"><img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$img. '" class="imagen"></a>';

			echo '</div>
			<div class="w3-container w3-small w3-text-gray">Por '. $nombre. ', '. $fecha. '</div>';
			echo '<p class="w3-justify">';
			echo noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 );
			if( strlen($tmp["MENSAJE"]) > 200 )
				echo '&#8230;';
			echo '</p>
			</div>

			<div class="w3-half w3-padding w3-center w3-hide-small">';
			$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
			$img= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'. $ext; # extraemos nombre real
			if( $tmp["IMAGENES_NOMBRE"] )
				echo '<a href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '#'. $tmp["ID"]. '"><img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$img. '" class="imagen"></a>';

			echo '</div>
			</article>';

/*
			echo '
				<div id="ajuste" class="socialthings">
					<div class="share">
						<div class="gplus_like">
							<div class="g-plusone" data-size="medium" data-href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '"></div>
						</div>
						<div class="twitter_like">';
						$linksrcmx= src_mx(url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0));
						if( (strlen(desproteger_cadena($tmp["TITULO"]))+strlen($linksrcmx))>145 )
							$corte= strlen(desproteger_cadena($tmp["TITULO"]))-(strlen($linksrcmx)+1);
						else		$corte=145;
						echo '<!-- data-hashtags="hashtageste" //-->
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="'. $linksrcmx. '" 
									data-text="'. noticia_cortada(desproteger_cadena($tmp["TITULO"]), $corte). '" data-lang="es">Twittear</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
						</div>
						<div class="face_like">
							<div class="fb-like" data-href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
						</div>
					</div>
					<a href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '" alt="'. desproteger_cadena($tmp["TITULO"]). '" title="'. desproteger_cadena($tmp["TITULO"]). '"><div class="boton">Leer mas...</div></a>
				</div>
			</div>';
*/
			$i++;
			unset($pais, $img, $nombre);
			}
		}
	else
		{
		deamon_logd( "page" ); # estadistica para paginas 
		$cons= consultar_con( "NOTICIAS", "ID='". $id_tmp. "'" );
		$tmp= mysql_fetch_array($cons);
		
		$fecha= date( "j", $tmp["FECHA"] ). " de ";
		$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). " del ";
		$fecha .= date( "Y", $tmp["FECHA"] ). " a las ";
		$fecha .= date( "g:i a", $tmp["FECHA"] );
		
		$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
		$comm= mysql_fetch_array($com_con);

		$nombre= consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "NOMBRE" );
		$pais= consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "PAIS" );
		
			//titulo noticia
			$jsonschema= array(
				"@context"=>"https://schema.org", 
				"@type"=>"BlogPosting", 
				"name"=>desproteger_cadena_src($tmp["TITULO"]), 
				"url"=>url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0), 
				"datePublished"=>date( "Y-m-d", $tmp["FECHA"] ). 'T'. date( "H:i:s", $tmp["FECHA"]), 
				"dateModified"=>($tmp["FECHA_MOD"] ? (date( "Y-m-d", $tmp["FECHA_MOD"] ). 'T'. date( "H:i:s", $tmp["FECHA_MOD"])):(date( "Y-m-d", $tmp["FECHA"] ). 'T'. date( "H:i:s", $tmp["FECHA"]))), 
				"headline"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 100 ), 
				"image"=>($tmp["IMAGENES_NOMBRE"] ? (HTTP_SERVER.$tmp["IMAGENES_URL"].substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '.'.substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 )):''), 
				"thumbnailUrl"=>($tmp["IMAGENES_NOMBRE"] ? (HTTP_SERVER.$tmp["IMAGENES_URL"].substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '.'.substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 )):''), 
				"description"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 ), 
				"articleBody"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 ), 
				"author"=>array( "@type"=>"Person", "name"=>$nombre), 
				"publisher"=>array( "@type"=>"Organization", "name"=>HTTP_SERVER, "logo"=>array( "@type"=>"ImageObject", "url"=>HTTP_SERVER. 'logo.jpg') ), 
				"mainEntityOfPage"=>array( "@type"=>"WebPage", "@id"=>url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0))
			);

echo '
			<article class="w3-container w3-justify" id="'. $id_tmp. '">
			<script type="application/ld+json">'. json_encode($jsonschema). '</script>
				<h1 class="w3-xxlarge">'. desproteger_cadena_src($tmp["TITULO"]). '</h1>
				<div class="w3-container w3-padding-16">
				<div class="w3-left fb-share-button" data-href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='. urlencode(url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0)). '&src=sdkpreparse">Compartir</a></div>';

						#$linksrcmx= src_mx(url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0));
						$linksrcmx= url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0);
						#if( (strlen(desproteger_cadena($tmp["TITULO"]))+strlen($linksrcmx))>145 )
						#	$corte= strlen(desproteger_cadena($tmp["TITULO"]))-(strlen($linksrcmx)+1);
						#else		$corte=145;
						echo '<!-- data-hashtags="hashtageste" //-->
						<div class="w3-left w3-margin-left">
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="'. $linksrcmx. '" 
									data-text="'. noticia_cortada(desproteger_cadena($tmp["TITULO"]), $corte). '" data-lang="es">Twittear</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
						</div>
						<div class="w3-left w3-margin-left">
							<a href="https://plus.google.com/share?url='. urlencode(url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0)). '" onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"><img src="https://www.gstatic.com/images/icons/gplus-32.png" alt="Share on Google+"/></a>
  						</div>
	  					<div class="w3-left w3-margin-left">
  							<a href="whatsapp://send?text='. desproteger_cadena_src($tmp["TITULO"]). ' - '. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '"><div class="sprite whatsapp34"></div></a>
  						</div>
						<div class="w3-left w3-margin-left">
							<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: es_ES</script>
							<script type="IN/Share" data-url="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '" data-counter="right"></script>
  						</div>
				</div>';

			$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
			$img= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '.'. $ext; # extraemos nombre real
							
			if( $tmp["IMAGENES_NOMBRE"] )
				echo '<div class="w3-center"><img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$img. '" class="imagen"></div>';

			echo '
			<div class="blogcontent">
			<div class="w3-container w3-small w3-text-gray w3-right-align w3-padding-16">Por '. $nombre. ', '. $fecha. '</div>
<div class="w3-container w3-margin-bottom w3-center">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- SIEGroup -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3976067236733552"
     data-ad-slot="8629885363"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
			'. desproteger_cadena_src($tmp["MENSAJE"]). '
			</div>
			</article>

		<div class="w3-container w3-blue w3-xlarge w3-center">
			<p>Necesita mayor informaci&oacute;n ? un asesor le contactare en <b class="w3-text-yellow">menos de 30 minutos</b> <a href="#contacto"><button class="w3-button w3-orange w3-hover-orange w3-hover-shadow w3-round-large w3-margin-left w3-text-white w3-hover-text-white bailar interes">Me interesa !</button></a></p>
		</div>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-3976067236733552"
     data-ad-slot="3258375233"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
';

			unset($pais, $img, $nombre);
			
			//comentarios
			# echo '<div id="content">';
			# ver_comentarios_tema( $comm["COMENTARIOS"], $tmp["ID"] );
			# echo '</div>';		
		limpiar($cons);
		unset($id_tmp);
		}
		
	unset($q, $tmp, $fecha, $comm);
	limpiar($com_con);
	}

//Muestra noticia LIMPIA/HOJA para TEMA
function ver_noticialimpia_tema( $q, $id_tmp )
	{
	if( $id_tmp!="0" )
		{
		$cons= consultar_con( "NOTICIAS", "ID='". $id_tmp. "'" );
		$tmp= mysql_fetch_array($cons);
		limpiar($cons);
		}
	else
		$tmp=mysql_fetch_array($q);
	deamon_logd( "page" ); # estadistica para paginas 
			
	//$fecha= date( "j", $tmp["FECHA"] ). " de ";
	//$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). " del ";
	//$fecha .= date( "Y", $tmp["FECHA"] ). " a las ";
	//$fecha .= date( "g:i a", $tmp["FECHA"] );
		
	//$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
	//$comm= mysql_fetch_array($com_con);
	
	//$nombre= consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "NOMBRE" );
	//$pais= consultar_datos_general( "USUARIOS", "ID='". $tmp["AUTOR"]. "'", "PAIS" );

			//titulo noticia
			$jsonschema= array(
				"@context"=>"https://schema.org", 
				"@type"=>"BlogPosting", 
				"name"=>desproteger_cadena_src($tmp["TITULO"]), 
				"url"=>url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0), 
				"datePublished"=>date( "Y-m-d", $tmp["FECHA"] ). 'T'. date( "H:i:s", $tmp["FECHA"]), 
				"dateModified"=>($tmp["FECHA_MOD"] ? (date( "Y-m-d", $tmp["FECHA_MOD"] ). 'T'. date( "H:i:s", $tmp["FECHA_MOD"])):(date( "Y-m-d", $tmp["FECHA"] ). 'T'. date( "H:i:s", $tmp["FECHA"]))), 
				"headline"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 100 ), 
				"image"=>($tmp["IMAGENES_NOMBRE"] ? (HTTP_SERVER.$tmp["IMAGENES_URL"].substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'.substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 )):''), 
				"thumbnailUrl"=>($tmp["IMAGENES_NOMBRE"] ? (HTTP_SERVER.$tmp["IMAGENES_URL"].substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r5.'.substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 )):''), 
				"description"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 ), 
				"articleBody"=>noticia_cortada(strip_tags(desproteger_cadena_src($tmp["MENSAJE"])), 200 ), 
				"author"=>array( "@type"=>"Person", "name"=>$nombre), 
				"publisher"=>array( "@type"=>"Organization", "name"=>HTTP_SERVER, "logo"=>array( "@type"=>"ImageObject", "url"=>HTTP_SERVER. 'logo.jpg') ), 
				"mainEntityOfPage"=>array( "@type"=>"WebPage", "@id"=>url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0))
			);

echo '<div class="w3-container w3-padding">
<script type="application/ld+json">'. json_encode($jsonschema). '</script>
				<h1>'. desproteger_cadena_src($tmp["TITULO"]). '</h1>';
				
				$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
				$nombre= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_post.'. $ext; # extraemos nombre real
							
				if( $tmp["IMAGENES_NOMBRE"] )
					echo '<div class="imagen"><img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$nombre. '"></div>';
				echo '<p class="w3-justify">'. desproteger_cadena_src($tmp["MENSAJE"]). '</p>
	</div>';

	//unset($pais);
	//unset($nombre);
	//limpiar($com_con);
	//unset($fecha);
	unset($tmp);
	}

//Muestra noticia GALERIA para TEMA
function ver_noticiagaleria_tema( $q, $id_tmp )
	{
	echo '<div id="contenido">';
	if( $id_tmp=="0" )
		{
		$c=0;
		#echo 'Data['. mysql_num_rows($q). ']';
		while( $tmp=mysql_fetch_array($q) )
			{
			$fecha= date( "j", $tmp["FECHA"] ). "/";
			$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). "/";
			$fecha .= date( "Y", $tmp["FECHA"] ). " ";
			$fecha .= date( "g:i a", $tmp["FECHA"] );
		
			$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
			$comm= mysql_fetch_array($com_con);

	# dimenciones oficiales del sistema
	# array( "medium"=>"600","otro"=>"500", 
	# "post"=>"115", "mini"=>"90", "r1"=>"200", 
	# "r2"=>"300", "r3"=>"730", "r4"=>"800", 
	# "r5"=>"900", "r6"=>"850", "r7"=>"880" );

			echo '<a href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '"><div id="galeria">';
			
			if( !$tmp["IMAGENES_NOMBRE"] ) # es un conjunto de imagenes
				{
				$img_random= getrandom_image($tmp["IMAGENES_URL"].$tmp["ID"], "r1");
				$ext= substr( strtolower($img_random), -3 ); # extraemos extencion
				$nombre= substr( $img_random, 0, -4 ). '.'. $ext; # extraemos nombre real
				$cont_img= getimages_number( $tmp["IMAGENES_URL"].$tmp["ID"], "_r1" );
				}
			else # es una sola imagen
				{
				$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
				$nombre= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r1.'. $ext; # extraemos nombre real
				}

			if( !$tmp["IMAGENES_NOMBRE"] )
				{
				#
				#echo '<a name="'. $tmp["ID"]. '"></a>';
				printallimage( $tmp["IMAGENES_URL"].$tmp["ID"], $tmp["ID"], $tmp["TITULO"], "r1" );
				}
			else
				echo '<img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$nombre. '">';
			
			$mensaje= $tmp["MENSAJE"];
			$arr= array(
					'/\[googlemaps\](.*?)\[\/googlemaps\]/is', 
					'/\[youtube\](.*?)\[\/youtube\]/is', 
					 );
			$b= array( '', '' );
			$mensaje= preg_replace( $arr, $b, $mensaje );
			unset($b, $arr);

			echo '<div class="in"></div>
				 <div class="text">'. desproteger_cadena($tmp["TITULO"]). '</div>
			</div></a>';
			limpiar($com_con);
			$c++;
			}
		#echo 'DATA OUT['. $c. ']'; 
		}
	else
		{
		deamon_logd( "page" ); # estadistica para paginas 
		$cons= consultar_con( "NOTICIAS", "ID='". $id_tmp. "'" );
		$tmp= mysql_fetch_array($cons);
		
		$fecha= date( "j", $tmp["FECHA"] ). " de ";
		$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). " del ";
		$fecha .= date( "Y", $tmp["FECHA"] ). " a las ";
		$fecha .= date( "g:i a", $tmp["FECHA"] );
		
		$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
		$comm= mysql_fetch_array($com_con);

		echo '<h1>'. desproteger_cadena($tmp["TITULO"]). '</h1>';

			if( !$tmp["IMAGENES_NOMBRE"] ) # es un conjunto de imagenes
				{
				$img_random= getrandom_image($tmp["IMAGENES_URL"].$tmp["ID"], "r7");
				$ext= substr( strtolower($img_random), -3 ); # extraemos extencion
				$nombre= substr( $img_random, 0, -4 ). '.'. $ext; # extraemos nombre real
				$cont_img= getimages_number( $tmp["IMAGENES_URL"].$tmp["ID"], "_r7" );
				}
			else # es una sola imagen
				{
				$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
				$nombre= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_r4.'. $ext; # extraemos nombre real
				}

	# dimenciones oficiales del sistema
	# 600  -  medium" ); # redimencion para medium
	# 500  -  otro" ); # redimencion para medium
	# 90		- mini" ); # redimencion para medium
	# 115	- post" ); # redimencion para post
	# 200	- r1" ); # redimencion para post
	# 300	- r2" ); # redimencion para post
	# 730", "r3" ); # redimencion para post
	# 800", "r4" ); # redimencion para post
	# 900", "r5" ); # redimencion para post

				if( !$tmp["IMAGENES_NOMBRE"] )
					{
					$mensaje= $tmp["MENSAJE"];
					$googlemap= $tmp["MENSAJE"];
					$video= $tmp["MENSAJE"];
					$arr= array(
							'/\[googlemaps\](.*?)\[\/googlemaps\]/is', 
							'/\[youtube\](.*?)\[\/youtube\]/is', 
							'/\[precio\](.*?)\[\/precio\]/is', 
					 		);
					$mensaje= preg_replace( $arr, '', $mensaje );
					preg_match( '/\[googlemaps\](.*?)\[\/googlemaps\]/', $tmp["MENSAJE"], $googlemap ); # obtenemos googlemap
					preg_match( '/\[youtube\](.*?)\[\/youtube\]/', $tmp["MENSAJE"], $video ); # obtenemos video 
					unset($arr);
					preg_match( '/\[precio\](.*?)\[\/precio\]/', $tmp["MENSAJE"], $precio2 );
					
					echo '<a name="'. $tmp["ID"]. '"></a>';
					echo '<div id="fastgalery">
								<div class="space">
									<div class="data">';
										#<div id="arrow_izq" class="arrow_'. $tmp["ID"]. '_0"></div>
										#<div id="arrow_der" class="arrow_'. $tmp["ID"]. '_1"></div>
							echo '</div>
									<div class="textarea">
										<div class="precio2">'. $precio2[1]. '</div>
									</div>
								</div>
							</div>

<div id="lasgalerias">';
					printallimage( $tmp["IMAGENES_URL"].$tmp["ID"], $tmp["ID"], $tmp["TITULO"], "r7" );
echo '</div>';
					if( $cont_img>1 ) # si es mayor a 1, ponermos selector de imagenes
						{
						$c=0;
						$limite=6;
						echo '
						<div id="galeria-nav2">';
						
						# poniendo flecha mini
						if( $cont_img>$limite )
							echo '<div id="miniarrow_izq" class="mini_0"><</div>';
						
						for( $i=0; $i<$cont_img; $i++ )
							{
							if( !($c%$limite) && !$i )
								echo '<div id="cleandiv" class="minithumb_'. $i. '">';
							else if( !($c%$limite) )
								echo '<div id="cleandiv" class="minithumb_'. ($i/$limite). ' invisible">';
								
							echo '<div class="in '. $tmp["ID"]. '_'. $i. '_clic"><a href="#'. $tmp["ID"]. '">'. printallimage_number( $tmp["IMAGENES_URL"].$tmp["ID"], $tmp["ID"], $tmp["TITULO"], "r7", $i, array( "html", "_mini") ). '</a></div>';
							if( $c>0 && !(($c+1)%$limite) )
								echo '</div>';
							$c++;
							}
							 
						if( $c%$limite )
							echo '</div>';
						
						# poniendo flecha mini
						if( $cont_img>$limite )
							echo '<div id="miniarrow_der" class="mini_1">></div>';
						echo '</div>';
						unset($c);
						}
						
					echo '<script type="text/javascript">
					$(document).ready(function(){
					$.fn.hiddenshow = function () {
						if( $(this).is( \':hidden\' ) )
							$(this).fadeIn( "slow" );
						else
							$(this).fadeOut( "slow" );
						}
					});
					
					$(\'#botonizq\').click(function(event)	{
						alert(\'Capa Derecha tiene el elemento: \'+ $(\'#arrow_der\').attr("class") );
					return false;
					});
					
					$(\'#miniarrow_der\').click( function(event) {';
					for( $i=0; $i<ceil($cont_img/$limite); $i++ )
						{
						if( $i>0 )
							{
							echo '
							else ';
							}
					echo '
					if( $(\'#miniarrow_der\').hasClass("mini_'. $i. '") )
						{
						$(\'#miniarrow_der\').removeClass( $(\'#miniarrow_der\').attr("class") );
						$(\'#miniarrow_izq\').removeClass( $(\'#miniarrow_izq\').attr("class") );
						';
						if( ($i+1) == ceil($cont_img/$limite) ) # ultimo
							{	
							echo '
							$(\'#miniarrow_der\').addClass( "mini_'. $i. '");
							$(\'#miniarrow_izq\').addClass( "mini_'. ($i-1). '");';
							}
						else
							{
							echo '
							$(\'#miniarrow_der\').addClass( "mini_'. ($i+1). '");
							$(\'#miniarrow_izq\').addClass( "mini_'. $i. '");';
							}
						for( $y=0; $y<ceil($cont_img/$limite); $y++ )
							{
							if( $i==$y )
								{
								echo '
								$(\'.minithumb_'. $y. '\').css( "display", "block");';
								}
							else
								{
								echo '
								$(\'.minithumb_'. $y. '\').css( "display", "none");';
								}
							}
						echo '
						}';
						}
					echo '
					});

					$(\'#miniarrow_izq\').click( function(event) {';
					for( $i=0; $i<ceil($cont_img/$limite); $i++ )
						{
						if( $i>0 )
							{
							echo '
							else ';
							}
					echo '
					if( $(\'#miniarrow_izq\').hasClass("mini_'. $i. '") )
						{
						$(\'#miniarrow_der\').removeClass( $(\'#miniarrow_der\').attr("class") );
						$(\'#miniarrow_izq\').removeClass( $(\'#miniarrow_izq\').attr("class") );
						';
						if( $i==0 ) # si es cero, se queda igual 
							{
							echo '
							$(\'#miniarrow_izq\').addClass( "mini_'. $i. '" );
							$(\'#miniarrow_der\').addClass( "mini_'. ($i+1). '" );';
							}
						else # entonces cambiamos 
							{
							echo '
							$(\'#miniarrow_izq\').addClass( "mini_'. ($i-1). '" );
							$(\'#miniarrow_der\').addClass( "mini_'. $i. '" );';
							}
						for( $y=0; $y<ceil($cont_img/$limite); $y++ )
							{
							if( $i==$y )
								{
								echo '
								$(\'.minithumb_'. $y. '\').css( "display", "block");';
								}
							else
								{
								echo '
								$(\'.minithumb_'. $y. '\').css( "display", "none");';
								}
							}
						echo '
						}';
						}
					echo '
					});
					';
					
					for($i=0; $i<$cont_img; $i++ )
						{
						echo '
						$(\'.'. $tmp["ID"]. '_'. $i. '_clic\').hover( function(event) {
						';
						# thumbnails mini images
						echo 'if( $(\'.'. $tmp["ID"]. '_'. $i. '\').is(\':hidden\') )
									{
									';
									for($y=0; $y<$cont_img; $y++ )
										{
										if( $y==$i )
											{
											echo '$(\'.'. $tmp["ID"]. '_'. $y. '\').css( "display", "block" );
									';
											}
										else
											{
											echo '$(\'.'. $tmp["ID"]. '_'. $y. '\').css( "display", "none" );
									';
											}
										}
									echo '}
								});';
							}
						
						# arrows derecha
						echo '
						$(\'#arrow_der\').click( function(event) {';
							for($i=0; $i<$cont_img; $i++ )
										{
									if( !$i )
										{
									echo '
									if';
										}
									else
										{
										echo '
										else if';
										}
									echo '( $(\'#arrow_der\').hasClass("arrow_'. $tmp["ID"]. '_'. $i. '") ) 
											{
											$(\'#arrow_der\').removeClass( $(\'#arrow_izq\').attr( "class") );
											$(\'#arrow_izq\').removeClass( $(\'#arrow_izq\').attr( "class") );';
											if( ($i+1)==$cont_img )
												{ 
												echo '
											$(\'#arrow_der\').attr( "class", "arrow_'. $tmp["ID"]. '_'. $i. '" );
											$(\'#arrow_izq\').attr( "class", "arrow_'. $tmp["ID"]. '_'. ($i-1). '" );';
												}
											else
												{
												echo '
											$(\'#arrow_der\').attr( "class", "arrow_'. $tmp["ID"]. '_'. ($i+1). '" );
											$(\'#arrow_izq\').attr( "class", "arrow_'. $tmp["ID"]. '_'. $i. '" );';
												}

									for($y=0; $y<$cont_img; $y++ )
										{
										if( $y==$i )
											{
											echo '
									$(\'.'. $tmp["ID"]. '_'. $y. '\').css( "display", "block" );';
											}
										else
											{
											echo '
									$(\'.'. $tmp["ID"]. '_'. $y. '\').css( "display", "none" );';
											}
										} 
											
											echo '
											}';
										}
									echo '
								});';

						# arrow izquierda
						echo '
						$(\'#arrow_izq\').click( function(event) {';
							for($i=0; $i<$cont_img; $i++ )
										{
									if( !$i )
										{
									echo '
									if';
										}
									else
										{
										echo '
										else if';
										}
									echo '( $(\'#arrow_izq\').hasClass("arrow_'. $tmp["ID"]. '_'. $i. '") ) 
											{
											$(\'#arrow_der\').removeClass( $(\'#arrow_der\').attr( "class") );
											$(\'#arrow_izq\').removeClass( $(\'#arrow_izq\').attr( "class") );';
											if( $i==0 ) # si es cero, se queda igual 
												{
												echo '
											$(\'#arrow_izq\').addClass( "arrow_'. $tmp["ID"]. '_'. $i. '" );
											$(\'#arrow_der\').addClass( "arrow_'. $tmp["ID"]. '_'. ($i+1). '" );';
												}
											else # entonces cambiamos 
												{
												echo '
											$(\'#arrow_izq\').addClass( "arrow_'. $tmp["ID"]. '_'. ($i-1). '" );
											$(\'#arrow_der\').addClass( "arrow_'. $tmp["ID"]. '_'. $i. '" );';
												}

									for($y=0; $y<$cont_img; $y++ )
										{
										if( $y==$i )
											{
											echo '
									$(\'.'. $tmp["ID"]. '_'. $y. '\').css( "display", "block" );';
											}
										else
											{
											echo '
									$(\'.'. $tmp["ID"]. '_'. $y. '\').css( "display", "none" );';
											}
										} 
											
											echo '
											}';
										}
									echo '
								});';

					echo '
					</script>';
					}
				else
					{
					$ext= substr( strtolower($tmp["IMAGENES_NOMBRE"]), -3 ); # extraemos extencion
					$nombre= substr( $tmp["IMAGENES_NOMBRE"], 0, -4 ). '_otro.'. $ext; # extraemos nombre real
					echo '<a href="'. url_amigable($tmp["ID"], $tmp["TITULO"], "contenido", 0). '"><img src="'. HTTP_SERVER.$tmp["IMAGENES_URL"].$nombre. '"></a>';
					}
		
		echo desproteger_cadena($mensaje);
		
		if( $googlemap || $video )
			{
			echo '<div id="controles">
					<div id="tabs">
  							<ul>';
  							if( $googlemap )
    							echo '<li id="mapa"><a href="#tabs"><span>Map Location</span></a></li>';
    						if( $video )
    							echo '<li id="video"><a href="#tabs"><span>Video</span></a></li>';
  							echo '
  								<li id="contact"><a href="#tabs"><span>Contact Us</span></a></li>
  							</ul>
					</div>
				</div>
			
			<div class="mapa">'. desproteger_cadena( '[googlemaps]'. $googlemap[1]. '[/googlemaps]'). '</div>
			<div class="video invisible">'. desproteger_cadena( '[youtube]'. $video[1]. '[/youtube]'). '</div>';
			}
		echo '<div id="endpost">
		<div class="share">
		<a href="mailto:?subject=Visita '. HTTP_SERVER. '&body=Mira el catalogo/galeria de '. TITULO_WEB. ' en '. HTTP_SERVER. '">
			<div class="sprite sharemail"></div>Compartir a un Amigo
		</a>
		</div>
		
		<div class="print">
		<a href="javascript:print();">
			<div class="sprite printhis"></div>Imprimir Pagina
		</a>
		</div>
		</div>';

		unset($id_tmp);
		limpiar($cons);
		limpiar($com_con);
		}
	unset($q, $tmp, $fecha, $comm);
	echo '</div>';
	}

//Muestra noticia DESCRIPTIVA para TEMA
function ver_noticiadescriptiva_tema( $q, $id_tmp )
	{
	if( $id_tmp=="0" )
		{
		echo '<div id="content">
		<table style="border:solid 0px #c0c0c0;margin:0px;padding:0px;width:545px;" cellpadding="2">';
		echo '<th>Nombre</th><th>Ranking</th><th>Enlace</th><tr>';
		while( $tmp=mysql_fetch_array($q) )
			{
			$fecha= date( "j", $tmp["FECHA"] ). " de ";
			$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). " del ";
			$fecha .= date( "Y", $tmp["FECHA"] ). " a las ";
			$fecha .= date( "g:i a", $tmp["FECHA"] );
		
			$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
			$comm= mysql_fetch_array($com_con);
		
			//titulo
			echo '<td style="border:solid 1px #e7e7e7;">'. $tmp["TITULO"]. '</td>';
			echo '<td style="text-align:center;border:solid 1px #e7e7e7;">'. $cont= consultar_datos_general( "DOWNLOAD_RATING", "ID='". $tmp["ID"]. "'", "rating" ). '</td>';
			echo '<td style="text-align:center;border:solid 1px #e7e7e7;"><a href="descargar.php?file_id='. $tmp["ID"]. '" alt="" title="">[Descargar]</a></td><tr>';
			
			//mensaje
			//echo "<p>". desproteger_cadena($tmp["MENSAJE"]). "</p>";

			//autor del post
			//echo "<div class=\"divider\"></div>";
			//echo "<div class=\"summary\">Por ";
			//echo "<a href=\"mailto:". consultar_datos_usuario( $tmp["AUTOR"], "email" ). "\" alt=\"". consultar_datos_usuario( $tmp["AUTOR"], "email" ). "\" title=\"". consultar_datos_usuario( $tmp["AUTOR"], "email" ). "\">";
			//echo "<b>". $tmp["AUTOR"]. "</b></a> el ". $fecha;

			//leer mas
			//echo "<a href=\"index.php?hoja=". $tmp["ID"]. "\" alt=\"". $tmp["TITULO"]. "\" title=\"". $tmp["TITULO"]. "\">[>Leer mas...<]</a>";
			//echo "</div>";
			
			//ver_comentarios_tema( $comm["COMENTARIOS"], $tmp["ID"], $id_tmp );

			//echo "<div id=\"descriptiva_download\">". $tmp["ARCHIVOS_NOMBRE"]. "</div>";
			//echo "<a href=\"index.php?hoja=". $tmp["ID"]. "\" alt=\"\" title=\"\">[Descargar]</a></div>";
			//echo "<div id=\"descriptiva_download_rate\">Descargado ". rating_download($tmp["ID"]). " veces</div>";
			}
		echo '</table></div>';
		}
	else
		{
		deamon_logd( "page" ); # estadistica para paginas 
		$cons= consultar_con( "NOTICIAS", "ID='". $id_tmp. "'" );
		$tmp= mysql_fetch_array($cons);

		$fecha= date( "j", $tmp["FECHA"] ). " de ";
		$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). " del ";
		$fecha .= date( "Y", $tmp["FECHA"] ). " a las ";
		$fecha .= date( "g:i a", $tmp["FECHA"] );
		
		$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
		$comm= mysql_fetch_array($com_con);
		
		//titulo
		echo "<h2>";
		echo "<span style=\"float:right;\"><a href=\"descargar.php?file_id=". $tmp["ID"]. "\" alt=\"\" title=\"\">[Descargar]</a></span>";
		echo $tmp["TITULO"]. "</h2>";
		
		//mensaje
		echo "<p>". desproteger_cadena($tmp["MENSAJE"]). "</p>";
		
		//autor
		//echo "<div class=\"divider\"></div>";
		//echo "<div class=\"summary\">Por ";
		//echo "<a href=\"mailto:". consultar_datos_usuario( $tmp["AUTOR"], "email" ). "\" alt=\"". consultar_datos_usuario( $tmp["AUTOR"], "email" ). "\" title=\"". consultar_datos_usuario( $tmp["AUTOR"], "email" ). "\">";
		//echo "<b>". $tmp["AUTOR"]. "</b></a> el ". $fecha;
		
		//echo "<div id=\"descriptiva_download\">". $tmp["ARCHIVOS_NOMBRE"]. " ";
		//echo "<a href=\"descargar.php?file_id=". $id_tmp. "\" alt=\"\" title=\"\">[Descargar]</a></div>";
		//echo "<div id=\"descriptiva_download_rate\">Descargado ". rating_download($id_tmp). " veces</div>";
		//echo "</div>";
		
		//ver_comentarios_tema( $comm["COMENTARIOS"], $tmp["ID"], $id_tmp );
		limpiar($cons);
		}
	unset($q);
	unset($tmp);
	unset($fecha);
	unset($comm);
	limpiar($com_con);
	}




//Muestra noticia SCRIPTIN para TEMA
function ver_noticiascriptin_tema( $q, $id_tmp )
	{
	$tmp=mysql_fetch_array($q);
	$fecha= date( "j", $tmp["FECHA"] ). " de ";
	$fecha .= mes_esp(date( "m", $tmp["FECHA"] )). " del ";
	$fecha .= date( "Y", $tmp["FECHA"] ). " a las ";
	$fecha .= date( "g:i a", $tmp["FECHA"] );
		
	$com_con= consultar_con( "SECCIONES", "ID='". $tmp["SECCION"]. "':RELACION='". $tmp["MENU"]. "'" );
	$comm= mysql_fetch_array($com_con);
		
			//titulo noticia
		echo '
			<div class="post">
				<h1>'. desproteger_cadena($tmp["TITULO"]). '</h1>';
			include($tmp["ARCHIVOS_URL"].$tmp["ARCHIVOS_NOMBRE"]);
			
			echo '</div>';
	
	unset($q, $tmp, $fecha, $comm);
	limpiar($com_con);
	}

//Funcion para visualizacion de comentarios para TEMA
function ver_comentarios_tema_cont( $fl, $tmp_id )
		{
		if( $fl )
			{
			echo '<a href="#" class="comments">';
			$num_cons= consultar_con( "COMENTARIOS", "ID_NOT='". $tmp_id. "'" );

			echo mysql_num_rows($num_cons);
		
			if( mysql_num_rows($num_cons)>1 || mysql_num_rows($num_cons)==0 ) echo " Comentarios";
			else echo " Comentario";
			echo '</a>';
			}
		}

	
//Funcion para visualizacion de comentarios para TEMA
function ver_comentarios_tema( $fl, $tmp_id )
	{
	# $fl indica si la seccion del menu esta habilitada para ser comentada

	if( !strcmp($fl, "1") )
		{		
		if( strcmp($tmp_id, "0" ) && $tmp_id>"0" )
			{
			echo '<div class="post">';
			if( is_admin() || is_admingrp() )
				$coment_cons= consultar_enorden_con( "COMENTARIOS", "ID_NOT='". $tmp_id. "'", "FECHA" );
			else
				$coment_cons= consultar_enorden_con( "COMENTARIOS", "ID_NOT='". $tmp_id. "':VISIBLE='1'", "FECHA" );
			
			if( mysql_num_rows($coment_cons)==0 )
				echo "<div id=\"comentario_par\"><center>...Sin comentarios...</center></div>";
			else
				{
				$c=0;
				echo "<table cellspacing=\"10\">";
				while( $coment=mysql_fetch_array($coment_cons) )
					{
					if( $coment["FECHA"]==0 )
						{
						$fix= array(
							"ID"=>"'". $coment["ID"]. "'",
							"FECHA"=>"'". time(). "'" );
							 
						actualizar_bdd( "COMENTARIOS", $fix );
						}
					if( $c==0 )
						{
						$c=1;
						echo "<td id=\"comentario_par\">";
						}
					else
						{
						$c=0;
						echo "<td id=\"comentario_none\">";
						}
						
					$usr_cons= consultar_con( "USUARIOS", "NICK='". $coment["AUTOR"]. "'" );
					$usr_buf= mysql_fetch_array( $usr_cons );
					
					echo "<div id=\"avatar\">"; //avatar
					
					//mostrando avatar
					if( mysql_num_rows($usr_cons)>0 ) //si el usuario es miembro
						{
						if( strcmp($usr_buf["AVATAR"], "") ) //si el usuario tiene un AVATAR definido 
							echo "<img src=\"http://". $_SERVER['HTTP_HOST']. "/usuarios/". $coment["AUTOR"]. "/imagenes/". $usr_buf["AVATAR"]. "\" border=\"0\">";
						else //no tiene avatar definido
							echo "<img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/default.png\" border=\"0\">";
						}
					else
						echo "<img src=\"http://". $_SERVER['HTTP_HOST']. "/admin/imagenes/default.png\" border=\"0\">"; //usuario visitante (anonimo)
						
					echo "<div class=\"nick\">". $coment["AUTOR"]. "</div>"; //nick
					
					//mostrando controles de contacto y accesibilidad
					if( mysql_num_rows($usr_cons)>0 ) //si el usuario es miembro
						{
						echo "<div id=\"avatar_controles\">"; //controles
						if( strcmp($usr_buf["MSN"], "") ) //si tiene MSN
							echo "<a href=\"mailto:". $usr_buf["MSN"]. "\"><img src=\"". TEMA_URL. "/imagenes/msn.gif\" border=\"0\" alte=\"". $usr_buf["MSN"]. "\" title=\"". $usr_buf["MSN"]. "\"></a>";
						if( strcmp($usr_buf["EMAIL"], "") ) //si tiene EMAIL
							echo "<a href=\"mailto:". $usr_buf["EMAIL"]. "\"><img src=\"". TEMA_URL. "/imagenes/email.gif\" border=\"0\" alt=\"". $usr_buf["EMAIL"]. "\" title=\"". $usr_buf["EMAIL"]. "\"></a>";
						if( strcmp($usr_buf["URL_WEB"], "") ) //si tiene Pagina Web
							echo "<a href=\"". $usr_buf["URL_WEB"]. "\" target=\"_blank\"><img src=\"". TEMA_URL. "/imagenes/www.gif\" border=\"0\" alt=\"". $usr_buf["URL_WEB"]. "\" title=\"". $usr_buf["URL_WEB"]. "\"></a>";
						if( strcmp($usr_buf["YOUTUBE"], "") ) //si tiene Pagina Web
							echo "<a href=\"". $usr_buf["YOUTUBE"]. "\" target=\"_blank\"><img src=\"". TEMA_URL. "/imagenes/youtube_02.png\" border=\"0\" alt=\"". $usr_buf["YOUTUBE"]. "\" title=\"". $usr_buf["YOUTUBE"]. "\"></a>";
						echo "</div>"; //fin controls
						}

					echo "</div> "; //fin avatar
					//echo " <a href=\"mailto:". $coment["EMAIL"]. "\" title=\"". $coment["EMAIL"]. "\" alt=\"". $coment["EMAIL"]. "\">". $coment["AUTOR"]. "</a> dice:";

					if( is_admin() || is_admingrp())
						{
						echo "<div id=\"moderar_comentarios". $coment["ID"]. "\" style=\"font-size:11px;font-weight:bold;margin:5px 0px 5px 0px;\">";
							echo "<select style=\"font-size:11px;\" name=\"opcion\" id=\"opcion\" onchange=\"cargar_datos( 'id=comentarios&mov=moderar&id_com=". $coment["ID"]. "&opcion='+this.value, 'moderar_comentarios". $coment["ID"]. "', 'GET', '0' );\">";
								echo "<option value=\"error\"></option>";
								if( is_admin() )
									echo "<option value=\"eliminar\">Eliminar</option>";
								if( !strcmp($coment["VISIBLE"], "1") )
									echo "<option value=\"esconder\">Esconder</option>";
								else
									echo "<option value=\"mostrar\">Mostrar</option>";
							echo "</select>";
							if( $coment["VISIBLE"]=="0" )
								echo '&nbsp;&nbsp;&nbsp;<u><b>esperando moderacion !!</b></u>';
						echo "</div>";
						}
					echo desproteger_cadena_comment($coment["MENSAJE"]); //decodifica simbolos

					if( strcmp(consultar_datos_usuario( $coment["AUTOR"], "firma" ), "" ) && strcmp(consultar_datos_usuario( $coment["AUTOR"], "firma" ), "0" ) )
						echo "<br><br><hr>". desproteger_cadena(consultar_datos_usuario( $coment["AUTOR"], "firma" ));
					
					echo "<br><div id=\"fecha_coment\">"; //fecha
						echo ">> Pegado el ". date( "d/m/Y", $coment["FECHA"] ). " a las ". date( "g:i a", $coment["FECHA"]);
					echo "</div>";
						
					echo "</td><tr>";
					unset($usr_buf);
					unset($usr_cons);
					}
				echo "</table>";
				unset($c); 
				}
			
			echo '<p>
				<div id="comentario_formulario">
				<div id="estado_comentario" style="margin:10px 0px 20px 0px;">
					<h1>Publicar Comentario:</h1><br>
						<div id=\"lista_caritas">';
						lista_caritas();
						echo '<br>';
						lista_controles();
						echo '</div>
					<br>
					<b>Nombre:</b>';
					if( usuario_legitimo() )
						echo "<b>". $_SESSION["log_usr"]. "</b><br>";
					else
						echo "<i><u>Anonimo</u></i><br>";
					echo "<b>E-Mail:</b> ";
					if( usuario_legitimo() )
						echo "<b>". consultar_datos_usuario( $_SESSION["log_usr"], "email" ). "</b><br>";
					else
						echo "<input type=\"text\" name=\"comentario_email\" id=\"comentario_email\"> (si desea recivir notificacion)<br>";
					echo "<b>Mensaje:</b><br>";
					echo "<textarea name=\"comentario_mensaje\" id=\"comentario_mensaje\" class=\"textarea_style\">";
					echo "</textarea>";
				echo '<div id="captcha">';
				get_captcha(); # ponemos captcha
				echo '</div>';
				echo "<input type=\"submit\" value=\"Comentar\" onclick=\"cargar_datos( 'id=comentarios&mov=agregar&hoja=". $_GET["hoja"]. "', 'estado_comentario', 'POST', '";
				
				if( !usuario_legitimo() ) # no es legitimo, incluimos e-mail del INPUT 
					echo 'comentario_email:';
					
				echo "comentario_mensaje:recaptcha_challenge_field:recaptcha_response_field' );\">";
				echo '</div>';			
			echo '</div>
			</div>';
			
			unset($coment);
			unset($coment_cons);
			}
		unset($num_cons);
		unset($tmp_id);
		}
	}
?>