<?php
echo '
		<div class="w3-container w3-padding-48 w3-white" id="soporte">
			<h2 class="w3-center w3-padding-24">Soluciones para tu negocio con tiempos de respuesta de <span class="w3-text-orange">30 minutos</span> ante incidentes y soporte tecnico..</h2>
			<div class="w3-row-padding">
				<div class="w3-third w3-center">
					<h3 class="w3-blue w3-left-align w3-padding">En Redes Sociales</h3>
						<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=1359532274107227";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
<div class="fb-page" data-href="https://www.facebook.com/siegroupmx" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/siegroupmx" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/siegroupmx">SIEGroup</a></blockquote></div>

					<p class="w3-center">
						<div class="w3-row">
							<div class="w3-half">
								<a href="https://twitter.com/sie_group" class="twitter-follow-button" data-size="large" data-show-count="false">Follow @sie_group</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>
							<div class="w3-half">
<script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: es_ES</script>
<script type="IN/FollowCompany" data-id="1688606" data-counter="top"></script>
							</div>
						</div>
					</p>
				</div>
				<div class="w3-third">
					<h3 class="w3-purple w3-padding">Carta de Presentaci&oacute;n</h3>
					<p class="w3-container">Mira nuestra carta de presentaci&oacute;n o <a href="https://www.sie-group.net/files/Diapositiva_Completa.pdf" target="_blank"><b>descargala</b></a>, animate a conocernos:</p>
					<p>
					<iframe src="https://docs.google.com/gview?url=https://www.sie-group.net/files/Diapositiva_Completa.pdf&embedded=true" class="alcien yt"></iframe>
					</p>
				</div>
				<div class="w3-third">
					<h3 class="w3-red w3-padding">Presupuesto para su Proyecto ?</h3>
					<p class="w3-container">Mandanos un correo y nos pondremos en contacto:</p>
					<p class="w3-center w3-xxlarge bailar w3-hide-small"><img src="https://www.moneybox.com.mx/mailprotect.php?msec=Y29udGFjdG9Ac2llLWdyb3VwLm5ldA%3D%3D&w=350&h=50&sf=25&x=10&y=35&bcolor=255,255,255&tcolor=0,0,0" alt="Correo" title="Correo"></p>
					<p class="w3-center w3-xxlarge bailar w3-hide-large w3-hide-medium"><img src="https://www.moneybox.com.mx/mailprotect.php?msec=Y29udGFjdG9Ac2llLWdyb3VwLm5ldA%3D%3D&w=240&h=50&sf=16&x=10&y=35&bcolor=255,255,255&tcolor=0,0,0" alt="Correo" title="Correo"></p>
				</div>
			</div>
		</div>
';
?>
