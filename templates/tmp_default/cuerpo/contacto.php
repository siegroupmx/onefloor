<?php
echo '
		<div class="w3-container w3-light-gray w3-padding-48" id="contacto">
			<div class="w3-row">
				<div class="w3-half w3-margin-bottom w3-padding w3-rightbar w3-border-gray w3-margin-bottom" id="form_p6">
					<p class="w3-padding"><label>Nombre:</label>
					<input type="text" class="w3-input w3-border w3-white" id="nombre_p6" name="nombre_p6" placeholder="Indique su nombre.."></p>
					<p class="w3-padding"><label>E-Mail:</label>
					<input type="text" class="w3-input w3-border w3-white" id="email_p6" name="email_p6" placeholder="Indique su correo.."></p>
					<p class="w3-padding"><label>Tel&eacute;fono(s):</label>
					<input type="text" class="w3-input w3-border w3-white" id="telefono_p6" name="telefono_p6" placeholder="Indique su(s) telefonos(s).."></p>
					<p class="w3-padding"><label>Mensaje:</label>
					<textarea class="w3-input w3-border w3-white" id="mensaje_p6" name="mensaje_p6" placeholder="Cual es su requerimiento o duda ?.."></textarea></p>
					<a href="javascript:;" class="w3-button w3-right w3-red w3-large w3-hover-red w3-hover-shadow w3-round-large w3-margin-right" onclick="cargar_datos(\'own=me&send=1&form=p6&ads='. $ads. '\', \'form_p6\', \'POST\', \'nombre_p6:email_p6:telefono_p6:mensaje_p6\');">Enviar Consulta</a>
				</div>
				<div class="w3-half w3-padding">
					<h4>Contactanos para cualquier duda !</h4>
					<div class="w3-row w3-margin-bottom">
						<i class="material-icons w3-xxxlarge w3-left">&#xE8A8;</i>
						<div class="w3-cell w3-container w3-xlarge">
							(899) 436 0640 <span class="w3-blue w3-small w3-round-large w3-padding-small">Reynosa, Tamps</span>
							<br>(818) 421 9924 <span class="w3-orange w3-small w3-round-large w3-padding-small">Monterrey, NL</span>
						</div>
					</div>
					<div class="w3-row w3-margin-bottom">
						<i class="material-icons w3-xxxlarge w3-left">&#xE8B4;</i>
						<div class="w3-cell w3-container w3-large">
							20 de noviembre #300<br>C.P. 88780, Col. Fracc Reynosa<br>Reynosa, Tamaulipas, MX
						</div>
					</div>
					<div class="w3-row w3-margin-bottom">
						<i class="material-icons w3-xxxlarge w3-left">&#xE0BE;</i>
						<div class="w3-cell w3-container w3-large w3-padding">
							<img src="https://www.sie-group.net/mailprotect.php?msec=Y29udGFjdG9Ac2llLWdyb3VwLm5ldA%3D%3D&w=200&h=30&sf=14&x=2&y=20&bcolor=241,241,241&tcolor=0,0,0" alt="Correo" title="Correo">
						</div>
					</div>
					<div class="w3-row">
						<i class="material-icons w3-xxxlarge w3-left">&#xE8B5;</i>
						<div class="w3-cell w3-container w3-large w3-padding">
							Abierto de 8:00am a 6:00pm<br>De Lunes a Viernes.
						</div>
					</div>
				</div>
			</div>
		</div>
';
?>
