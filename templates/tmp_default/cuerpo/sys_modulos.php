<?php
echo '
		<div class="w3-container w3-padding-48" id="servicios">
			<h1 class="w3-panel w3-center w3-xxlarge w3-padding-32">Un equipo disponible y experto para generarte soluciones basadas en software y/o hardware dedicado en proyectar la calidad de tu empresa !</h1>
			<div class="w3-row-padding">
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite moneybox"></div>
					</div>
					<div class="w3-cell">
					<a href="https://www.moneybox.com.mx" targer="_blank">
						<h4 class="w3-large"><b>SISTEMA ERP</b></h4>
						<p class="w3-justify">Sistema ERP para Administraci'. acento("o"). 'n de su negocio, sistema modular: Factura, Cotizaciones, PO, OC, Contabilidad, Almacen, Punto de Venta, Paqueteria y mas..</p>
					</a>
					</div>
				</div>
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite turundus"></div>
					</div>
					<div class="w3-cell">
					<a href="https://www.turundus.net" targer="_blank">
						<h4 class="w3-large"><b>MARKETING</b></h4>
						<p class="w3-justify">Plataforma para administraci'. acento("o"). 'n de campa&ntilde;as publicitarias (Socialmedia, Mailing y Ad\'s), Gestor de Trafico Web y Seguimiento de conversiones.</p>
					</a>
					</div>
				</div>
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite webdesign"></div>
					</div>
					<div class="w3-cell">
						<h4 class="w3-large"><b>PAGINAS WEB</b></h4>
						<p class="w3-justify">Desarrollo de sitios web con todo incluido (hosting, dominio y correos), desde dise&ntilde;os p'. acento("u"). 'blicos hasta 100% personalizados.</p>
					</div>
				</div>
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite panalweb"></div>
					</div>
					<div class="w3-cell">
						<a href="https://www.panalweb.com" targer="_blank">
						<h4 class="w3-large"><b>SERVIDORES</b></h4>
						<p class="w3-justify">Hosting, Venta y Arrendamiento de servidores, Buzones de Correos empresariales y soporte en configuraciones. </p>
						</a>
					</div>
				</div>
			</div>
			<div class="w3-row-padding w3-margin-top">
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite redes"></div>
					</div>
					<div class="w3-cell">
						<h4 class="w3-large"><b>SOPORTE Y SERVICIO</b></h4>
						<p class="w3-justify">Implementacion de proyectos IT como: CCTV, Control Acceso, Telefonia VoIP, Red y Cableado, planes de servicio y polizas a la medida de su negocio. </p>
					</div>
				</div>
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite seguridad"></div>
					</div>
					<div class="w3-cell">
						<h4 class="w3-large"><b>SEGURIDAD INFORMATICA</b></h4>
						<p class="w3-justify">Auditor'. acento("i"). 'a de Redes, Infraestructura, C'. acento("o"). 'digo Fuente, Recuperacion de datos y Pruebas de Intrusi'. acento("o"). 'n, somos expertos en SGSI, PDCA e ISO27000.</p>
					</div>
				</div>
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite software"></div>
					</div>
					<div class="w3-cell">
						<h4 class="w3-large"><b>DESARROLLO DE SOFTWARE</b></h4>
						<p class="w3-justify">Generamos soluciones basadas en software con: HTML/CSS, PHP, C/C++ y Python. PAra ambientes de Escritorio y Web.</p>
					</div>
				</div>
				<div class="w3-col l3 m12">
					<div class="w3-padding w3-left w3-margin-right">
						<div class="sprite crazyraptor"></div>
					</div>
					<div class="w3-cell">
					<a href="https://www.crazyraptor.com" targer="_blank">
						<h4 class="w3-large"><b>TIENDA EN LINEA</b></h4>
						<p class="w3-justify">Venta de Equipo de Computo, VoIP, Redes, Cableado, Refacciones, accesorios y consumibles.</p>
					</a>
					</div>
				</div>
			</div>
		</div>
';
?>