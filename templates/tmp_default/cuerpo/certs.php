<?php
echo '
<div class="w3-container w3-padding-48" id="certs">
	<h1 class="w3-padding-32 w3-center">La innovaci&oacute;n es parte de una mejora continua, somos socios de negocio de las marcas</h1>
	<div class="w3-row">
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite hpartner"></div>
		</div>
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite dellpartner"></div>
		</div>
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite bitdefender"></div>
		</div>
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite nod32"></div>
		</div>
	</div>
	<div class="w3-row">
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite kaspersky"></div>
		</div>
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite nationalsoft"></div>
		</div>
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite sasa"></div>
		</div>
		<div class="w3-col l3 m6 w3-padding">
			<div class="sprite syscom"></div>
		</div>
	</div>
</div>
';
?>