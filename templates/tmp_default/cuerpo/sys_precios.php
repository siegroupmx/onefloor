<?php
echo '
		<div class="w3-container w3-padding-48 w3-light-gray" id="precios">
			<div class="w3-row">
				<div class="w3-container w3-xlarge w3-center w3-margin">
					Necesitas empezar a facturar ya ?, no te preocupes nosotros en <b>15 minutos</b> tendremos lista tu cuenta, solo escoge alguno de los paquetes de consumo.</div>
				</div>
				<div class="w3-quarter w3-center w3-padding w3-margin-top">
					<div class="w3-container w3-xlarge w3-orange">Paquete B&aacute;sico</div>
					<div class="w3-container w3-white w3-small"><span class="w3-jumbo">100</span> facturas</div>
					<div class="w3-container w3-white w3-small">a solo<br><span class="w3-xxlarge w3-text-red precio">696.00 MXN</span> c/u</div>
					<div class="w3-container w3-cyan w3-small">** estas facturas no tienen caducidad</div>
					<button onclick="document.getElementById(\'p1\').style.display=\'block\';" class="w3-button w3-block w3-purple w3-hover-red w3-hover-shadow">Contratar</button>
				</div>
				<div class="w3-quarter w3-center w3-padding w3-margin-top">
					<div class="w3-container w3-xlarge w3-orange">Paquete Intermedio</div>
					<div class="w3-container w3-white w3-small"><span class="w3-jumbo">500</span>facturas</div>
					<div class="w3-container w3-white w3-small">a solo<br><span class="w3-xxlarge w3-text-red precio">2,900.00 MXN</span> c/u</div>
					<div class="w3-container w3-cyan w3-small">** estas facturas no tienen caducidad</div>
					<button onclick="document.getElementById(\'p2\').style.display=\'block\';" class="w3-button w3-block w3-purple w3-hover-red w3-hover-shadow">Contratar</button>
				</div>
				<div class="w3-quarter w3-center w3-padding w3-margin-top">
					<div class="w3-container w3-xlarge w3-orange">Paquete Avanzado</div>
					<div class="w3-container w3-white w3-small"><span class="w3-jumbo">2,000</span>facturas</div>
					<div class="w3-container w3-white w3-small">a solo<br><span class="w3-xxlarge w3-text-red precio">9,280.00 MXN</span> c/u</div>
					<div class="w3-container w3-cyan w3-small">** estas facturas no tienen caducidad</div>
					<button onclick="document.getElementById(\'p3\').style.display=\'block\';" class="w3-button w3-block w3-purple w3-hover-red w3-hover-shadow">Contratar</button>
				</div>
				<div class="w3-quarter w3-center w3-padding w3-margin-top">
					<div class="w3-container w3-xlarge w3-orange">Paquete Empresarial</div>
					<div class="w3-container w3-white w3-small"><span class="w3-jumbo">5,000</span>facturas</div>
					<div class="w3-container w3-white w3-small">a solo<br><span class="w3-xxlarge w3-text-red precio">11,600.00 MXN</span> c/u</div>
					<div class="w3-container w3-cyan w3-small">** estas facturas no tienen caducidad</div>
					<button onclick="document.getElementById(\'p4\').style.display=\'block\';" class="w3-button w3-block w3-purple w3-hover-red w3-hover-shadow">Contratar</button>
				</div>
				<div class="w3-row w3-center w3-padding">
					<div class="w3-large w3-panel w3-leftbar w3-border-red w3-padding w3-pale-yellow">
						Te interesan otros m&oacute;dulos aparte de Facturaci&oacute;n, entonces haz click para contactarte !
						<button class="w3-button w3-red w3-large w3-margin-left w3-hover-shadow w3-hover-red w3-round-large bailar" onclick="document.getElementById(\'p5\').style.display=\'block\';">Otros m&oacute;dulos</button>
					</div>
				</div>
			</div>
		</div>
';
?>