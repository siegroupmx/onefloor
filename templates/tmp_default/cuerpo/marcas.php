<?php
echo '
<div class="w3-container w3-padding-48 w3-light-gray" id="marcas">
	<div class="w3-row-padding">
		<div class="w3-third w3-right-align w3-padding">
			<h1>Marcas de calidad que manejamos para ti</h1>
		</div>
		<div class="w3-rest w3-leftbar w3-border-gray w3-padding">
			<div class="w3-row">
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite hp"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite dell"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite mikrotik"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite ubiquiti"></div>
				</div>
			</div>
			<div class="w3-row">
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite tplink"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite intellinet"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite panduit"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite samsung"></div>
				</div>
			</div>
			<div class="w3-row">
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite honeywell"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite hikvision"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite centos"></div>
				</div>
				<div class="w3-col l3 m6 w3-padding">
					<div class="sprite windowsserver"></div>
				</div>
			</div>
		</div>
	</div>
</div>
';
?>