<?php
/*
					<video controls class="v0 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v0.mp4">
						Su navegador no soporte HTML5
					</video>
*/
echo '
		<div class="w3-container w3-padding-48" id="sistema">
			<div class="w3-row">
				<div class="w3-half w3-rightbar w3-border-blue w3-center w3-padding-16">
					<iframe class="v0 w3-padding alcien yt"src="https://www.youtube.com/embed/trg7pAWSkkc" frameborder="0" allowfullscreen></iframe>
					<video controls class="v1 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v1.mp4">
						Su navegador no soporte HTML5
					</video>
					<video controls class="v2 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v2.mp4">
						Su navegador no soporte HTML5
					</video>
					<video controls class="v3 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v3.mp4">
						Su navegador no soporte HTML5
					</video>
					<video controls class="v4 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v4.mp4">
						Su navegador no soporte HTML5
					</video>
					<video controls class="v5 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v5.mp4">
						Su navegador no soporte HTML5
					</video>
					<video controls class="v6 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v6.mp4">
						Su navegador no soporte HTML5
					</video>
					<video controls class="v7 w3-padding video">
						<source src="'. HTTP_SERVER. 'videos/v7.mp4">
						Su navegador no soporte HTML5
					</video>
				</div>
				<div class="w3-half w3-container w3-large">
					Dale un vistazo a nuestra plataforma ERP es tan potente, robusta y sencilla que hasta un bebe puede manejarla !
					<div class="w3-bar w3-light-grey w3-margin-top">
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s1">Factura</a>
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s2">Clientes</a>
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s3">Cotizaciones</a>
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s4">Ordenes de Compra</a>
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s5">Nomina</a>
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s6">Almac&eacute;n</a>
						<a href="javascript:;" class="w3-bar-item w3-button w3-small w3-hover-purple w3-mobile" id="s7">Comercio Web</a>
					</div>
					<div class="w3-container w3-center w3-padding-64 s0 visible">
						<h4>Dale click a alguna de las opciones...</h4>
					</div>
					<div class="w3-container w3-justify w3-small s1 invisible">
						<p>Usted podr&aacute; generar facturas personalizadas con su logotipo, en cualquier tipo de moneda y ademas con validez 
						legal en <b>M&eacute;xico</b> (cumplimos con CFDi v3.3), <b>Espa&ntilde;a</b>, <b>Per&uacute;</b> y <b>Colombia</b>.</p>
						<p>El sistema tiene soporte para los modelos de factura: Factura, Arrendamiento, Honorarios, Aduana, Comercializadora, 
						Carta Porte y Nota de Cr&eacute;dito.</p>
						<p>Como servicios adicionales usted contara con <b>Kiosko Electr&oacute;nico</b> para evitarle retrasos y permitirle a sus clientes 
						la autofacturaci&oacute;n, del mismo modo somos <b>los &uacute;nicos proveedores</b> que ofrecemos <b>enlaces acortados</b> para facilitar 
						la disponibilidad y descarga del documentos de factura.</p>
					</div>
					<div class="w3-container w3-justify w3-small s2 invisible">
						<p>Usted podr&aacute; dar de alta la informaci&oacute;n fiscal de sus clientes y disponer de estos datos para generar <b>cotizaciones</b>, <b>facturas</b> u <b>ordenes</b>, con tan solo escribir parte de la identificaci&oacute;n fiscal o nombre.</p>
						<p>Tambi&eacute;n cuenta con sistema de direcciones de embarge y datos bancarios, por si su empresa y requerimientos crecen, podr&aacute; utilizar esta informaci&oacute;n en nuestros m&oacute;dulos de <b>Paqueter&iacute;a y Embarque</b> y <b>Cobros Electr&oacute;nicos</b>.</p>
					</div>
					<div class="w3-container w3-justify w3-small s3 invisible">
						<p>Si usted tiene vendedores por telemarketing, visitando a clientes o en cualquier ocasi&oacute;n, usted podr&aacute; generar 
						cotizaciones de forma r&aacute;pida y eficiente, tambi&eacute;n podr&aacute; <b>personalizar</b> su formato de cotizaci&oacute;n 
						para brindar mayor profesionalismo.</p>
						<p>Ademas si cuenta con el modulo de <b>Almac&eacute;n</b> podr&aacute; cotizar a partir de los SKU o n&uacute;meros de 
						Identificaci&oacute;n de sus productos para agilizar el proceso de conocimiento de costos.</p>
					</div>
					<div class="w3-container w3-justify w3-small s4 invisible">
						<p>La ordenes de compra le permitir&aacute;n formalizar la adquisici&oacute;n que esta realizando su clientes, para ello se requiere 
						de una cotizaci&oacute;n previa, este mismo modulo le permite genera <b>ordenes de producci&oacute;n</b> para su &aacute;rea de 
						producci&oacute;n o empaque.</p>
						<p>Y si no tiene paqueter&iacute;a contratada, nuestro ERP le permite adquirir gu&iacute;as prepagadas para adjudicarlas a su Orden 
						de Compra y realizar sus env&iacute;os de forma r&aacute;pida y confiable sin salir de la plataforma ERP.</p>
					</div>
					<div class="w3-container w3-justify w3-small s5 invisible">
						<p>Realizar su nomina ahora sera una tarea muy sencilla, ya que nuestro ERP le permite adquirir el modulo <b>con checador</b> o <b>sin checador</b>, esto principalmente para ajustarlos a la neecsidad y dimensi&oacute;n de su negocio.</p>
						<p>Si usted posee un checador moneyBox solo tendr&aacute; que generar un prenomina de sus empleados y si todo esta en orden, proceder a generar la nomina oficial.</p>
						<p>Si usted no posee un checador moneyBox podra generar su prenomina a partir de la generaci&oacute;n de una prenomina con validaci&oacute;n de cada empleado o bien si cuenta con checador personal podemos realisarle un <b>adaptaci&oacute;n</b> especial para facilitar su trabajo.</p>
						<p>Y en la cuesti&oacute;n de sus empleados podr&aacute; registrar todos sus datos personales, subir archivos de interes y ademas generaci&oacute;n de gafete personalizado con los logos de su empresa.</p>
					</div>
					<div class="w3-container w3-justify w3-small s6 invisible">
						<p>El sistema de almac&eacute;n se compone principalmente de Cat&aacute;logos y SubC&aacute;talogos, que posteriormente de haberlos creado podr&aacute; alimentarlos con sus productos o materia prima.</p>
						<p>Cada producto registrado en el ERP puede contener toda la informaci&oacute;n necesaria para ser usada en su facturaci&oacute;n de forma legal, ademas de tener opci&oacute;n de subir de una a m&uacute;ltiples im&aacute;genes del producto y la disponibilidad de acceso en linea en caso de tener el modulo de ComercioWeb.</p>
						<p>Usted podr&aacute; crear alertar de insuficiencia de stock de forma general o bien por cada producto independiente.</p>
					</div>
					<div class="w3-container w3-justify w3-small s7 invisible">
						<p>Este modulo tiene 4 (cuatro) enfoques a los que su negocios le beneficiara enormemente: Pagina de Ventas, Conexi&oacute;n a E-commerce (Mercadolibre y Linio), Pasarela de Cobro y Paqueter&iacute;a.</p>
						<p>Usted debe contar con el modulo de <b>Almacen</b>, <b>Ventas</b> y <b>Clientes</b> para disfrutar de todo el potencial del ERP.</p>
						<p>Su pagina web de Ventas es 100% personalizable y funcionara como un punto de venta en linea donde sus compradores podr&aacute;n registrarse, armar un carrito, realizar pago con tarjeta y comprar gu&iacutelas de paqueter&iacute;a. Para que usted solo se preocupe por su stock y dar seguimiento a las Ordenes de venta.</p>
						<p>La paqueter&iacute;a se carga al sistema ERP y usted solo debe cubrir mensualmente los gastos generador por las gu&iacute;as.</p>
						<p>Y si quiere ir mas arriba y vender en <b>Mercadolibre</b> y <b>Linio</b> solo debe activar la opci&oacute;n (requiere mensualidad <a href="#contacto"><b>click para cotizar</b></a>) y sus productos estar&aacute;n en menos de 24 horas en las redes de E-Commerce.</p>
					</div>
				</div>
				<script type="text/javascript">
$(document).ready(function(){';

for( $i=0; $i<8; $i++ )
	{
	echo '$(\'#s'. $i. '\').click( function(event) {
		if( $(\'.s'. $i. '\').is(\':hidden\') )
			{';
	for( $y=0; $y<8; $y++ )
		{
		if( $i!=$y ) # si son distintos entra
			{
			echo '$(\'.s'. $y. '\').css( "display", "none" );';
			echo '$(\'.v'. $y. '\').css( "display", "none" );';
			}
		else # es el mismo
			{
			echo '$(\'.s'. $y. '\').fadeIn( "slow" );';
			echo '$(\'.v'. $y. '\').fadeIn( "slow" );';
			}
		}
		echo '}
		});';
	}
echo '});
				</script>
			</div>
		</div>
';
?>