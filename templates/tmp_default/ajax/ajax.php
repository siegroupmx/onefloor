<?php
function enviar_correo_contacto( $from, $to, $asunto, $modo, $enlace, $adjunto, $mensaje )
	{
	$boundary= md5(time()); //valor boundary
	$htmlalt_boundary= $boundary. "_htmlalt"; //boundary suplementario
	$extra_args= NULL;
	$subject=$asunto; //titulo del correo

	//cabeceras para enviar correo en formato HTML
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: multipart/mixed; boundary=\"". $boundary. "\"\r\n"; //datos mixteados 
	$headers .= "From: SIEGroup <". $from. ">\r\n"; //correo del que lo envia

	//incia cuerpo del mensaje que se visualiza
	$cuerpo="--". $boundary. "\r\n";
	$cuerpo .= "Content-Type: multipart/alternative; boundary=\"". $htmlalt_boundary. "\"\r\n\r\n"; //contenido alternativo: texto o html
	$cuerpo .= "--". $htmlalt_boundary. "\r\n";
	$cuerpo .= "Content-Type: text/html; charset=iso-8859-1\r\n";
	$cuerpo .= "Content-Transfer-Encoding: 8bits\r\n\r\n";

	if( $modo==0 ) //enviar correo para comentarios
		$cuerpo .= desproteger_cadena($mensaje);

	$cuerpo .= "<p>Este mensaje fue generado automaticmanete por nuestro sistema web, asi que no es necesario contestar este correo.";
	$cuerpo .= "<br>". TITULO_WEB. "<br>http://". $_SERVER['HTTP_HOST'];
	
	$cuerpo .= "\r\n\r\n";
	$cuerpo .= "--". $htmlalt_boundary. "--\r\n\r\n"; //fin cuerpo mensaje a mostrar
	
	//archivos adjuntos
	if( strcmp($adjunto, "0") && strcmp($adjunto, "vacio")  )
		{
		set_time_limit(600);
		$archivo= $adjunto;
		$buf_type= obtener_extencion_stream_archivo($adjunto); //obtenemos tipo archivo
		
		$fp= fopen( "uploads/".$archivo, "r" ); //abrimos archivo
		$buf= fread( $fp, filesize("uploads/".$archivo) ); //leemos archivo completamente
		fclose($fp); //cerramos apuntador;
				
		$cuerpo .= "--". $boundary. "\r\n";
		$cuerpo .= "Content-Type: ". $buf_type. "; name=\"". $archivo. "\"\r\n"; //envio directo de datos
		$cuerpo .= "Content-Transfer-Encoding: base64\r\n";
		$cuerpo .= "Content-Disposition: attachment; filename=\"". $archivo. "\"\r\n\r\n";
		$cuerpo .= chunk_split(base64_encode($buf)). "\r\n\r\n";
		}
	$cuerpo .= "--". $boundary. "--\r\n\r\n"; 

	//funcion para enviar correo
	set_time_limit(600);
	if( mail($to, $subject, $cuerpo, $headers, $extra_args) == FALSE )
		return 0;
	return 1;
	}
	
if( !strcmp($_GET["own"], "me") )
	{
	if( isset($_GET["send"]) && !strcmp($_GET["send"], "1") ) //enviar email de cotizacion
		{
		$mail= 'SIEGroup <noreplay@crver.net>';
		$ip= get_ip(); # obtener IP
		# $gr= recaptcha_verify( $_POST["g-recaptcha-response"] );

		#if( !$gr->success )
		#	echo '<div id="display" class="error"><div class="sprite errormsg"></div>La Captcha, no coincide</div>';
		if( !strcmp($_GET["form"], "p1") && (!$_POST["nombre_p1"] || !$_POST["telefono_p1"] || !$_POST["email_p1"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ha dejado campos sin llenar.</div>';
			}
		else if( !strcmp($_GET["form"], "p2") && (!$_POST["nombre_p2"] || !$_POST["telefono_p2"] || !$_POST["email_p2"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ha dejado campos sin llenar.</div>';
			}
		else if( !strcmp($_GET["form"], "p3") && (!$_POST["nombre_p3"] || !$_POST["telefono_p3"] || !$_POST["email_p3"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ha dejado campos sin llenar.</div>';
			}
		else if( !strcmp($_GET["form"], "p4") && (!$_POST["nombre_p4"] || !$_POST["telefono_p4"] || !$_POST["email_p4"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ha dejado campos sin llenar.</div>';
			}
		else if( !strcmp($_GET["form"], "p5") && (!$_POST["nombre_p5"] || !$_POST["telefono_p5"] || !$_POST["email_p5"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ha dejado campos sin llenar.</div>';
			}
		else if( !strcmp($_GET["form"], "p6") && (!$_POST["nombre_p6"] || !$_POST["telefono_p6"] || !$_POST["email_p6"] || !$_POST["mensaje_p6"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ha dejado campos sin llenar.</div>';
			}
		else if( !strcmp($_GET["form"], "p1") && !validar_email($_POST["email_p1"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>El correo electr'. acento("o"). 'nico que proporciono, no existe.</div>';
			}
		else if( !strcmp($_GET["form"], "p2") && !validar_email($_POST["email_p2"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>El correo electr'. acento("o"). 'nico que proporciono, no existe.</div>';
			}
		else if( !strcmp($_GET["form"], "p3") && !validar_email($_POST["email_p3"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>El correo electr'. acento("o"). 'nico que proporciono, no existe.</div>';
			}
		else if( !strcmp($_GET["form"], "p4") && !validar_email($_POST["email_p4"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>El correo electr'. acento("o"). 'nico que proporciono, no existe.</div>';
			}
		else if( !strcmp($_GET["form"], "p5") && !validar_email($_POST["email_p5"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>El correo electr'. acento("o"). 'nico que proporciono, no existe.</div>';
			}
		else if( !strcmp($_GET["form"], "p6") && !validar_email($_POST["email_p6"]) )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>El correo electr'. acento("o"). 'nico que proporciono, no existe.</div>';
			}
		else if( consultar_datos_general("CONTACTO", "IP='". $ip. "' && FECHA BETWEEN '". strtotime(date("Y-m-d", time())."T00:00:00"). "' AND '". time(). "'", "ID") )
			{
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Ya recibimos anteriormente su interes de contacto, nosotros nos comunicaremos lo mas pronto posible.</div>';
			}
		else
			{
			if( $_GET["ads"] )
				{
				if( strstr($_GET["ads"], "gl_") ) # add de google
					{
					$ads= $_GET["ads"];
					$ads_name= 'Google';
					}
				else if( strstr($_GET["ads"], "bing_") ) # add de google
					{
					$ads= $_GET["ads"];
					$ads_name= 'Bing';
					}
				else if( strstr($_GET["ads"], "fb_") ) # add de facebook
					{
					echo '
					<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,\'script\',\'https://connect.facebook.net/en_US/fbevents.js\');
fbq(\'init\', \'1962331857372896\');
fbq(\'track\', \'PageView\');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1962331857372896&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
';
					$ads= $_GET["ads"];
					$ads_name= 'Facebook';
					}
				else if( strstr($_GET["ads"], "tw_") ) # add de twitter
					{
					$ads= $_GET["ads"];
					$ads_name= 'Twitter';
					}
				else if( strstr($_GET["ads"], "turundus_") ) # add de turundus
					{
					$ads= $_GET["ads"];
					$ads_name= 'Turundus';
					}
				else
					{
					$ads= 'unknow';
					$ads_name= 'Desconocido';
					}

				$ads_msg= "\nPatrociono: ". $ads_name. "\nRefencia: ". $_GET["red"]. "\nFecha: ". date( "d/m/Y, g:i a", time());
				}
			else 	$ads=0;

			if( !strcmp($_GET["form"], "p1") )
				{
				$form= array( "nombre"=>proteger_cadena($_POST["nombre_p1"]), 
								"mensaje"=>($_POST["sp"] ? proteger_cadena($_POST["sp"]):"Paquete Basico - 696.00 MXN". ($ads ? $ads_msg:'')), 
								"telefono"=>proteger_cadena($_POST["telefono_p1"]), 
								"email"=>proteger_cadena($_POST["email_p1"]) );
				}
			else if( !strcmp($_GET["form"], "p2") )
				{
				$form= array( "nombre"=>proteger_cadena($_POST["nombre_p2"]), 
								"mensaje"=>"Paquete Intermedio - 2,900.00 MXN". ($ads ? $ads_msg:''), 
								"telefono"=>proteger_cadena($_POST["telefono_p2"]), 
								"email"=>proteger_cadena($_POST["email_p2"]) );
				}
			else if( !strcmp($_GET["form"], "p3") )
				{
				$form= array( "nombre"=>proteger_cadena($_POST["nombre_p3"]), 
								"mensaje"=>"Paquete avanzado - 9,280.00 MXN". ($ads ? $ads_msg:''), 
								"telefono"=>proteger_cadena($_POST["telefono_p3"]), 
								"email"=>proteger_cadena($_POST["email_p3"]) );
				}
			else if( !strcmp($_GET["form"], "p4") )
				{
				$form= array( "nombre"=>proteger_cadena($_POST["nombre_p4"]), 
								"mensaje"=>"Paquete Empresarial - 11,600.00 MXN". ($ads ? $ads_msg:''), 
								"telefono"=>proteger_cadena($_POST["telefono_p4"]), 
								"email"=>proteger_cadena($_POST["email_p4"]) );
				}
			else if( !strcmp($_GET["form"], "p5") )
				{
				$form= array( "nombre"=>proteger_cadena($_POST["nombre_p5"]), 
								"mensaje"=>"Quiero Registrarme". ($ads ? $ads_msg:''), 
								"telefono"=>proteger_cadena($_POST["telefono_p5"]), 
								"email"=>proteger_cadena($_POST["email_p5"]) );
				}
			else if( !strcmp($_GET["form"], "p6") )
				{
				$form= array( "nombre"=>proteger_cadena($_POST["nombre_p6"]), 
								"mensaje"=>proteger_cadena($_POST["mensaje_p6"]). ($ads ? $ads_msg:''), 
								"telefono"=>proteger_cadena($_POST["telefono_p6"]), 
								"email"=>proteger_cadena($_POST["email_p6"]) );
				}

			$data= "Nombre: ". $form["nombre"]. "<br>";
			$data .= "Telefonos: ". $form["telefono"]. "<br>";
			$data .= "IP: ". $ip. "<br>";
			$data .= "E-Mail: ". $form["email"]. "<br><br>". $form["mensaje"];
			$title= 'Nuevo Contacto'. ($ads ? ' via '. $ads_name:'');
			$navegador= get_navegador("name"). '|'. get_navegador("os"). '|'. get_navegador("tipo"); # nombre navegador
			do //generamos numero aleatorio de 4 a 10 digitos
				{
				$idtrack= generar_idtrack(); //obtenemos digito aleatorio
				}while( !strcmp( $idtrack, consultar_datos_general( "CONTACTO", "ID='". $idtrack. "'", "ID" ) ) );

			$trama= array( 
				"id"=>"'". $idtrack. "'", 
				"id_soporte"=>"'mailform'", # id del asesor de soporte
				"session"=>"'mailform_". $idtrack. "'", # sesion del navegador
				"navegador"=>"'". $navegador. "'", # string: navegador|sistemaoperativo|tiponavegador
				"ip"=>"'". $ip. "'", # ip del cliente
				"id_cliente"=>"'mailform'", # id del cliente, o bien "mailform" si es por form web
				"nombre"=>"'". $form["nombre"]. "'", # nombre cliente
				"mensaje"=>"'". $form["mensaje"]. "'", # el mensaje
				"email"=>"'". $form["email"]. "'", # correo electronico
				"telefono"=>"'". $form["telefono"]. "'",  # telefono
				"fecha"=>"'". time(). "'" # fecha
				);
		
			if( !insertar_bdd("CONTACTO", $trama) )
				{
				echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Su correo no pudo ser enviado, por favor contactenos al correo: <a href="mailto:'. $mail. '">'. $mail. '</a></b></div>';
				}
			else
				{
				$imgmail= INCLUDES_TEMA_URL.'/imagenes/contacto.jpg'; # imagen del correo
				$asunto= 'Gracias por contactarnos !';
				$thanks= "Hola.<br><br>Hemos recibido tu mensaje, en un plazo de <b>30 minutos</b> nos pondremos en contacto contigo.";
				enviar_correo( $form["email"], $asunto, 7, $thanks, NULL, $mail, $imgmail, NULL ); # notificar al cliente
				enviar_correo( $mail, $title, 7, $data, NULL, $mail, $imgmail, NULL ); # notificar al soporte tecnico

				echo '<div class="w3-panel w3-padding w3-large w3-pale-green w3-leftbar w3-border-green"><div class="sprite exitomsg w3-left w3-margin-right"></div>Hemos recibido su mensaje, en un plazo de <b>30 minutos</b> nos pondremos en contacto con usted.</div>';
				}
				
			unset($data, $title, $form, $idtrack, $navegador, $ip);
			}
		}
	else if( !strcmp($_GET["a"], "boletin") ) # suscribirse al boletin
		{
		if( !$_POST["boletin_mail"] || !validar_email($_POST["boletin_mail"]) )
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite exitomsg w3-left w3-margin-right"></div>E-Mail invalido !</div>';
		else if( consultar_datos_general("BOLETIN", "email='". proteger_cadena($_POST["boletin_mail"]). "'", "ID") )
			echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite exitomsg w3-left w3-margin-right"></div>Ya estas registrado para recibir el boletin !</div>';
		else
			{
			$mail= 'SIEGroup <noreplay@crver.net>';
			$form= array( "nombre"=>"0", "telefono"=>"0", "email"=>proteger_cadena($_POST["boletin_mail"]) );
			$title= 'Bienvenido al Boletin de Noticias';
			$navegador= get_navegador("name"). '|'. get_navegador("os"). '|'. get_navegador("tipo"); # nombre navegador
			$ip= get_ip(); # obtener IP
			do //generamos numero aleatorio de 4 a 10 digitos
				{
				$idtrack= generar_idtrack(); //obtenemos digito aleatorio
				}while( !strcmp( $idtrack, consultar_datos_general( "BOLETIN", "ID='". $idtrack. "'", "ID" ) ) );

			$trama= array( 
				"id"=>"'". $idtrack. "'", 
				"id_usuario"=>"'". (is_login() ? $_SESSION["log_id"]:'0'). "'", 
				"nombre"=>"'". $form["nombre"]. "'", # nombre cliente
				"email"=>"'". $form["email"]. "'", # correo electronico
				"telefono"=>"'". $form["telefono"]. "'",  # telefono
				"fecha"=>"'". time(). "'", # fecha
				"status"=>"'1'"  # 1=inscrito activo, 2=desactivado
				);

		
			if( !insertar_bdd("BOLETIN", $trama) )
				{
				echo '<div class="w3-panel w3-padding w3-large w3-pale-red w3-leftbar w3-border-red"><div class="sprite errormsg w3-left w3-margin-right"></div>Su correo no pudo ser enviado, por favor contactenos al correo: <a href="mailto:'. $mail. '">'. $mail. '</a></b></div>';
				}
			else
				{
				$thanks= "Hola.<br><br>Te damos la bienvenida al boletin de noticias, te pondremos al tanto y estaremos al pendiente de ti !
				<br><br>Un abrazo de parte del equipo SIEGroup.";
				$asunto= 'Gracias por Inscribirte al boletin de noticias';
				$imgmail= INCLUDES_TEMA_URL.'/imagenes/boletin.jpg'; # imagen del correo
				enviar_correo( $form["email"], $asunto, 7, $thanks, NULL, $mail, $imgmail, NULL ); # notificar al cliente
				enviar_correo( $mail, 'Nuevo Suscriptor al Boletin', 7, 'Tenemos nuevo suscriptor: '. $form["email"], NULL, $mail, $imgmail, NULL ); # notificar al cliente

				echo '<div class="w3-panel w3-padding w3-large w3-pale-green w3-leftbar w3-border-green"><div class="sprite exitomsg w3-left w3-margin-right"></div>Te haz inscrito a nuestro boletin de noticias.</div>';
				}
			unset($title, $form, $idtrack, $navegador, $ip);
			}
		}
	else if( !strcmp($_GET["a"], "research") && is_login() )
		{
		if( !strcmp($_GET["m"], "save") ) # guardar vulnerabilidad
			{
			if( !$_POST["adv_csv"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">De indicar un CSV SIEGroup valido.</div>';
			else if( consultar_datos_general( "BUGTRACK", "CSV_LOCAL='". proteger_cadena($_POST["adv_csv"]). "'", "ID" ) )
				echo '<div class="w3-container w3-padding w3-pale-yellow">El codigo CSV SIEGroup ya existe.</div>';
			else if( !$_POST["adv_name"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">De indicar nombre del Afectado.</div>';
			else if( !$_POST["adv_www"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar la pagina web del afectado.</div>';
		 	else if( !$_POST["adv_fecha"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar fecha de publicacion.</div>';
			else if( !$_POST["adv_tipovuln"] ) 
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar tipo de vulnerabilidad.</div>';
			else if( !$_POST["ow_01"] || !$_POST["ow_02"] || !$_POST["ow_03"] || !$_POST["ow_04"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe inidicar los riesgos.</div>';
			else if( !$_POST["ow_05"] || !$_POST["ow_06"] || !$_POST["ow_07"] || !$_POST["ow_08"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar la vulnerabilidad.</div>';
			else if( !$_POST["ow_09"] || !$_POST["ow_10"] || !$_POST["ow_11"] || !$_POST["ow_12"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar el impacto.</div>';
			else if( !$_POST["ow_13"] || !$_POST["ow_14"] || !$_POST["ow_15"] || !$_POST["ow_16"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar la perdida del negocio.</div>';
			else if( !$_POST["adv_comments"] )
				echo '<div class="w3-container w3-padding w3-pale-yellow">Debe indicar los comentarios.</div>';
			else
				{
				$riesgo= '['. proteger_cadena($_POST["ow_01"]). ']['. proteger_cadena($_POST["ow_02"]). ']['. proteger_cadena($_POST["ow_03"]). ']['. proteger_cadena($_POST["ow_04"]). ']';
				$vuln= '['. proteger_cadena($_POST["ow_05"]). ']['. proteger_cadena($_POST["ow_06"]). ']['. proteger_cadena($_POST["ow_07"]). ']['. proteger_cadena($_POST["ow_08"]). ']';
				$impacto= '['. proteger_cadena($_POST["ow_09"]). ']['. proteger_cadena($_POST["ow_10"]). ']['. proteger_cadena($_POST["ow_11"]). ']['. proteger_cadena($_POST["ow_12"]). ']';
				$perdida= '['. proteger_cadena($_POST["ow_13"]). ']['. proteger_cadena($_POST["ow_14"]). ']['. proteger_cadena($_POST["ow_15"]). ']['. proteger_cadena($_POST["ow_16"]). ']';

				$probabilidad= $_POST["ow_01"]+$_POST["ow_02"]+(!strcmp(strtolower($_POST["ow_03"]), "na") ? 0:$_POST["ow_03"])+$_POST["ow_04"];
				$probabilidad += $_POST["ow_05"]+$_POST["ow_06"]+$_POST["ow_07"]+$_POST["ow_08"];
				$probabilidad= number_format(($probabilidad/8), 2, '.', '');
				$impactoreal= $_POST["ow_09"]+$_POST["ow_10"]+$_POST["ow_11"]+$_POST["ow_12"];
				$impactoreal += $_POST["ow_13"]+$_POST["ow_14"]+$_POST["ow_15"]+$_POST["ow_16"];
				$impactoreal= number_format(($impactoreal/8), 2, '.', '');

				$levels= array( 0=>"Bajo", 1=>"Medio", 2=>"Alto", 3=>"Critico", 4=>"Nota" );
				$a= ($probabilidad<=3 ? 0:($probabilidad<6 ? 1:($probabilidad<=9 ? 2:2)));
				$b= ($impactoreal<=3 ? 0:($impactoreal<6 ? 1:($impactoreal<=9 ? 2:2)));
				$score= array( 
					0=>array( 0=>1, 1=>2, 2=>3 ), 
					1=>array( 0=>0, 1=>1, 2=>2 ), 
					2=>array( 0=>4, 1=>0, 2=>1 ) 
				);

				do //generamos numero aleatorio de 4 a 10 digitos
					{
					$idtrack= generar_idtrack(); //obtenemos digito aleatorio
					}while( !strcmp( $idtrack, consultar_datos_general( "BUGTRACK", "ID='". $idtrack. "'", "ID" ) ) );

				$trama= array(
					"id"=>"'". $idtrack. "'", 
					"id_usuario"=>"'". $_SESSION["log_usr"]. "'", 
					"fecha"=>"'". strtotime(proteger_cadena($_POST["adv_fecha"]).'T00:00:00'). "'", 
					"fecha_update"=>"'0'", 
					"titulo"=>"'". proteger_cadena($_POST["adv_name"]). "'", 
					"csv_local"=>"'". proteger_cadena($_POST["adv_csv"]). "'", 
					"csv_mitre"=>"'0'", 
					"descripcion"=>"'". proteger_cadena($_POST["adv_comments"]). "'", 
					"file"=>"'0'", 
					"hashtag"=>"'". proteger_cadena($_POST["adv_tipovuln"]). "'", 
					"url"=>"'". proteger_cadena($_POST["adv_www"]). "'", 
					"riesgo"=>"'". $riesgo. "'", 
					"vuln"=>"'". $vuln. "'", 
					"impacto"=>"'". $impacto. "'", 
					"perdida"=>"'". $perdida. "'", 
					"probabilidad_ataque"=>"'". $probabilidad. "'", 
					"nivel_riesgo"=>"'". $impactoreal. "'", 
					"score"=>"'". $score[$a][$b]. "'", 
					"solucion"=>"'0'"
					);

				if( !insertar_bdd("BUGTRACK", $trama) )
					echo '<div class="w3-container w3-padding w3-pale-red w3-margin-bottom">Problemas para guardar la informaci'. acento("o"). 'n.</div>';
				else 
					{
					echo '<div class="w3-container w3-padding w3-pale-green w3-margin-bottom">Informaci'. acento("o"). 'n guardada con exito.</div>';
					onefloor_reload(2000);
					}
				}
			}
		else if( !strcmp($_GET["m"], "update") ) # actualizar vulnerabilidad
			{
			echo 'Actualizando..';
			}
		else if( !strcmp($_GET["m"], "del") ) # eliminar vulnerabilidad
			{
			echo 'Eliminando..';
			}
		else if( !strcmp($_GET["m"], "edit") ) # editar vulnerabilidad
			{
			echo 'Editando...';
			}
		}
	}
else
	echo 'Error AJAX Own';
	
?>