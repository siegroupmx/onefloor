<?php
echo '
<div class="w3-container w3-padding-48 w3-light-gray" id="marcas">
	<div class="w3-row-padding">
		<div class="w3-third w3-right-align w3-padding">
			<h1>Nuestras marcas preferenciales</h1>
		</div>
		<div class="w3-rest w3-leftbar w3-border-gray w3-padding">
			<div class="w3-row">
				<div class="w3-quarter">
					<div class="sprite hp"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite dell"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite mikrotik"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite ubiquiti"></div>
				</div>
			</div>
			<div class="w3-row">
				<div class="w3-quarter">
					<div class="sprite tplink"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite bitdefender"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite kaspersky"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite nod32"></div>
				</div>
			</div>
			<div class="w3-row">
				<div class="w3-quarter">
					<div class="sprite honeywell"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite panduit"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite intellinet"></div>
				</div>
				<div class="w3-quarter">
					<div class="sprite samsung"></div>
				</div>
			</div>
		</div>
	</div>
</div>
';
?>